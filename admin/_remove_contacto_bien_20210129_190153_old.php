<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-01-2021 
 * @copyright	Copyright (C) 25-01-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegContacto_bien', RUTA_BASE);
class Contacto_bien extends JrWeb
{
	private $oNegContacto_bien;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegContacto_bien = new NegContacto_bien;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Contacto_bien', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('contacto_bien','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Contacto_bien'), true);
			$this->rebi_id = $_REQUEST["_v1_"]; 

			$this->esquema = 'contacto_bien';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
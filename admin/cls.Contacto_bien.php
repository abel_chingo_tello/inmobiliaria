<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		02-02-2021 
 * @copyright	Copyright (C) 02-02-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegContacto_bien', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRegistro_bien', RUTA_BASE);
class Contacto_bien extends JrWeb
{
	private $oNegContacto_bien;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegContacto_bien = new NegContacto_bien;
		$this->oNegTabla = new NegTabla; 
		$this->oNegRegistro_bien = new NegRegistro_bien; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Contacto_bien', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('contacto_bien','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Contacto_bien'), true);
			$this->esquema = 'contacto_bien';
			$this->fkTipo_documento=$this->oNegTabla->buscar();
			$this->fkRebi_id=$this->oNegRegistro_bien->buscar();
			$this->rebi_id=$_REQUEST["_v1_"];
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function download(){
		$file = $_REQUEST["_v1_"];
		//$rutaFichero = 	"..static/templates/plantillas/web/assets/img/bienes/";
		//echo $rutaFichero.$file;
		
		//header("Content-type:application/pdf");
		$rutaFichero = 	"static/templates/plantillas/web/assets/img/bienes/";

		// It will be called downloaded.pdf
//		header("Content-Disposition:attachment;filename=downloaded.pdf");

		// The PDF source is in original.pdf readfile($_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ.$rutaFichero.$file);
		//echo  "../".$rutaFichero.$file;
//		readfile($file);

		$fichero = "http://localhost/".CARPETA_RAIZ.$rutaFichero.$file;
		echo $fichero;
		if (file_exists($fichero)) {
			echo "holaaa";
			/*header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.basename($fichero).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($fichero));
			readfile($fichero);*/
			exit;
		}else{
			echo "jaja";
		}

	}
}
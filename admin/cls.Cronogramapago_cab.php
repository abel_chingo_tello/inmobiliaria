<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021 
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegCronogramapago_cab', RUTA_BASE);
class Cronogramapago_cab extends JrWeb
{
	private $oNegCronogramapago_cab;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegCronogramapago_cab = new NegCronogramapago_cab;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Cronogramapago_cab', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('cronogramapago_cab','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Cronogramapago_cab'), true);
			$this->esquema = 'cronogramapago_cab';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
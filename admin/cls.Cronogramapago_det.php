<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021 
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegCronogramapago_det', RUTA_BASE);
JrCargador::clase('sys_negocio::NegCronogramapago_cab', RUTA_BASE);

JrCargador::clase('sys_negocio::NegPagos', RUTA_BASE);

class Cronogramapago_det extends JrWeb
{
	private $oNegCronogramapago_det;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegCronogramapago_det = new NegCronogramapago_det;
		$this->oNegCronogramapago_cab = new NegCronogramapago_cab;	
		$this->oNegPagos = new NegPagos;	
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Cronogramapago_det', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('cronogramapago_det','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Cronogramapago_det'), true);
			$this->esquema = 'cronogramapago_det';
			
			if($this->user["tipo_usuario"]=="s"){
				$this->cab_id = $_REQUEST["_v2_"];
				 			
			}elseif($this->user["tipo_usuario"]=="n"){
				$idpersona = $this->user["idpersona"];
				$cab = $this->oNegCronogramapago_cab->buscar(array("inner_personaid"=>$idpersona,"inner"=>"1"));
				//$this->cab_id = $cab[sizeof($cab)-1]["id"];
				$this->cab_id = $cab[0]["id"];
				$this->bienes = $cab;
			}
			$this->cabecera = $this->oNegCronogramapago_cab->buscar(array("inner"=>"1","inner_id"=>$this->cab_id))[0]; 
			$this->nro_documento = $this->cabecera["num_documento"];
			$this->tipo_documento = $this->cabecera["tipo_documento"];
			$this->nombres = $this->cabecera["nombres"]." ".$this->cabecera["apellido_1"]." ".$this->cabecera["apellido_2"];
			$this->inmueble = $this->cabecera["rebi_detalles"];
			$this->estado = $this->cabecera["estado_pago"] == '0' ? "Pendiente" : "Al día";
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function respuesta(){
		$preference_id = $_GET["preference_id"];
		$status = $_GET["status"];

		$this->documento->stylesheet('datatables.min','librerias/dataTables/');
		$this->documento->stylesheet('select2.min','librerias/select2/css/');
				
		$this->documento->stylesheet('tablas','templates/css/');			
		$this->documento->script('datatables.min','librerias/dataTables/');
		$this->documento->script('main','librerias/abelchingo/');
		$this->documento->script('select2.min','librerias/select2/js/');
		
		if($status == "approved"){
			$pago = $this->oNegPagos->buscar(array("preference_id"=>$preference_id,"sqlget"=>"1"));
			$this->oNegPagos->setId($pago["id"]);
			$this->oNegPagos->estado = "1";
			
			$det_ids = $this->oNegPagos->cronogramapagodet_ids;
			$this->oNegPagos->editar();
			$this->documento->setTitulo(JrTexto::_('Pago aprobado '), true);

			$detalle_pagos = array();
			$det_ids_array = explode(",",$det_ids);
			for($i=0;$i<count($det_ids_array);$i++){
				$detalle_pagos[]=$this->oNegCronogramapago_det->buscar(array("id"=>$det_ids_array[$i],"sqlget"=>"1"));
			}			
			$this->pagos = $detalle_pagos; 

			$this->esquema = 'respuestapagoaprobado';

		}else{
			$this->documento->setTitulo(JrTexto::_('Pago rechazado '), true);
			$this->esquema = 'respuestapagorechazado';

		}
		return parent::getEsquema();
	}
}
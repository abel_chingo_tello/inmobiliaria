<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
//JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Empresa extends JrWeb
{
	private $oNegEmpresa;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		//$this->oNegEmpresa = new NegEmpresa;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->script('datatables.min','librerias/dataTables/');

			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			$this->documento->script('select2.min','librerias/select2/js/');
			/// libreria para imagen
        	$this->documento->stylesheet('cropper.min','librerias/cropper/');
			$this->documento->script('cropper.min','librerias/cropper/');
			
			$this->documento->stylesheet('bootstrap-colorpicker.min','librerias/bootstrap/v3/bootstrap-colorpicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');

			$this->documento->stylesheet('tablas','templates/css/');			
			
			$this->documento->script('main','librerias/abelchingo/');
			
			$this->documento->script('empresa','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Empresa'), true);
			$this->esquema = 'empresa/listado';
			if($this->user["tipo_usuario"]=='n'){
				if($this->user["idrol"]==1){
					$this->configuracion();
				}else{
					return $aplicacion->redir('sinpermisos');					
				}
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function configuracion(){
		try{
			global $aplicacion;	
			$this->documento->stylesheet('select2.min','librerias/select2/css/');

			/// libreria para imagen
        	$this->documento->stylesheet('cropper.min','librerias/cropper/');
			$this->documento->script('cropper.min','librerias/cropper/');

			$this->documento->stylesheet('bootstrap-colorpicker.min','librerias/bootstrap/v3/bootstrap-colorpicker/css/');
			$this->documento->script('bootstrap-colorpicker.min','librerias/bootstrap/v3/bootstrap-colorpicker/js/');

			$this->documento->stylesheet('tablas','templates/css/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('empresa_configuracion','librerias/abelchingo/');
			$this->user=NegSesion::getUsuario();	
			if($this->user["tipo_usuario"]=='s'){
				$idempresa=!empty($_REQUEST["idempresa"])?$_REQUEST["idempresa"]:false;
				if($idempresa!=false){
					JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
					$this->oNegEmpresa = new NegEmpresa;
					$this->empresa=$this->oNegEmpresa->getAlldatos($idempresa);					
				}else $this->empresa=NegSesion::getEmpresa();
			}else $this->empresa=NegSesion::getEmpresa();
			$this->esquema = 'empresa/configuracion';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function modulos(){
		try{
			global $aplicacion;
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->stylesheet('jquery.nestable.min','librerias/jquery/jquery-nestable/');
			$this->documento->script('jquery.nestable.min','librerias/jquery/jquery-nestable/');
			
			$this->user=NegSesion::getUsuario();
			$this->empresa=NegSesion::getEmpresa();	
			JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
			JrCargador::clase('sys_negocio::NegRol', RUTA_BASE);
			$this->oNegEmpresa = new NegEmpresa;
			$filtroempresa=array('estado'=>1);
			if($this->user["tipo_usuario"]=='s'){
				$this->fkIdempresa=$this->oNegEmpresa->buscar(array('estado'=>1));
			}else 
				$this->fkIdempresa=array(0=>array('idempresa' =>$this->empresa["idempresa"],'nombre'=>$this->empresa["nombre"]));
			JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
			$oNegModulos = new NegModulos;
			$this->fkIdmodulo=$oNegModulos->buscar(array('estado'=>1));
			/*$this->oNegRol = new NegRol;
			$this->fkIdrol=$this->oNegRol->buscar(array('idmpresa'=>$this->empresa["idempresa"],'estado'=>1));*/
			$this->documento->stylesheet('tablas','templates/css/');
			$this->documento->script('empresa_modulos','librerias/abelchingo/');
			$this->esquema = 'empresa/modulos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
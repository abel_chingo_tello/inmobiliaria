<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegEmpresa_aulas', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa_local', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Empresa_aulas extends JrWeb
{
	private $oNegEmpresa_aulas;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegEmpresa_aulas = new NegEmpresa_aulas;
		$this->oNegEmpresa_local = new NegEmpresa_local; 
		$this->oNegEmpresa = new NegEmpresa; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_aulas', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('empresa_aulas','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Empresa_aulas'), true);
			$this->esquema = 'empresa_aulas';
			$this->fkIdlocal=$this->oNegEmpresa_local->buscar();
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
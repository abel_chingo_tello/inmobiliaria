<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-10-2020 
 * @copyright	Copyright (C) 04-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegEmpresa_config', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Empresa_config extends JrWeb
{
	private $oNegEmpresa_config;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegEmpresa_config = new NegEmpresa_config;
		$this->oNegEmpresa = new NegEmpresa; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_config', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('empresa_config','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Empresa_config'), true);
			$this->esquema = 'empresa_config';
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
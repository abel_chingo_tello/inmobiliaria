<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020 
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegEmpresa_modulo', RUTA_BASE);
JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE);
class Empresa_modulo extends JrWeb
{
	private $oNegEmpresa_modulo;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegEmpresa_modulo = new NegEmpresa_modulo;
		$this->oNegModulos = new NegModulos; 
		$this->oNegEmpresa = new NegEmpresa; 
		$this->oNegRol = new NegRol; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_modulo', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('empresa_modulo','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Empresa_modulo'), true);
			$this->esquema = 'empresa_modulo';
			$this->fkIdmodulo=$this->oNegModulos->buscar();
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			$this->fkIdrol=$this->oNegRol->buscar();
			$this->fkIdmoduloapdre=$this->oNegModulos->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
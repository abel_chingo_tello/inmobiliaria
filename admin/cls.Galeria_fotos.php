<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-01-2021 
 * @copyright	Copyright (C) 28-01-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegGaleria_fotos', RUTA_BASE);
class Galeria_fotos extends JrWeb
{
	private $oNegGaleria_fotos;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegGaleria_fotos = new NegGaleria_fotos;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Galeria_fotos', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('galeria_fotos','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Galeria_fotos'), true);
			$this->esquema = 'galeria_fotos';
			$this->rebi_id = $_REQUEST["_v1_"]; 
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
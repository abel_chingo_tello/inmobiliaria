<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
JrCargador::clase('sys_negocio::NegCursos_grupoaula', RUTA_BASE);
class Historial_sesion extends JrWeb
{
	private $oNegHistorial_sesion;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegPersona = new NegPersona; 
		$this->oNegRol = new NegRol; 
		$this->oNegEmpresa = new NegEmpresa; 
		$this->oNegCursos_grupoaula = new NegCursos_grupoaula; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Historial_sesion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('historial_sesion','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Historial_sesion'), true);
			$this->esquema = 'historial_sesion';
			$this->fkIdpersona=$this->oNegPersona->buscar();
			$this->fkIdrol=$this->oNegRol->buscar();
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			$this->fkIdgrupoaula=$this->oNegCursos_grupoaula->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
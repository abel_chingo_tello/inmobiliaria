<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020 
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
class Modulos extends JrWeb
{
	private $oNegModulos;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegModulos = new NegModulos;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Modulos', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('modulos','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Modulos'), true);
			$this->esquema = 'modulos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
class Persona extends JrWeb
{
	private $oNegPersona;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegPersona = new NegPersona;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');

			$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
			
			/// libreria para imagen
        	$this->documento->stylesheet('cropper.min','librerias/cropper/');
			$this->documento->script('cropper.min','librerias/cropper/');

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('persona','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Persona'), true);
			$this->esquema = 'persona/persona';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function perfil(){
		global $aplicacion;
		$this->persona=NegSesion::getUsuario();
		$this->esquema = 'persona/perfil';
		$this->documento->setTitulo(JrTexto::_('Persona'),true);
		return parent::getEsquema();
	}

	public function editar(){
		global $aplicacion;
		$this->persona=NegSesion::getUsuario();
		$idpersona=!empty($_REQUEST["uip"])?$_REQUEST["uip"]:$this->persona["idpersona"];		
		if($idpersona!=$this->persona["idpersona"]){
			JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
			$oNegEmpresa = new Negempresa;
			$this->persona=$oNegEmpresa->buscar(array('idpersona' =>$idpersona,'sqlget'=>true));			
		}
		if(empty($this->persona)){
			throw new Exception(JrTexto::_('Persona no existe').'!!');
			exit();
		}
		$this->idpersona=$this->persona["idpersona"];
		$this->documento->stylesheet('datatables.min','librerias/dataTables/');
		$this->documento->script('datatables.min','librerias/dataTables/');

		$this->documento->stylesheet('select2.min','librerias/select2/css/');
		$this->documento->script('select2.min','librerias/select2/js/');

		$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
		$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
			
		/// libreria para imagen
    	$this->documento->stylesheet('cropper.min','librerias/cropper/');
		$this->documento->script('cropper.min','librerias/cropper/');

		$this->documento->stylesheet('tablas','templates/css/');		
		$this->documento->script('main','librerias/abelchingo/');		
		$this->documento->script('persona','librerias/abelchingo/');
		$this->esquema = 'persona/formulario';
		return parent::getEsquema();
	}


}
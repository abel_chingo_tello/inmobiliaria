<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020 
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE);
class Persona_rol extends JrWeb
{
	private $oNegPersona_rol;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegPersona_rol = new NegPersona_rol;
		$this->oNegEmpresa = new NegEmpresa; 
		$this->oNegPersona = new NegPersona; 
		$this->oNegRol = new NegRol; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_rol', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('persona_rol','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Persona_rol'), true);
			$this->esquema = 'persona_rol';
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			$this->fkIdpersona=$this->oNegPersona->buscar();
			$this->fkIdrol=$this->oNegRol->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
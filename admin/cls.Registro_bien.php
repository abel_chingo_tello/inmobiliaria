<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-12-2020 
 * @copyright	Copyright (C) 24-12-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegRegistro_bien', RUTA_BASE);
JrCargador::clase('sys_negocio::NegCatalogo_sbn', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegDependencia', RUTA_BASE);
//JrCargador::clase('sys_negocio::NegDependencia', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);
class Registro_bien extends JrWeb
{
	private $oNegRegistro_bien;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegRegistro_bien = new NegRegistro_bien;
		$this->oNegCatalogo_sbn = new NegCatalogo_sbn; 
		//$this->oNegDependencia = new NegDependencia; 
		//$this->oNegDependencia = new NegDependencia; 
		$this->oNegTabla = new NegTabla; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Registro_bien', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			/// libreria para fecha
        	$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');
/// libreria para fecha
        	$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('registro_bien','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Registro_bien'), true);
			$this->esquema = 'registro_bien';
			$this->fkCasb_codigo=$this->oNegCatalogo_sbn->buscar();
			//$this->fkDepe_id_ubica=$this->oNegDependencia->buscar();
			//$this->fkDepe_id=$this->oNegDependencia->buscar();
			$this->fkTabl_tipo_interior=$this->oNegTabla->buscar(array("tabl_tipo"=>"TIPO_INTERIOR_INMUEBLE"));
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
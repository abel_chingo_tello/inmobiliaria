<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2020 
 * @copyright	Copyright (C) 23-12-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegClientes', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);

class Reporte extends JrWeb
{
	private $oNegClientes;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegClientes = new NegClientes;
		$this->oNegTabla = new NegTabla; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Clientes', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			/// libreria para fecha
        	$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('clientes','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Reporte'), true);
			$this->esquema = 'reporte';
			$this->fkTabl_tipocliente=$this->oNegTabla->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
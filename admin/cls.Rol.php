<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020 
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Rol extends JrWeb
{
	private $oNegRol;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegRol = new NegRol;
		$this->oNegEmpresa = new NegEmpresa; 
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Rol', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			/// libreria para imagen
        	$this->documento->stylesheet('cropper.min','librerias/cropper/');
			$this->documento->script('cropper.min','librerias/cropper/');

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('rol','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Rol'), true);
			$this->esquema = 'rol';
			$this->fkIdempresa=$this->oNegEmpresa->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
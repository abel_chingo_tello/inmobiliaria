<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class Sesion extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		global $aplicacion;
		JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
		$oNegEmpresa = new Negempresa;
		$oNegEmpresa->setconfiguracion('returnmodulo',_sitio_);
		return $aplicacion->redir('../json/');
	}
}
<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-12-2020 
 * @copyright	Copyright (C) 22-12-2020. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);
class Tabla extends JrWeb
{
	private $oNegTabla;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegTabla = new NegTabla;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tabla', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			/// libreria para fecha
        	$this->documento->stylesheet('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/css/');
			$this->documento->script('bootstrap-datepicker.min','librerias/bootstrap/v3/bootstrap-datepicker/js/');

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('tabla','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Tabla'), true);
			$this->esquema = 'tabla';
			$this->fkTabl_codigoauxiliar=$this->oNegTabla->buscar();
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
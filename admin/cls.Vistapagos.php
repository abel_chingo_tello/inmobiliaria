<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-02-2021 
 * @copyright	Copyright (C) 15-02-2021. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegVistapagos', RUTA_BASE);
class Vistapagos extends JrWeb
{
	private $oNegVistapagos;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNegVistapagos = new NegVistapagos;
		
	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Vistapagos', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			
			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('vistapagos','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('Vistapagos'), true);
			$this->esquema = 'vistapagos';
			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebIndex extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		global $aplicacion;
		//$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
		//$this->idproyecto = $usuarioAct["idproyecto"];
		//$this->documento->setTitulo(JrTexto::_('Criterios'), true);
		$this->esquema = 'inicio';
		return parent::getEsquema();
	}
}
<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2020 
 * @copyright	Copyright (C) 23-12-2020. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Clientes</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
                    <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tabla</label>                      
                      <select name="bustabl_tipocliente" id="bustabl_tipocliente" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkTabl_tipocliente))
                              foreach($this->fkTabl_tipocliente as $k=>$v){ ?>         
                          <option value="<?php echo $v["tabl_codigo"]; ?>"  ><?php echo ucfirst($v["tabl_descripcion"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Clie_estado</label>                      
                      <select name="busclie_estado" id="busclie_estado" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Clie_empleado</label>                      
                      <select name="busclie_empleado" id="busclie_empleado" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
                    </div>
            </form>
          </div>          <div class="grid-body" id="aquitable" >
          </div>
                  </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>clientes" class="">Clientes</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmclientes" action="#">
            <input type="hidden" name="clie_id" id="clie_id" value="">
                <div class="row">                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tabla <span class="help"> </span> </label>                      <select name="tabl_tipocliente" id="tabl_tipocliente" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkTabl_tipocliente))
                              foreach($this->fkTabl_tipocliente as $k=>$v){ ?>                          <option value="<?php echo $v["tabl_codigo"]; ?>"><?php echo $v["tabl_descripcion"] ?></option>
                        <?php } ?> 
                      </select>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie apellidos <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_apellidos" id="clie_apellidos" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie nombres <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_nombres" id="clie_nombres" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie razsocial <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_razsocial" id="clie_razsocial" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie dni <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_dni" id="clie_dni" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie direccion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_direccion" id="clie_direccion" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie telefono <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_telefono" id="clie_telefono" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie email <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_email" id="clie_email" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie fregistro <span class="help"> </span> </label>                  </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie estado <span class="help"> </span> </label>
                      <div class="row-fluid">
                        <div class="cambiarestado checkbox check-primary checkbox-circle" >
                          <input type="checkbox" value="0" name="clie_estado" id="clie_estado" v1="Activo" v2="Inactivo">
                          <label><span>Inactivo</span></label>
                        </div>
                      </div>                  </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie zona <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="clie_zona" id="clie_zona" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Clie empleado <span class="help"> </span> </label>
                      <div class="row-fluid">
                        <div class="cambiarestado checkbox check-primary checkbox-circle" >
                          <input type="checkbox" value="0" name="clie_empleado" id="clie_empleado" v1="Activo" v2="Inactivo">
                          <label><span>Inactivo</span></label>
                        </div>
                      </div>                  </div>
                </div>          
              
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oClientes=new BDClientes('<?php echo $ventana; ?>');
    oClientes.vista_tabla();
  });
</script>
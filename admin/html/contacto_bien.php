<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		05-02-2021 
 * @copyright	Copyright (C) 05-02-2021. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Contacto bien</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
                    <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tabla</label>                      
                      <select name="bustipo_documento" id="bustipo_documento" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkTipo_documento))
                              foreach($this->fkTipo_documento as $k=>$v){ ?>         
                          <option value="<?php echo $v["tabl_id"]; ?>"  ><?php echo ucfirst($v["tabl_descripcion"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
      
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Registro bien</label>                      
                      <select name="busrebi_id" id="busrebi_id" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkRebi_id))
                              foreach($this->fkRebi_id as $k=>$v){ ?>         
                          <option value="<?php echo $v["rebi_id"]; ?>"  ><?php echo ucfirst($v["rebi_detalles"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
                    </div>
            </form>
          </div>          <div class="grid-body" id="aquitable" >
          </div>
                  </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>contacto_bien" class="">Contacto bien</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmcontacto_bien" action="#">
            <input type="hidden" name="id" id="id" value="">
                <div class="row ">                
                <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Tabla <span class="help"> </span> </label>                      
                        <select name="tipo_documento" id="tipo_documento" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkTipo_documento))
                            foreach($this->fkTipo_documento as $k=>$v){ ?>                          
                            <option value="<?php echo $v["tabl_id"]; ?>"><?php echo $v["tabl_descripcion"] ?></option>
                        <?php } ?> 
                      </select>
                                      </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Nro documento <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nro_documento" id="nro_documento" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12 ocultar" >
                  <div class="form-group">
                    <label class="form-label">Apellidos <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="apellidos" id="apellidos" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Direccion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="direccion" id="direccion" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Registro bien <span class="help"> </span> </label>                      <select name="rebi_id" id="rebi_id" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkRebi_id))
                              foreach($this->fkRebi_id as $k=>$v){ ?>                          <option value="<?php echo $v["rebi_id"]; ?>"><?php echo $v["rebi_detalles"] ?></option>
                        <?php } ?> 
                      </select>
                                      </div>
                </div>          
                  <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Email <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="email" name="email" id="email" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                  <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Nombre <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nombre" id="nombre" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Telefono <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="telefono" id="telefono" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Mensaje <span class="help"> </span> </label>                                       
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                       <textarea name="mensaje" id="mensaje" class="form-control" placeholder=""></textarea>
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                       </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">File infocorp <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".docx, .pdf, .doc, .xlsx" name="file_infocorp" id="file_infocorp" class="form-control"  placeholder="">
                      
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">File recomendacion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".docx, .pdf, .doc, .xlsx" name="file_recomendacion" id="file_recomendacion" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">File policial judicial <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".docx, .pdf, .doc, .xlsx" name="file_policial_judicial" id="file_policial_judicial" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">File extra recomendacion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".docx, .pdf, .doc, .xlsx" name="file_extra_recomendacion" id="file_extra_recomendacion" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Punataje <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="punataje" id="punataje" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12 ocultar">
                  <div class="form-group">
                    <label class="form-label">Estado <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="estado" id="estado" class="form-control"  placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
              
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>

<!--modal to generate contract-->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h7 class="modal-title" id="exampleModalLabel">Generando Contrato</h7>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="container-fluid row">
              <div class="col-lg-12">
                <form id="contacto_bien"  method="post" role="form" class="contactar mt-4" autocomplete="off">
                  <input id="contactobien_id" name="contactobien_id" type="hidden">

                  <div class="form-group row">
                    <label class="col-sm-5"> Garantía: </label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control" step=".01" name="garantia" id="garantia" placeholder="Ingresar la garantía" />
                      <div class="validate"></div>
                    </div>
                  </div>    

                  <div class="form-group row">
                    <label class="col-sm-5"> Meses de Contrato: </label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control" name="meses_contrato" id="meses_contrato" placeholder="Ingrese los meses de contrato" />
                      <div class="validate"></div>
                    </div>
                  </div>    

                  <div class="form-group row">
                    <label class="col-sm-5"> Monto mensual: </label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control" step=".01" name="monto_mensual" id="monto_mensual" placeholder="Ingrese el monto mensual" />
                      <div class="validate"></div>
                    </div>
                  </div>    

                  <div class="form-group row">
                    <label class="col-sm-5"> Día de Pago: </label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control" name="dia_pago" id="dia_pago" placeholder="Ingrese el dia de pago" />
                      <div class="validate"></div>
                    </div>
                  </div>    
                  <div class="form-group row">
                    <label class="col-sm-5"> Usuario: </label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" name="usuario" id="usuario" placeholder="" />
                      <div class="validate"></div>
                    </div>
                  </div>    
                  <div class="form-group row">
                    <label class="col-sm-5"> Contraseña: </label>
                    <div class="col-sm-7">
                      <input type="password" class="form-control" name="password" id="password" placeholder="" />
                      <div class="validate"></div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-5"> Correo: </label>
                    <div class="col-sm-7">
                      <input type="email" class="form-control" name="emailmodal" id="emailmodal" placeholder="" />
                      <div class="validate"></div>
                    </div>
                  </div>    
                  <div class="form-group row">
                    <label class="col-sm-5"> Adjuntar archivo : </label>
                    <div class="col-sm-7">
                      <input type="file" accept=".pdf" class="form-control" name="archivo_contrato" id="archivo_contrato" placeholder="Adjuntar el contrato" />
                      <div class="validate"></div>
                    </div>
                  </div>    
                  <div class="text-center pb-3"><input class="btn btn-primary" type="submit" value=Enviar /></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>        


<script type="text/javascript"> 
  $(function(){    
    var oContacto_bien=new BDContacto_bien('<?php echo $ventana; ?>','<?php echo $this->rebi_id; ?>');
    oContacto_bien.vista_tabla();
   
  });
  $(document).ready(function(){
    
  })
</script>
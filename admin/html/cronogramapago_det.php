<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021 
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Detalle - Cronograma de Pagos</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">

        <div class="grid simple">
          <div class="grid-body">
            <strong><h5 class="text-dark">DATOS DEL CLIENTE</h5></strong>
            <div class="row">
              <div clasS="col-lg-4">
                <strong><span id="tipo_doc" class="h6 text-dark">DNI</span></strong> : <span class="h5 text-dark" id="nro_doc" class=""><?php echo $this->nro_documento; ?></span>
              </div>
              <div clasS="col-lg-8">
                <strong><span id="tipo_name" class="h6 text-dark">APELLIDOS Y NOMBRES</span></strong> : <span id="nombrecli" class="text-uppercase h5 text-dark"><?php echo $this->nombres; ?></span> 
              </div>            
            </div>
            
          </div>
          
          <div class="grid-body">
          <strong><h5 class="text-dark">DATOS DEL INMUEBLE</h5></strong>
            <div class="row">
              <div clasS="col-lg-12">

                <?php if($this->user["tipo_usuario"]=="n") {?>
                  <div class="row align-items-center">
                  <div class="col-lg-2">
                    <strong><span class="h6 text-dark">Seleccione un inmueble</span></strong> :                 
                  </div>
                  <div class="col-lg-7">
                    <select type="text" id="bienes" class="form-control" style="height:54px!important;font-size:1.25rem;" name="tipo_documento" id="tipo_documento" >
                      <?php 
                      $i=0;
                      foreach($this->bienes as $value){
                          if($i==0){?>
                        <option class="h5" value="<?php echo $value["id"] ?>" selected><?php echo $value["rebi_detalles"] ?></option>
                      <?php }else{ ?> 
                        <option class="h5" value="<?php echo $value["id"] ?>" ><?php echo $value["rebi_detalles"] ?></option>
                      <?php }$i++;}?>
                    </select>
                  </div>
                  </div>
                  
                <?php } elseif($this->user["tipo_usuario"]=="s") {?>
                  <strong><span class="h6 text-dark">DETALLES </span></strong> : <span class="h5 text-dark text-uppercase" id="rebi_detalles" class=""><?php echo $this->inmueble; ?></span>
                <?php }?>
                
              </div>
            </div>
            </div>
            
            <div class="grid-body">
            <strong><h5 class="text-dark">DATOS DEL PAGO</h5></strong>
            <div class="row">
              <div clasS="col-lg-4">
                <strong><span class="h6 text-dark">Estado</span></strong> : <span class="h5 text-dark" id="estado" class=""><?php echo $this->estado; ?></span>
              </div>
            </div>
            </div>
          <div class="grid-body" id="aquitable" >
          </div>
        </div>    
      </div>
    </div>
  </div>
  <div class="vistatablapagar shopping-cart" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>cronogramapago_det" class="">Detalle - Cronograma de Pagos</a></li>
      <li><a href="#" class="breadcrumbactive active">Pagar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >
        <h2>Resumen de Estado de Cuenta</h2>         
          <div class="grid-body" id="aquitablapagar" >
            


          </div>
          <div class="grid-body">
                  <div class="row align-items-center justify-content-center">
                    <div class="col-lg-2">
                      <h3 id="monto_pagar">600.00</h3>
                    </div>
                    <div class="col-lg-2">
                      <button id="checkout-btn" class="btn btn-danger" >PAGAR</button>
                    </div>
                  </div>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>

<!-- section resumen de pago -->

  <section class="payment-form dark container_payment grid-body" id="pago" style="display:none;">
    <div class="container_payment">
      <div class="block-heading">
        <h2>Listado del Pago a Efectuar</h2>
      </div>
      <div class="grid simple">
        <div class="form-payment grid-body">
          <div class="products">
            <h2 class="title">Resumen del Pago</h3>
            <table class="table table-hover table-condensed" id="resumen_pago">
            <thead>
              <tr>       
            	<th>Concepto</th>            	
				      <th>Cargo</th>            	
            	</tr>
            </thead>
            <tbody></tbody></table>

          </div>
          <div class="payment-details">
            <div class="form-group col-sm-12">
              <br>      
              <div id="button-checkout">

              </div>                 
              <br>
              <a id="go-back" href="<?php echo URL_SITIO; ?>cronogramapago_det">
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 10 10" class="chevron-left">
                  <path fill="#009EE3" fill-rule="nonzero"id="chevron_left" d="M7.05 1.4L6.2.552 1.756 4.997l4.449 4.448.849-.848-3.6-3.6z"></path>
                </svg>
                Regresar a pagos
              </a>
            </div>
          </div>
        </div>
      
      </div>
    </div>
  </section>



<script type="text/javascript"> 
  $(function(){    
    var oCronogramapago_det=new BDCronogramapago_det('<?php echo $ventana; ?>','<?php echo $this->cab_id; ?>','<?php echo $this->user["tipo_usuario"]; ?>');
    oCronogramapago_det.vista_tabla();
  });
</script>
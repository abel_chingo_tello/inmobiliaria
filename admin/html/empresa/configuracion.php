<?php 
$ventana="ven".date("YmdHis");
$logo=!empty($this->empresa["logo"])?$this->empresa["logo"]:'static/media/defecto/logoempresa.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$logo))) $logo='static/media/defecto/logoempresa.png';

$imagen_fondo_login=!empty($this->empresa["imagen_fondo_login"])?$this->empresa["imagen_fondo_login"]:'static/media/defecto/fondologin.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$imagen_fondo_login))) $imagen_fondo_login='static/media/defecto/fondologin.png';
$color_fondo_login=!empty($this->empresa["color_fondo_login"])?$this->empresa["color_fondo_login"]:'#dddddd';
$tipoempresa=!empty($this->empresa["tipo_empresa"])?$this->empresa["tipo_empresa"]:'page';
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
?><div id="<?php echo $ventana; ?>">  
  <div class="vistatablaformulario estilofrm" >
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>empresa" class="">Empresa</a></li>
      <li><a href="#" class="breadcrumbactive active">Configuración</a></li>
    </ul>
    <div class="row" id="aquiformulario">
      <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="row-fluid">
          <div class="span12">
            <div class="grid simple " >
              <div class="grid-title"><h4>Datos de Empresa</h4></div>         
              <div class="grid-body" >
                <form id="frmempresa" action="#">
                <input type="hidden" name="idempresa" id="idempresa" value="<?php echo $this->empresa["idempresa"]; ?>">
                  <div class="row">
                    <div class="col-md-9 col-sm-6 col-xs-12">
                      <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Ruc <span class="help"></span></label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="text" name="ruc" id="ruc" class="form-control" required value="<?php echo $this->empresa["ruc"]; ?>" >
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Nombre <span class="help"></span></label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="text" name="nombre" id="nombre" class="form-control" required value="<?php echo $this->empresa["nombre"]; ?>">
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Telefono <span class="help"></span> </label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="text" name="telefono" id="telefono" class="form-control" required value="<?php echo $this->empresa["telefono"]; ?>">
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Idioma <span class="help"></span> </label>
                            <div class="input-with-icon  right">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <select name="idioma" id="idioma" class="select2 form-control"  >
                                <option value="ES" <?php echo $this->empresa["idioma"]=='ES'?'selected="selected"':''; ?>>Español</option>
                                <option value="EN" <?php echo $this->empresa["idioma"]=='EN'?'selected="selected"':''; ?>>Ingles</option>
                              </select>                        
                            </div>                  
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Correo <span class="help"></span></label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="email" name="correo" id="correo" class="form-control" required value="<?php echo $this->empresa["correo"]; ?>">
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Direccion <span class="help"></span></label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="text" name="direccion" id="direccion" class="form-control" required value="<?php echo $this->empresa["direccion"]; ?>">
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Subdominio <span class="help"></span> </label>
                            <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                              <i class="error fa-exclamation fa"></i>
                              <i class="success fa fa-check"></i>
                              <input type="text" name="subdominio" id="subdominio" <?php echo $this->user["tipo_usuario"]=='s'?'':'readonly="readonly"'?> class="form-control" required value="<?php echo $this->empresa["subdominio"]; ?>">
                              <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Estado <span class="help"></span></label>
                            <div class="row-fluid">
                              <div class="cambiarestado checkbox check-primary checkbox-circle" >
                                <input type="checkbox" value="<?php echo $this->empresa["estado"]; ?>" name="estado" id="estado" v1="Activo" v2="Inactivo" <?php echo $this->empresa["estado"]=='1'?'checked="checked"':''; ?>>
                                <label><span><?php echo $this->empresa["estado"]=='1'?'Activo':'Inactivo'; ?></span></label>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <label class="form-label">Logo <span class="help"></span></label>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="img-container img-logo">
                                    <img id="fileimglogo" src="<?php echo URL_BASE.$logo; ?>" style="max-width: 100%;" alt="Picture">
                                    <input type="hidden" name="logo" tipo="url" >
                                </div>
                                <div class="col-md-12 text-center">
                                  <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                                      <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                                      <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                                      <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                                      <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                                    </div>
                                    <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                                      <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                                      <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                                      <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                                      <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions">  
                    <div class="text-center">
                      <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div> 
        <div class="row-fluid">
          <div class="span12">
            <div class="grid simple" >
              <div class="grid-title"><h4>Datos de servidor de correo</h4></div>         
              <div class="grid-body" >
                <form id="frmempresasmtp" action="#">
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Servidor (HOST) <span class="help"></span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="text" name="host" id="host" class="form-control" required value="<?php echo @$this->empresa["smtp"]["host"]; ?>"  >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Correo (Email) <span class="help"></span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="email" name="email" id="email" class="form-control" required value="<?php echo @$this->empresa["smtp"]["email"]; ?>" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Clave (Passowrd) <span class="help"></span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="text" name="clave" id="clave" class="form-control" required value="<?php echo @$this->empresa["smtp"]["clave"]; ?>" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Puerto (port) <span class="help">e.g. 556</span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="number" name="puerto" id="puerto" class="form-control" required value="<?php echo @$this->empresa["smtp"]["puerto"]; ?>" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Cifrado  <span class="help">e.g. TLS/SSL</span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="text" name="cifrado" id="cifrado" class="form-control" required value="<?php echo @$this->empresa["smtp"]["cifrado"]; ?>" >
                      </div>
                    </div>
                  </div>                                   
                </div>
                <div class="form-actions">  
                    <div class="text-center">
                      <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>    
          </div>
        </div>       
      </div>
      <div class="col-md-3 col-sm-12 col-xs-12" >
        <div class="row-fluid">
          <div class="span12">
            <div class="grid simple" >
              <div class="grid-title"><h4>Datos de Incio de Sesión</h4></div>         
              <div class="grid-body" >
                <form id="frmempresaconfig" action="#">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">color de Fondo <span class="help"></span></label>
                      <div class="input-with-icon  " claseerror="success-control" claseok="error-control">
                            <input type="text" style="background-color: <?php echo $color_fondo_login; ?>" name="color_fondo_login" id="color_fondo_login" value="<?php echo $color_fondo_login; ?>" class="form-control colorpicker" />                        
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Imagen de Fondo <span class="help"></span></label>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="img-container img-imagen_fondo_login">
                              <img id="fileimgimage_fondo_login" src="<?php echo URL_BASE.$imagen_fondo_login; ?>" style="max-width: 100%;" alt="Picture">
                              <input type="hidden" name="imagen_fondo_login" tipo="url" >
                          </div>
                          <div class="col-md-12 text-center">
                            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                              <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                                <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                                <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                                <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                                <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                              </div>
                              <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                                <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                                <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                                <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Tipo empresa <span class="help"></span> </label>
                      <div class="input-with-icon  right">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <select name="tipo_empresa" id="tipo_empresa" class="select2 form-control"  >
                          <option value="page"  <?php echo $tipoempresa=='page'?'selected="selected" ':''; ?>>Pagina web</option>
                          <option value="tienda" <?php echo $tipoempresa=='tienda'?'selected="selected" ':''; ?>>Tienda</option>
                        </select>                        
                      </div>                  
                    </div>                    
                  </div>
                </div>
                <div class="form-actions">  
                    <div class="text-center">
                      <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </div>    
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oBDEmpresa_configuracion=new BDEmpresa_configuracion('<?php echo $ventana; ?>');
  });
</script>
<?php 
$ventana="ven".date("YmdHis");
$logo='defecto/fotouser.png';
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
?><div id="<?php echo $ventana; ?>">
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Empresa</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
            <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <?php if($this->user["tipo_usuario"]=="s"){?>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Estado</label>                      
                      <select name="busestado" id="busestado" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
                <?php } ?>
              </div>
            </form>
          </div>
          <div class="grid-body" id="aquitable" >
          </div>          
        </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>empresa" class="">Empresa</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmempresa" action="#">
            <input type="hidden" name="idempresa" id="idempresa" value="">
              <div class="row">
                <div class="col-md-9 col-sm-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Ruc <span class="help"></span></label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="text" name="ruc" id="ruc" class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Nombre <span class="help"></span></label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="text" name="nombre" id="nombre" class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Telefono <span class="help"></span> </label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="text" name="telefono" id="telefono" class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Idioma <span class="help"></span> </label>
                        <div class="input-with-icon  right">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <select name="idioma" id="idioma" class="select2 form-control"  >
                            <option value="ES">Español</option>
                            <option value="EN">Ingles</option>
                          </select>                        
                        </div>                  
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Correo <span class="help"></span></label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="email" name="correo" id="correo" class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Direccion <span class="help"></span></label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="text" name="direccion" id="direccion" class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Subdominio <span class="help"></span> </label>
                        <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                          <i class="error fa-exclamation fa"></i>
                          <i class="success fa fa-check"></i>
                          <input type="text" name="subdominio" id="subdominio" <?php echo $this->user["tipo_usuario"]=='s'?'':'readonly="readonly"'?> class="form-control" required >
                          <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Estado <span class="help"></span></label>
                        <div class="row-fluid">
                          <div class="cambiarestado checkbox check-primary checkbox-circle" >
                            <input type="checkbox" value="0" name="estado" id="estado" v1="Activo" v2="Inactivo">
                            <label><span>Inactivo</span></label>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label class="form-label">Logo <span class="help"></span></label>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="img-container img-logo">
                                <img id="fileimglogo" src="<?php echo URL_MEDIA.$logo; ?>" alt="Picture">
                                <input type="hidden" name="logo" tipo="url" >
                            </div>
                            <div class="col-md-12 text-center">
                              <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                                  <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                                  <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                                  <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                                  <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                                </div>
                                <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                                  <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                                  <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                                  <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                                  <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var __ventanas=new Ventanas__('BDEmpresa','<?php echo $ventana; ?>');
    __ventanas.templatetabla();
  });
</script>
<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020 
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
?><div id="<?php echo $ventana; ?>">
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Empresa modulo</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
                    <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Modulos</label>                      
                      <select name="busidmodulo" id="busidmodulo" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdmodulo))
                              foreach($this->fkIdmodulo as $k=>$v){ ?>         
                          <option value="<?php echo $v["idmodulo"] ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?>  
                      </select>                        
                    </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Empresa</label>                      
                      <select name="busidempresa" id="busidempresa" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdempresa))
                              foreach($this->fkIdempresa as $k=>$v){ ?>         
                          <option value="<?php echo $v["idempresa"] ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?>  
                      </select>                        
                    </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Estado</label>                      
                      <select name="busestado" id="busestado" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Rol</label>                      
                      <select name="busidrol" id="busidrol" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdrol))
                              foreach($this->fkIdrol as $k=>$v){ ?>         
                          <option value="<?php echo $v["idrol"] ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?>  
                      </select>                        
                    </div>
                </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Modulos</label>                      
                      <select name="busidmoduloapdre" id="busidmoduloapdre" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdmoduloapdre))
                              foreach($this->fkIdmoduloapdre as $k=>$v){ ?>         
                          <option value="<?php echo $v["idmodulo"] ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?>  
                      </select>                        
                    </div>
                </div>
                    </div>
            </form>
          </div>          <div class="grid-body" id="aquitable" >
          </div>          
        </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>empresa_modulo" class="">Empresa modulo</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmempresa_modulo" action="#">
            <input type="hidden" name="idempresamodulo" id="idempresamodulo" value="">
                <div class="row">                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Modulos <span class="help">e.g </span> </label>                      
                    <select name="idmodulo" id="idmodulo" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdmodulo))
                              foreach($this->fkIdmodulo as $k=>$v){ ?>                          
                        <option value="<?php echo $v["idmodulo"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?> 
                    </select>
                  </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Empresa <span class="help">e.g </span> </label>                      
                    <select name="idempresa" id="idempresa" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdempresa))
                              foreach($this->fkIdempresa as $k=>$v){ ?>                          
                        <option value="<?php echo $v["idempresa"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?>                     
                    </select>
                  </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Estado <span class="help">e.g </span> </label>
                      <div class="row-fluid">
                        <div class="cambiarestado checkbox check-primary checkbox-circle" >
                          <input type="checkbox" value="0" name="estado" id="estado" v1="Activo" v2="Inactivo">
                          <label><span>Inactivo</span></label>
                        </div>
                      </div>                  </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rol <span class="help">e.g </span> </label>                      
                    <select name="idrol" id="idrol" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdrol))
                              foreach($this->fkIdrol as $k=>$v){ ?>                          
                                <option value="<?php echo $v["idrol"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?> 
                      </select>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Orden <span class="help">e.g </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="orden" id="orden" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Modulos <span class="help">e.g </span> </label>                      
                    <select name="idmoduloapdre" id="idmoduloapdre" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdmoduloapdre))
                              foreach($this->fkIdmoduloapdre as $k=>$v){ ?>                          <option value="<?php echo $v["idmodulo"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?> 
                      </select>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Nomtemporal <span class="help">e.g </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nomtemporal" id="nomtemporal" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
              
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var __ventanas=new Ventanas__('BDEmpresa_modulo','<?php echo $ventana; ?>');
  });
</script>
<form id="frmmodulos" action="#">
	<input type="hidden" name="idpersona" value="<?php echo !empty($this->idpersona)?$this->idpersona:''; ?>">
	<div class="row">
		<div class="col-md-9 col-sm-12 col-sx-12">
			<div class="row">
				<div class="col-md-6 col-sx-12">
					<div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Tipo Documento'); ?></label>
			            <span class="help"><?php echo JrTexto::_('e.g');?> "DNI"</span>
						<div class="input-with-icon  right">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<select name="tipo_documento" id="tipo_documento" class="select2 form-control"  >					                     
					            <option value="DNI">DNI</option>
					            <option value="PASSAPORTE">PASSPORTE</option>
			            	</select>
			            	<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio') ?></span>
						</div>
			        </div>
				</div>
				<div class="col-md-6 col-sx-12">
					<div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Numero de documento'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="text" name="num_documento" id="num_documento" class="form-control" required placeholder="<?php echo JrTexto::_('e.g');?> 12345678"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
						</div>
			        </div>
				</div>
				<div class="col-md-6 col-sx-12">
		        	<div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Primer Apellido'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="text" name="apellido_1" id="apellido_1" class="form-control" required placeholder="<?php echo JrTexto::_('e.g');?> Chingo"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z') ?></span>
						</div>
			        </div>
	        	</div>
	        	<div class="col-md-6 col-sx-12">
			        <div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Segundo Apellido'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="text" name="apellido_2" id="apellido_2" class="form-control" required placeholder="<?php echo JrTexto::_('e.g');?> Tello"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z') ?></span>
						</div>
			        </div>
			    </div>
			    <div class="col-md-6 col-sx-12">
			        <div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Nombres'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="text" name="nombres" id="nombres" class="form-control" required placeholder="<?php echo JrTexto::_('e.g');?> Abel"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z') ?></span>                                
						</div>
			        </div>
	    		</div>
	    		<div class="col-md-6 col-sx-12">
			        <div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Telefono'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="text" name="telefono" id="telefono" class="form-control" required placeholder="<?php echo JrTexto::_('e.g');?> +519999990"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z') ?></span>                                
						</div>
			        </div>
	    		</div>
	    		<div class="col-md-12">
	    			<div class="form-group">
			            <label class="form-label"><?php echo JrTexto::_('Email'); ?></label>
						<div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
							<i class="error fa-exclamation fa"></i>
							<i class="success fa fa-check"></i>
							<input type="email" name="email" id="email" class="form-control" placeholder="<?php echo JrTexto::_('e.g');?> abelito@tuempresa.com"> 
							<span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z') ?></span>                                
						</div>
			        </div>
	    		</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-12 col-sx-12">
			<div class="row">
				<div class="col-md-12">
					<div class="img-container">
			            <img src="<?php echo URL_MEDIA.$foto; ?>" alt="Picture">
			        </div>
			        <div class="col-md-12 text-center">
			        	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
						  <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
						    <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
						    <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
						    <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
						    <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
						  </div>
						  <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
						    <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
						    <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
						    <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
						  </div>
						</div>				        					        	
			        </div>
		        </div>
	    	</div>
		</div>
	</div>
	<div class="row">				
		<div class="clearfix"></div>			    
	</div>
      <div class="form-group">
        <label class="form-label">Your email</label>
        <span class="help">e.g. "john@examp.com"</span>
		<div class="input-with-icon  right">
			<i class=""></i>
			<input type="text" name="form1Email" id="form1Email" class="form-control">                                 
		</div>
      </div>
      <div class="form-group">
        <label class="form-label">Website</label>
        <span class="help">e.g. "http://www.webarc.com"</span>
		<div class="input-with-icon  right">
			<i class=""></i>
			<input type="text" name="form1Url" id="form1Url" class="form-control">                                 
		</div>
      </div>
                      
  <div class="form-actions">  
	<div class="pull-right">
	  <button type="submit" class="btn btn-success btn-cons"><i class="icon-ok"></i> Save</button>
	  <button type="button" class="btncancelar btn btn-white btn-cons">Cancel</button>
	</div>
	</div>
</form>

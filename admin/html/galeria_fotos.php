<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-01-2021 
 * @copyright	Copyright (C) 28-01-2021. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Galeria fotos</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
                    <div class="grid-body" id="aquitable" >
          </div>
                  </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>galeria_fotos" class="">Galeria fotos</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmgaleria_fotos" action="#">
            <input type="hidden" name="id_galeria" id="id_galeria" value="">
                <div class="row">                
                
                <input type="hidden" name="rebi_id" id="rebi_id" value="<?php echo $this->rebi_id;?>">

                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Titulo <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="titulo" id="titulo" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Descripcion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="descripcion" id="descripcion" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tipo <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <select name="tipo" id="tipo" class="form-control" required >
                        <option value="">Seleccionar</option>
                        <option value="Plano de Ubicación">Plano de Ubicación</option>
                        <option value="Propuesta Alternativa">Propuesta Alternativa</option>
                        <option value="Entorno del Predio">Entorno del Predio</option>
                      </select>
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
                              <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Imagen <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".jpg,.gif,.png" name="imagen" id="imagen" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                                      </div>
                </div>          
              
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oGaleria_fotos=new BDGaleria_fotos('<?php echo $ventana; ?>','<?php echo $this->rebi_id; ?>');
    oGaleria_fotos.vista_tabla();
  });
</script>
<?php 
$ventana="ven".date("YmdHis");
$foto='defecto/fotouser.png';
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>">
	<div class="vistatablaformulario" >
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>persona/perfil" class="">Persona</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmpersona" action="#">
              <input type="hidden" name="idpersona" id="idpersona" value="">
              <input type="hidden" name="tipo_usuario" id="tipo_usuario" value="n">
              <input type="hidden" name="ubigeo" id="ubigeo" class="form-control" placeholder="">
                <div class="row">
                  <div class="col-md-9 col-sm-6 col-xs-12">
                    <div class="row">
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Tipo documento <span class="help"></span></label>
                          <select name="tipo_documento" id="tipo_documento" class="select2 form-control"  >
                            <option value="D">DNI</option>
                            <option value="P">Pasaporte</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-8 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Num documento <span class="help">e.g 91199119</span></label>
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="num_documento" id="num_documento" class="form-control" required placeholder="e.g ">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Apellido 1 <span class="help">e.g. Fernandez</span></label>
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="apellido_1" id="apellido_1" class="form-control" required placeholder="">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Apellido 2 <span class="help">e.g. Muñoz</span></label>
                          
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="apellido_2" id="apellido_2" class="form-control" required placeholder="">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Nombres <span class="help">e.g. Abel</span></label>
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="nombres" id="nombres" class="form-control" required placeholder="">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Estado Civil</label>                      
                          <select name="estado_civil" id="estado_civil" class="select2 form-control"  >
                            <option value="S">Soltero</option>
                            <option value="C">Casado</option>
                            <option value="V">Viudo</option>
                            <option value="D">Divorciado</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Genero</label>                      
                          <select name="genero" id="genero" class="select2 form-control"  >
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Fecha nacimiento <span class="help"></span> </label>
                          <div class="input-with-icon input-append success date no-padding" style="width: calc(100% - 40px);">
                            <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control">
                            <span class="add-on"><span class="arrow"></span><i class="fa fa-th"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Telefono <span class="help">e.g. +51 942121827</span></label>
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="telefono" id="telefono" class="form-control" required placeholder="">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-8 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Direccion <span class="help">e.g. Chiclayo/Pimentel av. las flores SN</span></label>                          
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="direccion" id="direccion" class="form-control" required placeholder="">
                            <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div>
                      <!--div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Ubigeo</label>
                          <span class="help"></span>
                          <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                            <i class="error fa-exclamation fa"></i>
                            <i class="success fa fa-check"></i>
                            <input type="text" name="ubigeo" id="ubigeo" class="form-control" required placeholder="">
                            <span class="error"><?php //echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                          </div>
                        </div>
                      </div-->


                    </div>                    
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Foto</label>
                          <span class="help"></span>                    
                          <div class="row">
                            <div class="col-md-12">
                              <div class="img-container img-foto">
                                  <img id="fileimgfoto" src="<?php echo URL_MEDIA.$foto; ?>" alt="Picture" style="max-width: 100%;">
                                  <input type="hidden" name="foto" tipo="url" >
                              </div>
                              <div class="col-md-12 text-center">
                                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                  <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                                    <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                                    <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                                    <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                                    <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                                  </div>
                                  <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                                    <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                                    <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                                    <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label class="form-label">Estado <span class="help"></span></label>
                          <div class="row-fluid">
                            <div class="cambiarestado checkbox check-primary checkbox-circle" >
                              <input type="checkbox" value="0" name="estado" id="estado" v1="Activo" v2="Inactivo">
                              <label><span>Inactivo</span></label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                    
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Email <span class="help">e.g. micorreo@gmail.com</span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="email" name="email" id="email" class="form-control" required placeholder="">
                        <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                      </div>
                    </div>
                  </div>  
                  <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Usuario <span class="help">e.g. federico2020</span></label>                      
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="text" name="usuario" id="usuario" class="form-control" required placeholder="" autocomplete="username">
                        <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                      </div>
                    </div>
                  </div>          
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Clave <span class="help">e.g. ABD123</span></label>
                      <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <input type="password" name="clave" id="clave" class="form-control" required placeholder="" autocomplete="new-password">
                        <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                      </div>
                    </div>
                  </div>               
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<table style="display: none"><tr id="<?php echo $this->idpersona; ?>" ><td id="seleccionar">aaaaaaaaa</td></tr></table>
<script type="text/javascript"> 
  $(function(){    
    var __ventanas=new Ventanas__('BDPersona','<?php echo $ventana; ?>');
    let el=$('td#seleccionar');
    __ventanas.vista_editar(el);
  });
</script> 
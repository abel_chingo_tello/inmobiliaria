<?php 
$foto=!empty($this->persona["foto"])?$this->persona["foto"]:'defecto/fotouser.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$foto))) $foto='defecto/fotouser.png';
?>
<ul class="breadcrumb">
  <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
  <li><a href="#" class="active">Persona</a></li>
</ul>
<div class="row">
  <div class="col-md-9 col-sm-6 col-xs-12 ">
    <div class="tiles white">
       <div class="tiles-body">
        <div class="controller"> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
        <div class="text-center ">       
            <img src="<?php echo URL_BASE.$foto; ?>" alt="foto" class="img-responsive center-block img-circle" id="fotouser" style="max-height: 150px; max-width: 150px; min-width: 100px; ">  
        </div>
        <h2 class="text-center"><?php echo @$this->persona["apellido_1"]." ".@$this->persona["apellido_2"];?>, <span class="semi-bold text-success"><?php echo @$this->persona["nombres"];?></span>
        <br><small><strong><?php echo $this->persona["tipo_documento"]=='P'?'Pasaporte':'DNI';?>:</strong> <?php echo $this->persona["num_documento"];?></small>
        </h2>
      </div>
      <div class="title body " style="padding: 1ex">
        <div class="img-thumbnail" style="width: 100%">
          <div class="row">
            <div class="col-md-12 tiles-title table-responsive">
              <h4> <strong><?php echo JrTexto::_('General Information');?></strong></h4>
              <table class="table ">
                <tbody>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Genero') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["genero"]=='M'?'Masculino':'Femenino'; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Estado civil') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php 
                    $estado=array('C'=>'Casado','V'=>'Viudo','D'=>'Divorciado','S'=>'Soltero');
                    echo !empty($estado[$this->persona["estado_civil"]])?$estado[$this->persona["estado_civil"]]:'Soltero'; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Telephone') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["telefono"]; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Fecha de Nacimiento') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["fecha_nacimiento"]; ?></td></tr>
                  <tr><td><strong><?php echo JrTexto::_('Ubigeo') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["ubigeo"]; ?></td></tr>
                  <tr><td><strong><?php echo JrTexto::_('Address') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["direccion"]; ?></td></tr>
                  <tr><td><strong><?php echo JrTexto::_('Estado') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["estado"]==1?'Activo':'Inactivo'; ?></td></tr>          
                </tbody>
              </table>
              <hr>
              <h4> <strong><?php echo JrTexto::_('Informacion de Usuario');?></strong></h4>
              <table class="table ">
                <tbody>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Tipo de Usuario') ?> </strong></td>
                    <td>:</td><td class="btncampo">
                    <?php echo ($this->persona["tipo_usuario"]=='s'?'Super Admin':'Normal'); ?></td></tr>
                  <tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Rol Actual') ?> </strong></td>
                    <td>:</td><td class="btncampo">
                    <?php echo $this->persona["rol"]; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Email') ?> </strong></td>
                    <td>:</td><td class="btncampo">
                    <?php echo $this->persona["email"]; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Usuario') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["usuario"]; ?></td></tr>
                  <tr>
                    <td><strong><?php echo JrTexto::_('Tocken') ?> </strong></td><td>:</td>
                    <td class="btncampo" ><?php echo $this->persona["tocken"]; ?></td></tr>
                </tbody>
              </table>
              <hr>
              <div class="col-md-12 tiles-title text-right" style="padding-right: 2em;">
                  <!--button type="button" class="btn btnImprimir btn-default btn-cons">
                    <i class="fa fa-print"></i>&nbsp;<?php //echo JrTexto::_('Print');?>
                  </button-->
                  <a href="<?php echo URL_SITIO ?>persona/editar" class="btn btnEditar btn-warning  btn-cons">
                    <i class="fa fa-edit"></i> <?php echo JrTexto::_('Edit');?>
                  </a>                 
              </div>
            </div>

          </div>          
        </div>           
      </div>      
     
    </div>
  </div>
  <div class="clearfix"></div>
</div>
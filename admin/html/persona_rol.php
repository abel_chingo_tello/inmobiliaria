<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020 
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Persona rol</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
          <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <?php if($this->user["tipo_usuario"]=="s"){ ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Empresa</label>                      
                      <select name="busidempresa" id="busidempresa" class="select2 form-control"  >                        
                        <?php if(!empty($this->fkIdempresa))
                          foreach($this->fkIdempresa as $k=>$v){ ?>         
                          <option value="<?php echo $v["idempresa"]; ?>" <?php echo $v["idempresa"]==$this->idempresa?'selected="selected"':'';?> ><?php echo ucfirst($v["nombre"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
                <?php } ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Persona</label>                      
                      <select name="busidpersona" id="busidpersona" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdpersona))
                              foreach($this->fkIdpersona as $k=>$v){ ?>         
                          <option value="<?php echo $v["idpersona"]; ?>"  ><?php echo ucfirst($v["apellido_1"])." ".$v["apellido_2"].", ".$v["nombres"]; ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
      
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rol</label>                      
                      <select name="busidrol" id="busidrol" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdrol))
                              foreach($this->fkIdrol as $k=>$v){ ?>         
                          <option value="<?php echo $v["idrol"]; ?>"  ><?php echo ucfirst($v["nombre"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
                    </div>
            </form>
          </div>          
          <div class="grid-body" id="aquitable" >
          </div>
        </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>persona_rol" class="">Persona rol</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmpersona_rol" action="#">
            <input type="hidden" name="idpersonarol" id="idpersonarol" value="">
                <div class="row">
                 <?php if($this->user["tipo_usuario"]=="s"){ ?>                
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Empresa <span class="help"> </span> </label>                      
                    <select name="idempresa" id="idempresa" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php if(!empty($this->fkIdempresa))
                              foreach($this->fkIdempresa as $k=>$v){ ?>                          
                                <option value="<?php echo $v["idempresa"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?> 
                      </select>
                  </div>
                </div> 
                <?php } ?>         
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Persona <span class="help"> </span> </label>                      
                    <select name="idpersona" id="idpersona" class="select2 form-control">                        
                        <?php if(!empty($this->fkIdpersona))
                              foreach($this->fkIdpersona as $k=>$v){ ?>                          
                        <option value="<?php echo $v["idpersona"]; ?>"><?php echo $v["nombres"] ?></option>
                        <?php } ?> 
                      </select>
                  </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rol <span class="help"> </span> </label>                      
                    <select name="idrol" id="idrol" class="select2 form-control"  >                        
                        <?php if(!empty($this->fkIdrol))
                              foreach($this->fkIdrol as $k=>$v){ ?>                          
                        <option value="<?php echo $v["idrol"]; ?>"><?php echo $v["nombre"] ?></option>
                        <?php } ?> 
                      </select>
                  </div>
                </div> 
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oPersona_rol=new BDPersona_rol('<?php echo $ventana; ?>');
    oPersona_rol.vista_tabla();
  });
</script>
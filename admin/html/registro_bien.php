<?php 
  $ventana="ven".date("YmdHis");
   /**
   * @autor   Generador Abel Chingo Tello, ACHT
   * @fecha   24-12-2020 
   * @copyright Copyright (C) 24-12-2020. Todos los derechos reservados.
   */
  ?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Registro bien</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
          <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Catalogo sbn</label>                      
                    <select name="buscasb_codigo" id="buscasb_codigo" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkCasb_codigo))
                        foreach($this->fkCasb_codigo as $k=>$v){ ?>         
                      <option value="<?php echo $v["casb_codigo"]; ?>"  ><?php echo ucfirst($v["casb_descripcion"]); ?></option>
                      <?php } ?>  
                    </select>
                  </div>
                </div>
                <!--<div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Dependencia</label>                      
                    <select name="busdepe_id_ubica" id="busdepe_id_ubica" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkDepe_id_ubica))
                        foreach($this->fkDepe_id_ubica as $k=>$v){ ?>         
                      <option value="<?php echo $v["depe_id"]; ?>"  ><?php echo ucfirst($v["depe_nombre"]); ?></option>
                      <?php } ?>  
                    </select>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Dependencia</label>                      
                    <select name="busdepe_id" id="busdepe_id" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkDepe_id))
                        foreach($this->fkDepe_id as $k=>$v){ ?>         
                      <option value="<?php echo $v["depe_id"]; ?>"  ><?php echo ucfirst($v["depe_nombre"]); ?></option>
                      <?php } ?>  
                    </select>
                  </div>
                </div>-->
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tipo de Interior</label>                      
                    <select name="bustabl_tipo_interior" id="bustabl_tipo_interior" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkTabl_tipo_interior))
                        foreach($this->fkTabl_tipo_interior as $k=>$v){ ?>         
                      <option value="<?php echo $v["tabl_id"]; ?>"  ><?php echo ucfirst($v["tabl_descripcion"]); ?></option>
                      <?php } ?>  
                    </select>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="grid-body" id="aquitable" >
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>registro_bien" class="">Registro bien</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >
          <div class="grid-body" id="aquiformulario" >
            <form id="frmregistro_bien" action="#">
              <input type="hidden" name="rebi_id" id="rebi_id" value="">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Catalogo sbn <span class="help"> </span> </label>                      
                    <select name="casb_codigo" id="casb_codigo" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkCasb_codigo))
                        foreach($this->fkCasb_codigo as $k=>$v){ ?>                          
                      <option value="<?php echo $v["casb_codigo"]; ?>"><?php echo $v["casb_descripcion"] ?></option>
                      <?php } ?> 
                    </select>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi vadquisicion <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_vadquisicion" id="rebi_vadquisicion" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi ult val act <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_ult_val_act" id="rebi_ult_val_act" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi detalles <span class="help"> </span> </label>                                       
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <textarea name="rebi_detalles" id="rebi_detalles" class="form-control" placeholder=""></textarea>
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi reg publicos <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_reg_publicos" id="rebi_reg_publicos" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi area <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_area" id="rebi_area" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi observaciones <span class="help"> </span> </label>                                       
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <textarea name="rebi_observaciones" id="rebi_observaciones" class="form-control" placeholder=""></textarea>
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi Año <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_anno" id="rebi_anno" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Tipo Interior <span class="help"> </span> </label>                      
                    <select name="tabl_tipo_interior" id="tabl_tipo_interior" class="select2 form-control"  >
                      <option value="">Todos</option>
                      <?php if(!empty($this->fkTabl_tipo_interior))
                        foreach($this->fkTabl_tipo_interior as $k=>$v){ ?>                          
                      <option value="<?php echo $v["tabl_id"]; ?>"><?php echo $v["tabl_descripcion"] ?></option>
                      <?php } ?> 
                    </select>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Rebi numero interior <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="rebi_numero_interior" id="rebi_numero_interior" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Imagen Principal <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="file" accept=".jpg,.gif,.png" name="rebi_foto_principal" id="rebi_foto_principal" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div> 
              </div>
              <div class="form-actions">
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oRegistro_bien=new BDRegistro_bien('<?php echo $ventana; ?>');
    oRegistro_bien.vista_tabla();
  });
</script>
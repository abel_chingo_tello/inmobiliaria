<div class="jumbotron">
  <h1 class="display-4">Felicidades, su pago ha sido completado ¡¡ </h1>
  <p class="lead">
    Los pagos efectuados fueron por los siguientes conceptos :<br>
    <ul>
        <?php foreach($this->pagos as $a){ ?>    
            <li class="lead mb-3"><?php echo ucfirst($a["concepto"])." - ".number_format($a["cargo"],2,".",",") ?></li>
        <?php }?>
    </ul>
  
  </p>
  <hr class="my-4">
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="./" role="button">Ver mis pagos</a>
  </p>
</div>
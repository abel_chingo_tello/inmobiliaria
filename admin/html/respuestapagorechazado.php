<div class="jumbotron">
  <h1 class="display-4">Lo sentimos, su pago no ha sido completado.</h1>
  <p class="lead">Vuelva a intentarlo.</p>
  <hr class="my-4">
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="./" role="button">Ver mis pagos</a>
  </p>
</div>
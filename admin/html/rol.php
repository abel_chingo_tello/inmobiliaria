<?php 
$ventana="ven".date("YmdHis");
$imagen='static/media/defecto/nofoto.jpg';
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020 
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Rol</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
                    <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <?php if($this->user["tipo_usuario"]=="s"){ ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Empresa</label>                      
                      <select name="busidempresa" id="busidempresa" class="select2 form-control"  >
                        
                        <?php if(!empty($this->fkIdempresa))
                              foreach($this->fkIdempresa as $k=>$v){ ?>         
                          <option value="<?php echo $v["idempresa"]; ?>" <?php echo $v["idempresa"]==$this->idempresa?'selected="selected"':'';?> ><?php echo ucfirst($v["nombre"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
      <?php } ?><div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Estado</label>                      
                      <select name="busestado" id="busestado" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
                    </div>
            </form>
          </div>          <div class="grid-body" id="aquitable" >
          </div>
                  </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="<?php echo URL_SITIO; ?>rol" class="">Rol</a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frmrol" action="#">
            <input type="hidden" name="idrol" id="idrol" value="">
                <div class="row"> 
                <?php if($this->user["tipo_usuario"]=="s"){ ?>               
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">Empresa <span class="help"> </span> </label>                      
                      <select name="idempresa" id="idempresa" class="select2 form-control"  >
                          <option value="">Todos</option>
                          <?php if(!empty($this->fkIdempresa))
                                foreach($this->fkIdempresa as $k=>$v){ ?>                          
                          <option value="<?php echo $v["idempresa"]; ?>"><?php echo $v["nombre"] ?></option>
                          <?php } ?> 
                      </select>
                    </div>
                  </div>
                <?php } ?>          
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Nombre <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nombre" id="nombre" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Imagen <span class="help"> </span> </label>                    
                    <div class="row">
                      <div class="col-md-12">
                        <div class="img-container img-imagen">
                            <img id="fileimgimagen" style="max-width: 100%;"  src="<?php echo URL_MEDIA.$imagen; ?>" alt="Picture">
                            <input type="hidden" name="imagen" tipo="url" >
                        </div>
                        <div class="col-md-12 text-center">
                          <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                              <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                              <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                              <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                              <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                            </div>
                            <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                              <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                              <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                              <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                              <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                  
                  </div>
                </div>          
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">Estado <span class="help"> </span> </label>
                      <div class="row-fluid">
                        <div class="cambiarestado checkbox check-primary checkbox-circle" >
                          <input type="checkbox" value="0" name="estado" id="estado" v1="Activo" v2="Inactivo">
                          <label><span>Inactivo</span></label>
                        </div>
                      </div>                  
                    </div>
                </div>          
              
              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var oRol=new BDRol('<?php echo $ventana; ?>');
    oRol.vista_tabla();
  });
</script>
<?php 
$ventana="ven".date("YmdHis");
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-02-2021 
 * @copyright	Copyright (C) 15-02-2021. Todos los derechos reservados.
 */
?>
<div id="<?php echo $ventana; ?>" idrol="<?php echo $this->user["idrol"]; ?>" idpersona="<?php echo $this->user["idpersona"]; ?>" tuser="<?php echo $this->user["tipo_usuario"]; ?>" idempresa="<?php echo $this->empresa["idempresa"]; ?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo URL_SITIO; ?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active">Reporte</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
          <div class="grid-body">
            <div class="vistatablaformulario">
              
              <div class="row-fluid">
                <div class="span12">
                  <div class="grid simple" >         
                    <div class="grid-body" id="aquiformulario" >
                        <div class="row align-items-center">                
                          <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="form-group">
                              <label class="form-label">Buscar por Cliente : <span class="help"> </span> </label>
                              <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                                <i class="error fa-exclamation fa"></i>
                                <i class="success fa fa-check"></i>
                                <input type="text"  id="nombres" class="form-control" placeholder="Nombre / DNI / RUC">
                              </div>
                            </div>
                          </div>          
                                              
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                              <label class="form-label">Estado <span class="help"> </span> </label>
                              <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                                <i class="error fa-exclamation fa"></i>
                                <i class="success fa fa-check"></i>
                                <select id="estado" class="form-control" >
                                  <option value="">Elegir una opción</option>
                                  <option value="0">Moroso</option>
                                  <option value="1">Al día</option>
                                </select>
                              </div>
                            </div>
                          </div>          
                                          
                          <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                              <label class="form-label">Fecha inicial  <span class="help"> </span> </label>
                              <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                                <i class="error fa-exclamation fa"></i>
                                <i class="success fa fa-check"></i>
                                <input type="date"  id="fecha_inicio" class="form-control"  placeholder="">
                              </div>
                            </div>
                          </div>          
                          
                          <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                              <label class="form-label">Fecha final <span class="help"> </span> </label>
                              <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                                <i class="error fa-exclamation fa"></i>
                                <i class="success fa fa-check"></i>
                                <input type="date" id="fecha_fin" class="form-control" placeholder="">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-4 col-sm-4">
                            <div class="form-group text-center">
                              <button class="buscar btn-lg btn-success btn-cons"><i class="fa fa-search"></i> Buscar</button>
                            </div>
                          </div>
                        </div>

                      </form>
                    </div>
                  </div>    
                </div>
              </div>
            </div>
          </div>
          <div class="grid-body" id="aquitable" >
          </div>
                  </div>    
      </div>
    </div>
  </div>
 
</div>

<!-- modal para detalles-->

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h7 class="modal-title" id="exampleModalLabel">Detalles</h7>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container ">
             
                <div class="row justify-content-center align-items-center">
                  <div class="col-sm-6">
                    <label class="lead ">Fecha de atraso : </label>
                  </div>
                  <div class="col-sm-6">
                    <input class="form-control input-lg" type="text" id="m_fecha" readonly >
                  </div>
                </div>
                <div class="row justify-content-center align-items-center">
                  <div class="col-sm-6">
                    <label class="lead">Monto mensual : </label>
                  </div>
                  <div class="col-sm-6">
                    <input class="form-control input-lg" type="text" id="m_mensual" readonly >
                  </div>
                </div>
                <div class="row justify-content-center align-items-center">
                  <div class="col-sm-6">
                    <label class="lead">Monto total : </label>
                  </div>
                  <div class="col-sm-6">
                    <input class="form-control input-lg" type="text" id="m_total" readonly >
                  </div>
                </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>          

<script type="text/javascript"> 
  $(function(){    
    var oVistapagos=new BDVistapagos('<?php echo $ventana; ?>');
    oVistapagos.vista_tabla();
  });
</script>
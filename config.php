<?php
date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
ini_set('display_errors', '1');
define('CARPETA_RAIZ', 'Inmobiliaria/');
define('_version_', '1');
define('IDEMPRESA', '2');
define('_ishttp_', true);
define('DEFAULTMODULE',"paginas");
define('MODODEVELOP',true);
define(
    "_CONFIG_",
    json_encode(
        array(           
            "paginas" =>array("tema" =>"web" ,"_islogin_" =>false),
            "json" => array("tema" => "json", "_islogin_" => false),
            "admin" => array("tema" => "webarch", "_islogin_" => true),             
        )
    )
);

define("HOST_BD", "localhost"); // servidor local
define("USER_BD", "root");
define("PWD_BD", '');
define("NAME_BD", "abel_sbch");

$host = !empty($_SERVER["HTTP_HOST"]) ? $_SERVER["HTTP_HOST"] : 'localhost';
if(defined('_http_') == false) define('_http_', !empty($_SERVER['HTTPS']) ? 'https://' : 'http://');
if(defined('SD') == false) define('SD', DIRECTORY_SEPARATOR);
if(defined('_HOST_') == false) define('_HOST_', _http_ . $host);

$ruta_ini = dirname(__FILE__);
define('RUTA_BASE', $ruta_ini.SD);
define('RUTA_RAIZ', RUTA_BASE);
define('RUTA_LIBS', RUTA_RAIZ.'sys_librerias'.SD);
define('URL_BASE', _HOST_ .'/'. CARPETA_RAIZ);
define('URL_RAIZ', URL_BASE);
define('RUTA_STATIC', RUTA_BASE."static".SD);
define('RUTA_MEDIA', RUTA_STATIC."media".SD);
define('URL_STATIC', URL_BASE."static/");
define('URL_MEDIA', URL_STATIC."media/");

define('MAIL_HOST',"smtp.gmail.com");
define('MAIL_PORT',587);
define('MAIL_USERNAME',"@gmail.com");
define('MAIL_PASSWORD',"");
define('MAIL_FROM_ADDRESS',"@gmail.com");
define('MAIL_FROM_NAME',"INMOBILIARIA");

define('ACCES_TOKEN_MERCADOPAGO','TEST-5518363420133037-021402-2fe7e57da83df853c739dec21faa824c-715289615');

//DATOS COMPRADOR PRUEBA MERCADOPAGO

//Mastercard			
//Tarjeta Número	5031 7557 3453 0604
//Código de seguridad	123
//Fecha de vencimiento 11/25
//Correo   test_user_56878483@testuser.com
//nombres 
//APRO: Pago aprobado.
//CONT: Pago pendiente.
//OTHE: Rechazado por error general.
//CALL: Rechazado con validación para autorizar.
//FUND: Rechazado por monto insuficiente.
//SECU: Rechazado por código de seguridad inválido.
//EXPI: Rechazado por problema con la fecha de expiración.
//FORM: Rechazado por error en formulario.

//DATOS VENDEDOR
//{"id":715289615,"nickname":"TETE6071162","password":"qatest8644","site_status":"active","email":"test_user_41425563@testuser.com"} vendedor
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-03-2021 a las 14:31:49
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abel_sbch`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarmorosos` ()  MODIFIES SQL DATA
    COMMENT 'actualiza el estado "morosos" y "al dia" de cronogramacpagoscab '
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE fechavencimiento DATE;
DECLARE varid INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT cabid,fechaven from abel_sbch.vistapagos;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN cur;
read_loop: LOOP	
    FETCH cur INTO varid, fechavencimiento;
    IF done THEN
      	LEAVE read_loop;      	
    END IF;
    IF CURRENT_DATE()>fechavencimiento THEN
      UPDATE `cronogramapago_cab` SET `estado_pago`=0 where `id` = varid;    	 ELSE
      UPDATE `cronogramapago_cab` SET `estado_pago`=1 where `id`= varid;
      END IF;
    
  END LOOP;
CLOSE cur;
END$$

--
-- Funciones
--
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STRING` (`str` VARCHAR(255), `delim` VARCHAR(12), `pos` INT) RETURNS VARCHAR(255) CHARSET utf8mb4 RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(str, delim, pos),
       CHAR_LENGTH(SUBSTRING_INDEX(str, delim, pos-1)) + 1),
       delim, '')$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biblioteca`
--

CREATE TABLE `biblioteca` (
  `idbiblioteca` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Audio,video,pdf,imagen',
  `archivo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `biblioteca`
--

INSERT INTO `biblioteca` (`idbiblioteca`, `idempresa`, `idpersona`, `tipo`, `archivo`, `nombre`, `estado`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/buscar20201201231408.png', 'buscar20201201231408.png', 1, '2020-12-01 23:14:08', '2020-12-01 23:14:08'),
(2, 2, 1, 'image', 'static/media/E_2/U1/biblioteca/LOGO_INVIERTE20201207215628.png', 'LOGO_INVIERTE20201207215628.png', 1, '2020-12-07 20:56:28', '2020-12-07 20:56:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_sbn`
--

CREATE TABLE `catalogo_sbn` (
  `casb_codigo` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `casb_descripcion` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `casb_fecharegistro` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `catalogo_sbn`
--

INSERT INTO `catalogo_sbn` (`casb_codigo`, `casb_descripcion`, `casb_fecharegistro`) VALUES
('90000000', 'CEMENTERIO', '0000-00-00 00:00:00'),
('97000001', 'EDIFICIOS', '0000-00-00 00:00:00'),
('97000002', 'EDIFICIO PIEDRA LORA', '0000-00-00 00:00:00'),
('97000003', 'EDIFICIO DOS DE MAYO', '0000-00-00 00:00:00'),
('97000004', 'EDIFICIO GALERIAS LA PLAZUELA', '0000-00-00 00:00:00'),
('97000005', 'BIENES URBANOS', '0000-00-00 00:00:00'),
('98000001', 'TERRENOS', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `clie_id` int(11) NOT NULL,
  `tabl_tipocliente` int(11) NOT NULL,
  `clie_apellidos` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clie_nombres` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clie_razsocial` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_dni` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_telefono` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_fregistro` datetime NOT NULL DEFAULT current_timestamp(),
  `clie_estado` int(2) NOT NULL,
  `clie_zona` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_empleado` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`clie_id`, `tabl_tipocliente`, `clie_apellidos`, `clie_nombres`, `clie_razsocial`, `clie_dni`, `clie_direccion`, `clie_telefono`, `clie_email`, `clie_fregistro`, `clie_estado`, `clie_zona`, `clie_empleado`) VALUES
(1, 1, 'Gómez', 'Miluska', 'Miluska Gómez', '44140114', 'Casa 123', '015464561', 'mngomez_sy@outlook.com', '2020-12-23 20:17:53', 1, 'CENTRO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_bien`
--

CREATE TABLE `contacto_bien` (
  `id` int(11) NOT NULL,
  `tipo_documento` int(3) NOT NULL,
  `nro_documento` varchar(13) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` text COLLATE utf8_spanish_ci NOT NULL,
  `rebi_id` int(11) NOT NULL,
  `email` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` text COLLATE utf8_spanish_ci NOT NULL,
  `file_infocorp` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `file_recomendacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `file_policial_judicial` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `file_extra_recomendacion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `punataje` int(11) NOT NULL,
  `estado` int(2) NOT NULL DEFAULT 1 COMMENT '1=sin contrato,2=con contrato',
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contacto_bien`
--

INSERT INTO `contacto_bien` (`id`, `tipo_documento`, `nro_documento`, `apellidos`, `direccion`, `rebi_id`, `email`, `nombre`, `telefono`, `mensaje`, `file_infocorp`, `file_recomendacion`, `file_policial_judicial`, `file_extra_recomendacion`, `punataje`, `estado`, `fecha_insert`, `fecha_update`) VALUES
(1, 0, '', '', '', 1, 'armando.fch@gmail.com', 'arnadn', '', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-26 21:51:33', '2021-02-16 12:58:45'),
(2, 0, '', '', '', 1, 'armando.fch@gmail.com', 'arnadn', '', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-26 21:51:33', '2021-02-16 12:58:45'),
(3, 0, '', '', '', 1, 'armando.fch@gmail.com', 'asdasdasd', '', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-26 21:53:12', '2021-02-16 12:58:45'),
(4, 0, '', '', '', 1, 'armando.fch@gmail.com', 'asasd', '321654987', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-26 21:55:40', '2021-02-16 12:58:45'),
(5, 0, '', '', '', 1, 'armando.fch@gmail.com', 'asdasd', '951182135', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-27 18:17:55', '2021-02-16 12:58:45'),
(6, 0, '', '', '', 1, 'armando.fch@gmail.com', 'asdr', '951182135', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-28 00:26:31', '2021-02-16 12:58:45'),
(7, 0, '', '', '', 1, 'armando.fch@gmail.com', 'asd5', '951182135', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-28 00:28:02', '2021-02-16 12:58:45'),
(8, 0, '', '', '', 2786, 'armando.fch@gmail.com', 'arma', '951182135', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-28 00:29:39', '2021-02-16 12:58:45'),
(9, 51, '76824191', 'Flores Choque', 'Av Jesus 2407', 1, 'armando.fch@gmail.com', 'Armando', '951182135', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 1, '2021-01-29 19:29:28', '2021-02-16 12:58:45'),
(10, 51, '10101010', 'Prueba', 'direccion', 1, 'armando@armando.com', 'Armando', '999999999', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.\r\npiede ', '', '', '', '', 0, 1, '2021-01-29 20:07:09', '2021-02-16 12:58:45'),
(11, 51, '77678383', 'deza', 'direccion direccion', 1, 'ddezav@gmail.com', 'david', '321654987', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 2, '2021-02-02 19:54:17', '2021-02-16 14:14:20'),
(12, 51, '74185296', 'Suarez', 'direccion 123', 2287, 'suarez@gmail.com', 'Pepito', '999999999', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.\r\nurgente', '', '', '', '', 0, 1, '2021-02-02 21:30:02', '2021-02-16 12:58:45'),
(13, 51, '44445555', 'Cardenas', 'Av Jesus 2407', 2773, 'pepito@gmail.com', 'Pepito', '222222222', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias. etc', 'bba6984394f750d6ae95', '', '', '78e9df8d5d7cf75a2b71f9c9de792da5.pdf', 4, 1, '2021-02-05 21:39:36', '2021-02-19 22:40:22'),
(14, 50, '98765432', 'perez', 'calle san martin', 2286, 'ddezav@gmail.com', 'juan', '9876543211', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 2, '2021-02-16 12:49:10', '2021-02-16 14:13:36'),
(15, 50, '77777777', 'basadre', 'calle san  miguel', 4773, 'ddezav@gmail.com', 'diego', '888888888', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 2, '2021-02-16 14:30:11', '2021-02-16 14:31:15'),
(16, 50, '66666666', 'galardon', 'calle piedra', 1, 'ddezav@gmail.com', 'david', '321654878', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 2, '2021-02-18 20:39:48', '2021-02-18 20:41:11'),
(17, 50, '165165165', 'DD', 'asdffsdafd', 1, 'ddesa@asd.com', 'DD', '561651', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', 'bfa425ca7e7bb1dd82ce', '522666762256e1650eac', 'efe2d951d979ee520dccff7362146ce1.pdf', '', 6, 1, '2021-02-18 20:55:09', '2021-03-11 09:03:19'),
(18, 50, '99999999', 'veliz', 'calle ugarte', 4773, 'ddezav@gmail.com', 'teresa', '6549819', 'Hola, me interesa este inmueble y quiero que me contacten. Gracias.', '', '', '', '', 0, 2, '2021-02-22 17:37:44', '2021-02-22 17:39:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cronogramapago_cab`
--

CREATE TABLE `cronogramapago_cab` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `rebi_id` int(11) NOT NULL,
  `garantia` decimal(10,2) NOT NULL,
  `meses_contrato` tinyint(4) NOT NULL,
  `monto_mensual` decimal(10,2) NOT NULL,
  `dia_pago` tinyint(4) NOT NULL,
  `archivo_contrato` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0 = sin aceptar, 1 = acepto',
  `estado_pago` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=moroso,1=al dia',
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cronogramapago_cab`
--

INSERT INTO `cronogramapago_cab` (`id`, `persona_id`, `rebi_id`, `garantia`, `meses_contrato`, `monto_mensual`, `dia_pago`, `archivo_contrato`, `observacion`, `estado`, `estado_pago`, `fecha_insert`, `fecha_update`, `fecha`) VALUES
(1, 6, 2286, '2500.00', 12, '100.00', 5, '385f2d220faa3a633beda1a373f8281b.pdf', '', 1, 0, '2021-02-16 14:13:36', '2021-03-10 10:01:06', '2021-03-05'),
(2, 3, 1, '1000.00', 12, '630.00', 15, 'a7daf3494041354fd356c91e3d59920d.pdf', '', 1, 1, '2021-02-16 14:14:20', '2021-02-17 12:37:12', '2021-04-15'),
(3, 7, 4773, '26000.00', 3, '500.00', 2, 'a82833c8ed2ac41e2d553b43531ac32d.pdf', '', 1, 0, '2021-02-16 14:31:15', '2021-03-10 10:01:06', '2021-01-02'),
(4, 8, 1, '2600.00', 10, '500.00', 20, 'fbbec71acf8280837f53ac7c5c9aadaf.pdf', '', 1, 1, '2021-02-18 20:41:11', '2021-02-18 20:47:51', '0000-00-00'),
(5, 9, 4773, '3500.00', 10, '500.00', 13, 'f8c5c71444e096c71a16b828ebd8d3e4.pdf', '', 1, 1, '2021-02-22 17:39:55', '2021-02-22 17:47:51', '0000-00-00');

--
-- Disparadores `cronogramapago_cab`
--
DELIMITER $$
CREATE TRIGGER `crear_cronograma` AFTER INSERT ON `cronogramapago_cab` FOR EACH ROW BEGIN

SET @cant := new.meses_contrato+1;
SET @dia := new.dia_pago;
SET @mes := MONTH(CURRENT_DATE());

SET @anio := YEAR(CURRENT_DATE());
SET @primerafecha := CONCAT(@anio,'/',@mes,'/',@dia);
SET @actual := CURRENT_DATE();
IF @actual < @primerafecha THEN
	SET @mes := @mes + 1;
    SET @primerafecha := CONCAT(@anio,'/',@mes,'/',@dia);
END IF;

SET @i:= -1;
label1: LOOP	
	SET @i := @i+1;
    IF @i = 0 THEN
    	INSERT INTO `cronogramapago_det` (`cab_id`, `fecha_vencimiento`, `cargo`, `saldo`,`concepto`) VALUES (new.id,@primerafecha,new.garantia,new.garantia,"Garantía de alquiler");
        IF @mes+1 > 12 THEN
        	SET @anio := @anio+1;
			SET @mes := 1;
		ELSE
			SET @mes := @mes+1;
        END IF;
         
        SET @primerafecha := CONCAT(@anio,'/',@mes,'/',@dia);
        ITERATE label1;
    ELSEIF @i < @cant THEN
        INSERT INTO `cronogramapago_det` (`cab_id`, `fecha_vencimiento`, `cargo`, `saldo`,`concepto`) VALUES (new.id,@primerafecha,new.monto_mensual,new.monto_mensual,CONCAT("mes ",@i));
        IF @mes+1 > 12 THEN
        	SET @anio := @anio+1;
			SET @mes := 1;
		ELSE
			SET @mes := @mes+1;
        END IF;
         
        SET @primerafecha := CONCAT(@anio,'/',@mes,'/',@dia);
    	ITERATE label1;    
    END IF;
    LEAVE label1;
END LOOP label1; 


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cronogramapago_det`
--

CREATE TABLE `cronogramapago_det` (
  `id` int(11) NOT NULL,
  `cab_id` int(11) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `concepto` varchar(100) NOT NULL,
  `cargo` decimal(10,2) NOT NULL,
  `pago` decimal(10,2) NOT NULL DEFAULT 0.00,
  `saldo` decimal(10,2) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `observacion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_pago` date NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0=no pago,1=si pago',
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cronogramapago_det`
--

INSERT INTO `cronogramapago_det` (`id`, `cab_id`, `fecha_vencimiento`, `concepto`, `cargo`, `pago`, `saldo`, `subtotal`, `observacion`, `fecha_pago`, `estado`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, '2021-03-05', 'Garantía de alquiler', '2500.00', '0.00', '2500.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(2, 1, '2021-04-05', 'mes 1', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(3, 1, '2021-05-05', 'mes 2', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(4, 1, '2021-06-05', 'mes 3', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(5, 1, '2021-07-05', 'mes 4', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(6, 1, '2021-08-05', 'mes 5', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(7, 1, '2021-09-05', 'mes 6', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(8, 1, '2021-10-05', 'mes 7', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(9, 1, '2021-11-05', 'mes 8', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(10, 1, '2021-12-05', 'mes 9', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(11, 1, '2022-01-05', 'mes 10', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(12, 1, '2022-02-05', 'mes 11', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(13, 1, '2022-03-05', 'mes 12', '100.00', '0.00', '100.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:13:36', '0000-00-00 00:00:00'),
(14, 2, '2021-03-15', 'Garantía de alquiler', '1000.00', '1000.00', '0.00', '0.00', '', '2021-02-16', 1, '2021-02-16 14:14:20', '2021-02-16 14:17:00'),
(15, 2, '2021-04-15', 'mes 1', '630.00', '630.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:14:20', '2021-02-17 12:24:01'),
(16, 2, '2021-05-15', 'mes 2', '630.00', '630.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:14:20', '2021-02-17 12:24:01'),
(17, 2, '2021-06-15', 'mes 3', '630.00', '630.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:14:20', '2021-02-17 12:24:01'),
(18, 2, '2021-07-15', 'mes 4', '630.00', '630.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:14:20', '2021-02-17 12:24:01'),
(19, 2, '2021-08-15', 'mes 5', '630.00', '630.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:14:20', '2021-02-17 12:24:01'),
(20, 2, '2021-09-15', 'mes 6', '630.00', '630.00', '0.00', '0.00', '', '2021-02-19', 1, '2021-02-16 14:14:20', '2021-02-19 22:21:04'),
(21, 2, '2021-10-15', 'mes 7', '630.00', '630.00', '0.00', '0.00', '', '2021-02-19', 1, '2021-02-16 14:14:20', '2021-02-19 22:21:04'),
(22, 2, '2021-11-15', 'mes 8', '630.00', '630.00', '0.00', '0.00', '', '2021-02-19', 1, '2021-02-16 14:14:20', '2021-02-19 22:21:04'),
(23, 2, '2021-12-15', 'mes 9', '630.00', '630.00', '0.00', '0.00', '', '2021-02-19', 1, '2021-02-16 14:14:20', '2021-02-19 22:21:04'),
(24, 2, '2022-01-15', 'mes 10', '630.00', '630.00', '0.00', '0.00', '', '2021-02-19', 1, '2021-02-16 14:14:20', '2021-02-19 22:21:04'),
(25, 2, '2022-02-15', 'mes 11', '630.00', '0.00', '630.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:14:20', '0000-00-00 00:00:00'),
(26, 2, '2022-03-15', 'mes 12', '630.00', '0.00', '630.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:14:20', '0000-00-00 00:00:00'),
(27, 3, '2021-01-02', 'Garantía de alquiler', '26000.00', '26000.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:31:15', '2021-02-17 12:27:59'),
(28, 3, '2021-02-02', 'mes 1', '500.00', '500.00', '0.00', '0.00', '', '2021-02-17', 1, '2021-02-16 14:31:15', '2021-02-17 12:39:19'),
(29, 3, '2021-03-02', 'mes 2', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:31:15', '2021-02-16 14:32:05'),
(30, 3, '2021-04-02', 'mes 3', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-16 14:31:15', '2021-02-16 14:32:10'),
(31, 4, '2021-03-20', 'Garantía de alquiler', '2600.00', '2600.00', '0.00', '0.00', '', '2021-02-18', 1, '2021-02-18 20:41:11', '2021-02-18 20:47:51'),
(32, 4, '2021-04-20', 'mes 1', '500.00', '500.00', '0.00', '0.00', '', '2021-02-18', 1, '2021-02-18 20:41:11', '2021-02-18 20:47:51'),
(33, 4, '2021-05-20', 'mes 2', '500.00', '500.00', '0.00', '0.00', '', '2021-02-18', 1, '2021-02-18 20:41:11', '2021-02-18 20:47:51'),
(34, 4, '2021-06-20', 'mes 3', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(35, 4, '2021-07-20', 'mes 4', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(36, 4, '2021-08-20', 'mes 5', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(37, 4, '2021-09-20', 'mes 6', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(38, 4, '2021-10-20', 'mes 7', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(39, 4, '2021-11-20', 'mes 8', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(40, 4, '2021-12-20', 'mes 9', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(41, 4, '2022-01-20', 'mes 10', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-18 20:41:11', '0000-00-00 00:00:00'),
(42, 5, '2021-03-13', 'Garantía de alquiler', '3500.00', '3500.00', '0.00', '0.00', '', '2021-02-22', 1, '2021-02-22 17:39:55', '2021-02-22 17:47:51'),
(43, 5, '2021-04-13', 'mes 1', '500.00', '500.00', '0.00', '0.00', '', '2021-02-22', 1, '2021-02-22 17:39:55', '2021-02-22 17:47:51'),
(44, 5, '2021-05-13', 'mes 2', '500.00', '500.00', '0.00', '0.00', '', '2021-02-22', 1, '2021-02-22 17:39:55', '2021-02-22 17:47:51'),
(45, 5, '2021-06-13', 'mes 3', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(46, 5, '2021-07-13', 'mes 4', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(47, 5, '2021-08-13', 'mes 5', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(48, 5, '2021-09-13', 'mes 6', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(49, 5, '2021-10-13', 'mes 7', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(50, 5, '2021-11-13', 'mes 8', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(51, 5, '2021-12-13', 'mes 9', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00'),
(52, 5, '2022-01-13', 'mes 10', '500.00', '0.00', '500.00', '0.00', '', '0000-00-00', 0, '2021-02-22 17:39:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencia`
--

CREATE TABLE `dependencia` (
  `depe_id` int(11) NOT NULL,
  `depe_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depe_depeid` int(11) DEFAULT NULL,
  `depe_direccion` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `dependencia`
--

INSERT INTO `dependencia` (`depe_id`, `depe_nombre`, `depe_depeid`, `depe_direccion`) VALUES
(0, 'INICIO', NULL, NULL),
(2, 'SOCIEDAD DE BENEFICENCIA CHICLAYO', 0, 'CHICLAYO'),
(3, 'PRESIDENCIA', 2, NULL),
(4, 'DIRECTORIO', 3, NULL),
(5, 'GERENCIA GENERAL', 3, NULL),
(6, 'OFICINA DE CONTROL INSTITUCIONAL', 3, NULL),
(8, 'OFICINA DE ASESORIA JURIDICA', 5, NULL),
(9, 'OFICINA DE PLANEAMIENTO Y PRESUPUESTO', 5, NULL),
(10, 'OFICINA GENERAL DE ADMINISTRACION', 5, NULL),
(11, 'UNIDAD DE RECURSOS HUMANOS', 10, NULL),
(12, 'UNIDAD DE CONTABILIDAD', 10, NULL),
(13, 'UNIDAD DE TESORERIA', 10, NULL),
(14, 'UNIDAD DE LOGISTICA, CONTROL PATRIMONIAL Y SERV. INTERNOS', 10, 'ELIAS AGUIRRE N° 248 - INT.07'),
(15, 'UNIDAD DE SISTEMAS Y SOPORTE TECNOLOGICO', 10, NULL),
(18, 'DEPARTAMENTO DE SERVICIO FUNERARIO INTEGRAL-SERFIN', 116, NULL),
(19, 'SUB GERENCIA DE GESTION INMOBILIARIA', 113, NULL),
(20, 'OFICINA CONTROL PATRIMONIAL', 14, NULL),
(22, 'UNIDAD DE IMAGEN INSTITUCIONAL Y GESTION DOCUMENTARIA', 3, NULL),
(23, 'OBRAS SOCIALES', 5, NULL),
(24, 'GERENCIA DE ASISTENCIA Y PROMOCION SOCIAL', 5, NULL),
(25, 'ALMACEN CENTRAL', 14, 'ELIAS AGUIRRE 248-07 - CHICLAYO'),
(28, 'DEPARTAMENTO CENTRO MEDICO SALUD VIDA', 114, NULL),
(31, 'DEPARTAMENTO DE CEMENTERIO', 116, NULL),
(32, 'GESTION DOCUMENTARIA', 22, NULL),
(33, 'OFICINA DE NUTRICION', 24, NULL),
(34, 'CONSULTORIO JURIDICO', 112, NULL),
(35, 'ENTIDAD EXTERNA / PERSONAL EXTERNO', 0, NULL),
(36, 'ARCHIVO INSTITUCIONAL', 22, NULL),
(37, 'EMPLEADOS / VIGILANTES', 2, NULL),
(39, 'COMITE ESPECIAL DE OBRAS', 2, NULL),
(40, 'I.E.P. VICENTE DE LA VEGA', 114, NULL),
(41, 'COMITE DE CONTROL', 5, NULL),
(43, 'CASA REFUGIO DORITA RIVAS MARTINO', 45, NULL),
(44, 'COMITE ESPECIAL DE PROCESOS DE SELECCION', 5, NULL),
(45, 'SUB GERENCIA DE ASISTENCIA SOCIAL', 24, NULL),
(46, 'COMISION DE CHOCOLATADAS', 5, NULL),
(47, 'ORIENTACION DE OFICIO', 6, NULL),
(48, 'PROCESOS ADMINISTRATIVO DISCIPLINARIOS', 11, NULL),
(49, 'SUB GERENCIA DE PROYECTOS Y OBRAS', 109, NULL),
(50, 'OFICINA DE COSTOS', 10, NULL),
(51, 'ALMACEN DE OBRAS', 14, 'AV. EL CARMEN S/N-(CEMENTERIO EL CARMEN)-CHICLAYO'),
(52, 'COMEDOR POPULAR SBCH', 45, NULL),
(53, 'CAI. CRUZ DE LA ESPERANZA', 45, NULL),
(54, 'CAI. 1° DE JUNIO-LA VICTORIA', 45, NULL),
(55, 'CAI. 1° DE MAYO-JLO', 45, NULL),
(56, 'COMEDOR ADULTO MAYOR - VILLA HERMOSA', 45, NULL),
(57, 'CAI. 9 DE OCTUBRE', 45, NULL),
(58, 'I.E. JESUS DE NAZARETH', 112, NULL),
(59, 'SUPERVISION DE SEGURIDAD', 11, NULL),
(60, 'PROGRAMA JORNADAS DE PROYECCION SOCIAL', 28, NULL),
(61, 'BOTICA SBCH', 28, NULL),
(62, 'COMISION DE AUDITORIA DE CUMPLIMIENTO 1', 6, NULL),
(63, 'COMISION DE INVENTARIOS', 5, NULL),
(64, 'COMISION DE SANEAMIENTO SBCH', 3, 'ELIAS AGUIRRE N° 248 - INT.07'),
(65, 'COMISION DE ANIVERSARIO SBCH', 5, NULL),
(67, 'COMISION SANEAMIENTO DE INMUEBLES Y CEMENTERIO', 5, NULL),
(68, 'SUB GERENCIA DE INGENIERIA', 5, NULL),
(69, 'BIENES URBANOS', 19, NULL),
(70, 'GALERIAS LA PLAZUELA', 19, NULL),
(71, 'TEATRO DOS DE MAYO', 19, NULL),
(72, 'ALMACEN DE PATRIMONIO', 19, NULL),
(73, 'TRANSPORTES-SBCH', 14, NULL),
(74, 'CONSULTORIO DE TRAUMATOLOGIA', 28, NULL),
(75, 'CONSULTORIO UROLOGIA - CARDIOLOGIA', 28, NULL),
(76, 'EDIFICIO DOS DE MAYO', 19, NULL),
(77, 'SUB GERENCIA', 5, NULL),
(78, 'CENTRO DE EMERGENCIA MUJER', 24, NULL),
(79, 'CONSULTORIO PSICOLOGIA   ', 28, NULL),
(80, 'LABORATORIO CENTRO MEDICO', 28, NULL),
(81, 'CONSULTORIO GINECOLOGIA', 28, NULL),
(82, 'TRIAJE CENTRO MEDICO  ', 28, NULL),
(83, 'DEPOSITO BOMBA', 28, NULL),
(84, 'CONSULTORIO PEDIATRIA', 28, NULL),
(85, 'CENTRO DE FOTOCOPIADORA', 19, NULL),
(86, 'AMBIENTE DE ASEO - CENTRO MEDICO ', 28, NULL),
(87, 'CONSULTORIO MEDICINA FISICA ', 28, NULL),
(88, 'WAWA WASI INSTITUCIONA', 24, NULL),
(89, 'CONSULTORIO OTORRINO', 28, NULL),
(90, 'CONSULTORIO DERMATOLOGIA - NEUROLOGIA', 28, NULL),
(91, 'ASESORIA LEGAL EXTERNA', 5, NULL),
(92, 'CONSULTORIO ODONTOLOGIA', 28, NULL),
(93, 'VIGILANCIA OFICINAS SBCH', 19, NULL),
(94, 'CONSULTORIO OFTALMOLOFIA ', 28, NULL),
(95, 'PROGRAMA NACIONAL CUNA MAS', 5, NULL),
(96, 'PROGRAMA SOCIAL SEÑOR DE LA MISECORDIA', 24, NULL),
(97, 'CONSULTORIO MEDICINA GENERAL', 28, NULL),
(98, 'VELATORIO SBCH', 18, NULL),
(99, 'TRANSPORTES-SERFIN', 18, NULL),
(100, 'SISTEMA DE CONTROL INTERNO', 5, NULL),
(101, 'ORGANO SANCIONADOR', 48, NULL),
(102, 'ORGANO INSTRUCTOR', 48, NULL),
(103, 'FONDO DE CAJA CHICA', 10, NULL),
(104, 'COMITE ADM. DEL FONDO ASIST. Y ESTIMULO-CAFAE', 2, NULL),
(105, 'COMISION DE AUDITORIA DE CUMPLIMIENTO 2', 6, NULL),
(106, 'COMISION DE ALTAS Y BAJAS', 5, NULL),
(107, 'SECRETARIA DE SIST.CONTROL INTERNO', 100, NULL),
(109, 'GERENCIA DE INGENIERIA', 5, NULL),
(110, 'ALMACEN TEMPORAL DE BIENES DE INVENTARIO', 19, NULL),
(111, 'COMITE ADQUISICION DE ATAUDES', 5, NULL),
(112, 'SUB GERENCIA DE PROMOCION Y DESARROLLO HUMANO', 24, NULL),
(113, 'GERENCIA DE GESTION DE NEGOCIOS', 5, NULL),
(114, 'SUB GERENCIA DE NEGOCIOS', 113, NULL),
(116, 'SUB GERENCIA DE SERVICIOS FUNERARIOS', 113, NULL),
(117, 'COMITE ESPEC. DE ARRENDAMIENTOS CONV. PUB.', 5, NULL),
(118, 'COMISION CONVOCATORIA DE PERSONAL', 5, NULL),
(119, 'COMISION PAQUETES DE ALIMENTOS', 5, NULL),
(120, 'COMISION ACTUALIZACION DOCUMENTOS DE GESTION', 5, NULL),
(121, 'CAI. VILLA HERMOSA ', 45, NULL),
(122, 'CAI. JORGE CHAVEZ', 45, NULL),
(123, 'CAI. NUEVO 2', 45, NULL),
(124, 'CAI. NUEVO 3', 45, NULL),
(125, 'CAI. NUEVO 4', 45, NULL),
(126, 'COMISION DE UNIFORMES', 5, NULL),
(127, 'SINDICATO DE TRABAJADORES', 11, NULL),
(128, 'COMITE DE SEGURIDAD Y SALUD EN EL TRABAJO', 5, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idempresa` bigint(20) NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idioma` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdominio` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '	0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`idempresa`, `nombre`, `direccion`, `telefono`, `correo`, `logo`, `ruc`, `idioma`, `subdominio`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Serfin', 'chiclayo2', '987654321', 'info@ingytal.com', 'static/media/E_1/U1/empresa/logo20201008095907.png', '20145698713', 'ES', 'all', 1, 1, '2020-08-08 20:52:07', '2020-12-01 23:48:41'),
(2, 'InMobiliario', '....', '9999999', 'asdasd@correo.com', 'static/media/E_2/U1/empresa/logo.png', '31235456', 'ES', '.', 1, 1, '2020-12-01 23:49:59', '2020-12-07 20:57:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_config`
--

CREATE TABLE `empresa_config` (
  `idempresaconfig` bigint(20) NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `parametro` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_config`
--

INSERT INTO `empresa_config` (`idempresaconfig`, `idempresa`, `parametro`, `valor`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'color_fondo_login', '#f6f7f8', 1, 1, '2020-10-04 09:17:36', '2020-10-04 09:21:13'),
(2, 1, 'imagen_fondo_login', '', 1, 1, '2020-10-04 09:19:12', '2020-10-04 09:47:35'),
(3, 1, 'tipo_empresa', 'tienda', 1, 2, '2020-10-13 15:25:39', '2020-10-23 20:52:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_modulo`
--

CREATE TABLE `empresa_modulo` (
  `idempresamodulo` bigint(20) NOT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idrol` int(11) NOT NULL,
  `orden` int(11) DEFAULT 0,
  `idmodulopadre` int(11) DEFAULT NULL,
  `nomtemporal` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_modulo`
--

INSERT INTO `empresa_modulo` (`idempresamodulo`, `idmodulo`, `idempresa`, `estado`, `idrol`, `orden`, `idmodulopadre`, `nomtemporal`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, NULL, 1, 1, 1, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2dzIn0=', 1, '2020-12-01 23:27:23', '2020-12-01 23:27:46'),
(2, 3, 1, 1, 1, 2, 1, NULL, 1, '2020-12-01 23:27:36', '2020-12-01 23:30:02'),
(3, 4, 1, 1, 1, 3, 1, NULL, 1, '2020-12-01 23:27:57', '2020-12-01 23:30:02'),
(4, 2, 1, 1, 1, 1, 1, NULL, 1, '2020-12-01 23:29:54', '2020-12-01 23:30:02'),
(5, NULL, 2, 1, 2, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2cifQ==', 1, '2020-12-01 23:54:48', '2020-12-01 23:55:48'),
(6, 2, 2, 1, 2, 1, 5, NULL, 1, '2020-12-01 23:54:57', '2020-12-01 23:55:48'),
(7, 3, 2, 1, 2, 2, 5, NULL, 1, '2020-12-01 23:55:18', '2020-12-01 23:55:48'),
(8, 4, 2, 1, 2, 3, 5, NULL, 1, '2020-12-01 23:55:26', '2020-12-01 23:55:48'),
(10, 6, 2, 1, 2, 2, NULL, NULL, 1, '2020-12-07 20:47:31', '2020-12-07 20:49:51'),
(15, 12, 2, 1, 2, 3, NULL, NULL, 1, '2020-12-23 20:16:33', '2020-12-23 20:17:16'),
(16, 13, 2, 1, 2, 5, NULL, NULL, 1, '2020-12-24 00:36:57', '2020-12-24 00:36:57'),
(17, 14, 2, 1, 2, 6, NULL, NULL, 1, '2021-02-02 21:27:48', '2021-02-02 21:27:48'),
(19, 16, 2, 1, 4, 7, NULL, NULL, 1, '2021-02-08 21:47:36', '2021-02-08 21:47:36'),
(20, 17, 2, 1, 2, 8, NULL, NULL, 1, '2021-02-12 13:52:45', '2021-02-12 13:52:45'),
(21, 18, 2, 1, 2, 9, NULL, NULL, 1, '2021-02-16 12:01:33', '2021-02-16 12:01:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_smtp`
--

CREATE TABLE `empresa_smtp` (
  `idempresasmtp` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `host` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puerto` int(11) NOT NULL,
  `cifrado` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_fotos`
--

CREATE TABLE `galeria_fotos` (
  `id_galeria` int(11) NOT NULL,
  `rebi_id` int(11) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `tipo` varchar(60) NOT NULL,
  `imagen` varchar(250) NOT NULL,
  `estado` smallint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `galeria_fotos`
--

INSERT INTO `galeria_fotos` (`id_galeria`, `rebi_id`, `titulo`, `descripcion`, `tipo`, `imagen`, `estado`) VALUES
(6, 2285, 'sdfsafsadf', 'fsdfsadf', 'gfdgfd', 'eb39144c6694afffa5ead1ea7582507b.jpg', -1),
(7, 2285, 'AAA', 'DDD', 'VVVV', 'cf3d18b5012bfa66a0901f4c0252b41e.jpg', 1),
(8, 2285, '123', '1234', '12345', 'f8647c91ace28a14063b8b7b9d672011.jpg', -1),
(9, 2285, '2341234', '21342134', '213432421', 'c59c8edf8012a41b525f984c6f09238a.jpg', -1),
(10, 2285, '2332432', '324324', '234234324', '62885bcc47f6394bfd9cc50a41b97c03.png', -1),
(11, 1, 'TITULO', 'asdasdasd', 'TIPO DE IMAGE CLASIFICA', '47684a44c184b12ecf164e222ba137f4.jpg', -1),
(12, 1, 'TITULO 2', 'desc 2 ', 'TIPO 2', '2844ad52a3cf4e4d17948fdbff488602.jpg', -1),
(13, 1, 'TITULO 2.1', 'DESC', 'TIPO 2', '5eed1f087fba39901f2c284142c147b1.jpg', -1),
(14, 1, 'nuevo tit', 'ddddd', 'TIPO 2', '5820a3cba6eb31a42f8727eda98e290e.jpg', -1),
(15, 4769, 'MI ITUTLO', 'decsrio', 'TIPO DE ', '5f8d39b031fb49627c7995d8d95b8690.jpg', 1),
(16, 4774, 'imagen de titulo', 'desrrcipoinn', 'TIPO NUEVO', '0f9f73fc18d925cf7dd0b74fe7689758.jpg', 1),
(17, 4774, 'img de titu', 'SDASDASD', 'TIPO NUEVO', '1c7cfa20d56426b58ab6aa3e945300e5.jpg', 1),
(18, 2812, 'sadf', 'asdf', 'asdf', '45a564d6fbfdda7147b3b782ccef4f8a.png', 1),
(19, 1, 'Dirección Chiclayo', 'Descripción de dirección', 'Plano de Ubicación', '7a86a71e9a58a9d4a2e7206cfc2f4c95.png', 1),
(20, 1, 'Fotografía del predio 1', 'Desc fot predio 1', 'Propuesta Alternativa', '85877404aaf8a6a8ca7f120c0dd14d5d.jpg', 1),
(21, 1, 'Fotografía del predio 2', 'Desc fot predio 2', 'Propuesta Alternativa', '4475bc83f0106bd23ff10a10e6b37da2.jpg', 1),
(22, 1, 'Entorno del predio 1', 'Desc entorno del predio 1', 'Entorno del Predio', '387653b78d15a5100b7545bdcc854af1.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_sesion`
--

CREATE TABLE `historial_sesion` (
  `idhistorial` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navegador` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgrupoaula` bigint(20) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_final` datetime NOT NULL DEFAULT current_timestamp(),
  `lugar` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `idmodulo` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1 COMMENT '	0=Inactivo,1,Activo,-1=eliminado',
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icono` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`idmodulo`, `nombre`, `descripcion`, `estado`, `url`, `icono`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Empresa', '', 0, '#', 'fa-config', 1, '2020-10-04 08:41:00', '2020-10-07 17:23:28'),
(2, 'Configuracion', 'Parámetros de configuración de empresa', 1, 'admin/empresa/configuracion', 'fa-building-o', 1, '2020-10-04 08:43:41', '2020-10-07 17:23:24'),
(3, 'Módulos', '', 1, 'admin/modulos', 'fa-tags', 1, '2020-10-10 08:56:24', '2020-12-01 23:29:09'),
(4, 'Asignación de módulos', '', 1, 'admin/empresa/modulos', 'fa-tags', 1, '2020-10-10 08:56:49', '2020-12-01 23:29:29'),
(5, 'Perfil', '', 1, 'admin/persona/perfil', 'fa-pencil', 2, '2020-10-10 12:01:13', '2020-10-17 02:40:33'),
(6, 'Persona', NULL, 1, 'persona', 'fa-user', 1, '2020-12-07 20:45:47', '2020-12-07 20:45:47'),
(7, 'Clientes', '', -1, 'admin/clientes', 'fa-user-plus', 1, '2020-12-07 20:46:12', '2020-12-22 20:55:36'),
(8, 'Configuración de datos generales', NULL, -1, 'admin/tabla', 'fa-bars', 1, '2020-12-21 21:17:15', '2020-12-22 20:55:32'),
(9, 'Configuración Tabla', NULL, 1, 'admin/tabla', 'fa-check-square', 1, '2020-12-22 20:57:37', '2020-12-22 20:57:37'),
(10, 'Clientes', NULL, -1, 'admin/clientes', 'fa-user', 1, '2020-12-22 21:47:24', '2020-12-23 19:14:24'),
(11, 'Clientes', NULL, -1, 'admin/clientes', 'fa-user', 1, '2020-12-23 19:54:39', '2020-12-23 19:58:43'),
(12, 'Clientes', NULL, 1, 'admin/clientes', 'fa-user', 1, '2020-12-23 20:16:12', '2020-12-23 20:16:12'),
(13, 'Registro Bien', NULL, 1, 'admin/registro_bien', 'fa-home', 1, '2020-12-24 00:36:47', '2020-12-24 00:36:47'),
(14, 'Cotizaciones', 'cotizaciones de clientes', 1, 'admin/contacto_bien', 'fa-file-excel-o', 1, '2021-02-02 21:27:30', '2021-02-02 21:27:30'),
(15, 'Mis Pagos', NULL, -1, 'mis_pagos', 'fa-credit-card-alt', 1, '2021-02-08 21:43:41', '2021-02-08 21:44:15'),
(16, 'Mis Pagos', '', 1, 'admin/cronogramapago_det', 'fa-credit-card-alt', 1, '2021-02-08 21:44:39', '2021-02-13 11:56:33'),
(17, 'Cronograma Pagos', '', 1, 'admin/cronogramapago_cab', 'fa-calendar', 1, '2021-02-12 13:51:56', '2021-02-12 13:53:51'),
(18, 'Reporte', NULL, 1, 'admin/vistapagos', 'fa-file-excel-o', 1, '2021-02-16 12:01:13', '2021-02-16 12:01:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `preference_id` varchar(150) NOT NULL,
  `cronogramapagocab_id` int(11) NOT NULL,
  `cronogramapagodet_ids` varchar(150) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=no aprobado,1=aprobado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `persona_id`, `preference_id`, `cronogramapagocab_id`, `cronogramapagodet_ids`, `estado`) VALUES
(1, 3, '', 0, '272, 273', 0),
(2, 3, '715289615-16dc8472-dbc5-4c34-9c3b-37639142353a', 0, '272,273', 1),
(3, 3, '715289615-95dbe34d-e13f-49d2-b64a-e3e84fe13683', 0, '272,273', 1),
(4, 3, '715289615-021dad51-f192-4f8e-a9ae-d0a2e89662ae', 0, '272,273', 0),
(5, 3, '715289615-d168b7d3-5319-4650-9490-3ee16333b237', 0, '272', 0),
(6, 3, '715289615-78539065-90d0-4e7c-a824-b3489b9a23a8', 0, '272,273,274,275', 1),
(7, 3, '715289615-e4e73f04-e21b-4435-9479-081528ec78d6', 0, '272,278,277,276,275,274,273', 1),
(8, 3, '715289615-042fb3b1-8886-4cfd-ae4c-18ff1bc25c3f', 0, '274,275', 1),
(9, 3, '715289615-3e69e24f-bfed-4ac4-83b0-3d85f02f5d88', 0, '276,277,278', 0),
(10, 3, '715289615-ef1fd20e-3e9a-48ff-ae7c-adfa8e52d14f', 0, '155', 1),
(11, 3, '715289615-76eb39df-133f-4846-a996-c2e6e19d6269', 0, '276', 0),
(12, 3, '715289615-6cf0b36b-5f05-4910-968a-19344beabb6d', 0, '276', 0),
(13, 3, '715289615-44e47678-2bed-4cfc-b6b5-eee5f8d3d776', 0, '276', 0),
(14, 3, '715289615-c9303f3e-5506-4c53-b392-e6e0a324e5d5', 0, '276', 0),
(15, 3, '715289615-7c1a3740-0562-4d82-bcad-75f57c7fad62', 0, '276', 0),
(16, 3, '715289615-2df0cfa1-be06-4af2-a915-db2ae517aeb7', 0, '276', 0),
(17, 3, '715289615-2073802b-a4ae-4a0f-a14a-865dd2552afc', 0, '276,277', 0),
(18, 3, '715289615-aee94f7e-1d11-401e-9750-e67296ddcad8', 0, '276,277', 0),
(19, 3, '715289615-ef8b5a32-d86e-45c6-8dec-aa74357e65ac', 0, '276,277', 0),
(20, 3, '715289615-05cfe39c-919a-4ded-94d7-662c80046090', 0, '276,277', 0),
(21, 3, '715289615-25b68fb7-31bb-416f-a6d6-44282a9ff013', 0, '276,277,278', 0),
(22, 3, '715289615-fc9334d0-88c5-4f10-b142-c863f3454050', 0, '276,277,278', 0),
(23, 3, '715289615-0ab9a29a-0882-4113-b4e3-af2170da507a', 0, '276,277', 0),
(24, 3, '715289615-4c8b40ff-b044-444a-ba52-6e875ed4a2fe', 0, '276,278,277', 0),
(25, 3, '715289615-c6d3f41f-b8ef-446e-ac8d-f078dace1f3e', 0, '276', 0),
(26, 3, '715289615-ef681841-7f46-4d50-9dfb-5623a0eb821d', 0, '276', 0),
(27, 3, '715289615-6e691a6f-a2ba-4c97-8ec0-202c21ddf7fa', 0, '276', 0),
(28, 3, '715289615-51129fcf-01c9-4051-bd8a-bc147ef2bd6f', 0, '276', 1),
(29, 6, '715289615-2d4697af-d408-46ef-b330-b13ea7efe0c2', 0, '303,304,305,306,307,308,309,310,311,312,313', 1),
(30, 3, '715289615-5d65acfc-eda3-4e87-8ea0-a09cf0074d7a', 0, '14', 1),
(31, 3, '715289615-f4b2ed8f-7c00-4c13-abdd-ae991b457567', 0, '15,16,17', 0),
(32, 3, '715289615-882cf02b-3ebc-45e2-9d50-dbf6ac7f0e1c', 0, '15,16', 0),
(33, 3, '715289615-b23cebd4-0fa0-4368-96d5-952bfda9b6e5', 0, '15,16,17,18,19', 0),
(34, 3, '715289615-3589a171-b6d8-4fcb-8345-55b315086de6', 0, '15,16,17,18,19', 0),
(35, 3, '715289615-85eb46ff-7a53-4e54-aa36-d8476ec56e1c', 2, '15,16,17,18,19', 0),
(36, 3, '715289615-13a6ad9a-ab59-4d87-bac0-1c9723fb96a7', 2, '15,16,17,18,19', 1),
(37, 7, '715289615-e520d143-f8fc-45bc-9f75-3226c13735b6', 3, '27', 1),
(38, 7, '715289615-36f54c4b-79f5-4c18-870d-c64617334ef9', 3, '28', 1),
(39, 8, '715289615-2325d807-1c43-474d-ba08-f644adb13176', 4, '31,32,32', 0),
(40, 8, '715289615-c2bf6314-e7c6-4950-abdb-72198f41b83e', 4, '31,32,33,32', 1),
(41, 3, '715289615-424213ee-fa43-4171-ad99-9e1558a7cb20', 2, '20', 0),
(42, 3, '715289615-b9e44aba-71f1-4bca-ad35-de28f3932994', 2, '20,21,22,23,24', 1),
(43, 9, '715289615-9480c232-3c3c-4fcc-9884-459ae07f3d65', 5, '42,43,44', 1);

--
-- Disparadores `pagos`
--
DELIMITER $$
CREATE TRIGGER `actualizarDet` AFTER UPDATE ON `pagos` FOR EACH ROW BEGIN
IF old.estado = 0 and new.estado=1 THEN
	set @i = 1;
    set @det_ids = (SELECT `cronogramapagodet_ids` from  `pagos` where `preference_id` = old.preference_id);
    UPDATE cronogramapago_cab set estado = 1 where id = new.cronogramapagocab_id;
   	bucle : LOOP
    	set @det_id = SPLIT_STRING(@det_ids,",",@i);
    	IF @det_id != '' THEN
            UPDATE `cronogramapago_det` SET `pago`= `cargo`,`saldo`=0 ,`estado`=1,`fecha_pago`=CURRENT_DATE() WHERE id=@det_id;
            set @i=@i+1;
        ITERATE bucle;
        END IF;
        LEAVE bucle;
    END LOOP bucle;  
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` bigint(20) NOT NULL,
  `tipo_documento` char(3) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'D',
  `num_documento` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verclave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tocken` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_usuario` enum('s','n') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 's=superadmin\r\nn: normal',
  `telefono` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ubigeo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) DEFAULT NULL,
  `fecha_insert` datetime DEFAULT current_timestamp(),
  `fecha_update` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `tipo_documento`, `num_documento`, `apellido_1`, `apellido_2`, `nombres`, `usuario`, `email`, `clave`, `verclave`, `tocken`, `foto`, `genero`, `fecha_nacimiento`, `estado_civil`, `tipo_usuario`, `telefono`, `ubigeo`, `direccion`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'D', '14253645', 'Abel', 'Chingo', 'Tello', 'abelchingo', 'abelchingo@gmail.com', 'abf333f2019d70ac7da2ccbcb08a489d', 'abelchingo@', 'dc51d5fbcbf47927901cf3cc11ee2f28', 'static/media/E_1/persona/U1/foto.png', 'M', '2013-07-01', 'S', 's', '942171837', '010101', 'los lirios Mz. D Lote 6', 1, 1, '2020-08-08 20:55:39', '2020-10-12 19:33:05'),
(2, 'D', '11111111', 'asd', 'asd', 'adasd', 'armflo', 'sdsd', '4d186321c1a7f0f354b297e8914ab240', 'hola', 'asd', 'asd', 'm', '2021-01-01', 's', 'n', NULL, NULL, NULL, 1, NULL, '2021-01-29 20:20:01', '2021-02-08 21:46:30'),
(3, '5', '77678383', 'deza', NULL, 'david', '77678383', 'ddezav@gmail.com', '85afa1151e61411e5f38842c4ed3e6e6', '77678383', NULL, NULL, NULL, NULL, NULL, 'n', '321654987', NULL, 'direccion direccion', 1, NULL, '2021-02-10 18:31:15', '2021-02-10 18:31:15'),
(4, '5', '74185296', 'Suarez', NULL, 'Pepito', '74185296', 'suarez@gmail.com', '9aa4a2e7edefb8022bb2680c4a03353e', '74185296', NULL, NULL, NULL, NULL, NULL, 'n', '999999999', NULL, 'direccion 123', 1, NULL, '2021-02-10 19:19:38', '2021-02-10 19:19:38'),
(5, '5', '44445555', 'Cardenas', NULL, 'Pepito', '44445555', 'pepito@gmail.com', 'ced944c3bf7bb99e84ccf35249b10cd1', '44445555', NULL, NULL, NULL, NULL, NULL, 'n', '222222222', NULL, 'Av Jesus 2407', 1, NULL, '2021-02-10 19:25:23', '2021-02-10 19:25:23'),
(6, '50', '98765432', 'perez', NULL, 'juan', '98765432', 'ddezav@gmail.com', 'bac76b0feb747e3bde11269cf367c97b', '98765432', NULL, NULL, NULL, NULL, NULL, 'n', '9876543211', NULL, 'calle san martin', 1, NULL, '2021-02-16 12:53:21', '2021-02-16 12:53:21'),
(7, '50', '77777777', 'basadre', NULL, 'diego', '77777777', 'ddezav@gmail.com', '30e535568de1f9231e7d9df0f4a5a44d', '77777777', NULL, NULL, NULL, NULL, NULL, 'n', '888888888', NULL, 'calle san  miguel', 1, NULL, '2021-02-16 14:31:12', '2021-02-16 14:31:12'),
(8, '50', '66666666', 'galardon', NULL, 'david', '66666666', 'ddezav@gmail.com', '7c497868c9e6d3e4cf2e87396372cd3b', '66666666', NULL, NULL, NULL, NULL, NULL, 'n', '321654878', NULL, 'calle piedra', 1, NULL, '2021-02-18 20:41:08', '2021-02-18 20:41:08'),
(9, '50', '99999999', 'veliz', NULL, 'teresa', '99999999', 'ddezav@gmail.com', 'ef775988943825d2871e1cfa75473ec0', '99999999', NULL, NULL, NULL, NULL, NULL, 'n', '6549819', NULL, 'calle ugarte', 1, NULL, '2021-02-22 17:39:51', '2021-02-22 17:39:51'),
(10, '50', '165165165', 'DD', NULL, 'DD', '165165165', 'ddesa@asd.com', 'a72b14d1178199878c77f52ae35e3498', '165165165', NULL, NULL, NULL, NULL, NULL, 'n', '561651', NULL, 'asdffsdafd', 1, NULL, '2021-03-16 16:14:21', '2021-03-16 16:14:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_rol`
--

CREATE TABLE `persona_rol` (
  `idpersonarol` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona_rol`
--

INSERT INTO `persona_rol` (`idpersonarol`, `idempresa`, `idpersona`, `idrol`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 1, 1, '2020-08-09 11:25:51', '2020-08-09 11:25:51'),
(2, 2, 1, 2, 1, '2020-12-01 23:56:51', '2020-12-01 23:56:51'),
(3, 2, 2, 4, 1, '2021-01-29 20:20:46', '2021-02-08 21:47:08'),
(4, 2, 3, 4, 1, '2021-02-10 18:31:15', '2021-02-10 18:31:15'),
(5, 2, 4, 4, 1, '2021-02-10 19:19:38', '2021-02-10 19:19:38'),
(6, 2, 5, 4, 1, '2021-02-10 19:25:23', '2021-02-10 19:25:23'),
(7, 2, 6, 4, 1, '2021-02-16 12:53:21', '2021-02-16 12:53:21'),
(8, 2, 7, 4, 1, '2021-02-16 14:31:12', '2021-02-16 14:31:12'),
(9, 2, 8, 4, 1, '2021-02-18 20:41:08', '2021-02-18 20:41:08'),
(10, 2, 9, 4, 1, '2021-02-22 17:39:51', '2021-02-22 17:39:51'),
(11, 2, 10, 4, 1, '2021-03-16 16:14:22', '2021-03-16 16:14:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_bien`
--

CREATE TABLE `registro_bien` (
  `rebi_id` int(11) NOT NULL,
  `rebi_modalidad` int(11) NOT NULL,
  `rebi_tipo_bien` int(11) NOT NULL,
  `casb_codigo` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rebi_fadquisicion` datetime NOT NULL DEFAULT current_timestamp(),
  `rebi_vadquisicion` float(10,2) NOT NULL,
  `rebi_ult_dep_acum` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Es la dirección del bien',
  `rebi_ult_val_act` float(10,2) NOT NULL,
  `tabl_estado` tinyint(4) NOT NULL DEFAULT 1,
  `rebi_direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rebi_detalles` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depe_id_ubica` int(11) NOT NULL,
  `tabl_condicion` tinyint(4) NOT NULL DEFAULT 1,
  `rebi_reg_publicos` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rebi_area` float(10,2) DEFAULT NULL,
  `rebi_observaciones` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rebi_fregistro` datetime NOT NULL DEFAULT current_timestamp(),
  `depe_id` int(11) NOT NULL,
  `rebi_anno` int(11) DEFAULT NULL,
  `rebi_alquiler` float(4,0) NOT NULL,
  `tabl_tipo_interior` int(11) DEFAULT NULL,
  `rebi_numero_interior` int(11) DEFAULT NULL,
  `rebi_foto_principal` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'no-foto.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `registro_bien`
--

INSERT INTO `registro_bien` (`rebi_id`, `rebi_modalidad`, `rebi_tipo_bien`, `casb_codigo`, `rebi_fadquisicion`, `rebi_vadquisicion`, `rebi_ult_dep_acum`, `rebi_ult_val_act`, `tabl_estado`, `rebi_direccion`, `rebi_detalles`, `depe_id_ubica`, `tabl_condicion`, `rebi_reg_publicos`, `rebi_area`, `rebi_observaciones`, `rebi_fregistro`, `depe_id`, `rebi_anno`, `rebi_alquiler`, `tabl_tipo_interior`, `rebi_numero_interior`, `rebi_foto_principal`) VALUES
(1, 0, 0, '90000000', '0000-00-00 00:00:00', 1.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 0, '', 'CEMENTERIO EL CARMEN DE CHICLAYO', 0, 0, '11009248', 60000.00, '', '0000-00-00 00:00:00', 0, 1997, 0, 639, 25, 'foto-muestra.jpg'),
(2285, 0, 0, '97000001', '0000-00-00 00:00:00', 14612.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 2.00, 0, '', 'CEMENTERIO \"EL CARMEN\" - CHICLAYO       ', 0, 0, '11009184', 60000.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 0, 1997, 0, 639, 7, 'foto-muestra.jpg'),
(2286, 3, 2, '98000001', '0000-00-00 00:00:00', 296332.53, 'ELIAS AGUIRRE N°288 - DPTO 25', 396900.00, 1, '', 'PARTE FUNDO RUST.LA PAMPA,PATAZCA,MOLINO', 69, 1, '2198803', 8100.00, 'TERRENO EN LITIGIO                 ', '0000-00-00 00:00:00', 2, 1945, 0, 639, NULL, 'foto-muestra.jpg'),
(2287, 3, 2, '97000005', '0000-00-00 00:00:00', 1480920.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 3449092.75, 1, '', 'ALDEAS INFANTILES - PASEO DEL DEPORTE   ', 69, 1, '11052993', 17220.00, 'PARTE FUNDO RUST.LA PAMPA,PATAZCA, ', '0000-00-00 00:00:00', 2, 1945, 0, 639, NULL, 'foto-muestra.jpg'),
(2288, 3, 2, '97000005', '0000-00-00 00:00:00', 48659.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 53027.54, 1, '', 'CASA HABITACIÓN TERMINADO               ', 69, 1, '2212350', 247.00, 'AREA CONSTRUIDA:49.78M2            ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2289, 3, 2, '97000005', '0000-00-00 00:00:00', 16617.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 47492.53, 1, '', 'CASA HABITACIÓN TERMINADO               ', 69, 1, '11155768', 144.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2290, 3, 2, '97000005', '0000-00-00 00:00:00', 25300.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 67552.25, 1, '', 'CASA HABITACIÓN TERMINADO               ', 69, 1, '11155763', 220.00, '                                   ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2720, 3, 2, '97000005', '0000-00-00 00:00:00', 419680.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 1254336.62, 1, '', 'TERRENO - MINSA (SERVICIOS TERMINADO)   ', 69, 1, '11045791', 4880.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 639, NULL, 'foto-muestra.jpg'),
(2721, 3, 2, '98000001', '0000-00-00 00:00:00', 405748.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 405748.00, 1, '', NULL, 69, 1, '11045797', 4718.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2722, 3, 2, '97000004', '0000-00-00 00:00:00', 16318.64, 'ELIAS AGUIRRE N°288 - DPTO 25', 28732.39, 1, '', 'STANDS', 70, 1, '11263289', 48.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 49, 'foto-muestra.jpg'),
(2723, 3, 2, '97000004', '0000-00-00 00:00:00', 6643.98, 'ELIAS AGUIRRE N°288 - DPTO 25', 6643.98, 1, '', 'STAND 55', 70, 1, '11263289', 24.23, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 55, 'foto-muestra.jpg'),
(2724, 3, 2, '97000004', '0000-00-00 00:00:00', 3893.76, 'ELIAS AGUIRRE N°288 - DPTO 25', 7089.94, 1, '', 'STAND 59', 70, 1, '11263289', 34.91, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, 59, 'foto-muestra.jpg'),
(2725, 3, 2, '97000004', '0000-00-00 00:00:00', 4688.06, 'ELIAS AGUIRRE N°288 - DPTO 25', 8254.31, 1, '', 'STAND 47-48-48A', 70, 1, '11263289', 37.79, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 47, 'foto-muestra.jpg'),
(2726, 3, 2, '97000004', '0000-00-00 00:00:00', 8404.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 12377.30, 1, '', 'STAND', 70, 1, '11263289', 15.45, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 69, 'foto-muestra.jpg'),
(2727, 3, 2, '97000004', '0000-00-00 00:00:00', 15322.30, 'ELIAS AGUIRRE N°288 - DPTO 25', 32548.79, 1, '', 'STAND 03', 70, 1, '11263289', 59.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, 3, 'foto-muestra.jpg'),
(2728, 3, 2, '97000004', '0000-00-00 00:00:00', 6105.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 12497.96, 1, '', 'STAND 34-36', 70, 1, '11263289', 24.13, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 34, 'foto-muestra.jpg'),
(2729, 3, 2, '97000004', '0000-00-00 00:00:00', 8913.06, 'ELIAS AGUIRRE N°288 - DPTO 25', 15693.31, 1, '', 'STANDS 46-46A-47A', 70, 1, '11263289', 33.23, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 46, 'foto-muestra.jpg'),
(2730, 3, 2, '97000004', '0000-00-00 00:00:00', 13229.32, 'ELIAS AGUIRRE N°288 - DPTO 25', 23293.00, 1, '', 'STAND 43-44', 70, 1, '11263289', 25.69, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, NULL, 'foto-muestra.jpg'),
(2731, 3, 2, '97000004', '0000-00-00 00:00:00', 8913.06, 'ELIAS AGUIRRE N°288 - DPTO 25', 15693.31, 1, '', 'STAND 41-42', 70, 1, '11263289', 21.33, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 41, 'foto-muestra.jpg'),
(2732, 3, 2, '97000004', '0000-00-00 00:00:00', 3893.76, 'ELIAS AGUIRRE N°288 - DPTO 25', 7089.94, 1, '', 'STAND 39-40', 70, 1, '11263289', 21.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 39, 'foto-muestra.jpg'),
(2733, 3, 2, '97000004', '0000-00-00 00:00:00', 9826.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 20113.90, 1, '', 'STAND 33-35', 70, 1, '11263289', 24.17, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 33, 'foto-muestra.jpg'),
(2734, 3, 2, '97000004', '0000-00-00 00:00:00', 10176.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 21343.57, 1, '', NULL, 70, 1, '11263289', 25.89, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 29, 'foto-muestra.jpg'),
(2735, 3, 2, '97000004', '0000-00-00 00:00:00', 6667.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 13647.94, 1, '', 'STAND 27', 70, 1, '11263289', 26.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 27, 'foto-muestra.jpg'),
(2736, 3, 2, '97000004', '0000-00-00 00:00:00', 14671.68, 'ELIAS AGUIRRE N°288 - DPTO 25', 21606.21, 1, '', 'STAND', 70, 1, '11263289', 26.97, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 70, 'foto-muestra.jpg'),
(2737, 3, 2, '97000004', '0000-00-00 00:00:00', 25214.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 37131.91, 1, '', 'STAND 66-67', 70, 1, '11263289', 30.67, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 66, 'foto-muestra.jpg'),
(2738, 3, 2, '97000004', '0000-00-00 00:00:00', 8323.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 12257.14, 1, '', 'STAND 65', 70, 1, '11263289', 16.61, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 65, 'foto-muestra.jpg'),
(2739, 3, 2, '97000004', '0000-00-00 00:00:00', 2968.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 6304.85, 1, '', 'STAND 05', 70, 1, '11263289', 12.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 5, 'foto-muestra.jpg'),
(2740, 3, 2, '97000004', '0000-00-00 00:00:00', 12428.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 23781.58, 1, '', 'STAND 02', 70, 1, '11263289', 46.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 2, 'foto-muestra.jpg'),
(2741, 3, 2, '97000004', '0000-00-00 00:00:00', 9839.18, 'ELIAS AGUIRRE N°288 - DPTO 25', 17323.94, 1, '', 'STAND 76-77', 70, 1, '11263289', 27.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 76, 'foto-muestra.jpg'),
(2742, 3, 2, '97000004', '0000-00-00 00:00:00', 3088.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 10725.26, 1, '', 'STAND 82-83', 70, 1, '11263289', 36.81, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 82, 'foto-muestra.jpg'),
(2743, 3, 2, '97000004', '0000-00-00 00:00:00', 3246.25, 'ELIAS AGUIRRE N°288 - DPTO 25', 6895.93, 1, '', 'STAND 26', 70, 1, '11263289', 12.93, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 26, 'foto-muestra.jpg'),
(2744, 3, 2, '97000004', '0000-00-00 00:00:00', 3246.25, 'ELIAS AGUIRRE N°288 - DPTO 25', 6644.96, 1, '', 'STAND 24', 70, 1, '11263289', 13.46, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 24, 'foto-muestra.jpg'),
(2745, 3, 2, '97000004', '0000-00-00 00:00:00', 8335.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 16370.37, 1, '', 'STAND 18-20', 70, 1, '11263289', 26.09, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 18, 'foto-muestra.jpg'),
(2746, 3, 2, '97000004', '0000-00-00 00:00:00', 482.56, 'ELIAS AGUIRRE N°288 - DPTO 25', 4253.05, 1, '', 'STAND 79', 70, 1, '11263289', 5.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 79, 'foto-muestra.jpg'),
(2747, 3, 2, '97000004', '0000-00-00 00:00:00', 2962.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 6144.05, 1, '', 'STAND 32', 70, 1, '11263289', 12.68, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 32, 'foto-muestra.jpg'),
(2748, 3, 2, '97000004', '0000-00-00 00:00:00', 5961.28, 'ELIAS AGUIRRE N°288 - DPTO 25', 24638.60, 1, '', 'STAND 01', 70, 1, '11263289', 53.99, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 1, 'foto-muestra.jpg'),
(2749, 3, 2, '97000004', '0000-00-00 00:00:00', 4340.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 9001.74, 1, '', 'STAND 61', 70, 1, '11263289', 16.90, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 61, 'foto-muestra.jpg'),
(2750, 3, 2, '97000004', '0000-00-00 00:00:00', 3907.28, 'ELIAS AGUIRRE N°288 - DPTO 25', 7114.56, 1, '', 'STAND 15', 70, 1, '11263289', 12.86, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 15, 'foto-muestra.jpg'),
(2751, 3, 2, '97000004', '0000-00-00 00:00:00', 3619.90, 'ELIAS AGUIRRE N°288 - DPTO 25', 7409.81, 1, '', 'STAND 31', 70, 1, '11263289', 14.36, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 31, 'foto-muestra.jpg'),
(2752, 3, 2, '97000004', '0000-00-00 00:00:00', 4377.10, 'ELIAS AGUIRRE N°288 - DPTO 25', 8235.33, 1, '', NULL, 70, 1, '11263289', 12.95, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 13, 'foto-muestra.jpg'),
(2753, 3, 2, '97000004', '0000-00-00 00:00:00', 8504.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 16000.07, 1, '', 'STAND 8-8A', 70, 1, '11263289', 26.80, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 8, 'foto-muestra.jpg'),
(2754, 3, 2, '97000004', '0000-00-00 00:00:00', 4377.10, 'ELIAS AGUIRRE N°288 - DPTO 25', 8062.11, 1, '', 'STAND 13-14-41E-41F', 70, 1, '11263289', 48.14, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 13, 'foto-muestra.jpg'),
(2755, 3, 2, '97000004', '0000-00-00 00:00:00', 2968.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 5985.22, 1, '', 'STAND 04', 70, 1, '11263289', 12.94, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 4, 'foto-muestra.jpg'),
(2756, 3, 2, '97000004', '0000-00-00 00:00:00', 15260.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 28108.45, 1, '', 'STAND 7-11A-11B', 70, 1, '11263289', 46.84, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 7, 'foto-muestra.jpg'),
(2757, 3, 2, '97000004', '0000-00-00 00:00:00', 3792.36, 'ELIAS AGUIRRE N°288 - DPTO 25', 6541.48, 1, '', 'STAND 09.', 70, 1, '11263289', 24.85, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 9, 'foto-muestra.jpg'),
(2758, 3, 2, '97000004', '0000-00-00 00:00:00', 3792.36, 'ELIAS AGUIRRE N°288 - DPTO 25', 6814.97, 1, '', 'STAND 9A', 70, 1, '11263289', 12.57, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 9, 'foto-muestra.jpg'),
(2759, 3, 2, '97000004', '0000-00-00 00:00:00', 12228.84, 'ELIAS AGUIRRE N°288 - DPTO 25', 21975.55, 1, '', 'STAND 10', 70, 1, '11263289', 25.06, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 10, 'foto-muestra.jpg'),
(2760, 3, 2, '97000004', '0000-00-00 00:00:00', 4252.04, 'ELIAS AGUIRRE N°288 - DPTO 25', 7383.32, 1, '', 'STAND 16.', 70, 1, '11263289', 13.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 16, 'foto-muestra.jpg'),
(2761, 3, 2, '97000004', '0000-00-00 00:00:00', 4252.04, 'ELIAS AGUIRRE N°288 - DPTO 25', 8182.88, 1, '', 'STAND 17', 70, 1, '11263289', 12.82, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 17, 'foto-muestra.jpg'),
(2762, 3, 2, '97000004', '0000-00-00 00:00:00', 2304.64, 'ELIAS AGUIRRE N°288 - DPTO 25', 8116.88, 1, '', 'STAND 19 ', 70, 1, '11263289', 12.86, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 19, 'foto-muestra.jpg'),
(2763, 3, 2, '97000004', '0000-00-00 00:00:00', 4502.16, 'ELIAS AGUIRRE N°288 - DPTO 25', 7817.63, 1, '', 'STAND 22', 70, 1, '11263289', 13.72, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 22, 'foto-muestra.jpg'),
(2764, 3, 2, '97000004', '0000-00-00 00:00:00', 7490.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 13302.32, 1, '', 'STAND 23', 70, 1, '11263289', 13.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 23, 'foto-muestra.jpg'),
(2765, 3, 2, '97000004', '0000-00-00 00:00:00', 35245.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 49810.85, 1, '', NULL, 70, 1, '11263289', 133.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2766, 3, 2, '97000004', '0000-00-00 00:00:00', 1404.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 5131.80, 1, '', 'STANDO 68', 70, 1, '11263289', 29.25, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 68, 'foto-muestra.jpg'),
(2767, 3, 2, '97000004', '0000-00-00 00:00:00', 6208.95, 'ELIAS AGUIRRE N°288 - DPTO 25', 12040.89, 1, '', 'STAND 58', 70, 1, '11263289', 23.43, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 58, 'foto-muestra.jpg'),
(2768, 3, 2, '97000004', '0000-00-00 00:00:00', 3445.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 6680.82, 1, '', 'STAND 74 ', 70, 1, '11263289', 11.95, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1999, 1, 624, 74, 'foto-muestra.jpg'),
(2769, 3, 2, '97000004', '0000-00-00 00:00:00', 11339.35, 'ELIAS AGUIRRE N°288 - DPTO 25', 23341.52, 1, '', 'STAND 72-73', 70, 1, '11263289', 26.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 72, 'foto-muestra.jpg'),
(2770, 3, 2, '97000004', '0000-00-00 00:00:00', 8109.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 15976.87, 1, '', 'STAND 63 - 64', 70, 1, '11263289', 29.79, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 63, 'foto-muestra.jpg'),
(2771, 3, 2, '97000004', '0000-00-00 00:00:00', 8215.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 16185.72, 1, '', 'STAND 60', 70, 1, '11263289', 35.95, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 60, 'foto-muestra.jpg'),
(2772, 0, 0, '97000002', '0000-00-00 00:00:00', 14612.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 24809.60, 0, '', 'COMERCIO', 0, 0, '11263289', 39.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 0, 2009, 0, 622, 123, 'foto-muestra.jpg'),
(2773, 0, 0, '97000002', '0000-00-00 00:00:00', 20776.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 46431.30, 0, '', '', 0, 0, '11263289', 78.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 0, 1997, 0, 621, 21, 'foto-muestra.jpg'),
(2774, 3, 2, '97000002', '0000-00-00 00:00:00', 11734.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 26224.21, 1, '', NULL, 60, 1, '11263289', 44.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 16, 'foto-muestra.jpg'),
(2775, 3, 2, '97000002', '0000-00-00 00:00:00', 11209.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 25051.58, 1, '', NULL, 60, 1, '11263289', 42.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 13, 'foto-muestra.jpg'),
(2776, 3, 2, '97000002', '0000-00-00 00:00:00', 18698.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 41788.17, 1, '', NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 28, 'foto-muestra.jpg'),
(2777, 3, 2, '97000002', '0000-00-00 00:00:00', 16761.25, 'ELIAS AGUIRRE N°288 - DPTO 25', 37458.93, 1, '', NULL, 60, 1, '11263289', 63.25, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 23, 'foto-muestra.jpg'),
(2778, 3, 2, '97000002', '0000-00-00 00:00:00', 8763.55, 'ELIAS AGUIRRE N°288 - DPTO 25', 19585.25, 1, '', NULL, 60, 1, '11263289', 33.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 10, 'foto-muestra.jpg'),
(2779, 0, 0, '97000002', '0000-00-00 00:00:00', 18698.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 41788.17, 0, '', '', 0, 0, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 0, 2000, 0, 621, 29, 'foto-muestra.jpg'),
(2780, 3, 2, '97000002', '0000-00-00 00:00:00', 1869.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 41788.17, 1, '', NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 2000, 1, 621, 27, 'foto-muestra.jpg'),
(2781, 3, 2, '97000002', '0000-00-00 00:00:00', 9412.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 21036.22, 1, '', NULL, 60, 1, '11263289', 35.52, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 9, 'foto-muestra.jpg'),
(2782, 3, 2, '97000002', '0000-00-00 00:00:00', 10883.55, 'ELIAS AGUIRRE N°288 - DPTO 25', 24323.13, 1, '', NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 15, 'foto-muestra.jpg'),
(2783, 3, 2, '97000002', '0000-00-00 00:00:00', 11050.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 24696.24, 1, '', NULL, 60, 1, '11263289', 41.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 17, 'foto-muestra.jpg'),
(2784, 3, 2, '97000002', '0000-00-00 00:00:00', 9693.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 21663.99, 1, '', NULL, 60, 1, '11263289', 36.58, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 12, 'foto-muestra.jpg'),
(2785, 3, 2, '97000002', '0000-00-00 00:00:00', 19663.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 41450.35, 1, '', NULL, 60, 1, '11263289', 74.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, 'foto-muestra.jpg'),
(2786, 3, 2, '97000002', '0000-00-00 00:00:00', 20776.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 47615.38, 1, '', NULL, 60, 1, '11263289', 78.40, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 8, 'foto-muestra.jpg'),
(2787, 3, 2, '97000002', '0000-00-00 00:00:00', 10883.55, 'ELIAS AGUIRRE N°288 - DPTO 25', 24323.13, 1, '', NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 20, 'foto-muestra.jpg'),
(2788, 3, 2, '97000002', '0000-00-00 00:00:00', 10883.55, 'ELIAS AGUIRRE N°288 - DPTO 25', 24323.13, 1, '', NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 19, 'foto-muestra.jpg'),
(2789, 3, 2, '97000002', '0000-00-00 00:00:00', 21472.95, 'ELIAS AGUIRRE N°288 - DPTO 25', 44317.41, 1, '', NULL, 60, 1, '11263289', 81.03, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2790, 3, 2, '97000002', '0000-00-00 00:00:00', 13393.10, 'ELIAS AGUIRRE N°288 - DPTO 25', 27641.64, 1, '', NULL, 60, 1, '11263289', 50.54, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, 'foto-muestra.jpg'),
(2791, 3, 2, '97000002', '0000-00-00 00:00:00', 11734.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 26551.45, 1, '', NULL, 60, 1, '11263289', 44.28, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 18, 'foto-muestra.jpg'),
(2792, 3, 2, '97000002', '0000-00-00 00:00:00', 10883.55, 'ELIAS AGUIRRE N°288 - DPTO 25', 22462.25, 1, '', NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 21, 'foto-muestra.jpg'),
(2793, 3, 2, '97000002', '0000-00-00 00:00:00', 15878.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 32771.81, 1, '', NULL, 60, 1, '11263289', 59.92, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 22, 'foto-muestra.jpg'),
(2794, 3, 2, '97000002', '0000-00-00 00:00:00', 18364.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 37901.97, 1, '', NULL, 60, 1, '11263289', 69.30, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 24, 'foto-muestra.jpg'),
(2795, 3, 2, '97000002', '0000-00-00 00:00:00', 18698.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 38591.10, 1, '', NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 29, 'foto-muestra.jpg'),
(2796, 3, 2, '97000002', '0000-00-00 00:00:00', 11315.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 25868.56, 1, '', NULL, 60, 1, '11263289', 42.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 14, 'foto-muestra.jpg'),
(2797, 3, 2, '97000002', '0000-00-00 00:00:00', 9693.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 19269.94, 1, '', NULL, 60, 1, '11263289', 36.58, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 11, 'foto-muestra.jpg'),
(2798, 3, 2, '97000005', '0000-00-00 00:00:00', 143765.16, 'ELIAS AGUIRRE N°288 - DPTO 25', 307634.97, 1, '', 'CENTRO MEDICO SALUD VIDA', 69, 1, '11263289', 542.51, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 6, 'foto-muestra.jpg'),
(2799, 3, 2, '97000005', '0000-00-00 00:00:00', 24610.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 70312.22, 1, '', 'CASA', 69, 1, 'P.ELECT.                      ', 214.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2800, 3, 2, '98000001', '0000-00-00 00:00:00', 40871.04, 'ELIAS AGUIRRE N°288 - DPTO 25', 40871.04, 1, '', NULL, 69, 1, '11000661', 425.74, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2801, 3, 2, '97000003', '0000-00-00 00:00:00', 817.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 3398.96, 1, '', 'COMERCIO', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 29, 'foto-muestra.jpg'),
(2802, 3, 2, '97000003', '0000-00-00 00:00:00', 1591.92, 'ELIAS AGUIRRE N°288 - DPTO 25', 6781.78, 1, '', 'COMERCIO', 76, 1, '10125537', 12.06, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 17, 'foto-muestra.jpg'),
(2803, 3, 2, '97000003', '0000-00-00 00:00:00', 33734.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 55104.75, 1, '', 'COMERCIO', 76, 1, '10125537', 69.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2804, 3, 2, '97000003', '0000-00-00 00:00:00', 64130.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 104732.91, 1, '', 'COMERCIO', 76, 1, '10125537', 132.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2805, 3, 2, '97000003', '0000-00-00 00:00:00', 10807.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 24545.55, 1, '', 'COMERCIO', 76, 1, '10125537', 22.33, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2806, 3, 2, '97000003', '0000-00-00 00:00:00', 27757.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 63170.46, 1, '', 'COMERCIO', 76, 1, '10125537', 57.35, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, 'foto-muestra.jpg'),
(2807, 3, 2, '97000003', '0000-00-00 00:00:00', 1606.44, 'ELIAS AGUIRRE N°288 - DPTO 25', 6683.97, 1, '', 'COMERCIO', 76, 1, '10125537', 12.17, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2808, 3, 2, '97000003', '0000-00-00 00:00:00', 1515.36, 'ELIAS AGUIRRE N°288 - DPTO 25', 6305.48, 1, '', 'COMERCIO', 76, 1, '10125537', 11.48, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 11, 'foto-muestra.jpg'),
(2809, 3, 2, '97000003', '0000-00-00 00:00:00', 984.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 4098.30, 1, '', 'COMERCIO', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 14, 'foto-muestra.jpg'),
(2810, 3, 2, '97000003', '0000-00-00 00:00:00', 984.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 4586.71, 1, '', 'CASA HABITACION', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 639, NULL, 'foto-muestra.jpg'),
(2811, 3, 2, '97000003', '0000-00-00 00:00:00', 3784.44, 'ELIAS AGUIRRE N°288 - DPTO 25', 16121.47, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 0.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 639, NULL, 'foto-muestra.jpg'),
(2812, 0, 0, '97000003', '0000-00-00 00:00:00', 1261.92, 'ELIAS AGUIRRE N°288 - DPTO 25', 5249.70, 0, '', 'COMERCIO                                ', 0, 0, '10125537', 9.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 0, 1977, 0, 621, 89, 'foto-muestra.jpg'),
(2813, 3, 2, '97000003', '0000-00-00 00:00:00', 984.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 4098.30, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 16, 'foto-muestra.jpg'),
(2814, 3, 2, '97000003', '0000-00-00 00:00:00', 894.96, 'ELIAS AGUIRRE N°288 - DPTO 25', 3721.13, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 6.78, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 21, 'foto-muestra.jpg'),
(2815, 3, 2, '97000003', '0000-00-00 00:00:00', 850.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 2897.37, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 6.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 5, 'foto-muestra.jpg'),
(2816, 3, 2, '97000003', '0000-00-00 00:00:00', 850.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 3537.34, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 6.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 4, 'foto-muestra.jpg'),
(2817, 3, 2, '97000003', '0000-00-00 00:00:00', 1515.36, 'ELIAS AGUIRRE N°288 - DPTO 25', 6458.08, 1, '', 'SERVICIOS                               ', 76, 1, '10125537', 11.48, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 621, 9, 'foto-muestra.jpg'),
(2818, 3, 2, '97000003', '0000-00-00 00:00:00', 1743.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 7252.36, 1, '', 'SERVICIOS                               ', 76, 1, '10125537', 13.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, 'foto-muestra.jpg'),
(2819, 3, 2, '97000003', '0000-00-00 00:00:00', 1458.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 6066.70, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 11.05, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 10, 'foto-muestra.jpg'),
(2820, 3, 2, '97000003', '0000-00-00 00:00:00', 1677.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 7149.31, 1, '', 'SERVICIOS                               ', 76, 1, '10125537', 12.71, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, 'foto-muestra.jpg'),
(2821, 3, 2, '97000003', '0000-00-00 00:00:00', 3247.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 13507.64, 1, '', 'SERVICIOS                               ', 76, 1, '10125537', 24.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, 'foto-muestra.jpg'),
(2822, 3, 2, '97000003', '0000-00-00 00:00:00', 4775.76, 'ELIAS AGUIRRE N°288 - DPTO 25', 20345.33, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 36.18, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, 'foto-muestra.jpg'),
(2823, 3, 2, '97000003', '0000-00-00 00:00:00', 817.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 3398.96, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 27, 'foto-muestra.jpg'),
(2824, 3, 2, '97000003', '0000-00-00 00:00:00', 817.08, 'ELIAS AGUIRRE N°288 - DPTO 25', 3398.96, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 28, 'foto-muestra.jpg'),
(2825, 3, 2, '97000003', '0000-00-00 00:00:00', 6616.28, 'ELIAS AGUIRRE N°288 - DPTO 25', 12316.52, 1, '', 'CASA HABITACION                         ', 76, 1, '10125537', 13.67, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 23, 'foto-muestra.jpg'),
(2826, 3, 2, '97000003', '0000-00-00 00:00:00', 1968.12, 'ELIAS AGUIRRE N°288 - DPTO 25', 8185.70, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 14.94, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 22, 'foto-muestra.jpg'),
(2827, 3, 2, '97000003', '0000-00-00 00:00:00', 1623.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 6753.82, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 12.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 24, 'foto-muestra.jpg'),
(2828, 3, 2, '97000003', '0000-00-00 00:00:00', 5691.84, 'ELIAS AGUIRRE N°288 - DPTO 25', 11366.36, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 11.76, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, 'foto-muestra.jpg'),
(2829, 3, 2, '97000003', '0000-00-00 00:00:00', 3032.04, 'ELIAS AGUIRRE N°288 - DPTO 25', 12612.28, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 22.97, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, 'foto-muestra.jpg'),
(2830, 3, 2, '97000003', '0000-00-00 00:00:00', 539660.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 642420.69, 1, '', 'TEATRO DOS DE MAYO                      ', 76, 1, '10125537', 1115.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2831, 3, 2, '97000005', '0000-00-00 00:00:00', 18265.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 25918.52, 1, '', 'COMERCIO                                ', 69, 1, '                              ', 49.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, 'foto-muestra.jpg'),
(2832, 3, 2, '97000005', '0000-00-00 00:00:00', 22878.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 45605.59, 1, '', 'COMERCIO                                ', 69, 1, '                              ', 62.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, NULL, NULL, 'foto-muestra.jpg'),
(2833, 3, 2, '97000005', '0000-00-00 00:00:00', 78450.24, 'ELIAS AGUIRRE N°288 - DPTO 25', 87457.02, 1, '', 'COMERCIO                                ', 69, 1, '10125899', 144.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2834, 3, 2, '97000005', '0000-00-00 00:00:00', 47490.30, 'ELIAS AGUIRRE N°288 - DPTO 25', 63598.26, 1, '', 'COMERCIO                                ', 69, 1, '11263289', 128.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2835, 3, 2, '97000005', '0000-00-00 00:00:00', 27783.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 35858.21, 1, '', 'SERVICIOS                               ', 69, 1, '11061926', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2836, 3, 2, '97000002', '0000-00-00 00:00:00', 6492.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 15532.83, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 24.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2837, 3, 2, '97000003', '0000-00-00 00:00:00', 7436.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 14258.88, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 22.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2838, 3, 2, '97000003', '0000-00-00 00:00:00', 3974.88, 'ELIAS AGUIRRE N°288 - DPTO 25', 4604.25, 1, '', 'COMERCIO                                ', 76, 1, '10125537', 11.76, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, 'foto-muestra.jpg'),
(2839, 3, 2, '97000005', '0000-00-00 00:00:00', 97812.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 122940.22, 1, '', 'COMERCIO                                ', 69, 1, '10125899', 247.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2840, 3, 2, '97000005', '0000-00-00 00:00:00', 28386.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 36619.39, 1, '', 'CASA HABITACION                         ', 69, 1, '11061924', 166.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2841, 3, 2, '97000005', '0000-00-00 00:00:00', 7128.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 18024.46, 1, '', 'COMERCIO                                ', 69, 1, '10125629', 18.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, 'foto-muestra.jpg'),
(2842, 3, 2, '97000005', '0000-00-00 00:00:00', 7920.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 8962.12, 1, '', 'COMERCIO                                ', 69, 1, '10125629', 20.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, 'foto-muestra.jpg'),
(2843, 3, 2, '97000005', '0000-00-00 00:00:00', 28488.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 36721.99, 1, '', 'SERVICIOS                               ', 69, 1, '11061921', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2844, 3, 2, '97000005', '0000-00-00 00:00:00', 4867.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 5552.22, 1, '', 'COMERCIO                                ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 1, 'foto-muestra.jpg'),
(2845, 3, 2, '97000005', '0000-00-00 00:00:00', 6906.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 11804.22, 1, '', 'SERVICIOS                               ', 69, 1, '10165934', 20.44, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 6, 'foto-muestra.jpg'),
(2846, 3, 2, '97000005', '0000-00-00 00:00:00', 6908.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 11804.22, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '10165934', 20.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 9, 'foto-muestra.jpg'),
(2847, 3, 2, '97000005', '0000-00-00 00:00:00', 5847.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 9990.85, 1, '', 'COMERCIO                                ', 69, 1, '10165934', 17.30, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 8, 'foto-muestra.jpg'),
(2848, 3, 2, '97000005', '0000-00-00 00:00:00', 17820.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 32921.01, 1, '', 'COMERCIO                                ', 69, 1, '10125866', 45.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2849, 3, 2, '97000005', '0000-00-00 00:00:00', 4867.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 5552.22, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 3, 'foto-muestra.jpg'),
(2850, 3, 2, '97000005', '0197-01-01 00:00:00', 67205.76, 'ELIAS AGUIRRE N°288 - DPTO 25', 74921.57, 1, '', 'COMERCIO                                ', 69, 1, '10125866', 123.54, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 197, 1, 622, NULL, 'foto-muestra.jpg'),
(2851, 3, 2, '97000005', '0000-00-00 00:00:00', 95087.52, 'ELIAS AGUIRRE N°288 - DPTO 25', 105569.83, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '10125869', 240.12, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2852, 3, 2, '97000005', '0000-00-00 00:00:00', 7480.10, 'ELIAS AGUIRRE N°288 - DPTO 25', 44781.12, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11009395', 57.10, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2853, 3, 2, '97000005', '0000-00-00 00:00:00', 28488.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 36721.99, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11061923', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2854, 3, 2, '97000005', '0000-00-00 00:00:00', 27783.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 35140.32, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11061928', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2855, 3, 2, '97000005', '0000-00-00 00:00:00', 27783.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 34787.86, 1, '', 'CASA HABITACION                         ', 69, 1, '11061927', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2856, 3, 2, '97000005', '0000-00-00 00:00:00', 92510.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 58631.20, 1, '', 'COMERCIO                                ', 69, 1, '10126377', 290.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 629, 136, 'foto-muestra.jpg'),
(2857, 3, 2, '97000002', '0000-00-00 00:00:00', 19663.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 38769.87, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 74.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, 'foto-muestra.jpg'),
(2858, 3, 2, '97000005', '0000-00-00 00:00:00', 8736.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 15735.84, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '2192719', 84.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2859, 3, 2, '97000002', '0000-00-00 00:00:00', 44722.80, 'ELIAS AGUIRRE N°288 - DPTO 25', 90034.87, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 121.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 628, NULL, 'foto-muestra.jpg'),
(2860, 3, 2, '97000005', '0000-00-00 00:00:00', 5900.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 7237.95, 1, '', 'COMERCIO                                ', 69, 1, '11263289', 25.00, 'LUIS GONZALES N° 645 - OF. A       ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, 'foto-muestra.jpg'),
(2861, 3, 2, '97000005', '0000-00-00 00:00:00', 7830.48, 'ELIAS AGUIRRE N°288 - DPTO 25', 9606.21, 1, '', 'COMERCIO                                ', 69, 1, '11263289', 33.18, 'LUIS GONZALES N° 645 - OF. A       ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, 'foto-muestra.jpg'),
(2862, 3, 2, '97000005', '0000-00-00 00:00:00', 14514.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 21268.98, 1, '', 'COMERCIO                                ', 69, 1, '10126378', 45.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, 'foto-muestra.jpg'),
(2863, 3, 2, '97000005', '0000-00-00 00:00:00', 5847.40, 'ELIAS AGUIRRE N°288 - DPTO 25', 9990.85, 1, '', 'CASA HABITACION                         ', 69, 1, '10165934', 17.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 5, 'foto-muestra.jpg'),
(2864, 3, 2, '97000002', '0000-00-00 00:00:00', 12793.23, 'ELIAS AGUIRRE N°288 - DPTO 25', 23701.04, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 34.67, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2865, 3, 2, '97000005', '0000-00-00 00:00:00', 11640.72, 'ELIAS AGUIRRE N°288 - DPTO 25', 13688.66, 1, '', 'COMERCIO                                ', 69, 1, '10165934', 34.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2866, 3, 2, '97000005', '0000-00-00 00:00:00', 4867.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 5637.86, 1, '', 'COMERCIO                                ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 1, 'foto-muestra.jpg'),
(2867, 3, 2, '97000005', '0000-00-00 00:00:00', 6854.64, 'ELIAS AGUIRRE N°288 - DPTO 25', 11711.82, 1, '', 'COMERCIO                                ', 69, 1, '10165934', 20.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 7, 'foto-muestra.jpg'),
(2868, 3, 2, '97000005', '0000-00-00 00:00:00', 32604.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 37330.20, 1, '', 'CASA HABITACIÓN                         ', 69, 1, 'P.ELECT. 02212350             ', 247.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2869, 3, 2, '97000005', '0000-00-00 00:00:00', 28488.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 36721.99, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11061920', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2870, 3, 2, '97000005', '0000-00-00 00:00:00', 28488.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 36721.99, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11061919', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2871, 3, 2, '97000005', '0000-00-00 00:00:00', 28488.60, 'ELIAS AGUIRRE N°288 - DPTO 25', 36721.99, 1, '', 'CASA HABITACIÓN                         ', 69, 1, '11061923', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2872, 3, 2, '97000002', '0000-00-00 00:00:00', 16704.63, 'ELIAS AGUIRRE N°288 - DPTO 25', 16528.85, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 45.27, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2873, 3, 2, '97000005', '0000-00-00 00:00:00', 15757.78, 'ELIAS AGUIRRE N°288 - DPTO 25', 20833.04, 1, '', 'COCHERA CARROZAS                        ', 69, 1, '2192722', 215.86, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(2874, 3, 2, '97000005', '0000-00-00 00:00:00', 3601013.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 4142760.75, 1, '', 'HOSPITAL REGIONAL DOCENTE \"LAS MERCEDES\"', 69, 1, '11263289', 15258.53, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 625, NULL, 'foto-muestra.jpg'),
(2875, 3, 2, '97000002', '0000-00-00 00:00:00', 15424.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 27208.71, 1, '', 'SERVICIOS                               ', 60, 1, '11263289', 41.80, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(2876, 3, 2, '97000005', '0000-00-00 00:00:00', 83475.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 285741.59, 1, '', 'MUTUAL CHICLAYO                         ', 69, 1, '11263289', 315.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, 'foto-muestra.jpg'),
(2877, 3, 2, '97000005', '0000-00-00 00:00:00', 27321.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 58975.50, 1, '', 'DEPARTAMENTO                      ', 69, 1, '11263289', 103.10, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 1, 'foto-muestra.jpg'),
(2878, 3, 2, '97000005', '0000-00-00 00:00:00', 369542.50, 'ELIAS AGUIRRE N°288 - DPTO 25', 1004739.19, 1, '', 'OFICINAS ADMINISTRATIVAS SBCH           ', 69, 1, '11263289', 1394.50, 'AUTOVALUO 2011.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, 'foto-muestra.jpg'),
(2879, 3, 2, '97000005', '0000-00-00 00:00:00', 71303.44, 'ELIAS AGUIRRE N°288 - DPTO 25', 91479.70, 1, '', 'OFICINAS ADMINISTRATIVAS SERFIN         ', 69, 1, '2184126', 327.08, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, 'foto-muestra.jpg'),
(2880, 3, 2, '97000005', '0000-00-00 00:00:00', 58440.20, 'ELIAS AGUIRRE N°288 - DPTO 25', 92723.09, 1, '', 'CENTRO EMERGENCIA MUJER                 ', 69, 1, '10165933', 172.90, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2881, 3, 2, '98000001', '0000-00-00 00:00:00', 97125.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 97125.00, 1, '', 'SBCH                                    ', 69, 1, 'P.ELECT. 11000661             ', 1312.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 2000, 0, NULL, NULL, 'foto-muestra.jpg'),
(2882, 3, 2, '97000005', '0000-00-00 00:00:00', 1388576.62, 'ELIAS AGUIRRE N°288 - DPTO 25', 7028058.00, 1, '', 'DOMEL                                   ', 69, 1, '11050317', 14464.34, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 1902, NULL, 'foto-muestra.jpg'),
(2883, 3, 2, '97000005', '0000-00-00 00:00:00', 11281.25, 'ELIAS AGUIRRE N°288 - DPTO 25', 52311.04, 1, '', 'CASA REFUGIO                            ', 69, 1, '10124737', 118.75, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, 'foto-muestra.jpg'),
(2884, 3, 2, '97000005', '0000-00-00 00:00:00', 32161.05, 'ELIAS AGUIRRE N°288 - DPTO 25', 106303.49, 1, '', 'ASILO DE ANCIANOS                       ', 69, 1, '10126244', 132.35, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, 'foto-muestra.jpg'),
(2885, 3, 2, '97000002', '0000-00-00 00:00:00', 1921.25, 'ELIAS AGUIRRE N°288 - DPTO 25', 4257.22, 1, '', 'COMERCIO                                ', 60, 1, '11263289', 7.25, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, 'foto-muestra.jpg'),
(4059, 3, 2, '97000005', '0000-00-00 00:00:00', 11174372.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 11174372.00, 2, '', 'FINCA URBANA HOSPITAL LAS MERCEDES', 19, 1, 'PLLA ELECTRONICA 11009997     ', 23431.24, '                                   ', '0000-00-00 00:00:00', 2, 1945, 0, NULL, NULL, 'foto-muestra.jpg'),
(4655, 1, 2, '97000002', '0000-00-00 00:00:00', 9693.70, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'AZOTEA DE EDIF. PIEDRA LORA', 19, 1, 'P.ELECT. 11009997', NULL, NULL, '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(4656, 1, 2, '97000004', '0000-00-00 00:00:00', 8215.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 6', 70, 1, 'P.ELECT. 11263289', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 6, 'foto-muestra.jpg'),
(4657, 1, 2, '97000004', '0000-00-00 00:00:00', 7800.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 21', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 1, 624, 21, 'foto-muestra.jpg'),
(4658, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 21 A', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 21, 'foto-muestra.jpg'),
(4659, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 45', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 45, 'foto-muestra.jpg'),
(4660, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND. 12-A / 3-A', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, NULL, 'foto-muestra.jpg'),
(4661, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-A', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 1, 624, 41, 'foto-muestra.jpg'),
(4662, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-B', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, 'foto-muestra.jpg'),
(4663, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-C', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, 'foto-muestra.jpg'),
(4664, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-D', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, 'foto-muestra.jpg'),
(4665, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-E', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, NULL, 'foto-muestra.jpg'),
(4666, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-F', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, 'foto-muestra.jpg'),
(4667, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 41-G', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, 'foto-muestra.jpg'),
(4668, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 39-40', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 39, 'foto-muestra.jpg'),
(4669, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 25', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 25, 'foto-muestra.jpg'),
(4670, 1, 2, '97000005', '0000-00-00 00:00:00', 25000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'CASA', 19, 1, '1', 50.00, NULL, '0000-00-00 00:00:00', 2, 2000, 0, 620, NULL, 'foto-muestra.jpg'),
(4671, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 12-B / 3-B', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 2000, 1, 624, 12, 'foto-muestra.jpg');
INSERT INTO `registro_bien` (`rebi_id`, `rebi_modalidad`, `rebi_tipo_bien`, `casb_codigo`, `rebi_fadquisicion`, `rebi_vadquisicion`, `rebi_ult_dep_acum`, `rebi_ult_val_act`, `tabl_estado`, `rebi_direccion`, `rebi_detalles`, `depe_id_ubica`, `tabl_condicion`, `rebi_reg_publicos`, `rebi_area`, `rebi_observaciones`, `rebi_fregistro`, `depe_id`, `rebi_anno`, `rebi_alquiler`, `tabl_tipo_interior`, `rebi_numero_interior`, `rebi_foto_principal`) VALUES
(4672, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 62', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2000, 0, 624, 62, 'foto-muestra.jpg'),
(4673, 1, 2, '97000003', '0000-00-00 00:00:00', 150000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'TIENDA', 19, 1, '1', 45.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 622, NULL, 'foto-muestra.jpg'),
(4674, 1, 2, '97000005', '0000-00-00 00:00:00', 80000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'PARTE EXTERNA ', 19, 1, '1', 150.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 1902, NULL, 'foto-muestra.jpg'),
(4675, 1, 2, '97000005', '0000-00-00 00:00:00', 15000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'OFICINA 8', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 623, 8, 'foto-muestra.jpg'),
(4676, 1, 2, '97000005', '0000-00-00 00:00:00', 25000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'OFICINA 4', 19, 1, '1', 60.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 623, 4, 'foto-muestra.jpg'),
(4677, 1, 2, '97000005', '0000-00-00 00:00:00', 25000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'OFICINA 2', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 623, 2, 'foto-muestra.jpg'),
(4678, 1, 2, '97000005', '0000-00-00 00:00:00', 25000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'TIENDA', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 622, NULL, 'foto-muestra.jpg'),
(4679, 1, 2, '97000004', '0000-00-00 00:00:00', 8000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'STAND 22-A', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 624, 22, 'foto-muestra.jpg'),
(4680, 1, 2, '97000003', '0000-00-00 00:00:00', 15000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'DEPARTAMENTO 41-42', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 621, NULL, 'foto-muestra.jpg'),
(4681, 1, 2, '97000005', '0000-00-00 00:00:00', 50000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'CASA', 19, 1, '1', 100.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, 'foto-muestra.jpg'),
(4682, 1, 2, '97000005', '0000-00-00 00:00:00', 50000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'CASA', 19, 1, '1', 100.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, 'foto-muestra.jpg'),
(4683, 1, 2, '97000003', '0000-00-00 00:00:00', 50000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'CASA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, 'foto-muestra.jpg'),
(4684, 1, 2, '97000003', '0000-00-00 00:00:00', 50000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'CASA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, 40, 'foto-muestra.jpg'),
(4685, 1, 2, '97000003', '0000-00-00 00:00:00', 80000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'TIENDA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 622, NULL, 'foto-muestra.jpg'),
(4686, 1, 2, '97000005', '0000-00-00 00:00:00', 80000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'OF. 2', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 623, 2, 'foto-muestra.jpg'),
(4689, 1, 2, '98000001', '0000-00-00 00:00:00', 2946726.75, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 4, '', 'CONTRATO COMPRA/VENTA TERRENO PREDIO RURAL CE-61603/CN 9245920, UBICACION RURAL PREDIO LAS PAMPAS/VALLE CHANCAY/LAMBAYEQUE', 0, 0, '', 0.00, '', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 'foto-muestra.jpg'),
(4690, 1, 2, '98000001', '0000-00-00 00:00:00', 989100.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 4, '', 'CONTRATO COMPRA/VENTA DE PREDIO RURAL A FAVOR DE BENEFICENCIA DE CHICLAYO, UBICADO PAMPAS DE PIMENTEL/VALLE CHANCAY/LAMBAYEQUE, ÁREA HA. 1.0000 UC-116499 PARTIDA 11183758', 2, 1, 'PARTIDA 11183758', 1.00, 'ESCRITURA PUBLICA N° 2603 DEL 19/07/2018, ANTE NOTARIO ANTONIO VERA MENDEZ -CHICLAYO, TITULO PRESENTADO EL 31/07/2018 BAJO EL N° 2018-01702419, TOMO DIARIO 0030', '0000-00-00 00:00:00', 2, 2018, 0, 639, NULL, 'foto-muestra.jpg'),
(4766, 1, 2, '97000005', '0000-00-00 00:00:00', 500.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 0.00, 1, '', 'ELIAS AGUIRRE N° 248 - AREA DE PARQUEO', 69, 1, '11263289', 20.00, NULL, '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, 'foto-muestra.jpg'),
(4767, 0, 0, '90000000', '0000-00-00 00:00:00', 0.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 50000.00, 0, '', '', 0, 0, 'reg', 1000.00, 'observ', '2021-01-28 19:33:28', 0, 0, 0, 0, 22, 'foto-muestra.jpg'),
(4768, 0, 0, '98000001', '2021-02-01 22:53:09', 10000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 13000.00, 1, '', 'estos son dtalles que estoy agregando de manera manual', 0, 1, '001354', 150.00, 'aqui agrego obserrvaciones del inmueble, no me estoy fijando en la otrografía', '2021-02-01 22:53:09', 0, 2005, 0, 1902, 0, 'foto-muestra.jpg'),
(4769, 0, 0, '97000003', '0000-00-00 00:00:00', 10.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 15.00, 0, '', 'detalles', 0, 0, '01234', 1000.00, 'observaiones', '2021-02-02 21:21:23', 0, 2013, 0, 630, 23, 'foto-muestra.jpg'),
(4770, 0, 0, '', '2021-02-04 18:29:02', 500.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 500.00, 1, '', 'INMUEBLE PRUEBA', 0, 1, '123456', 500.00, 'observaciones', '2021-02-04 18:29:02', 0, 2020, 0, 623, 15, 'foto-muestra.jpg'),
(4771, 0, 0, '', '2021-02-04 18:29:59', 500.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 500.00, 1, '', 'prueba', 0, 1, '123', 500.00, 'OBSR', '2021-02-04 18:29:59', 0, 2020, 0, 620, 123, 'foto-muestra.jpg'),
(4772, 0, 0, '98000001', '0000-00-00 00:00:00', 100.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 100.00, 0, '', 'terreno pueba', 0, 0, '123', 100.00, 'obs', '2021-02-04 18:39:03', 0, 2020, 0, 1902, 321, 'foto-muestra.jpg'),
(4773, 0, 0, '90000000', '0000-00-00 00:00:00', 20000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 25000.00, 0, '', 'CEMENTERIO PRUEBA', 0, 0, '666', 10000.00, 'CON OBSERVACIONES', '2021-02-04 21:49:16', 0, 2021, 0, 1902, 1, 'foto-muestra.jpg'),
(4774, 0, 0, '97000004', '2021-02-05 21:30:19', 20000.00, 'ELIAS AGUIRRE N°288 - DPTO 25', 220000.00, 1, '', 'GALERIA CENTRAL', 0, 1, '333', 3000.00, 'obs', '2021-02-05 21:30:19', 0, 2050, 0, 624, 25, 'foto-muestra.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `idempresa`, `nombre`, `imagen`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'Administrador', 'static/media/E_1/U1/rol/imagen20201004152420.png', 1, 1, '2020-08-09 11:24:39', '2020-12-01 23:53:48'),
(2, 2, 'Administrador', NULL, 1, 1, '2020-12-01 23:54:12', '2020-12-01 23:54:12'),
(3, 1, 'Cliente', NULL, 1, 1, '2021-01-29 20:19:11', '2021-01-29 20:19:11'),
(4, 2, 'Cliente', NULL, 1, 1, '2021-01-29 20:19:11', '2021-01-29 20:19:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sys_configuracion`
--

CREATE TABLE `sys_configuracion` (
  `config` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autocargar` enum('si','no') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sys_configuracion`
--

INSERT INTO `sys_configuracion` (`config`, `valor`, `autocargar`) VALUES
('multiidioma', 'SI', 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla`
--

CREATE TABLE `tabla` (
  `tabl_id` int(11) NOT NULL,
  `tabl_tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabl_codigo` int(11) DEFAULT NULL,
  `tabl_descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabl_fecharegistro` datetime NOT NULL DEFAULT current_timestamp(),
  `tabl_descripaux` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabl_codigoauxiliar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla`
--

INSERT INTO `tabla` (`tabl_id`, `tabl_tipo`, `tabl_codigo`, `tabl_descripcion`, `tabl_fecharegistro`, `tabl_descripaux`, `tabl_codigoauxiliar`) VALUES
(1, '4', 1, 'SOLTERO(A)', '2009-11-06 00:00:00', NULL, NULL),
(2, '4', 2, 'CASADO(A)', '2009-11-06 00:00:00', NULL, NULL),
(3, '4', 3, 'VIUDO(A)', '2009-11-06 00:00:00', NULL, NULL),
(4, '4', 4, 'DIVORCIADO(A)', '2009-11-06 00:00:00', NULL, NULL),
(5, '4', 5, 'CONVIVIENTE', '2006-08-14 00:00:00', NULL, NULL),
(14, 'DURACION_SOLICITUD_DONACION', 1, 'PERMANENTE', '2009-11-22 00:00:00', NULL, NULL),
(15, 'DURACION_SOLICITUD_DONACION', 2, 'VARIABLE', '2009-11-22 00:00:00', NULL, NULL),
(19, 'TIPO_INFRAESTRUCTURA', 1, 'PROGRAMAA SOCIALES', '2009-11-23 00:00:00', NULL, NULL),
(20, 'TIPO_INFRAESTRUCTURA', 2, 'EDIFICIOS HISTORICOS', '2009-11-23 00:00:00', NULL, NULL),
(26, '9', 1, 'NOMBRADO', '2009-11-06 00:00:00', NULL, NULL),
(27, '9', 2, 'CARG.DE CONFIANZA', '2009-11-06 00:00:00', 'CONFIANZA', NULL),
(29, '9', 4, 'CONTRAT.SERV.PERSONALES D.LEG.276', '2009-11-06 00:00:00', 'CO.SERV.PERS.', NULL),
(30, '9', 5, 'CONTRAT.SERVICIOS NO PERSONALES', '2009-11-06 00:00:00', 'CO.SERV.NO PERS.', NULL),
(31, '9', 8, 'MIEMBRO DE DIRECTORIO', '2009-11-06 00:00:00', 'DIRECTORIO', NULL),
(32, '9', 15, 'CONT.ADMINIST.DE SERVICIOS DEC.1057', '2009-11-16 00:00:00', 'CAS', NULL),
(35, '9', 35, 'INVITADO', '2009-11-17 00:00:00', NULL, NULL),
(36, '9', 9, 'PENSIONISTA', '2009-11-06 00:00:00', NULL, NULL),
(40, 'TIPO_PROGRAMACION', NULL, 'SEMANAL', '2009-11-17 00:00:00', NULL, NULL),
(41, 'TIPO_PROGRAMACION', NULL, 'MENSUAL', '2009-11-17 00:00:00', NULL, NULL),
(49, 'NACIONALIDAD', NULL, 'PERUANO', '2009-11-17 00:00:00', NULL, NULL),
(50, 'TIPO_PROVEEDOR', 1, 'PERSONA JURIDICA', '2007-01-17 00:00:00', 'PJ', NULL),
(51, 'TIPO_PROVEEDOR', 2, 'PERSONA NATURAL', '2007-01-17 00:00:00', 'PN', NULL),
(52, 'TIPO_PROVEEDOR', 3, 'AUXILIAR', '2015-03-14 00:00:00', 'PA', NULL),
(57, 'CARGO_ESTRUCTURAL', NULL, 'PRESIDENTE', '2009-11-28 00:00:00', NULL, NULL),
(58, 'CARGO_ESTRUCTURAL', NULL, 'GERENTE', '2009-11-28 00:00:00', NULL, NULL),
(59, 'CARGO_ESTRUCTURAL', NULL, 'DIRECTOR', '2009-11-28 00:00:00', NULL, NULL),
(60, 'CARGO_ESTRUCTURAL', NULL, 'SUB-GERENTE', '2009-11-28 00:00:00', NULL, NULL),
(61, 'CARGO_ESTRUCTURAL', NULL, 'JEFE UNIDAD', '2009-11-28 00:00:00', NULL, NULL),
(62, 'CARGO_ESTRUCTURAL', NULL, 'JEFE DE DIVISION', '2009-11-28 00:00:00', NULL, NULL),
(65, 'PROFESION', NULL, 'CONTADOR PUBLICO COLEGIADO', '2009-11-28 00:00:00', 'CPC', NULL),
(66, 'PROFESION', NULL, 'INGENIERO', '2009-11-28 00:00:00', 'Ing.', NULL),
(67, 'PROFESION', NULL, 'PROFESOR', '2009-11-28 00:00:00', 'Prof.', NULL),
(68, 'PROFESION', NULL, 'DOCTOR', '2009-11-28 00:00:00', 'Dr.', NULL),
(72, 'CARGO_ESTRUCTURAL', NULL, 'PROCURADOR ', '2009-11-28 00:00:00', NULL, NULL),
(73, '24', 1, 'ENERO', '2006-03-15 00:00:00', NULL, NULL),
(74, '24', 2, 'FEBRERO', '2006-03-15 00:00:00', NULL, NULL),
(75, '24', 3, 'MARZO', '2006-03-15 00:00:00', NULL, NULL),
(76, '24', 4, 'ABRIL', '2006-03-15 00:00:00', NULL, NULL),
(77, '24', 5, 'MAYO', '2006-03-15 00:00:00', NULL, NULL),
(78, '24', 6, 'JUNIO', '2006-03-15 00:00:00', NULL, NULL),
(79, '24', 7, 'JULIO', '2006-03-15 00:00:00', NULL, NULL),
(80, '24', 8, 'AGOSTO', '2006-03-15 00:00:00', NULL, NULL),
(81, '24', 9, 'SETIEMBRE', '2006-03-15 00:00:00', NULL, NULL),
(82, '24', 10, 'OCTUBRE', '2006-03-15 00:00:00', NULL, NULL),
(83, '24', 11, 'NOVIEMBRE', '2006-03-15 00:00:00', NULL, NULL),
(84, '24', 12, 'DICIEMBRE', '2006-03-15 00:00:00', NULL, NULL),
(86, 'CARGO_ESTRUCTURAL', NULL, 'DIRECTOR EJECUTIVO', '2009-11-29 00:00:00', NULL, NULL),
(87, 'PROFESION', NULL, 'LICENCIADO', '2009-11-29 00:00:00', 'Lic.', NULL),
(89, 'TEMAS_CONSULTAS', NULL, 'Servicios Funerarios', '2009-12-04 00:00:00', NULL, 92),
(90, 'TEMAS_CONSULTAS', NULL, 'Estudio Juridico Gratuito', '2009-12-04 00:00:00', NULL, 91),
(91, 'DEPENDENCIAS_ATENCION', NULL, 'ESTUDIO JURIDICO GRATUITO', '2009-12-05 00:00:00', NULL, NULL),
(92, 'DEPENDENCIAS_ATENCION', NULL, 'SERFIN', '2009-12-05 00:00:00', NULL, NULL),
(93, 'TIPO_USUARIO', 1, 'ADMINISTRACION GENERAL', '2008-11-04 00:00:00', NULL, NULL),
(94, '16', 1, 'PERSONA JURIDICA', '2006-03-28 00:00:00', 'PJ', NULL),
(95, '16', 2, 'PERSONA NATURAL', '2006-03-28 00:00:00', 'PN', NULL),
(96, '16', 3, 'AUXILIAR', '2006-03-28 00:00:00', 'AUX', NULL),
(97, 'TIPO_MONEDA', NULL, 'SOLES', '2015-05-23 00:00:00', NULL, NULL),
(98, 'TIPO_MONEDA', NULL, 'DOLARES', '2015-05-23 00:00:00', NULL, NULL),
(102, 'ESTADO_DESPACHO', 2, 'REGISTRADO', '2010-09-19 00:00:00', NULL, NULL),
(103, 'ESTADO_DESPACHO', 3, 'EN PROCESO', '2010-09-19 00:00:00', NULL, NULL),
(104, 'ESTADO_DESPACHO', 4, 'DERIVADO', '2010-09-19 00:00:00', NULL, NULL),
(106, 'ESTADO_DESPACHO', 6, 'ARCHIVADO', '2010-09-19 00:00:00', NULL, NULL),
(107, 'ESTADO_DESPACHO', 7, 'ACTIVADO', '2010-09-19 00:00:00', NULL, NULL),
(111, '9', 10, 'DESIGNADO', '2010-09-20 00:00:00', NULL, NULL),
(112, 'CARGO_ESTRUCTURAL', NULL, 'ABOGADO', '2010-10-13 00:00:00', NULL, NULL),
(113, 'CARGO_ESTRUCTURAL', NULL, 'ENCARGADO', '2010-10-14 00:00:00', NULL, NULL),
(114, 'CARGO_ESTRUCTURAL', NULL, 'SECRETARIA DE OFICINA', '2013-05-14 00:00:00', NULL, NULL),
(115, 'CARGO_ESTRUCTURAL', NULL, 'APOYO ADMINISTRATIVO', '2013-05-14 00:00:00', NULL, NULL),
(116, '9', 11, 'CONTRATO D.LEG. 728', '2013-05-14 00:00:00', NULL, NULL),
(117, 'CARGO_ESTRUCTURAL', NULL, 'NUTRICIONISTA', '2013-05-14 00:00:00', NULL, NULL),
(118, 'CARGO_ESTRUCTURAL', NULL, 'RECAUDADOR', '2013-05-14 00:00:00', NULL, NULL),
(119, 'CARGO_ESTRUCTURAL', NULL, 'JEFE DE OFICINA', '2013-05-14 00:00:00', NULL, NULL),
(120, 'CARGO_ESTRUCTURAL', NULL, 'ADMINISTRADOR', '2013-05-14 00:00:00', NULL, NULL),
(130, 'PERIODO_PRIORIDAD', NULL, 'DIAS ORDINARIOS', '2010-07-16 00:00:00', NULL, NULL),
(131, 'PERIODO_PRIORIDAD', NULL, 'DIAS HABILES', '2010-07-19 00:00:00', NULL, NULL),
(134, 'TDENT_FARMACIA', NULL, 'NORTFARMA', '2013-09-13 00:00:00', NULL, NULL),
(140, 'TIPO_DESPACHO', NULL, 'INSTITUCIONAL', '2010-07-16 00:00:00', NULL, NULL),
(141, 'TIPO_DESPACHO', NULL, 'PERSONAL', '2010-07-16 00:00:00', NULL, NULL),
(142, 'TIPO_DESPACHO', NULL, 'OTRAS ENTIDADES', '2010-07-16 00:00:00', NULL, NULL),
(145, 'MODO_RECEPCION', NULL, 'DIRECTA', '2010-07-19 00:00:00', NULL, NULL),
(146, 'MODO_RECEPCION', NULL, 'FAX', '2010-07-19 00:00:00', NULL, NULL),
(147, 'MODO_RECEPCION', NULL, 'CORREO ELECTRONICO', '2010-07-19 00:00:00', NULL, NULL),
(150, 'TIPOS_DE_PENSION', NULL, 'JUBILACION', '2010-07-28 00:00:00', NULL, NULL),
(151, 'TIPOS_DE_PENSION', NULL, 'VIUDEZ', '2010-07-28 00:00:00', NULL, NULL),
(154, 'CARGO_ESTRUCTURAL', NULL, 'PSICOLOGO', '2014-02-10 00:00:00', NULL, NULL),
(156, 'CARGO_ESTRUCTURAL', NULL, 'COORDINADORA', '2014-04-03 00:00:00', NULL, NULL),
(157, 'VISTA_COMPONENTE', NULL, 'FRENTE', '2014-03-08 00:00:00', NULL, NULL),
(159, 'VISTA_COMPONENTE', NULL, 'PLANTA', '2014-03-17 00:00:00', NULL, NULL),
(160, 'VISTA_COMPONENTE', NULL, 'ANTIGUO', '2014-03-08 00:00:00', NULL, NULL),
(161, 'REFERENCIA_COMPONENTE', NULL, 'ZONA ANTIGUA - LADO DERECHO', '2014-03-24 00:00:00', 'Z.ANT.L.DER.', NULL),
(162, 'REFERENCIA_COMPONENTE', NULL, 'ZONA ANTIGUA - LADO IZQUIERDO', '2014-03-24 00:00:00', 'Z.ANT.L.IZQ.', NULL),
(163, 'REFERENCIA_COMPONENTE', NULL, 'ZONA AMPLIAC.- LADO DERECHO', '2014-03-24 00:00:00', 'Z.AMP.L.DER.', NULL),
(164, 'REFERENCIA_COMPONENTE', NULL, 'ZONA AMPLIAC.- LADO IZQUIERDO', '2014-03-24 00:00:00', 'Z.AMP.L.IZQ.', NULL),
(165, 'TIPO_DOC_IDENTIDAD', 1, 'DNI', '2006-12-05 00:00:00', NULL, NULL),
(166, 'REFERENCIA_COMPONENTE', NULL, 'AV.PRINCIPAL', '2014-03-24 00:00:00', 'AV.PRINCIPAL', NULL),
(167, 'TIPO_DOC_IDENTIDAD', 18, 'DOC.PROVISIONAL DE IDENTIDAD', '2006-12-05 00:00:00', NULL, NULL),
(168, 'REFERENCIA_COMPONENTE', NULL, 'AV.INTERMEDIA', '2014-03-24 00:00:00', 'AV.INTERMEDIA', NULL),
(170, 'TDING_RECAUDACION', NULL, 'RECIBO', '2007-01-17 00:00:00', 'RECI', NULL),
(171, 'TDING_RECAUDACION', NULL, 'DEVOLUCION', '2007-01-17 00:00:00', 'DEV', NULL),
(175, 'TIPO_DOC_IDENTIDAD', 8, 'SIN DNI', '2007-01-17 00:00:00', NULL, NULL),
(178, 'TIPO_DOC_IDENTIDAD', 4, 'CARNET DE EXTRANJERIA', '2007-01-17 00:00:00', NULL, NULL),
(180, 'TDENT_FARMACIA', NULL, 'INVENTARIO INICIAL', '2007-01-17 00:00:00', 'INV.I', NULL),
(181, 'TDENT_FARMACIA', NULL, 'FACTURA', '2007-01-17 00:00:00', 'FACT', NULL),
(182, 'TDENT_FARMACIA', NULL, 'AJUSTE DE INVENTARIO', '2007-01-17 00:00:00', 'AJU.I', NULL),
(184, 'TDING_FARMACIA', NULL, 'BOLETA DE VENTA', '2016-06-14 00:00:00', 'B/V', 1),
(185, 'TDING_FARMACIA', NULL, 'RECIBO', '2007-01-17 00:00:00', 'RECI', 1),
(186, 'TDING_FARMACIA', NULL, 'ORDEN DE CREDITO', '2007-01-17 00:00:00', 'O/C', 2),
(187, 'TDING_FARMACIA', NULL, 'GUIA DE REMISION', '2007-01-17 00:00:00', 'G/R', 3),
(188, 'TDING_FARMACIA', NULL, 'AJUSTE DE INVENTARIO', '2007-01-17 00:00:00', 'AJU.I', 4),
(190, 'TDING_TESORERIA', NULL, 'RECIBO', '2007-01-17 00:00:00', 'RECI', 1),
(191, 'TDING_TESORERIA', NULL, 'BOLETA DE VENTA', '2007-01-17 00:00:00', 'B/V', 1),
(192, 'TDING_TESORERIA', NULL, 'FACTURA', '2007-01-17 00:00:00', 'FACT', 1),
(193, 'TDING_TESORERIA', NULL, 'ATENCIONES SERFIN', '2007-01-17 00:00:00', 'A/SE', 2),
(194, 'TDING_TESORERIA', NULL, 'DONACIONES', '2007-01-17 00:00:00', 'DONA', 3),
(195, 'TDING_TESORERIA', NULL, 'NOTA DE CREDITO', '2007-01-17 00:00:00', 'N/C', 4),
(196, 'TDING_TESORERIA', NULL, 'NOTA DE DEBITO', '2007-01-17 00:00:00', 'N/D', 5),
(197, 'TDING_TESORERIA', NULL, 'ORDEN DE INHUMACION', '2007-01-17 00:00:00', 'O/I', NULL),
(198, 'TDING_TESORERIA', NULL, 'ORDEN DE EXHUMACION', '2007-01-17 00:00:00', 'O/I', NULL),
(199, 'TDING_TESORERIA', NULL, 'REGULARIZACION DE DATOS', '2007-01-17 00:00:00', 'REG', 3),
(200, 'TIPO_ARCHIVADOR', NULL, 'DEFINITIVO', '2010-09-16 00:00:00', NULL, NULL),
(201, 'TIPO_ARCHIVADOR', NULL, 'TEMPORAL', '2010-09-16 00:00:00', NULL, NULL),
(205, 'MODALIDAD_PAGO', 1, 'CONTADO', '2013-10-22 00:00:00', NULL, NULL),
(206, 'MODALIDAD_PAGO', 2, 'DEPOSITO', '2013-10-22 00:00:00', NULL, NULL),
(210, 'TDING_SERFIN', NULL, 'RECIBO DE APORTE', '2007-01-17 00:00:00', 'RECI', 1),
(211, 'TDING_SERFIN', NULL, 'RECIBO INSCRIPCION', '2007-01-17 00:00:00', 'INS', 1),
(212, 'TDING_SERFIN', NULL, 'DEPOSITO', '2007-01-17 00:00:00', 'DEPO', 2),
(215, 'SECUENCIA_VISTA_COMPONENTE', 1, 'IMPAR', '2014-03-08 00:00:00', NULL, NULL),
(216, 'SECUENCIA_VISTA_COMPONENTE', 2, 'PAR', '2014-03-08 00:00:00', NULL, NULL),
(217, 'SECUENCIA_VISTA_COMPONENTE', 3, 'SECUENCIAL', '2014-03-08 00:00:00', NULL, NULL),
(220, 'TIPO_CEMENTERIO', 1, 'CEMENTERIO EL CARMEN DE CHICLAYO', '2013-10-22 00:00:00', NULL, NULL),
(225, 'TIPO_COMPONENTE', 1, 'CUARTEL', '2013-10-22 00:00:00', NULL, NULL),
(226, 'TIPO_COMPONENTE', 2, 'MAUSOLEO', '2013-10-22 00:00:00', NULL, NULL),
(227, 'TIPO_COMPONENTE', 3, 'TUMBA', '2013-10-22 00:00:00', NULL, NULL),
(229, 'TIPO_COMPONENTE', 5, 'CRIPTA', '2014-03-17 00:00:00', NULL, NULL),
(230, 'TIPO_COMPONENTE', 6, 'TERRENO', '2014-03-17 00:00:00', NULL, NULL),
(231, 'TIPO_COMPONENTE', 7, 'FOSA', '2013-10-22 00:00:00', NULL, NULL),
(232, 'TIPO_COMPONENTE', 99, 'OTROS', '2013-10-22 00:00:00', NULL, NULL),
(240, 'SUBTIPO_COMPONENTE', 1, 'NICHO ADULTO', '2013-10-22 00:00:00', NULL, NULL),
(241, 'SUBTIPO_COMPONENTE', 2, 'NICHO PARVULO', '2013-10-22 00:00:00', NULL, NULL),
(242, 'SUBTIPO_COMPONENTE', 3, 'TUMBA PERSONAL', '2013-10-22 00:00:00', NULL, NULL),
(243, 'SUBTIPO_COMPONENTE', 4, 'TUMBA BIPERSONAL', '2013-10-22 00:00:00', NULL, NULL),
(244, 'SUBTIPO_COMPONENTE', 5, 'TUMBA GEMELA', '2013-10-22 00:00:00', NULL, NULL),
(245, 'SUBTIPO_COMPONENTE', 6, 'TUMBA MATRIMONIAL', '2013-10-22 00:00:00', NULL, NULL),
(246, 'SUBTIPO_COMPONENTE', 7, 'TERRENO PARA MAUSOLEO', '2014-04-24 00:00:00', NULL, NULL),
(250, 'NIVELES_COMPONENTE', 0, 'TODAS LAS FILAS', '2013-10-22 00:00:00', NULL, NULL),
(251, 'NIVELES_COMPONENTE', 1, 'FILA 1', '2013-10-22 00:00:00', NULL, NULL),
(252, 'NIVELES_COMPONENTE', 2, 'FILA 2', '2013-10-22 00:00:00', NULL, NULL),
(253, 'NIVELES_COMPONENTE', 3, 'FILA 3', '2013-10-22 00:00:00', NULL, NULL),
(254, 'NIVELES_COMPONENTE', 4, 'FILA 4', '2013-10-22 00:00:00', NULL, NULL),
(255, 'NIVELES_COMPONENTE', 5, 'FILA 5', '2013-10-22 00:00:00', NULL, NULL),
(256, 'NIVELES_COMPONENTE', 6, 'FILA 6', '2013-10-22 00:00:00', NULL, NULL),
(257, 'NIVELES_COMPONENTE', 7, 'FILA 7', '2013-10-22 00:00:00', NULL, NULL),
(258, 'NIVELES_COMPONENTE', 8, 'FILA 8', '2013-10-22 00:00:00', NULL, NULL),
(259, 'NIVELES_COMPONENTE', 9, 'FILA 9', '2013-10-22 00:00:00', NULL, NULL),
(260, 'NIVELES_COMPONENTE', 10, 'FILA 10', '2013-10-22 00:00:00', NULL, NULL),
(262, 'TIPO_PARTE_PROCESAL', 2, 'DENUNCIADO', '2014-10-02 00:00:00', NULL, NULL),
(263, 'TIPO_EVENTO_LEGAL', NULL, 'VISTA DE LA CAUSA', '2014-10-02 00:00:00', NULL, NULL),
(264, 'TIPO_EVENTO_LEGAL', NULL, 'CONTESTACION DE DEMANDA', '2014-10-02 00:00:00', NULL, NULL),
(265, 'DESTINOS_TRANSPORTE_CARROZA', 1, 'CEMENTERIO \"EL CARMEN\" DE CHICLAYO', '2013-10-22 00:00:00', NULL, NULL),
(266, 'DESTINOS_TRANSPORTE_CARROZA', 2, 'CEMENTERIO \"JARDINES DE LA PAZ\" DE CHICLAYO', '2013-10-22 00:00:00', NULL, NULL),
(267, 'DESTINOS_TRANSPORTE_CARROZA', 3, 'OTRO DESTINO', '2013-10-22 00:00:00', NULL, NULL),
(268, 'DESTINOS_TRANSPORTE_CARROZA', 4, 'CEMENTERIO SAN MIGUEL ARCANGEL - PIURA', '2013-10-22 00:00:00', NULL, NULL),
(269, 'DESTINOS_TRANSPORTE_CARROZA', 5, 'CEMENTERIO SANTA ANA DE CAJABAMBA', '2013-10-22 00:00:00', NULL, NULL),
(270, 'DESTINOS_TRANSPORTE_CARROZA', 6, 'CEMENTERIO PARQUE DEL RECUERDO DIST.LURIN-LIMA', '2013-10-22 00:00:00', NULL, NULL),
(290, 'TIPO_RECAUDACION', 1, 'CONTADO/SERFIN/DONACION', '2013-10-22 00:00:00', NULL, NULL),
(291, 'TIPO_RECAUDACION', 2, 'CREDITO (INICIAL/LETRAS)', '2013-10-22 00:00:00', NULL, NULL),
(292, 'TIPO_RECAUDACION', 4, 'NOTA DE CREDITO', '2013-10-22 00:00:00', NULL, NULL),
(293, 'TIPO_RECAUDACION', 5, 'NOTA DE DEBITO (SIN AFECT.TRIB)', '2013-10-22 00:00:00', NULL, NULL),
(294, 'TIPO_RECAUDACION', 3, 'ALQUILERES', '2013-10-22 00:00:00', NULL, NULL),
(300, 'MODALIDAD_PAGO_RECAUDACION', 1, 'CONTADO', '2013-10-22 00:00:00', NULL, NULL),
(301, 'MODALIDAD_PAGO_RECAUDACION', 2, 'SERFIN', '2013-10-22 00:00:00', NULL, NULL),
(302, 'MODALIDAD_PAGO_RECAUDACION', 3, 'DONACIONES Y REGULARIZACIONES DE DATOS', '2014-05-01 00:00:00', NULL, NULL),
(303, 'MODALIDAD_PAGO_RECAUDACION', 4, 'N/C', '2014-05-01 00:00:00', NULL, NULL),
(304, 'MODALIDAD_PAGO_RECAUDACION', 5, 'N/D', '2014-05-01 00:00:00', NULL, NULL),
(310, 'ESTADO_LETRA', 1, 'POR COBRAR', '2010-09-19 00:00:00', NULL, NULL),
(311, 'ESTADO_LETRA', 2, 'PAGADO', '2010-09-19 00:00:00', NULL, NULL),
(312, 'ESTADO_LETRA', 8, 'LEGAL', '2010-09-19 00:00:00', NULL, NULL),
(313, 'ESTADO_LETRA', 9, 'ANULADO', '2010-09-19 00:00:00', NULL, NULL),
(315, 'TIPO_BENEFICIO_RECAUDACION', NULL, 'TARIFA SOCIAL', '2010-09-19 00:00:00', NULL, NULL),
(316, 'TIPO_BENEFICIO_RECAUDACION', NULL, 'DONACION', '2014-08-13 00:00:00', NULL, NULL),
(317, 'DISTRITO_JUDICIAL', NULL, 'NACIONAL', '2014-09-04 00:00:00', 'NAC', NULL),
(318, 'DISTRITO_JUDICIAL', NULL, 'LAMBAYEQUE', '2014-09-04 00:00:00', NULL, NULL),
(319, 'ORGANO_JURISDICCIONAL', NULL, 'TRIBUNAL CONSTITUCIONAL', '2014-09-15 00:00:00', NULL, NULL),
(320, 'ORGANO_JURISDICCIONAL', NULL, 'FISCALIA', '2014-09-15 00:00:00', NULL, NULL),
(321, 'ORGANO_JURISDICCIONAL', NULL, 'SALA SUPERIOR', '2014-09-15 00:00:00', NULL, NULL),
(322, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO ESPECIALIZADO', '2014-09-15 00:00:00', NULL, NULL),
(323, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO MIXTO', '2014-09-15 00:00:00', NULL, NULL),
(324, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO DE PAZ LETRADO', '2014-09-15 00:00:00', NULL, NULL),
(325, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO DE PAZ LETRADO', '2014-09-15 00:00:00', NULL, NULL),
(326, 'ESPECIALIDAD_LEGAL', NULL, 'CONSTITUCIONAL', '2014-10-01 00:00:00', NULL, NULL),
(327, 'ESPECIALIDAD_LEGAL', NULL, 'PENAL', '2014-10-01 00:00:00', NULL, NULL),
(328, 'ESPECIALIDAD_LEGAL', NULL, 'CIVIL', '2014-09-15 00:00:00', NULL, NULL),
(329, 'ESPECIALIDAD_LEGAL', NULL, 'COMERCIAL', '2014-09-15 00:00:00', NULL, NULL),
(330, 'ESPECIALIDAD_LEGAL', NULL, 'CONTENCIOSO ADMINISTRATIVO', '2014-09-15 00:00:00', NULL, NULL),
(331, 'ESPECIALIDAD_LEGAL', NULL, 'FAMILIAR CIVIL', '2014-09-15 00:00:00', NULL, NULL),
(332, 'ESPECIALIDAD_LEGAL', NULL, 'FAMILIAR TUTELAR', '2014-09-15 00:00:00', NULL, NULL),
(333, 'ESPECIALIDAD_LEGAL', NULL, 'LABORAL', '2014-09-15 00:00:00', NULL, NULL),
(334, 'ESPECIALIDAD_LEGAL', NULL, 'ARBITRAL', '2015-05-20 00:00:00', NULL, NULL),
(335, 'TIPO_PARTE_PROCESAL', 1, 'DEMANDANTE', '2014-09-20 00:00:00', NULL, NULL),
(336, 'TIPO_PARTE_PROCESAL', 2, 'DEMANDADO', '2014-09-20 00:00:00', NULL, NULL),
(337, 'TIPO_REPRESENTANTE', NULL, 'DEFENSOR', '2014-09-25 00:00:00', NULL, NULL),
(338, 'TIPO_REPRESENTANTE', NULL, 'ASESOR', '2014-09-25 00:00:00', NULL, NULL),
(339, 'TIPO_REPRESENTANTE', NULL, 'COLABORADOR', '2014-09-25 00:00:00', NULL, NULL),
(340, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'INICIADO', '2014-09-25 00:00:00', NULL, NULL),
(341, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'FUNDADO', '2014-09-25 00:00:00', NULL, NULL),
(342, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'PROCEDENTE', '2014-09-25 00:00:00', NULL, NULL),
(343, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'INPROCEDENTE', '2014-09-25 00:00:00', NULL, NULL),
(344, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'EN PROCESO', '2014-09-25 00:00:00', NULL, NULL),
(345, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'PARA SENTENCIA', '2014-09-25 00:00:00', NULL, NULL),
(354, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'CON SENTENCIA FAVORABLE', '2014-09-25 00:00:00', NULL, NULL),
(355, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'CON SENTENCIA DESFAVORABLE', '2014-09-25 00:00:00', NULL, NULL),
(356, 'ESTADO_EXPEDIENTE_LEGAL', NULL, 'ARCHIVADO', '2014-09-25 00:00:00', NULL, NULL),
(360, 'INSTANCIA_LEGAL', NULL, 'PRIMERA', '2014-09-26 00:00:00', NULL, NULL),
(361, 'INSTANCIA_LEGAL', NULL, 'SEGUNDA', '2014-09-26 00:00:00', NULL, NULL),
(362, 'INSTANCIA_LEGAL', NULL, 'SUPERIOR', '2014-09-26 00:00:00', NULL, NULL),
(370, 'TIPO_EVENTO_LEGAL', NULL, 'AUDIENCIA', '2014-09-27 00:00:00', NULL, NULL),
(371, 'TIPO_EVENTO_LEGAL', NULL, 'PRESENTACION DE ALEGATOS', '2014-09-27 00:00:00', NULL, NULL),
(372, 'TIPO_EVENTO_LEGAL', NULL, 'DECLARACION', '2014-09-27 00:00:00', NULL, NULL),
(380, 'ORGANO_JURISDICCIONAL', NULL, 'SALA LABORAL', '2014-10-03 00:00:00', NULL, NULL),
(381, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO LABORAL', '2014-10-03 00:00:00', NULL, NULL),
(382, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO DE TRABAJO TRANSITORIO', '2014-10-03 00:00:00', NULL, NULL),
(383, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO DE PAZ LETRADO CIVIL', '2014-10-10 00:00:00', NULL, NULL),
(384, 'ORGANO_JURISDICCIONAL', NULL, 'JUZGADO CIVIL', '2014-10-14 00:00:00', NULL, NULL),
(385, 'CARGO_ESTRUCTURAL', NULL, 'ENCARGADO DE COMPRAS', '2014-10-24 00:00:00', NULL, NULL),
(386, 'ORGANO_JURISDICCIONAL', NULL, 'SALA CIVIL', '2014-10-24 00:00:00', NULL, NULL),
(387, 'CARGO_ESTRUCTURAL', NULL, 'SECRETARIA TECNICA', '2015-02-20 00:00:00', NULL, NULL),
(390, 'TIPO_IGV', 1, 'AFECTO', '2018-10-29 00:00:00', NULL, NULL),
(391, 'TIPO_IGV', 2, 'EXONERADO', '2018-10-29 00:00:00', NULL, NULL),
(392, 'TIPO_IGV', 3, 'INAFECTO', '2018-10-29 00:00:00', NULL, NULL),
(402, 'TIPO_ACUMULADO_PRESUPUESTAL', 3, 'ACUMULADO PRESUPUESTAL', '2015-01-30 00:00:00', NULL, NULL),
(506, 'PORTAL_TIPO_MENU', NULL, 'PRINCIPAL', '2007-09-04 00:00:00', NULL, NULL),
(514, 'POS_BANNER', NULL, 'INFERIOR', '2007-10-05 00:00:00', NULL, NULL),
(515, 'DESTINO_ENLACE', NULL, 'SECCION CONTENIDO DEL PORTAL', '2007-10-09 00:00:00', NULL, NULL),
(516, 'DESTINO_ENLACE', NULL, 'NUEVA VENTANA', '2009-11-27 00:00:00', NULL, NULL),
(529, 'SECTOR', NULL, 'SEDE SBCH', '2015-08-15 00:00:00', NULL, NULL),
(538, 'MODULO_BASICO_JUSTICIA', NULL, 'NINGUNO', '2015-08-15 00:00:00', NULL, NULL),
(543, 'ESTADOS_DOC.FUENTE', 1, 'EMITIDO', '2015-06-16 00:00:00', NULL, NULL),
(544, 'ESTADOS_DOC.FUENTE', 2, 'PROCESADO', '2015-06-16 00:00:00', NULL, NULL),
(545, 'ESTADOS_DOC.FUENTE', 9, 'ANULADO', '2015-06-16 00:00:00', NULL, NULL),
(554, 'CLASES_PLAN_CONTABLE', NULL, '1 ACTIVO', '2015-01-30 00:00:00', NULL, NULL),
(555, 'CLASES_PLAN_CONTABLE', NULL, '2 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(556, 'CLASES_PLAN_CONTABLE', NULL, '3  PATRIMONIO', '2015-01-30 00:00:00', NULL, NULL),
(557, 'CLASES_PLAN_CONTABLE', NULL, '4 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(558, 'CLASES_PLAN_CONTABLE', NULL, '5 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(559, 'CLASES_PLAN_CONTABLE', NULL, '6 RESULTADOS', '2015-01-30 00:00:00', NULL, NULL),
(560, 'CLASES_PLAN_CONTABLE', NULL, '8 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(561, 'GRUPOS_PLAN_CONTABLE', NULL, '11 ACTIVO DISPONIBLE', '2015-01-30 00:00:00', NULL, NULL),
(562, 'GRUPOS_PLAN_CONTABLE', NULL, '12 ACTIVO EXIGIBLE', '2015-01-30 00:00:00', NULL, NULL),
(563, 'GRUPOS_PLAN_CONTABLE', NULL, '14 ACTIVO INMOVILIZADO', '2015-01-30 00:00:00', NULL, NULL),
(564, 'GRUPOS_PLAN_CONTABLE', NULL, '15 ACTIVOS FIJOS', '2015-01-30 00:00:00', NULL, NULL),
(565, 'GRUPOS_PLAN_CONTABLE', NULL, '16 OTROS ACTIVOS', '2015-01-30 00:00:00', NULL, NULL),
(569, 'PROCEDENCIAS_NEAS', NULL, 'DONACION', '2015-02-05 00:00:00', NULL, NULL),
(570, 'PROCEDENCIAS_NEAS', NULL, 'REINGRESO', '2015-02-05 00:00:00', NULL, NULL),
(571, 'PROCEDENCIAS_NEAS', NULL, 'TRANSFERENCIA', '2015-02-05 00:00:00', NULL, NULL),
(575, 'CARGO_ESTRUCTURAL', NULL, 'PRESIDENTE DE COMISION', '2015-11-04 00:00:00', NULL, NULL),
(592, 'PROCEDENCIAS_NEAS', NULL, 'COMPRAS CAJA CHICA', '2015-02-05 00:00:00', NULL, NULL),
(600, 'ESTADO_CONTRATO_ALQUILER', 1, 'ACTIVO', '2018-11-08 00:00:00', NULL, NULL),
(601, 'ESTADO_CONTRATO_ALQUILER', 2, 'DUDOSO (EX-INQUILINO)', '2018-11-08 00:00:00', NULL, NULL),
(602, 'ESTADO_CONTRATO_ALQUILER', 3, 'CANCELADO', '2018-11-08 00:00:00', NULL, NULL),
(603, 'ESTADO_CONTRATO_ALQUILER', 7, 'JUDICIALIZADO', '2018-11-08 00:00:00', NULL, NULL),
(604, 'CARGO_ESTRUCTURAL', NULL, 'SECRETARIO SCI', '2017-05-29 00:00:00', NULL, NULL),
(607, 'CARGO_ESTRUCTURAL', NULL, 'INGENIERO', '2018-01-09 00:00:00', NULL, NULL),
(608, 'VISTA_COMPONENTE', NULL, 'SECUENCIAL', '2018-02-26 00:00:00', NULL, NULL),
(609, 'TIPO_EVENTO_LEGAL', NULL, 'CASACION', '2018-07-05 00:00:00', NULL, NULL),
(610, 'ESTADO_CONTRATO_ALQUILER', 8, 'BAJA', '2018-11-08 00:00:00', NULL, NULL),
(611, 'PROCEDENCIAS_NEAS', NULL, 'INVENTARIO FISICO', '2015-02-05 00:00:00', NULL, NULL),
(612, 'ESTADO_CONTRATO_ALQUILER', 9, 'ANULADO', '2018-11-08 00:00:00', NULL, NULL),
(613, 'DESTINO_OCUPANTE', 1, 'SEPULTURA', '2018-10-29 00:00:00', NULL, NULL),
(614, 'DESTINO_OCUPANTE', 2, 'RESERVADO EN VIDA', '2018-10-29 00:00:00', NULL, NULL),
(615, 'DESTINO_OCUPANTE', 3, 'TRASLADO', '2018-10-29 00:00:00', NULL, NULL),
(616, 'DESTINO_OCUPANTE', 4, 'REQUISITO DE INTERESADO', '2018-10-29 00:00:00', NULL, NULL),
(620, 'TIPO_INTERIOR_INMUEBLE', NULL, 'CASA', '2018-10-04 00:00:00', 'CASA', NULL),
(621, 'TIPO_INTERIOR_INMUEBLE', NULL, 'DEPARTAMENTO', '2018-10-04 00:00:00', 'DPTO.', NULL),
(622, 'TIPO_INTERIOR_INMUEBLE', NULL, 'TIENDA', '2018-10-04 00:00:00', 'TDA.', NULL),
(623, 'TIPO_INTERIOR_INMUEBLE', NULL, 'OFICINA', '2018-10-04 00:00:00', 'OF.', NULL),
(624, 'TIPO_INTERIOR_INMUEBLE', NULL, 'STAND', '2018-10-04 00:00:00', 'STAND', NULL),
(625, 'TIPO_INTERIOR_INMUEBLE', NULL, 'CLINICA', '2018-10-04 00:00:00', 'CLIN.', NULL),
(626, 'TIPO_INTERIOR_INMUEBLE', NULL, 'CONSULTORIO', '2018-10-04 00:00:00', 'CONSULT.', NULL),
(627, 'TIPO_INTERIOR_INMUEBLE', NULL, 'CEO', '2018-10-04 00:00:00', 'CEO', NULL),
(628, 'TIPO_INTERIOR_INMUEBLE', NULL, 'BOTICA', '2018-10-04 00:00:00', 'BOT.', NULL),
(629, 'TIPO_INTERIOR_INMUEBLE', NULL, 'IMPRENTA', '2018-10-04 00:00:00', 'IMP.', NULL),
(630, 'TIPO_INTERIOR_INMUEBLE', NULL, 'INSTITUCION EDUCATIVA', '2018-10-04 00:00:00', 'IE.', NULL),
(639, 'TIPO_INTERIOR_INMUEBLE', NULL, 'NINGUNO', '2018-10-04 00:00:00', 'NINGUNO', NULL),
(997, 'PROFESION', NULL, 'NO IDENTIFICADO', '2009-11-28 00:00:00', NULL, NULL),
(998, 'CARGO_ESTRUCTURAL', NULL, 'NO IDENTIFICADO', '2009-11-28 00:00:00', NULL, NULL),
(1219, 'CLASES_PLAN_CONTABLE', NULL, '9 CUENTAS DE ORDEN', '2015-01-30 00:00:00', NULL, NULL),
(1319, 'GRUPOS_PLAN_CONTABLE', NULL, '13 ACTIVO REALIZABLE', '2015-01-30 00:00:00', NULL, NULL),
(1320, 'GRUPOS_PLAN_CONTABLE', NULL, '21 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(1321, 'GRUPOS_PLAN_CONTABLE', NULL, '22 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(1322, 'GRUPOS_PLAN_CONTABLE', NULL, '23 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(1323, 'GRUPOS_PLAN_CONTABLE', NULL, '24 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(1324, 'GRUPOS_PLAN_CONTABLE', NULL, '25 PASIVO', '2015-01-30 00:00:00', NULL, NULL),
(1325, 'GRUPOS_PLAN_CONTABLE', NULL, '31 PATRIMONIO', '2015-01-30 00:00:00', NULL, NULL),
(1326, 'GRUPOS_PLAN_CONTABLE', NULL, '32 PATRIMONIO', '2015-01-30 00:00:00', NULL, NULL),
(1327, 'GRUPOS_PLAN_CONTABLE', NULL, '33 PATRIMONIO', '2015-01-30 00:00:00', NULL, NULL),
(1328, 'GRUPOS_PLAN_CONTABLE', NULL, '34 PATRIMONIO', '2015-01-30 00:00:00', NULL, NULL),
(1329, 'GRUPOS_PLAN_CONTABLE', NULL, '41 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1330, 'GRUPOS_PLAN_CONTABLE', NULL, '42 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1331, 'GRUPOS_PLAN_CONTABLE', NULL, '43 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1332, 'GRUPOS_PLAN_CONTABLE', NULL, '44 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1333, 'GRUPOS_PLAN_CONTABLE', NULL, '45 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1334, 'GRUPOS_PLAN_CONTABLE', NULL, '46 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1339, 'GRUPOS_PLAN_CONTABLE', NULL, '54 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1340, 'GRUPOS_PLAN_CONTABLE', NULL, '55 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1341, 'GRUPOS_PLAN_CONTABLE', NULL, '56 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1342, 'GRUPOS_PLAN_CONTABLE', NULL, '57 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1343, 'GRUPOS_PLAN_CONTABLE', NULL, '58 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1344, 'GRUPOS_PLAN_CONTABLE', NULL, '61 RESULTADOS', '2015-01-30 00:00:00', NULL, NULL),
(1345, 'GRUPOS_PLAN_CONTABLE', NULL, '81 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1346, 'GRUPOS_PLAN_CONTABLE', NULL, '82 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1347, 'GRUPOS_PLAN_CONTABLE', NULL, '83 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1348, 'GRUPOS_PLAN_CONTABLE', NULL, '84 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1349, 'GRUPOS_PLAN_CONTABLE', NULL, '85 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1350, 'GRUPOS_PLAN_CONTABLE', NULL, '86 PRESUPUESTO', '2015-01-30 00:00:00', NULL, NULL),
(1351, 'GRUPOS_PLAN_CONTABLE', NULL, '91 CUENTAS DE ORDEN', '2015-01-30 00:00:00', NULL, NULL),
(1434, 'GRUPOS_PLAN_CONTABLE', NULL, '51 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1435, 'GRUPOS_PLAN_CONTABLE', NULL, '52 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1436, 'GRUPOS_PLAN_CONTABLE', NULL, '53 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1438, 'GRUPOS_PLAN_CONTABLE', NULL, '47 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1440, 'GRUPOS_PLAN_CONTABLE', NULL, '59 GASTOS', '2015-01-30 00:00:00', NULL, NULL),
(1739, 'GRUPOS_PLAN_CONTABLE', NULL, '30 PATRIMONIO.', '2015-01-30 00:00:00', NULL, NULL),
(1749, 'GRUPOS_PLAN_CONTABLE', NULL, '48 INGRESOS', '2015-01-30 00:00:00', NULL, NULL),
(1756, 'TIPO_EXPEDIENTE_LEGAL', 1, 'PROCESO CIVIL', '2015-05-09 00:00:00', 'MAGISTRADO/JUEZ', NULL),
(1757, 'TIPO_EXPEDIENTE_LEGAL', 2, 'PROCESO PENAL', '2015-05-09 00:00:00', 'MAGISTRADO/JUEZ', NULL),
(1758, 'TIPO_EXPEDIENTE_LEGAL', 3, 'PROCESO FISCAL', '2015-05-09 00:00:00', 'FISCAL', NULL),
(1759, 'TIPO_EXPEDIENTE_LEGAL', 4, 'CONCILIACION', '2015-05-09 00:00:00', 'CONCILIADOR', NULL),
(1760, 'TIPO_EXPEDIENTE_LEGAL', 5, 'PROCESO ARBITRAL', '2015-05-09 00:00:00', 'ARBITRO', NULL),
(1761, 'TIPO_EXPEDIENTE_LEGAL', 6, 'EXP.PRELIMINAR', '2015-05-09 00:00:00', 'RESPONSABLE', NULL),
(1762, 'CONDICION_EXPEDIENTE_LEGAL', 1, 'ACTIVO', '2015-05-16 00:00:00', NULL, NULL),
(1763, 'CONDICION_EXPEDIENTE_LEGAL', 2, 'CERRADO', '2015-05-16 00:00:00', NULL, NULL),
(1764, 'CONDICION_EXPEDIENTE_LEGAL', 9, 'ANULADO', '2015-05-16 00:00:00', NULL, NULL),
(1767, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 1, 'PRIMERA INSTANCIA', '2015-05-20 00:00:00', NULL, NULL),
(1768, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 1, 'SEGUNDA INSTANCIA', '2015-05-20 00:00:00', NULL, NULL),
(1769, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 1, 'CASACION', '2015-05-20 00:00:00', NULL, NULL),
(1770, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 1, 'TRIBUNAL CONSTITUCIONAL', '2015-05-20 00:00:00', NULL, NULL),
(1771, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 1, 'EJECUCION', '2015-05-20 00:00:00', NULL, NULL),
(1774, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'DENUNCIA/DILIGENCIAS PRELIMINARES', '2015-05-20 00:00:00', NULL, NULL),
(1776, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'INSTRUCCION/INVESTIGACION PREPARATORIA', '2015-05-20 00:00:00', NULL, NULL),
(1777, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'ETAPA INTERMEDIA', '2015-05-20 00:00:00', NULL, NULL),
(1778, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'JUZGAMIENTO/JUICIO ORAL', '2015-05-20 00:00:00', NULL, NULL),
(1779, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'SENTENCIA DE 1RA INSTANCIA', '2015-05-20 00:00:00', NULL, NULL),
(1780, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'EN APELACION', '2015-05-20 00:00:00', NULL, NULL),
(1781, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'SENTENCIA DE 2DA INSTANCIA', '2015-05-20 00:00:00', NULL, NULL),
(1782, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'CASACION', '2015-05-20 00:00:00', NULL, NULL),
(1783, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 2, 'EJECUCION', '2015-05-20 00:00:00', NULL, NULL),
(1784, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 3, 'DENUNCIA/DILIGENCIAS PRELIMINARES', '2015-05-20 00:00:00', NULL, NULL),
(1788, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 4, 'ETAPA DE CONCILIACION', '2015-05-20 00:00:00', NULL, NULL),
(1789, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'ETAPA DE INSTALACION', '2015-05-20 00:00:00', NULL, NULL),
(1790, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'ETAPA DE DEMANDA Y CONTESTACION', '2015-05-20 00:00:00', NULL, NULL),
(1791, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'ETAPA DE AUDIENCIAS (PUNTOS CONTROVERTIDOS-PRUEBAS-ETC)', '2015-05-20 00:00:00', NULL, NULL),
(1792, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'ETAPA DE ALEGACIONES', '2015-05-20 00:00:00', NULL, NULL),
(1793, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'LAUDO', '2015-05-20 00:00:00', NULL, NULL),
(1794, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'ACLARACION-RECTIFICACION-INTEGRACION-EXCLUSION', '2015-05-20 00:00:00', NULL, NULL),
(1795, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 5, 'EJECUCION DEL LAUDO', '2015-05-20 00:00:00', NULL, NULL),
(1796, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 6, 'ETAPA PRELIMINAR', '2015-05-20 00:00:00', NULL, NULL),
(1797, 'TIPO_SENTENCIA_EXPEDIENTE_LEGAL', 1, 'FAVORABLE', '2015-05-20 00:00:00', NULL, NULL),
(1798, 'TIPO_SENTENCIA_EXPEDIENTE_LEGAL', 2, 'PARCIALMENTE FAVORABLE', '2015-05-20 00:00:00', NULL, NULL),
(1799, 'TIPO_SENTENCIA_EXPEDIENTE_LEGAL', 3, 'DESFAVORABLE', '2015-05-20 00:00:00', NULL, NULL),
(1800, 'TIPO_SENTENCIA_EXPEDIENTE_LEGAL', 4, 'PARCIALMENTE DESFAVORABLE', '2015-05-20 00:00:00', NULL, NULL),
(1801, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 7, 'EJECUCION', '2015-05-20 00:00:00', NULL, NULL),
(1802, 'CLASIFICACION_ESTADO_EXPEDIENTE_LEGAL', 8, 'EJECUCION', '2015-05-20 00:00:00', NULL, NULL),
(1811, 'TIPO_ARBITRO', NULL, 'TRIBUNAL EN CENTRO DE ARBITRAJE', '2015-05-21 00:00:00', NULL, NULL),
(1812, 'TIPO_ARBITRO', NULL, 'UNICO ADSCRITO A CENTRO DE ARBITRAJE', '2015-05-21 00:00:00', NULL, NULL),
(1813, 'TIPO_ARBITRO', NULL, 'TRIBUNAL AD HOC', '2015-05-21 00:00:00', NULL, NULL),
(1814, 'TIPO_ARBITRO', NULL, 'ARBITRO UNICO AD HOC', '2015-05-21 00:00:00', NULL, NULL),
(1821, 'TIPO_PARTE_PROCESAL', 3, 'TERCERO CIVIL', '2015-06-03 00:00:00', NULL, NULL),
(1822, 'TIPO_PARTE_PROCESAL', 1, 'AGRAVIADO', '2015-06-03 00:00:00', NULL, NULL),
(1823, 'TIPO_PARTE_PROCESAL', 2, 'INCULPADO', '2015-06-03 00:00:00', NULL, NULL),
(1833, 'ESTADO_PROPUESTA', 1, 'ADMITIDO', '2015-07-18 00:00:00', NULL, NULL),
(1835, 'ESTADO_PROPUESTA', 2, 'NO ADMITIDO', '2015-07-18 00:00:00', NULL, NULL),
(1836, 'ESTADO_PROPUESTA', 2, 'NO PRESENTADO', '2015-07-18 00:00:00', NULL, NULL),
(1837, 'ESTADO_PROPUESTA', 1, 'CALIFICADO', '2015-07-18 00:00:00', NULL, NULL),
(1838, 'ESTADO_PROPUESTA', 1, 'DESCALIFICADO', '2015-07-18 00:00:00', NULL, NULL),
(1840, 'MODALIDAD_PROCESO_SELECCION', NULL, 'PROCESO ELECTRONICO', '2015-07-26 00:00:00', NULL, NULL),
(1841, 'MODALIDAD_PROCESO_SELECCION', NULL, 'ACTO PUBLICO', '2015-07-26 00:00:00', NULL, NULL),
(1843, 'MODALIDAD_PROCESO_SELECCION', NULL, 'ACTO PRIVADO', '2015-07-29 00:00:00', NULL, NULL),
(1844, 'ESTADOS_PEDIDO_BS', 1, 'EMITIDO', '2015-06-16 00:00:00', NULL, NULL),
(1845, 'ESTADOS_PEDIDO_BS', 2, 'EN PROCESO', '2015-06-16 00:00:00', NULL, NULL),
(1846, 'ESTADOS_PEDIDO_BS', 9, 'ANULADO', '2015-06-16 00:00:00', NULL, NULL),
(1848, 'TIPO_JUSTIFICACION_BP', NULL, 'POR MAYOR PUNTAJE TOTAL', '2015-08-23 00:00:00', NULL, NULL),
(1849, 'TIPO_JUSTIFICACION_BP', NULL, 'A FAVOR DE MICROEMPRESA Y PEQUEÃA EMPRESA', '2015-08-23 00:00:00', NULL, NULL),
(1850, 'TIPO_JUSTIFICACION_BP', NULL, 'A FAVOR DE MICROEMPRESA Y PEQUEÃA EMPRESA INTEGRADAS POR PERSONAS CON DISCAPACIDAD', '2015-08-23 00:00:00', NULL, NULL),
(1851, 'TIPO_JUSTIFICACION_BP', NULL, 'POR MEJOR PUNTAJE ECONOMICO', '2015-08-23 00:00:00', NULL, NULL),
(1852, 'TIPO_JUSTIFICACION_BP', NULL, 'POR MEJOR PUNTAJE TECNICO', '2015-08-23 00:00:00', NULL, NULL),
(1853, 'TIPO_JUSTIFICACION_BP', NULL, 'POR MEJOR PRECIO ECONOMICO', '2015-08-23 00:00:00', NULL, NULL),
(1854, 'TIPO_JUSTIFICACION_BP', NULL, 'A PRORRATA ENTRE LOS GANADORES', '2015-08-23 00:00:00', NULL, NULL),
(1855, 'TIPO_JUSTIFICACION_BP', NULL, 'MEDIANTE SORTEO EN PRESENCIA DE LOS POSTORES', '2015-08-23 00:00:00', NULL, NULL),
(1856, 'ETAPA_PROCESO_SELECCION', 1, 'EV. DE MERCADO (VALORES REFERENCIALES)', '2013-10-22 00:00:00', NULL, NULL),
(1857, 'ETAPA_PROCESO_SELECCION', 2, 'EV. DE PROPUESTAS', '2013-10-22 00:00:00', NULL, NULL),
(1867, 'ESTADO_BIEN', 1, 'BUENO', '2015-12-26 00:00:00', NULL, NULL),
(1868, 'ESTADO_BIEN', 2, 'REGULAR', '2015-12-26 00:00:00', NULL, NULL),
(1869, 'ESTADO_BIEN', 3, 'MALO', '2015-12-26 00:00:00', NULL, NULL),
(1870, 'ESTADO_BIEN', 4, 'NUEVO', '2015-12-26 00:00:00', NULL, NULL),
(1871, 'ESTADO_BIEN', 5, 'MUY MALO', '2015-12-26 00:00:00', NULL, NULL),
(1873, 'CONDICION_BIEN', 1, 'PERTENECIENTE A LA EMPRESA', '2015-12-26 00:00:00', 'D/EMP', NULL),
(1874, 'CONDICION_BIEN', 2, 'RECIBIDO EN CESION DE USO', '2015-12-26 00:00:00', 'REC.S.US', NULL),
(1875, 'CONDICION_BIEN', 3, 'ENTREGADO EN CESION DE USO', '2015-12-26 00:00:00', 'ENT.S.US', NULL),
(1876, 'CONDICION_BIEN', 4, 'TRANSFERIDO A OTRA ENTIDAD', '2015-12-26 00:00:00', 'TRANSF', NULL),
(1877, 'CONDICION_BIEN', 9, 'DADO DE BAJA', '2015-12-26 00:00:00', 'BAJA', NULL),
(1878, 'TIPO_BIEN', 1, 'BIEN MUEBLE', '2016-01-10 00:00:00', 'BMU', NULL),
(1879, 'TIPO_BIEN', 2, 'BIEN INMUEBLE', '2016-01-10 00:00:00', 'BIN', NULL),
(1880, 'TIPO_BIEN', 3, 'BIEN NO DEPRECIABLE', '2016-01-10 00:00:00', 'BND', NULL),
(1881, 'TIPO_BIEN', 4, 'OTROS BIENES', '2016-01-10 00:00:00', 'OTB', NULL),
(1891, 'MODALIDAD_MOVIMIENTO_BIEN', 1, 'DESPLAZAMIENTO EXTERNO', '2016-02-19 00:00:00', 'DESP.EXTERNO', NULL),
(1892, 'MODALIDAD_MOVIMIENTO_BIEN', 2, 'DESPLAZAMIENTO INTERNO', '2016-02-19 00:00:00', 'DESP.INTERNO', NULL),
(1893, 'MODALIDAD_MOVIMIENTO_BIEN', 3, 'CAMBIAR ESTADO', '2016-02-19 00:00:00', 'CAMB.ESTADO', NULL),
(1894, 'MODALIDAD_MOVIMIENTO_BIEN', 4, 'CAMBIAR CONDICION', '2016-02-19 00:00:00', 'CAMB.CONDCION', NULL),
(1895, 'CONDICION_DESPLAZAMIENTO_BIEN', 0, 'NINGUNO', '2016-02-22 00:00:00', NULL, NULL),
(1897, 'CONDICION_DESPLAZAMIENTO_BIEN', 1, 'PENDIENTE DE RETORNO', '2016-02-22 00:00:00', NULL, NULL),
(1898, 'CONDICION_DESPLAZAMIENTO_BIEN', 2, 'RETORNADO', '2016-02-22 00:00:00', NULL, NULL),
(1899, 'TIPO_EXPEDIENTE_LEGAL', 7, 'PROCESO LABORAL', '2015-05-09 00:00:00', 'MAGISTRADO/JUEZ', NULL),
(1900, 'TIPO_EXPEDIENTE_LEGAL', 8, 'MEDIDA CAUTELAR', '2015-05-09 00:00:00', 'MAGISTRADO/JUEZ', NULL),
(1902, 'TIPO_INTERIOR_INMUEBLE', NULL, 'TERRENO', '2018-11-09 00:00:00', NULL, NULL),
(1903, 'CARGO_ESTRUCTURAL', NULL, 'JEFE DE DEPARTAMENTO', '2019-01-03 00:00:00', NULL, NULL),
(1904, 'CARGO_ESTRUCTURAL', NULL, 'DIRECTORA IEP. VICENTE DE LA VEGA', '2019-01-03 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tester_demo`
--

CREATE TABLE `tester_demo` (
  `idtest` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `textarea` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idpadre` int(11) DEFAULT NULL,
  `orden` tinyint(4) NOT NULL DEFAULT 1,
  `imagen` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tester_demo`
--

INSERT INTO `tester_demo` (`idtest`, `nombre`, `textarea`, `color`, `estado`, `idpadre`, `orden`, `imagen`, `fecha_insert`, `fecha_update`) VALUES
(1, 'asdasd', 'asdasdasd', 'rgba(198,38,38,0.59)', 1, NULL, 1, 'static/media/E_1/U1/tester_demo/imagen.png', '2020-12-01 23:14:38', '2020-12-01 23:14:38');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vistapagos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vistapagos` (
`fecha_vencimiento` varchar(10)
,`vivienda` text
,`num_documento` varchar(45)
,`nombres` varchar(352)
,`estado` tinyint(1)
,`monto_total` decimal(14,2)
,`monto_mensual` decimal(10,2)
,`fecha_insert` datetime
,`cabid` int(11)
,`fechaven` date
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vistapagos`
--
DROP TABLE IF EXISTS `vistapagos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vistapagos`  AS  select date_format(`det`.`fecha_vencimiento`,'%d/%m/%Y') AS `fecha_vencimiento`,`rebi`.`rebi_detalles` AS `vivienda`,`p`.`num_documento` AS `num_documento`,concat(coalesce(`p`.`nombres`,''),' ',coalesce(`p`.`apellido_1`,''),' ',coalesce(`p`.`apellido_2`,'')) AS `nombres`,`cab`.`estado_pago` AS `estado`,`cab`.`monto_mensual` * `cab`.`meses_contrato` + `cab`.`garantia` AS `monto_total`,`cab`.`monto_mensual` AS `monto_mensual`,`cab`.`fecha_insert` AS `fecha_insert`,`cab`.`id` AS `cabid`,`det`.`fecha_vencimiento` AS `fechaven` from (((`cronogramapago_cab` `cab` join `persona` `p` on(`cab`.`persona_id` = `p`.`idpersona`)) join `registro_bien` `rebi` on(`rebi`.`rebi_id` = `cab`.`rebi_id`)) join `cronogramapago_det` `det` on(`det`.`cab_id` = `cab`.`id`)) where `det`.`estado` = 0 group by `cab`.`id` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  ADD PRIMARY KEY (`idbiblioteca`),
  ADD KEY `fk_biblioteca_empresa` (`idempresa`),
  ADD KEY `fk_biblioteca_persona` (`idpersona`);

--
-- Indices de la tabla `catalogo_sbn`
--
ALTER TABLE `catalogo_sbn`
  ADD PRIMARY KEY (`casb_codigo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`clie_id`);

--
-- Indices de la tabla `contacto_bien`
--
ALTER TABLE `contacto_bien`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cronogramapago_cab`
--
ALTER TABLE `cronogramapago_cab`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cronogramapago_det`
--
ALTER TABLE `cronogramapago_det`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dependencia`
--
ALTER TABLE `dependencia`
  ADD PRIMARY KEY (`depe_id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indices de la tabla `empresa_config`
--
ALTER TABLE `empresa_config`
  ADD PRIMARY KEY (`idempresaconfig`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indices de la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  ADD PRIMARY KEY (`idempresamodulo`),
  ADD KEY `fk_empresa_modulo_empresa` (`idempresa`),
  ADD KEY `fkempresa_modulos_modulos` (`idmodulo`);

--
-- Indices de la tabla `empresa_smtp`
--
ALTER TABLE `empresa_smtp`
  ADD PRIMARY KEY (`idempresasmtp`),
  ADD KEY `fk_modulos_empresa_smtp` (`idempresa`);

--
-- Indices de la tabla `galeria_fotos`
--
ALTER TABLE `galeria_fotos`
  ADD PRIMARY KEY (`id_galeria`),
  ADD KEY `rebi_id` (`rebi_id`);

--
-- Indices de la tabla `historial_sesion`
--
ALTER TABLE `historial_sesion`
  ADD PRIMARY KEY (`idhistorial`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`idmodulo`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- Indices de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  ADD PRIMARY KEY (`idpersonarol`),
  ADD KEY `fk_persona_rol_persona` (`idpersona`),
  ADD KEY `fk_persona_rol_rol` (`idrol`);

--
-- Indices de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  ADD PRIMARY KEY (`rebi_id`),
  ADD KEY `casb_codigo` (`casb_codigo`),
  ADD KEY `tabl_tipo_interior` (`tabl_tipo_interior`),
  ADD KEY `depe_id_ubica` (`depe_id_ubica`),
  ADD KEY `depe_id` (`depe_id`),
  ADD KEY `tabl_tipo_interior_2` (`tabl_tipo_interior`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `sys_configuracion`
--
ALTER TABLE `sys_configuracion`
  ADD PRIMARY KEY (`config`);

--
-- Indices de la tabla `tabla`
--
ALTER TABLE `tabla`
  ADD PRIMARY KEY (`tabl_id`);

--
-- Indices de la tabla `tester_demo`
--
ALTER TABLE `tester_demo`
  ADD PRIMARY KEY (`idtest`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  MODIFY `idbiblioteca` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `clie_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `contacto_bien`
--
ALTER TABLE `contacto_bien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `cronogramapago_cab`
--
ALTER TABLE `cronogramapago_cab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cronogramapago_det`
--
ALTER TABLE `cronogramapago_det`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idempresa` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empresa_config`
--
ALTER TABLE `empresa_config`
  MODIFY `idempresaconfig` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  MODIFY `idempresamodulo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `empresa_smtp`
--
ALTER TABLE `empresa_smtp`
  MODIFY `idempresasmtp` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galeria_fotos`
--
ALTER TABLE `galeria_fotos`
  MODIFY `id_galeria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `historial_sesion`
--
ALTER TABLE `historial_sesion`
  MODIFY `idhistorial` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `idmodulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  MODIFY `idpersonarol` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  MODIFY `rebi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4775;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tabla`
--
ALTER TABLE `tabla`
  MODIFY `tabl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1905;

--
-- AUTO_INCREMENT de la tabla `tester_demo`
--
ALTER TABLE `tester_demo`
  MODIFY `idtest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  ADD CONSTRAINT `fkempresa_modulos_modulos` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`idmodulo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  ADD CONSTRAINT `fk_registro_bien_dependencia` FOREIGN KEY (`depe_id_ubica`) REFERENCES `dependencia` (`depe_id`);

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `actualizarMorosos` ON SCHEDULE EVERY 2 MINUTE STARTS '2020-01-01 00:00:00' ENDS '2031-02-16 00:00:00' ON COMPLETION NOT PRESERVE DISABLE COMMENT 'actualiza el campo estado_pago de la tb cronogramapago_cab' DO BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE fechavencimiento DATE;
DECLARE varid INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT cabid,fechaven from abel_sbch.vistapagos;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

OPEN cur;
read_loop: LOOP	
    FETCH cur INTO varid, fechavencimiento;
    IF done THEN
      	LEAVE read_loop;      	
    END IF;
    IF CURRENT_DATE()>fechavencimiento THEN
      UPDATE `cronogramapago_cab` SET `estado_pago`=0 where `id` = varid;    	 ELSE
      UPDATE `cronogramapago_cab` SET `estado_pago`=1 where `id`= varid;
      END IF;
    
  END LOOP;
CLOSE cur;
END$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

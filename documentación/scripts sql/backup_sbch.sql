-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-01-2021 a las 20:59:51
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abel_sbch`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biblioteca`
--

CREATE TABLE `biblioteca` (
  `idbiblioteca` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Audio,video,pdf,imagen',
  `archivo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `biblioteca`
--

INSERT INTO `biblioteca` (`idbiblioteca`, `idempresa`, `idpersona`, `tipo`, `archivo`, `nombre`, `estado`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/buscar20201201231408.png', 'buscar20201201231408.png', 1, '2020-12-01 23:14:08', '2020-12-01 23:14:08'),
(2, 2, 1, 'image', 'static/media/E_2/U1/biblioteca/LOGO_INVIERTE20201207215628.png', 'LOGO_INVIERTE20201207215628.png', 1, '2020-12-07 20:56:28', '2020-12-07 20:56:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo_sbn`
--

CREATE TABLE `catalogo_sbn` (
  `casb_codigo` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `casb_descripcion` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `casb_fecharegistro` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `catalogo_sbn`
--

INSERT INTO `catalogo_sbn` (`casb_codigo`, `casb_descripcion`, `casb_fecharegistro`) VALUES
('90000000', 'CEMENTERIO', '0000-00-00 00:00:00'),
('97000001', 'EDIFICIOS', '0000-00-00 00:00:00'),
('97000002', 'EDIFICIO PIEDRA LORA', '0000-00-00 00:00:00'),
('97000003', 'EDIFICIO DOS DE MAYO', '0000-00-00 00:00:00'),
('97000004', 'EDIFICIO GALERIAS LA PLAZUELA', '0000-00-00 00:00:00'),
('97000005', 'BIENES URBANOS', '0000-00-00 00:00:00'),
('98000001', 'TERRENOS', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `clie_id` int(11) NOT NULL,
  `tabl_tipocliente` int(11) NOT NULL,
  `clie_apellidos` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clie_nombres` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clie_razsocial` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_dni` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_direccion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_telefono` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_fregistro` datetime NOT NULL DEFAULT current_timestamp(),
  `clie_estado` int(2) NOT NULL,
  `clie_zona` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clie_empleado` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`clie_id`, `tabl_tipocliente`, `clie_apellidos`, `clie_nombres`, `clie_razsocial`, `clie_dni`, `clie_direccion`, `clie_telefono`, `clie_email`, `clie_fregistro`, `clie_estado`, `clie_zona`, `clie_empleado`) VALUES
(1, 1, 'Gómez', 'Miluska', 'Miluska Gómez', '44140114', 'Casa 123', '015464561', 'mngomez_sy@outlook.com', '2020-12-23 20:17:53', 1, 'CENTRO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencia`
--

CREATE TABLE `dependencia` (
  `depe_id` int(11) NOT NULL,
  `depe_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depe_depeid` int(11) DEFAULT NULL,
  `depe_direccion` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `dependencia`
--

INSERT INTO `dependencia` (`depe_id`, `depe_nombre`, `depe_depeid`, `depe_direccion`) VALUES
(0, 'INICIO', NULL, NULL),
(2, 'SOCIEDAD DE BENEFICENCIA CHICLAYO', 0, 'CHICLAYO'),
(3, 'PRESIDENCIA', 2, NULL),
(4, 'DIRECTORIO', 3, NULL),
(5, 'GERENCIA GENERAL', 3, NULL),
(6, 'OFICINA DE CONTROL INSTITUCIONAL', 3, NULL),
(8, 'OFICINA DE ASESORIA JURIDICA', 5, NULL),
(9, 'OFICINA DE PLANEAMIENTO Y PRESUPUESTO', 5, NULL),
(10, 'OFICINA GENERAL DE ADMINISTRACION', 5, NULL),
(11, 'UNIDAD DE RECURSOS HUMANOS', 10, NULL),
(12, 'UNIDAD DE CONTABILIDAD', 10, NULL),
(13, 'UNIDAD DE TESORERIA', 10, NULL),
(14, 'UNIDAD DE LOGISTICA, CONTROL PATRIMONIAL Y SERV. INTERNOS', 10, 'ELIAS AGUIRRE N° 248 - INT.07'),
(15, 'UNIDAD DE SISTEMAS Y SOPORTE TECNOLOGICO', 10, NULL),
(18, 'DEPARTAMENTO DE SERVICIO FUNERARIO INTEGRAL-SERFIN', 116, NULL),
(19, 'SUB GERENCIA DE GESTION INMOBILIARIA', 113, NULL),
(20, 'OFICINA CONTROL PATRIMONIAL', 14, NULL),
(22, 'UNIDAD DE IMAGEN INSTITUCIONAL Y GESTION DOCUMENTARIA', 3, NULL),
(23, 'OBRAS SOCIALES', 5, NULL),
(24, 'GERENCIA DE ASISTENCIA Y PROMOCION SOCIAL', 5, NULL),
(25, 'ALMACEN CENTRAL', 14, 'ELIAS AGUIRRE 248-07 - CHICLAYO'),
(28, 'DEPARTAMENTO CENTRO MEDICO SALUD VIDA', 114, NULL),
(31, 'DEPARTAMENTO DE CEMENTERIO', 116, NULL),
(32, 'GESTION DOCUMENTARIA', 22, NULL),
(33, 'OFICINA DE NUTRICION', 24, NULL),
(34, 'CONSULTORIO JURIDICO', 112, NULL),
(35, 'ENTIDAD EXTERNA / PERSONAL EXTERNO', 0, NULL),
(36, 'ARCHIVO INSTITUCIONAL', 22, NULL),
(37, 'EMPLEADOS / VIGILANTES', 2, NULL),
(39, 'COMITE ESPECIAL DE OBRAS', 2, NULL),
(40, 'I.E.P. VICENTE DE LA VEGA', 114, NULL),
(41, 'COMITE DE CONTROL', 5, NULL),
(43, 'CASA REFUGIO DORITA RIVAS MARTINO', 45, NULL),
(44, 'COMITE ESPECIAL DE PROCESOS DE SELECCION', 5, NULL),
(45, 'SUB GERENCIA DE ASISTENCIA SOCIAL', 24, NULL),
(46, 'COMISION DE CHOCOLATADAS', 5, NULL),
(47, 'ORIENTACION DE OFICIO', 6, NULL),
(48, 'PROCESOS ADMINISTRATIVO DISCIPLINARIOS', 11, NULL),
(49, 'SUB GERENCIA DE PROYECTOS Y OBRAS', 109, NULL),
(50, 'OFICINA DE COSTOS', 10, NULL),
(51, 'ALMACEN DE OBRAS', 14, 'AV. EL CARMEN S/N-(CEMENTERIO EL CARMEN)-CHICLAYO'),
(52, 'COMEDOR POPULAR SBCH', 45, NULL),
(53, 'CAI. CRUZ DE LA ESPERANZA', 45, NULL),
(54, 'CAI. 1° DE JUNIO-LA VICTORIA', 45, NULL),
(55, 'CAI. 1° DE MAYO-JLO', 45, NULL),
(56, 'COMEDOR ADULTO MAYOR - VILLA HERMOSA', 45, NULL),
(57, 'CAI. 9 DE OCTUBRE', 45, NULL),
(58, 'I.E. JESUS DE NAZARETH', 112, NULL),
(59, 'SUPERVISION DE SEGURIDAD', 11, NULL),
(60, 'PROGRAMA JORNADAS DE PROYECCION SOCIAL', 28, NULL),
(61, 'BOTICA SBCH', 28, NULL),
(62, 'COMISION DE AUDITORIA DE CUMPLIMIENTO 1', 6, NULL),
(63, 'COMISION DE INVENTARIOS', 5, NULL),
(64, 'COMISION DE SANEAMIENTO SBCH', 3, 'ELIAS AGUIRRE N° 248 - INT.07'),
(65, 'COMISION DE ANIVERSARIO SBCH', 5, NULL),
(67, 'COMISION SANEAMIENTO DE INMUEBLES Y CEMENTERIO', 5, NULL),
(68, 'SUB GERENCIA DE INGENIERIA', 5, NULL),
(69, 'BIENES URBANOS', 19, NULL),
(70, 'GALERIAS LA PLAZUELA', 19, NULL),
(71, 'TEATRO DOS DE MAYO', 19, NULL),
(72, 'ALMACEN DE PATRIMONIO', 19, NULL),
(73, 'TRANSPORTES-SBCH', 14, NULL),
(74, 'CONSULTORIO DE TRAUMATOLOGIA', 28, NULL),
(75, 'CONSULTORIO UROLOGIA - CARDIOLOGIA', 28, NULL),
(76, 'EDIFICIO DOS DE MAYO', 19, NULL),
(77, 'SUB GERENCIA', 5, NULL),
(78, 'CENTRO DE EMERGENCIA MUJER', 24, NULL),
(79, 'CONSULTORIO PSICOLOGIA   ', 28, NULL),
(80, 'LABORATORIO CENTRO MEDICO', 28, NULL),
(81, 'CONSULTORIO GINECOLOGIA', 28, NULL),
(82, 'TRIAJE CENTRO MEDICO  ', 28, NULL),
(83, 'DEPOSITO BOMBA', 28, NULL),
(84, 'CONSULTORIO PEDIATRIA', 28, NULL),
(85, 'CENTRO DE FOTOCOPIADORA', 19, NULL),
(86, 'AMBIENTE DE ASEO - CENTRO MEDICO ', 28, NULL),
(87, 'CONSULTORIO MEDICINA FISICA ', 28, NULL),
(88, 'WAWA WASI INSTITUCIONA', 24, NULL),
(89, 'CONSULTORIO OTORRINO', 28, NULL),
(90, 'CONSULTORIO DERMATOLOGIA - NEUROLOGIA', 28, NULL),
(91, 'ASESORIA LEGAL EXTERNA', 5, NULL),
(92, 'CONSULTORIO ODONTOLOGIA', 28, NULL),
(93, 'VIGILANCIA OFICINAS SBCH', 19, NULL),
(94, 'CONSULTORIO OFTALMOLOFIA ', 28, NULL),
(95, 'PROGRAMA NACIONAL CUNA MAS', 5, NULL),
(96, 'PROGRAMA SOCIAL SEÑOR DE LA MISECORDIA', 24, NULL),
(97, 'CONSULTORIO MEDICINA GENERAL', 28, NULL),
(98, 'VELATORIO SBCH', 18, NULL),
(99, 'TRANSPORTES-SERFIN', 18, NULL),
(100, 'SISTEMA DE CONTROL INTERNO', 5, NULL),
(101, 'ORGANO SANCIONADOR', 48, NULL),
(102, 'ORGANO INSTRUCTOR', 48, NULL),
(103, 'FONDO DE CAJA CHICA', 10, NULL),
(104, 'COMITE ADM. DEL FONDO ASIST. Y ESTIMULO-CAFAE', 2, NULL),
(105, 'COMISION DE AUDITORIA DE CUMPLIMIENTO 2', 6, NULL),
(106, 'COMISION DE ALTAS Y BAJAS', 5, NULL),
(107, 'SECRETARIA DE SIST.CONTROL INTERNO', 100, NULL),
(109, 'GERENCIA DE INGENIERIA', 5, NULL),
(110, 'ALMACEN TEMPORAL DE BIENES DE INVENTARIO', 19, NULL),
(111, 'COMITE ADQUISICION DE ATAUDES', 5, NULL),
(112, 'SUB GERENCIA DE PROMOCION Y DESARROLLO HUMANO', 24, NULL),
(113, 'GERENCIA DE GESTION DE NEGOCIOS', 5, NULL),
(114, 'SUB GERENCIA DE NEGOCIOS', 113, NULL),
(116, 'SUB GERENCIA DE SERVICIOS FUNERARIOS', 113, NULL),
(117, 'COMITE ESPEC. DE ARRENDAMIENTOS CONV. PUB.', 5, NULL),
(118, 'COMISION CONVOCATORIA DE PERSONAL', 5, NULL),
(119, 'COMISION PAQUETES DE ALIMENTOS', 5, NULL),
(120, 'COMISION ACTUALIZACION DOCUMENTOS DE GESTION', 5, NULL),
(121, 'CAI. VILLA HERMOSA ', 45, NULL),
(122, 'CAI. JORGE CHAVEZ', 45, NULL),
(123, 'CAI. NUEVO 2', 45, NULL),
(124, 'CAI. NUEVO 3', 45, NULL),
(125, 'CAI. NUEVO 4', 45, NULL),
(126, 'COMISION DE UNIFORMES', 5, NULL),
(127, 'SINDICATO DE TRABAJADORES', 11, NULL),
(128, 'COMITE DE SEGURIDAD Y SALUD EN EL TRABAJO', 5, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idempresa` bigint(20) NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idioma` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdominio` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '	0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`idempresa`, `nombre`, `direccion`, `telefono`, `correo`, `logo`, `ruc`, `idioma`, `subdominio`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Serfin', 'chiclayo2', '987654321', 'info@ingytal.com', 'static/media/E_1/U1/empresa/logo20201008095907.png', '20145698713', 'ES', 'all', 1, 1, '2020-08-08 20:52:07', '2020-12-01 23:48:41'),
(2, 'InMobiliario', '....', '9999999', 'asdasd@correo.com', 'static/media/E_2/U1/empresa/logo.png', '31235456', 'ES', '.', 1, 1, '2020-12-01 23:49:59', '2020-12-07 20:57:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_config`
--

CREATE TABLE `empresa_config` (
  `idempresaconfig` bigint(20) NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `parametro` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_config`
--

INSERT INTO `empresa_config` (`idempresaconfig`, `idempresa`, `parametro`, `valor`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'color_fondo_login', '#f6f7f8', 1, 1, '2020-10-04 09:17:36', '2020-10-04 09:21:13'),
(2, 1, 'imagen_fondo_login', '', 1, 1, '2020-10-04 09:19:12', '2020-10-04 09:47:35'),
(3, 1, 'tipo_empresa', 'tienda', 1, 2, '2020-10-13 15:25:39', '2020-10-23 20:52:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_modulo`
--

CREATE TABLE `empresa_modulo` (
  `idempresamodulo` bigint(20) NOT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idrol` int(11) NOT NULL,
  `orden` int(11) DEFAULT 0,
  `idmodulopadre` int(11) DEFAULT NULL,
  `nomtemporal` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_modulo`
--

INSERT INTO `empresa_modulo` (`idempresamodulo`, `idmodulo`, `idempresa`, `estado`, `idrol`, `orden`, `idmodulopadre`, `nomtemporal`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, NULL, 1, 1, 1, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2dzIn0=', 1, '2020-12-01 23:27:23', '2020-12-01 23:27:46'),
(2, 3, 1, 1, 1, 2, 1, NULL, 1, '2020-12-01 23:27:36', '2020-12-01 23:30:02'),
(3, 4, 1, 1, 1, 3, 1, NULL, 1, '2020-12-01 23:27:57', '2020-12-01 23:30:02'),
(4, 2, 1, 1, 1, 1, 1, NULL, 1, '2020-12-01 23:29:54', '2020-12-01 23:30:02'),
(5, NULL, 2, 1, 2, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2cifQ==', 1, '2020-12-01 23:54:48', '2020-12-01 23:55:48'),
(6, 2, 2, 1, 2, 1, 5, NULL, 1, '2020-12-01 23:54:57', '2020-12-01 23:55:48'),
(7, 3, 2, 1, 2, 2, 5, NULL, 1, '2020-12-01 23:55:18', '2020-12-01 23:55:48'),
(8, 4, 2, 1, 2, 3, 5, NULL, 1, '2020-12-01 23:55:26', '2020-12-01 23:55:48'),
(10, 6, 2, 1, 2, 2, NULL, NULL, 1, '2020-12-07 20:47:31', '2020-12-07 20:49:51'),
(15, 12, 2, 1, 2, 3, NULL, NULL, 1, '2020-12-23 20:16:33', '2020-12-23 20:17:16'),
(16, 13, 2, 1, 2, 5, NULL, NULL, 1, '2020-12-24 00:36:57', '2020-12-24 00:36:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_smtp`
--

CREATE TABLE `empresa_smtp` (
  `idempresasmtp` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `host` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puerto` int(11) NOT NULL,
  `cifrado` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_fotos`
--

CREATE TABLE `galeria_fotos` (
  `id_galeria` int(11) NOT NULL,
  `rebi_id` int(11) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `tipo` smallint(6) NOT NULL,
  `imagen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_sesion`
--

CREATE TABLE `historial_sesion` (
  `idhistorial` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navegador` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgrupoaula` bigint(20) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_final` datetime NOT NULL DEFAULT current_timestamp(),
  `lugar` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `idmodulo` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1 COMMENT '	0=Inactivo,1,Activo,-1=eliminado',
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icono` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`idmodulo`, `nombre`, `descripcion`, `estado`, `url`, `icono`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Empresa', '', 0, '#', 'fa-config', 1, '2020-10-04 08:41:00', '2020-10-07 17:23:28'),
(2, 'Configuracion', 'Parámetros de configuración de empresa', 1, 'admin/empresa/configuracion', 'fa-building-o', 1, '2020-10-04 08:43:41', '2020-10-07 17:23:24'),
(3, 'Módulos', '', 1, 'admin/modulos', 'fa-tags', 1, '2020-10-10 08:56:24', '2020-12-01 23:29:09'),
(4, 'Asignación de módulos', '', 1, 'admin/empresa/modulos', 'fa-tags', 1, '2020-10-10 08:56:49', '2020-12-01 23:29:29'),
(5, 'Perfil', '', 1, 'admin/persona/perfil', 'fa-pencil', 2, '2020-10-10 12:01:13', '2020-10-17 02:40:33'),
(6, 'Persona', NULL, 1, 'persona', 'fa-user', 1, '2020-12-07 20:45:47', '2020-12-07 20:45:47'),
(7, 'Clientes', '', -1, 'admin/clientes', 'fa-user-plus', 1, '2020-12-07 20:46:12', '2020-12-22 20:55:36'),
(8, 'Configuración de datos generales', NULL, -1, 'admin/tabla', 'fa-bars', 1, '2020-12-21 21:17:15', '2020-12-22 20:55:32'),
(9, 'Configuración Tabla', NULL, 1, 'admin/tabla', 'fa-check-square', 1, '2020-12-22 20:57:37', '2020-12-22 20:57:37'),
(10, 'Clientes', NULL, -1, 'admin/clientes', 'fa-user', 1, '2020-12-22 21:47:24', '2020-12-23 19:14:24'),
(11, 'Clientes', NULL, -1, 'admin/clientes', 'fa-user', 1, '2020-12-23 19:54:39', '2020-12-23 19:58:43'),
(12, 'Clientes', NULL, 1, 'admin/clientes', 'fa-user', 1, '2020-12-23 20:16:12', '2020-12-23 20:16:12'),
(13, 'Registro Bien', NULL, 1, 'admin/registro_bien', 'fa-home', 1, '2020-12-24 00:36:47', '2020-12-24 00:36:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` bigint(20) NOT NULL,
  `tipo_documento` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'D',
  `num_documento` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verclave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tocken` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_usuario` enum('s','n') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 's=superadmin\r\nn: normal',
  `telefono` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ubigeo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT 1 COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) DEFAULT NULL,
  `fecha_insert` datetime DEFAULT current_timestamp(),
  `fecha_update` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `tipo_documento`, `num_documento`, `apellido_1`, `apellido_2`, `nombres`, `usuario`, `email`, `clave`, `verclave`, `tocken`, `foto`, `genero`, `fecha_nacimiento`, `estado_civil`, `tipo_usuario`, `telefono`, `ubigeo`, `direccion`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'D', '14253645', 'Abel', 'Chingo', 'Tello', 'abelchingo', 'abelchingo@gmail.com', 'abf333f2019d70ac7da2ccbcb08a489d', 'abelchingo@', 'dc51d5fbcbf47927901cf3cc11ee2f28', 'static/media/E_1/persona/U1/foto.png', 'M', '2013-07-01', 'S', 's', '942171837', '010101', 'los lirios Mz. D Lote 6', 1, 1, '2020-08-08 20:55:39', '2020-10-12 19:33:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_rol`
--

CREATE TABLE `persona_rol` (
  `idpersonarol` bigint(20) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona_rol`
--

INSERT INTO `persona_rol` (`idpersonarol`, `idempresa`, `idpersona`, `idrol`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 1, 1, '2020-08-09 11:25:51', '2020-08-09 11:25:51'),
(2, 2, 1, 2, 1, '2020-12-01 23:56:51', '2020-12-01 23:56:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_bien`
--

CREATE TABLE `registro_bien` (
  `rebi_id` int(11) NOT NULL,
  `rebi_modalidad` int(11) NOT NULL,
  `rebi_tipo_bien` int(11) NOT NULL,
  `casb_codigo` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rebi_fadquisicion` date NOT NULL,
  `rebi_vadquisicion` float(10,2) NOT NULL,
  `rebi_ult_dep_acum` float(10,2) NOT NULL,
  `rebi_ult_val_act` float(10,2) NOT NULL,
  `tabl_estado` tinyint(4) NOT NULL,
  `rebi_detalles` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depe_id_ubica` int(11) NOT NULL,
  `tabl_condicion` tinyint(4) NOT NULL,
  `rebi_reg_publicos` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rebi_area` float(10,2) DEFAULT NULL,
  `rebi_observaciones` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rebi_fregistro` datetime NOT NULL DEFAULT current_timestamp(),
  `depe_id` int(11) NOT NULL,
  `rebi_anno` int(11) DEFAULT NULL,
  `rebi_alquiler` float(4,0) NOT NULL,
  `tabl_tipo_interior` int(11) DEFAULT NULL,
  `rebi_numero_interior` int(11) DEFAULT NULL,
  `rebi_foto_principal` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `registro_bien`
--

INSERT INTO `registro_bien` (`rebi_id`, `rebi_modalidad`, `rebi_tipo_bien`, `casb_codigo`, `rebi_fadquisicion`, `rebi_vadquisicion`, `rebi_ult_dep_acum`, `rebi_ult_val_act`, `tabl_estado`, `rebi_detalles`, `depe_id_ubica`, `tabl_condicion`, `rebi_reg_publicos`, `rebi_area`, `rebi_observaciones`, `rebi_fregistro`, `depe_id`, `rebi_anno`, `rebi_alquiler`, `tabl_tipo_interior`, `rebi_numero_interior`, `rebi_foto_principal`) VALUES
(1, 1, 2, '90000000', '0000-00-00', 1860000.00, 0.00, 0.00, 1, 'CEMENTERIO EL CARMEN DE CHICLAYO', 19, 1, '11009248', 60000.00, NULL, '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2285, 3, 2, '97000001', '0000-00-00', 1860000.00, 243594.19, 2029950.38, 1, 'CEMENTERIO \"EL CARMEN\" - CHICLAYO       ', 69, 1, '11009184', 60000.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2286, 3, 2, '98000001', '0000-00-00', 296332.53, 0.00, 396900.00, 1, 'PARTE FUNDO RUST.LA PAMPA,PATAZCA,MOLINO', 69, 1, '2198803', 8100.00, 'TERRENO EN LITIGIO                 ', '0000-00-00 00:00:00', 2, 1945, 0, 639, NULL, NULL),
(2287, 3, 2, '97000005', '0000-00-00', 1480920.00, 0.00, 3449092.75, 1, 'ALDEAS INFANTILES - PASEO DEL DEPORTE   ', 69, 1, '11052993', 17220.00, 'PARTE FUNDO RUST.LA PAMPA,PATAZCA, ', '0000-00-00 00:00:00', 2, 1945, 0, 639, NULL, NULL),
(2288, 3, 2, '97000005', '0000-00-00', 48659.00, 1590.83, 53027.54, 1, 'CASA HABITACIÓN TERMINADO               ', 69, 1, '2212350', 247.00, 'AREA CONSTRUIDA:49.78M2            ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2289, 3, 2, '97000005', '0000-00-00', 16617.50, 17719.62, 47492.53, 1, 'CASA HABITACIÓN TERMINADO               ', 69, 1, '11155768', 144.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2290, 3, 2, '97000005', '0000-00-00', 25300.00, 18603.41, 67552.25, 1, 'CASA HABITACIÓN TERMINADO               ', 69, 1, '11155763', 220.00, '                                   ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2720, 3, 2, '97000005', '0000-00-00', 419680.00, 37630.10, 1254336.62, 1, 'TERRENO - MINSA (SERVICIOS TERMINADO)   ', 69, 1, '11045791', 4880.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 639, NULL, NULL),
(2721, 3, 2, '98000001', '0000-00-00', 405748.00, 0.00, 405748.00, 1, NULL, 69, 1, '11045797', 4718.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2722, 3, 2, '97000004', '0000-00-00', 16318.64, 861.97, 28732.39, 1, 'STANDS', 70, 1, '11263289', 48.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 49, NULL),
(2723, 3, 2, '97000004', '0000-00-00', 6643.98, 199.32, 6643.98, 1, 'STAND 55', 70, 1, '11263289', 24.23, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 55, NULL),
(2724, 3, 2, '97000004', '0000-00-00', 3893.76, 212.70, 7089.94, 1, 'STAND 59', 70, 1, '11263289', 34.91, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, 59, NULL),
(2725, 3, 2, '97000004', '0000-00-00', 4688.06, 247.63, 8254.31, 1, 'STAND 47-48-48A', 70, 1, '11263289', 37.79, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 47, NULL),
(2726, 3, 2, '97000004', '0000-00-00', 8404.80, 371.32, 12377.30, 1, 'STAND', 70, 1, '11263289', 15.45, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 69, NULL),
(2727, 3, 2, '97000004', '0000-00-00', 15322.30, 976.46, 32548.79, 1, 'STAND 03', 70, 1, '11263289', 59.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, 3, NULL),
(2728, 3, 2, '97000004', '0000-00-00', 6105.60, 374.94, 12497.96, 1, 'STAND 34-36', 70, 1, '11263289', 24.13, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 34, NULL),
(2729, 3, 2, '97000004', '0000-00-00', 8913.06, 470.80, 15693.31, 1, 'STANDS 46-46A-47A', 70, 1, '11263289', 33.23, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 46, NULL),
(2730, 3, 2, '97000004', '0000-00-00', 13229.32, 698.79, 23293.00, 1, 'STAND 43-44', 70, 1, '11263289', 25.69, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 624, NULL, NULL),
(2731, 3, 2, '97000004', '0000-00-00', 8913.06, 470.80, 15693.31, 1, 'STAND 41-42', 70, 1, '11263289', 21.33, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 41, NULL),
(2732, 3, 2, '97000004', '0000-00-00', 3893.76, 212.70, 7089.94, 1, 'STAND 39-40', 70, 1, '11263289', 21.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 39, NULL),
(2733, 3, 2, '97000004', '0000-00-00', 9826.20, 603.42, 20113.90, 1, 'STAND 33-35', 70, 1, '11263289', 24.17, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 33, NULL),
(2734, 3, 2, '97000004', '0000-00-00', 10176.00, 640.31, 21343.57, 1, NULL, 70, 1, '11263289', 25.89, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 29, NULL),
(2735, 3, 2, '97000004', '0000-00-00', 6667.40, 409.44, 13647.94, 1, 'STAND 27', 70, 1, '11263289', 26.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 27, NULL),
(2736, 3, 2, '97000004', '0000-00-00', 14671.68, 648.19, 21606.21, 1, 'STAND', 70, 1, '11263289', 26.97, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 70, NULL),
(2737, 3, 2, '97000004', '0000-00-00', 25214.40, 1113.96, 37131.91, 1, 'STAND 66-67', 70, 1, '11263289', 30.67, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 66, NULL),
(2738, 3, 2, '97000004', '0000-00-00', 8323.20, 367.71, 12257.14, 1, 'STAND 65', 70, 1, '11263289', 16.61, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 65, NULL),
(2739, 3, 2, '97000004', '0000-00-00', 2968.00, 189.15, 6304.85, 1, 'STAND 05', 70, 1, '11263289', 12.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 5, NULL),
(2740, 3, 2, '97000004', '0000-00-00', 12428.50, 713.45, 23781.58, 1, 'STAND 02', 70, 1, '11263289', 46.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 2, NULL),
(2741, 3, 2, '97000004', '0000-00-00', 9839.18, 519.72, 17323.94, 1, 'STAND 76-77', 70, 1, '11263289', 27.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 76, NULL),
(2742, 3, 2, '97000004', '0000-00-00', 3088.80, 321.76, 10725.26, 1, 'STAND 82-83', 70, 1, '11263289', 36.81, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 82, NULL),
(2743, 3, 2, '97000004', '0000-00-00', 3246.25, 206.88, 6895.93, 1, 'STAND 26', 70, 1, '11263289', 12.93, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 26, NULL),
(2744, 3, 2, '97000004', '0000-00-00', 3246.25, 199.35, 6644.96, 1, 'STAND 24', 70, 1, '11263289', 13.46, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 24, NULL),
(2745, 3, 2, '97000004', '0000-00-00', 8335.08, 491.11, 16370.37, 1, 'STAND 18-20', 70, 1, '11263289', 26.09, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 18, NULL),
(2746, 3, 2, '97000004', '0000-00-00', 482.56, 127.59, 4253.05, 1, 'STAND 79', 70, 1, '11263289', 5.20, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 79, NULL),
(2747, 3, 2, '97000004', '0000-00-00', 2962.70, 184.32, 6144.05, 1, 'STAND 32', 70, 1, '11263289', 12.68, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 32, NULL),
(2748, 3, 2, '97000004', '0000-00-00', 5961.28, 739.16, 24638.60, 1, 'STAND 01', 70, 1, '11263289', 53.99, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 1, NULL),
(2749, 3, 2, '97000004', '0000-00-00', 4340.70, 270.05, 9001.74, 1, 'STAND 61', 70, 1, '11263289', 16.90, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 61, NULL),
(2750, 3, 2, '97000004', '0000-00-00', 3907.28, 213.44, 7114.56, 1, 'STAND 15', 70, 1, '11263289', 12.86, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 15, NULL),
(2751, 3, 2, '97000004', '0000-00-00', 3619.90, 222.29, 7409.81, 1, 'STAND 31', 70, 1, '11263289', 14.36, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 31, NULL),
(2752, 3, 2, '97000004', '0000-00-00', 4377.10, 247.06, 8235.33, 1, NULL, 70, 1, '11263289', 12.95, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 13, NULL),
(2753, 3, 2, '97000004', '0000-00-00', 8504.08, 480.00, 16000.07, 1, 'STAND 8-8A', 70, 1, '11263289', 26.80, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 8, NULL),
(2754, 3, 2, '97000004', '0000-00-00', 4377.10, 241.86, 8062.11, 1, 'STAND 13-14-41E-41F', 70, 1, '11263289', 48.14, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 13, NULL),
(2755, 3, 2, '97000004', '0000-00-00', 2968.00, 179.56, 5985.22, 1, 'STAND 04', 70, 1, '11263289', 12.94, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 624, 4, NULL),
(2756, 3, 2, '97000004', '0000-00-00', 15260.70, 843.25, 28108.45, 1, 'STAND 7-11A-11B', 70, 1, '11263289', 46.84, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 7, NULL),
(2757, 3, 2, '97000004', '0000-00-00', 3792.36, 196.24, 6541.48, 1, 'STAND 09.', 70, 1, '11263289', 24.85, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 9, NULL),
(2758, 3, 2, '97000004', '0000-00-00', 3792.36, 204.45, 6814.97, 1, 'STAND 9A', 70, 1, '11263289', 12.57, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 9, NULL),
(2759, 3, 2, '97000004', '0000-00-00', 12228.84, 659.27, 21975.55, 1, 'STAND 10', 70, 1, '11263289', 25.06, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 10, NULL),
(2760, 3, 2, '97000004', '0000-00-00', 4252.04, 221.50, 7383.32, 1, 'STAND 16.', 70, 1, '11263289', 13.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 16, NULL),
(2761, 3, 2, '97000004', '0000-00-00', 4252.04, 245.49, 8182.88, 1, 'STAND 17', 70, 1, '11263289', 12.82, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 17, NULL),
(2762, 3, 2, '97000004', '0000-00-00', 2304.64, 243.51, 8116.88, 1, 'STAND 19 ', 70, 1, '11263289', 12.86, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 19, NULL),
(2763, 3, 2, '97000004', '0000-00-00', 4502.16, 234.53, 7817.63, 1, 'STAND 22', 70, 1, '11263289', 13.72, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 22, NULL),
(2764, 3, 2, '97000004', '0000-00-00', 7490.08, 399.07, 13302.32, 1, 'STAND 23', 70, 1, '11263289', 13.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 23, NULL),
(2765, 3, 2, '97000004', '0000-00-00', 35245.00, 1494.33, 49810.85, 1, NULL, 70, 1, '11263289', 133.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2766, 3, 2, '97000004', '0000-00-00', 1404.00, 153.95, 5131.80, 1, 'STANDO 68', 70, 1, '11263289', 29.25, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 68, NULL),
(2767, 3, 2, '97000004', '0000-00-00', 6208.95, 361.23, 12040.89, 1, 'STAND 58', 70, 1, '11263289', 23.43, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 58, NULL),
(2768, 3, 2, '97000004', '0000-00-00', 3445.00, 200.42, 6680.82, 1, 'STAND 74 ', 70, 1, '11263289', 11.95, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1999, 1, 624, 74, NULL),
(2769, 3, 2, '97000004', '0000-00-00', 11339.35, 700.25, 23341.52, 1, 'STAND 72-73', 70, 1, '11263289', 26.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 72, NULL),
(2770, 3, 2, '97000004', '0000-00-00', 8109.00, 479.31, 15976.87, 1, 'STAND 63 - 64', 70, 1, '11263289', 29.79, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 624, 63, NULL),
(2771, 3, 2, '97000004', '0000-00-00', 8215.00, 485.57, 16185.72, 1, 'STAND 60', 70, 1, '11263289', 35.95, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 624, 60, NULL),
(2772, 3, 2, '97000002', '0000-00-00', 14612.40, 744.29, 24809.60, 1, 'COMERCIO', 60, 1, '11263289', 39.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2773, 3, 2, '97000002', '0000-00-00', 20776.00, 0.00, 46431.30, 1, NULL, 60, 1, '11263289', 78.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 25, NULL),
(2774, 3, 2, '97000002', '0000-00-00', 11734.20, 0.00, 26224.21, 1, NULL, 60, 1, '11263289', 44.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 16, NULL),
(2775, 3, 2, '97000002', '0000-00-00', 11209.50, 0.00, 25051.58, 1, NULL, 60, 1, '11263289', 42.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 13, NULL),
(2776, 3, 2, '97000002', '0000-00-00', 18698.40, 0.00, 41788.17, 1, NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 28, NULL),
(2777, 3, 2, '97000002', '0000-00-00', 16761.25, 0.00, 37458.93, 1, NULL, 60, 1, '11263289', 63.25, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 23, NULL),
(2778, 3, 2, '97000002', '0000-00-00', 8763.55, 0.00, 19585.25, 1, NULL, 60, 1, '11263289', 33.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 10, NULL),
(2779, 3, 2, '97000002', '0000-00-00', 18698.40, 0.00, 41788.17, 1, NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 2000, 1, 621, 26, NULL),
(2780, 3, 2, '97000002', '0000-00-00', 1869.40, 0.00, 41788.17, 1, NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 2000, 1, 621, 27, NULL),
(2781, 3, 2, '97000002', '0000-00-00', 9412.80, 0.00, 21036.22, 1, NULL, 60, 1, '11263289', 35.52, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 9, NULL),
(2782, 3, 2, '97000002', '0000-00-00', 10883.55, 0.00, 24323.13, 1, NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 15, NULL),
(2783, 3, 2, '97000002', '0000-00-00', 11050.50, 0.00, 24696.24, 1, NULL, 60, 1, '11263289', 41.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 17, NULL),
(2784, 3, 2, '97000002', '0000-00-00', 9693.70, 0.00, 21663.99, 1, NULL, 60, 1, '11263289', 36.58, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 12, NULL),
(2785, 3, 2, '97000002', '0000-00-00', 19663.00, 0.00, 41450.35, 1, NULL, 60, 1, '11263289', 74.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, NULL),
(2786, 3, 2, '97000002', '0000-00-00', 20776.00, 0.00, 47615.38, 1, NULL, 60, 1, '11263289', 78.40, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 8, NULL),
(2787, 3, 2, '97000002', '0000-00-00', 10883.55, 0.00, 24323.13, 1, NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 20, NULL),
(2788, 3, 2, '97000002', '0000-00-00', 10883.55, 0.00, 24323.13, 1, NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 19, NULL),
(2789, 3, 2, '97000002', '0000-00-00', 21472.95, 0.00, 44317.41, 1, NULL, 60, 1, '11263289', 81.03, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2790, 3, 2, '97000002', '0000-00-00', 13393.10, 0.00, 27641.64, 1, NULL, 60, 1, '11263289', 50.54, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, NULL),
(2791, 3, 2, '97000002', '0000-00-00', 11734.20, 0.00, 26551.45, 1, NULL, 60, 1, '11263289', 44.28, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 18, NULL),
(2792, 3, 2, '97000002', '0000-00-00', 10883.55, 0.00, 22462.25, 1, NULL, 60, 1, '11263289', 41.07, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 21, NULL),
(2793, 3, 2, '97000002', '0000-00-00', 15878.80, 0.00, 32771.81, 1, NULL, 60, 1, '11263289', 59.92, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 22, NULL),
(2794, 3, 2, '97000002', '0000-00-00', 18364.50, 0.00, 37901.97, 1, NULL, 60, 1, '11263289', 69.30, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 24, NULL),
(2795, 3, 2, '97000002', '0000-00-00', 18698.40, 0.00, 38591.10, 1, NULL, 60, 1, '11263289', 70.56, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 29, NULL),
(2796, 3, 2, '97000002', '0000-00-00', 11315.50, 0.00, 25868.56, 1, NULL, 60, 1, '11263289', 42.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 14, NULL),
(2797, 3, 2, '97000002', '0000-00-00', 9693.70, 0.00, 19269.94, 1, NULL, 60, 1, '11263289', 36.58, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 11, NULL),
(2798, 3, 2, '97000005', '0000-00-00', 143765.16, 9229.05, 307634.97, 1, 'CENTRO MEDICO SALUD VIDA', 69, 1, '11263289', 542.51, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 6, NULL),
(2799, 3, 2, '97000005', '0000-00-00', 24610.00, 0.00, 70312.22, 1, 'CASA', 69, 1, 'P.ELECT.                      ', 214.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2800, 3, 2, '98000001', '0000-00-00', 40871.04, 0.00, 40871.04, 1, NULL, 69, 1, '11000661', 425.74, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2801, 3, 2, '97000003', '0000-00-00', 817.08, 101.97, 3398.96, 1, 'COMERCIO', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 29, NULL),
(2802, 3, 2, '97000003', '0000-00-00', 1591.92, 203.45, 6781.78, 1, 'COMERCIO', 76, 1, '10125537', 12.06, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 17, NULL),
(2803, 3, 2, '97000003', '0000-00-00', 33734.80, 1653.14, 55104.75, 1, 'COMERCIO', 76, 1, '10125537', 69.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2804, 3, 2, '97000003', '0000-00-00', 64130.00, 3141.99, 104732.91, 1, 'COMERCIO', 76, 1, '10125537', 132.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2805, 3, 2, '97000003', '0000-00-00', 10807.72, 736.37, 24545.55, 1, 'COMERCIO', 76, 1, '10125537', 22.33, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2806, 3, 2, '97000003', '0000-00-00', 27757.40, 1895.11, 63170.46, 1, 'COMERCIO', 76, 1, '10125537', 57.35, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, NULL),
(2807, 3, 2, '97000003', '0000-00-00', 1606.44, 200.52, 6683.97, 1, 'COMERCIO', 76, 1, '10125537', 12.17, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2808, 3, 2, '97000003', '0000-00-00', 1515.36, 189.16, 6305.48, 1, 'COMERCIO', 76, 1, '10125537', 11.48, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 11, NULL),
(2809, 3, 2, '97000003', '0000-00-00', 984.72, 122.95, 4098.30, 1, 'COMERCIO', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 14, NULL),
(2810, 3, 2, '97000003', '0000-00-00', 984.72, 0.00, 4586.71, 1, 'CASA HABITACION', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 639, NULL, NULL),
(2811, 3, 2, '97000003', '0000-00-00', 3784.44, 483.64, 16121.47, 1, 'COMERCIO                                ', 76, 1, '10125537', 0.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 639, NULL, NULL),
(2812, 3, 2, '97000003', '0000-00-00', 1261.92, 157.49, 5249.70, 1, 'COMERCIO                                ', 76, 1, '10125537', 9.56, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 8, NULL),
(2813, 3, 2, '97000003', '0000-00-00', 984.72, 122.95, 4098.30, 1, 'COMERCIO                                ', 76, 1, '10125537', 7.46, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 16, NULL),
(2814, 3, 2, '97000003', '0000-00-00', 894.96, 111.63, 3721.13, 1, 'COMERCIO                                ', 76, 1, '10125537', 6.78, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 21, NULL),
(2815, 3, 2, '97000003', '0000-00-00', 850.08, 86.92, 2897.37, 1, 'COMERCIO                                ', 76, 1, '10125537', 6.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 5, NULL),
(2816, 3, 2, '97000003', '0000-00-00', 850.08, 106.12, 3537.34, 1, 'COMERCIO                                ', 76, 1, '10125537', 6.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 4, NULL),
(2817, 3, 2, '97000003', '0000-00-00', 1515.36, 193.74, 6458.08, 1, 'SERVICIOS                               ', 76, 1, '10125537', 11.48, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 0, 621, 9, NULL),
(2818, 3, 2, '97000003', '0000-00-00', 1743.72, 217.57, 7252.36, 1, 'SERVICIOS                               ', 76, 1, '10125537', 13.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, NULL),
(2819, 3, 2, '97000003', '0000-00-00', 1458.60, 182.00, 6066.70, 1, 'COMERCIO                                ', 76, 1, '10125537', 11.05, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 10, NULL),
(2820, 3, 2, '97000003', '0000-00-00', 1677.72, 214.48, 7149.31, 1, 'SERVICIOS                               ', 76, 1, '10125537', 12.71, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, NULL),
(2821, 3, 2, '97000003', '0000-00-00', 3247.20, 405.23, 13507.64, 1, 'SERVICIOS                               ', 76, 1, '10125537', 24.60, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, NULL),
(2822, 3, 2, '97000003', '0000-00-00', 4775.76, 610.36, 20345.33, 1, 'COMERCIO                                ', 76, 1, '10125537', 36.18, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, NULL, NULL),
(2823, 3, 2, '97000003', '0000-00-00', 817.08, 101.97, 3398.96, 1, 'COMERCIO                                ', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 27, NULL),
(2824, 3, 2, '97000003', '0000-00-00', 817.08, 101.97, 3398.96, 1, 'COMERCIO                                ', 76, 1, '10125537', 6.19, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 28, NULL),
(2825, 3, 2, '97000003', '0000-00-00', 6616.28, 0.00, 12316.52, 1, 'CASA HABITACION                         ', 76, 1, '10125537', 13.67, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 23, NULL),
(2826, 3, 2, '97000003', '0000-00-00', 1968.12, 245.57, 8185.70, 1, 'COMERCIO                                ', 76, 1, '10125537', 14.94, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 22, NULL),
(2827, 3, 2, '97000003', '0000-00-00', 1623.60, 202.61, 6753.82, 1, 'COMERCIO                                ', 76, 1, '10125537', 12.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1977, 1, 621, 24, NULL),
(2828, 3, 2, '97000003', '0000-00-00', 5691.84, 340.99, 11366.36, 1, 'COMERCIO                                ', 76, 1, '10125537', 11.76, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, NULL),
(2829, 3, 2, '97000003', '0000-00-00', 3032.04, 378.37, 12612.28, 1, 'COMERCIO                                ', 76, 1, '10125537', 22.97, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 621, NULL, NULL),
(2830, 3, 2, '97000003', '0000-00-00', 539660.00, 19272.62, 642420.69, 1, 'TEATRO DOS DE MAYO                      ', 76, 1, '10125537', 1115.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2831, 3, 2, '97000005', '0000-00-00', 18265.50, 777.56, 25918.52, 1, 'COMERCIO                                ', 69, 1, '                              ', 49.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, NULL),
(2832, 3, 2, '97000005', '0000-00-00', 22878.00, 1368.17, 45605.59, 1, 'COMERCIO                                ', 69, 1, '                              ', 62.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, NULL, NULL, NULL),
(2833, 3, 2, '97000005', '0000-00-00', 78450.24, 2623.71, 87457.02, 1, 'COMERCIO                                ', 69, 1, '10125899', 144.21, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2834, 3, 2, '97000005', '0000-00-00', 47490.30, 1907.95, 63598.26, 1, 'COMERCIO                                ', 69, 1, '11263289', 128.70, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2835, 3, 2, '97000005', '0000-00-00', 27783.00, 1075.75, 35858.21, 1, 'SERVICIOS                               ', 69, 1, '11061926', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2836, 3, 2, '97000002', '0000-00-00', 6492.50, 465.98, 15532.83, 1, 'COMERCIO                                ', 60, 1, '11263289', 24.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2837, 3, 2, '97000003', '0000-00-00', 7436.00, 427.77, 14258.88, 1, 'COMERCIO                                ', 76, 1, '10125537', 22.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2838, 3, 2, '97000003', '0000-00-00', 3974.88, 138.13, 4604.25, 1, 'COMERCIO                                ', 76, 1, '10125537', 11.76, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, NULL),
(2839, 3, 2, '97000005', '0000-00-00', 97812.00, 3688.21, 122940.22, 1, 'COMERCIO                                ', 69, 1, '10125899', 247.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2840, 3, 2, '97000005', '0000-00-00', 28386.00, 0.00, 36619.39, 1, 'CASA HABITACION                         ', 69, 1, '11061924', 166.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2841, 3, 2, '97000005', '0000-00-00', 7128.00, 540.73, 18024.46, 1, 'COMERCIO                                ', 69, 1, '10125629', 18.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, NULL),
(2842, 3, 2, '97000005', '0000-00-00', 7920.00, 268.86, 8962.12, 1, 'COMERCIO                                ', 69, 1, '10125629', 20.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, NULL),
(2843, 3, 2, '97000005', '0000-00-00', 28488.60, 1101.66, 36721.99, 1, 'SERVICIOS                               ', 69, 1, '11061921', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2844, 3, 2, '97000005', '0000-00-00', 4867.20, 166.57, 5552.22, 1, 'COMERCIO                                ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 1, NULL),
(2845, 3, 2, '97000005', '0000-00-00', 6906.72, 354.13, 11804.22, 1, 'SERVICIOS                               ', 69, 1, '10165934', 20.44, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 6, NULL),
(2846, 3, 2, '97000005', '0000-00-00', 6908.72, 0.00, 11804.22, 1, 'CASA HABITACIÓN                         ', 69, 1, '10165934', 20.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 9, NULL),
(2847, 3, 2, '97000005', '0000-00-00', 5847.40, 299.73, 9990.85, 1, 'COMERCIO                                ', 69, 1, '10165934', 17.30, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 8, NULL),
(2848, 3, 2, '97000005', '0000-00-00', 17820.00, 987.63, 32921.01, 1, 'COMERCIO                                ', 69, 1, '10125866', 45.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2849, 3, 2, '97000005', '0000-00-00', 4867.20, 0.00, 5552.22, 1, 'CASA HABITACIÓN                         ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 3, NULL),
(2850, 3, 2, '97000005', '0197-01-01', 67205.76, 2247.65, 74921.57, 1, 'COMERCIO                                ', 69, 1, '10125866', 123.54, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 197, 1, 622, NULL, NULL),
(2851, 3, 2, '97000005', '0000-00-00', 95087.52, 0.00, 105569.83, 1, 'CASA HABITACIÓN                         ', 69, 1, '10125869', 240.12, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2852, 3, 2, '97000005', '0000-00-00', 7480.10, 0.00, 44781.12, 1, 'CASA HABITACIÓN                         ', 69, 1, '11009395', 57.10, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2853, 3, 2, '97000005', '0000-00-00', 28488.60, 0.00, 36721.99, 1, 'CASA HABITACIÓN                         ', 69, 1, '11061923', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2854, 3, 2, '97000005', '0000-00-00', 27783.00, 0.00, 35140.32, 1, 'CASA HABITACIÓN                         ', 69, 1, '11061928', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2855, 3, 2, '97000005', '0000-00-00', 27783.00, 0.00, 34787.86, 1, 'CASA HABITACION                         ', 69, 1, '11061927', 189.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2856, 3, 2, '97000005', '0000-00-00', 92510.00, 1758.94, 58631.20, 1, 'COMERCIO                                ', 69, 1, '10126377', 290.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 629, 136, NULL),
(2857, 3, 2, '97000002', '0000-00-00', 19663.00, 1163.10, 38769.87, 1, 'COMERCIO                                ', 60, 1, '11263289', 74.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, NULL),
(2858, 3, 2, '97000005', '0000-00-00', 8736.00, 0.00, 15735.84, 1, 'CASA HABITACIÓN                         ', 69, 1, '2192719', 84.00, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2859, 3, 2, '97000002', '0000-00-00', 44722.80, 2701.05, 90034.87, 1, 'COMERCIO                                ', 60, 1, '11263289', 121.20, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 628, NULL, NULL),
(2860, 3, 2, '97000005', '0000-00-00', 5900.00, 217.14, 7237.95, 1, 'COMERCIO                                ', 69, 1, '11263289', 25.00, 'LUIS GONZALES N° 645 - OF. A       ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, NULL),
(2861, 3, 2, '97000005', '0000-00-00', 7830.48, 288.19, 9606.21, 1, 'COMERCIO                                ', 69, 1, '11263289', 33.18, 'LUIS GONZALES N° 645 - OF. A       ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, NULL),
(2862, 3, 2, '97000005', '0000-00-00', 14514.50, 638.07, 21268.98, 1, 'COMERCIO                                ', 69, 1, '10126378', 45.50, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 622, NULL, NULL),
(2863, 3, 2, '97000005', '0000-00-00', 5847.40, 0.00, 9990.85, 1, 'CASA HABITACION                         ', 69, 1, '10165934', 17.30, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 5, NULL),
(2864, 3, 2, '97000002', '0000-00-00', 12793.23, 711.03, 23701.04, 1, 'COMERCIO                                ', 60, 1, '11263289', 34.67, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2865, 3, 2, '97000005', '0000-00-00', 11640.72, 410.66, 13688.66, 1, 'COMERCIO                                ', 69, 1, '10165934', 34.44, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2866, 3, 2, '97000005', '0000-00-00', 4867.20, 169.14, 5637.86, 1, 'COMERCIO                                ', 69, 1, '10165934', 14.40, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 621, 1, NULL),
(2867, 3, 2, '97000005', '0000-00-00', 6854.64, 351.35, 11711.82, 1, 'COMERCIO                                ', 69, 1, '10165934', 20.28, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 623, 7, NULL),
(2868, 3, 2, '97000005', '0000-00-00', 32604.00, 0.00, 37330.20, 1, 'CASA HABITACIÓN                         ', 69, 1, 'P.ELECT. 02212350             ', 247.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2869, 3, 2, '97000005', '0000-00-00', 28488.60, 0.00, 36721.99, 1, 'CASA HABITACIÓN                         ', 69, 1, '11061920', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2870, 3, 2, '97000005', '0000-00-00', 28488.60, 0.00, 36721.99, 1, 'CASA HABITACIÓN                         ', 69, 1, '11061919', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2871, 3, 2, '97000005', '0000-00-00', 28488.60, 0.00, 36721.99, 1, 'CASA HABITACIÓN                         ', 69, 1, '11061923', 166.60, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2872, 3, 2, '97000002', '0000-00-00', 16704.63, 495.87, 16528.85, 1, 'COMERCIO                                ', 60, 1, '11263289', 45.27, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2873, 3, 2, '97000005', '0000-00-00', 15757.78, 2499.87, 20833.04, 1, 'COCHERA CARROZAS                        ', 69, 1, '2192722', 215.86, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(2874, 3, 2, '97000005', '0000-00-00', 3601013.00, 497131.22, 4142760.75, 1, 'HOSPITAL REGIONAL DOCENTE \"LAS MERCEDES\"', 69, 1, '11263289', 15258.53, 'AUTOVALUO 2012                     ', '0000-00-00 00:00:00', 2, 1997, 0, 625, NULL, NULL),
(2875, 3, 2, '97000002', '0000-00-00', 15424.20, 816.26, 27208.71, 1, 'SERVICIOS                               ', 60, 1, '11263289', 41.80, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(2876, 3, 2, '97000005', '0000-00-00', 83475.00, 8572.25, 285741.59, 1, 'MUTUAL CHICLAYO                         ', 69, 1, '11263289', 315.00, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, NULL),
(2877, 3, 2, '97000005', '0000-00-00', 27321.50, 0.00, 58975.50, 1, 'DEPARTAMENTO                      ', 69, 1, '11263289', 103.10, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 621, 1, NULL),
(2878, 3, 2, '97000005', '0000-00-00', 369542.50, 30142.18, 1004739.19, 1, 'OFICINAS ADMINISTRATIVAS SBCH           ', 69, 1, '11263289', 1394.50, 'AUTOVALUO 2011.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 621, NULL, NULL),
(2879, 3, 2, '97000005', '0000-00-00', 71303.44, 2744.39, 91479.70, 1, 'OFICINAS ADMINISTRATIVAS SERFIN         ', 69, 1, '2184126', 327.08, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 623, NULL, NULL),
(2880, 3, 2, '97000005', '0000-00-00', 58440.20, 2781.69, 92723.09, 1, 'CENTRO EMERGENCIA MUJER                 ', 69, 1, '10165933', 172.90, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2881, 3, 2, '98000001', '0000-00-00', 97125.00, 0.00, 97125.00, 1, 'SBCH                                    ', 69, 1, 'P.ELECT. 11000661             ', 1312.50, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 2000, 0, NULL, NULL, NULL),
(2882, 3, 2, '97000005', '0000-00-00', 1388576.62, 843367.12, 7028058.00, 1, 'DOMEL                                   ', 69, 1, '11050317', 14464.34, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 1902, NULL, NULL),
(2883, 3, 2, '97000005', '0000-00-00', 11281.25, 1569.33, 52311.04, 1, 'CASA REFUGIO                            ', 69, 1, '10124737', 118.75, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 620, NULL, NULL),
(2884, 3, 2, '97000005', '0000-00-00', 32161.05, 3189.10, 106303.49, 1, 'ASILO DE ANCIANOS                       ', 69, 1, '10126244', 132.35, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 0, 620, NULL, NULL),
(2885, 3, 2, '97000002', '0000-00-00', 1921.25, 127.72, 4257.22, 1, 'COMERCIO                                ', 60, 1, '11263289', 7.25, 'AUTOVALUO 2012.                    ', '0000-00-00 00:00:00', 2, 1997, 1, 622, NULL, NULL),
(4059, 3, 2, '97000005', '0000-00-00', 11174372.00, 0.00, 11174372.00, 2, 'FINCA URBANA HOSPITAL LAS MERCEDES', 19, 1, 'PLLA ELECTRONICA 11009997     ', 23431.24, '                                   ', '0000-00-00 00:00:00', 2, 1945, 0, NULL, NULL, NULL),
(4655, 1, 2, '97000002', '0000-00-00', 9693.70, 0.00, 0.00, 1, 'AZOTEA DE EDIF. PIEDRA LORA', 19, 1, 'P.ELECT. 11009997', NULL, NULL, '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL),
(4656, 1, 2, '97000004', '0000-00-00', 8215.00, 0.00, 0.00, 1, 'STAND 6', 70, 1, 'P.ELECT. 11263289', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 6, NULL),
(4657, 1, 2, '97000004', '0000-00-00', 7800.00, 0.00, 0.00, 1, 'STAND 21', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 1, 624, 21, NULL),
(4658, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 21 A', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 21, NULL),
(4659, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 45', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1977, 0, 624, 45, NULL),
(4660, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND. 12-A / 3-A', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, NULL, NULL),
(4661, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-A', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 1, 624, 41, NULL),
(4662, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-B', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, NULL),
(4663, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-C', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, NULL),
(4664, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-D', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, NULL),
(4665, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-E', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, NULL, NULL),
(4666, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-F', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, NULL),
(4667, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 41-G', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 41, NULL),
(4668, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 39-40', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 39, NULL),
(4669, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 25', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2007, 0, 624, 25, NULL),
(4670, 1, 2, '97000005', '0000-00-00', 25000.00, 0.00, 0.00, 1, 'CASA', 19, 1, '1', 50.00, NULL, '0000-00-00 00:00:00', 2, 2000, 0, 620, NULL, NULL),
(4671, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 12-B / 3-B', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 2000, 1, 624, 12, NULL),
(4672, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 62', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 2000, 0, 624, 62, NULL),
(4673, 1, 2, '97000003', '0000-00-00', 150000.00, 0.00, 0.00, 1, 'TIENDA', 19, 1, '1', 45.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 622, NULL, NULL),
(4674, 1, 2, '97000005', '0000-00-00', 80000.00, 0.00, 0.00, 1, 'PARTE EXTERNA ', 19, 1, '1', 150.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 1902, NULL, NULL),
(4675, 1, 2, '97000005', '0000-00-00', 15000.00, 0.00, 0.00, 1, 'OFICINA 8', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 623, 8, NULL),
(4676, 1, 2, '97000005', '0000-00-00', 25000.00, 0.00, 0.00, 1, 'OFICINA 4', 19, 1, '1', 60.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 623, 4, NULL),
(4677, 1, 2, '97000005', '0000-00-00', 25000.00, 0.00, 0.00, 1, 'OFICINA 2', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 623, 2, NULL),
(4678, 1, 2, '97000005', '0000-00-00', 25000.00, 0.00, 0.00, 1, 'TIENDA', 19, 1, '1', 25.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 622, NULL, NULL),
(4679, 1, 2, '97000004', '0000-00-00', 8000.00, 0.00, 0.00, 1, 'STAND 22-A', 19, 1, '1', 15.00, NULL, '0000-00-00 00:00:00', 2, 1990, 0, 624, 22, NULL),
(4680, 1, 2, '97000003', '0000-00-00', 15000.00, 0.00, 0.00, 1, 'DEPARTAMENTO 41-42', 19, 1, '1', 35.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 621, NULL, NULL),
(4681, 1, 2, '97000005', '0000-00-00', 50000.00, 0.00, 0.00, 1, 'CASA', 19, 1, '1', 100.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, NULL),
(4682, 1, 2, '97000005', '0000-00-00', 50000.00, 0.00, 0.00, 1, 'CASA', 19, 1, '1', 100.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, NULL),
(4683, 1, 2, '97000003', '0000-00-00', 50000.00, 0.00, 0.00, 1, 'CASA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, NULL, NULL),
(4684, 1, 2, '97000003', '0000-00-00', 50000.00, 0.00, 0.00, 1, 'CASA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 620, 40, NULL),
(4685, 1, 2, '97000003', '0000-00-00', 80000.00, 0.00, 0.00, 1, 'TIENDA', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 622, NULL, NULL),
(4686, 1, 2, '97000005', '0000-00-00', 80000.00, 0.00, 0.00, 1, 'OF. 2', 19, 1, '1', 80.00, NULL, '0000-00-00 00:00:00', 2, 1980, 0, 623, 2, NULL),
(4689, 1, 2, '98000001', '0000-00-00', 2946726.75, 0.00, 0.00, 4, 'CONTRATO COMPRA/VENTA TERRENO PREDIO RURAL CE-61603/CN 9245920, UBICACION RURAL PREDIO LAS PAMPAS/VALLE CHANCAY/LAMBAYEQUE', 0, 0, '', 0.00, '', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, NULL),
(4690, 1, 2, '98000001', '0000-00-00', 989100.00, 0.00, 0.00, 4, 'CONTRATO COMPRA/VENTA DE PREDIO RURAL A FAVOR DE BENEFICENCIA DE CHICLAYO, UBICADO PAMPAS DE PIMENTEL/VALLE CHANCAY/LAMBAYEQUE, ÁREA HA. 1.0000 UC-116499 PARTIDA 11183758', 2, 1, 'PARTIDA 11183758', 1.00, 'ESCRITURA PUBLICA N° 2603 DEL 19/07/2018, ANTE NOTARIO ANTONIO VERA MENDEZ -CHICLAYO, TITULO PRESENTADO EL 31/07/2018 BAJO EL N° 2018-01702419, TOMO DIARIO 0030', '0000-00-00 00:00:00', 2, 2018, 0, 639, NULL, NULL),
(4766, 1, 2, '97000005', '0000-00-00', 500.00, 0.00, 0.00, 1, 'ELIAS AGUIRRE N° 248 - AREA DE PARQUEO', 69, 1, '11263289', 20.00, NULL, '0000-00-00 00:00:00', 2, 1997, 0, 639, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `idempresa`, `nombre`, `imagen`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'Administrador', 'static/media/E_1/U1/rol/imagen20201004152420.png', 1, 1, '2020-08-09 11:24:39', '2020-12-01 23:53:48'),
(2, 2, 'Administrador', NULL, 1, 1, '2020-12-01 23:54:12', '2020-12-01 23:54:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sys_configuracion`
--

CREATE TABLE `sys_configuracion` (
  `config` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autocargar` enum('si','no') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sys_configuracion`
--

INSERT INTO `sys_configuracion` (`config`, `valor`, `autocargar`) VALUES
('multiidioma', 'SI', 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabla`
--

CREATE TABLE `tabla` (
  `tabl_id` int(11) NOT NULL,
  `tabl_tipo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabl_codigo` int(11) NOT NULL,
  `tabl_descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabl_fecharegistro` datetime NOT NULL DEFAULT current_timestamp(),
  `tabl_descripaux` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabl_codigoauxiliar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabla`
--

INSERT INTO `tabla` (`tabl_id`, `tabl_tipo`, `tabl_codigo`, `tabl_descripcion`, `tabl_fecharegistro`, `tabl_descripaux`, `tabl_codigoauxiliar`) VALUES
(50, 'TIPO_PROVEEDOR', 1, 'PERSONA JURIDICA', '0000-00-00 00:00:00', '', NULL),
(51, 'TIPO_PROVEEDOR', 2, 'PERSONA NATURAL', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tester_demo`
--

CREATE TABLE `tester_demo` (
  `idtest` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `textarea` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idpadre` int(11) DEFAULT NULL,
  `orden` tinyint(4) NOT NULL DEFAULT 1,
  `imagen` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT current_timestamp(),
  `fecha_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tester_demo`
--

INSERT INTO `tester_demo` (`idtest`, `nombre`, `textarea`, `color`, `estado`, `idpadre`, `orden`, `imagen`, `fecha_insert`, `fecha_update`) VALUES
(1, 'asdasd', 'asdasdasd', 'rgba(198,38,38,0.59)', 1, NULL, 1, 'static/media/E_1/U1/tester_demo/imagen.png', '2020-12-01 23:14:38', '2020-12-01 23:14:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  ADD PRIMARY KEY (`idbiblioteca`),
  ADD KEY `fk_biblioteca_empresa` (`idempresa`),
  ADD KEY `fk_biblioteca_persona` (`idpersona`);

--
-- Indices de la tabla `catalogo_sbn`
--
ALTER TABLE `catalogo_sbn`
  ADD PRIMARY KEY (`casb_codigo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`clie_id`);

--
-- Indices de la tabla `dependencia`
--
ALTER TABLE `dependencia`
  ADD PRIMARY KEY (`depe_id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indices de la tabla `empresa_config`
--
ALTER TABLE `empresa_config`
  ADD PRIMARY KEY (`idempresaconfig`),
  ADD KEY `idempresa` (`idempresa`);

--
-- Indices de la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  ADD PRIMARY KEY (`idempresamodulo`),
  ADD KEY `fk_empresa_modulo_empresa` (`idempresa`),
  ADD KEY `fkempresa_modulos_modulos` (`idmodulo`);

--
-- Indices de la tabla `empresa_smtp`
--
ALTER TABLE `empresa_smtp`
  ADD PRIMARY KEY (`idempresasmtp`),
  ADD KEY `fk_modulos_empresa_smtp` (`idempresa`);

--
-- Indices de la tabla `galeria_fotos`
--
ALTER TABLE `galeria_fotos`
  ADD KEY `rebi_id` (`rebi_id`);

--
-- Indices de la tabla `historial_sesion`
--
ALTER TABLE `historial_sesion`
  ADD PRIMARY KEY (`idhistorial`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`idmodulo`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- Indices de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  ADD PRIMARY KEY (`idpersonarol`),
  ADD KEY `fk_persona_rol_persona` (`idpersona`),
  ADD KEY `fk_persona_rol_rol` (`idrol`);

--
-- Indices de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  ADD PRIMARY KEY (`rebi_id`),
  ADD KEY `casb_codigo` (`casb_codigo`),
  ADD KEY `tabl_tipo_interior` (`tabl_tipo_interior`),
  ADD KEY `depe_id_ubica` (`depe_id_ubica`),
  ADD KEY `depe_id` (`depe_id`),
  ADD KEY `tabl_tipo_interior_2` (`tabl_tipo_interior`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `sys_configuracion`
--
ALTER TABLE `sys_configuracion`
  ADD PRIMARY KEY (`config`);

--
-- Indices de la tabla `tabla`
--
ALTER TABLE `tabla`
  ADD PRIMARY KEY (`tabl_id`);

--
-- Indices de la tabla `tester_demo`
--
ALTER TABLE `tester_demo`
  ADD PRIMARY KEY (`idtest`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  MODIFY `idbiblioteca` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `clie_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idempresa` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empresa_config`
--
ALTER TABLE `empresa_config`
  MODIFY `idempresaconfig` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  MODIFY `idempresamodulo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `empresa_smtp`
--
ALTER TABLE `empresa_smtp`
  MODIFY `idempresasmtp` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial_sesion`
--
ALTER TABLE `historial_sesion`
  MODIFY `idhistorial` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `idmodulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona_rol`
--
ALTER TABLE `persona_rol`
  MODIFY `idpersonarol` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  MODIFY `rebi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4767;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tabla`
--
ALTER TABLE `tabla`
  MODIFY `tabl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `tester_demo`
--
ALTER TABLE `tester_demo`
  MODIFY `idtest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  ADD CONSTRAINT `fkempresa_modulos_modulos` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`idmodulo`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  ADD CONSTRAINT `fk_registro_bien_dependencia` FOREIGN KEY (`depe_id_ubica`) REFERENCES `dependencia` (`depe_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

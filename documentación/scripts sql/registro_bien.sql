-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2020 a las 18:02:44
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alquileres`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_bien`
--

CREATE TABLE `registro_bien` (
  `id_registro_bien` int(11) NOT NULL,
  `tipo_bien` int(11) NOT NULL DEFAULT 1,
  `estado` smallint(6) NOT NULL,
  `condicion` smallint(6) NOT NULL,
  `nombre` text NOT NULL,
  `registros_publicos` varchar(50) NOT NULL,
  `area` float(14,2) NOT NULL,
  `observaciones` text NOT NULL,
  `fecha_adquisicion` date NOT NULL,
  `valor_adquisicion` float(14,2) NOT NULL,
  `valor_actual` float(14,2) NOT NULL,
  `id_dependencia` int(11) NOT NULL,
  `imagen_principal` varchar(50) NOT NULL,
  `activo` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  ADD PRIMARY KEY (`id_registro_bien`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registro_bien`
--
ALTER TABLE `registro_bien`
  MODIFY `id_registro_bien` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

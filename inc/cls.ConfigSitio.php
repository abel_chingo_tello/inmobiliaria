<?php
/**
 * @autor		Abel Chingo Tello ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class ConfigSitio extends JrConfiguracion
{
	public function __construct()
	{
		parent::__construct();
		self::$configs['nombre_sitio'] = '';
		self::$configs['url_corta'] = true;
		self::$configs['tema'] = !empty(_mitema_)?_mitema_:'tema1';
		self::$configs['sitio'] = !empty(_sitio_)?_sitio_:DEFAULTMODULE;
		self::$configs['plantilla'] = 'index';
		self::$configs['url_static'] = URL_BASE.'static';		
		self::$configs['url_base'] = URL_BASE;
		self::$configs['url_media'] = URL_BASE.'static/media';
	}	
	public static function getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}	
	public static function set($propiedad, $valor)
	{
		//TinkuConfiguracion::getInstancia();		
		if(isset(self::$configs[$propiedad])) {
			self::$configs[$propiedad] = $valor;
		}
	}
}
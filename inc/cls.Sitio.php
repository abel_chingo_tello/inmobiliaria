<?php
/**
 * @autor		Abel Chingo Tello : ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016 Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();
class Sitio extends JrAplicacion{
	public static $msjs;
	private $BD = null;
	public function __construct()
	{		
		parent::__construct();
	}
	
	public function iniciar($compat = array())
	{	
		
		if(true === $this->inicio){
			return;
		}	
		JrCargador::clase('ACHT::JrModulo',RUTA_LIBS);
		JrCargador::clase('inc::ConfigSitio',RUTA_BASE);
		JrCargador::clase('sys_datos::DatBase',RUTA_BASE);
		JrCargador::clase('sys_negocio::NegSesion',RUTA_BASE);
		JrCargador::clase('sys_negocio::NegConfiguracion',RUTA_BASE);	
		$oConfigSitio = ConfigSitio::getInstancia();
		parent::iniciar($oConfigSitio->get_());
	}	
	public function enrutar($rec = null, $ax = null)
 	{
		/*$usuarioAct = null;
		if(NegSesion::existeSesion()){
			$usuarioAct = NegSesion::getUsuario();
		}*/
		$permitenologueado=array('sesion','persona','catalogo');
		$permiteempresa=array('tienda_productos');
		$_url=$_SERVER["HTTP_HOST"];
		$_url=str_replace("www.", '', $_url);
		$_haysubdomain=stripos($_url,".");
		$_urlsubdomain='empresa';
		if($_haysubdomain==true){
			$validar=substr($_url, 0,$_haysubdomain);
			$_bussubdomain=stripos($validar,"/");
			if($_bussubdomain===false){
				$_urlsubdomain=substr($_url,0,$_haysubdomain);
			}			
		}
		$sesion = JrSession::getInstancia();
		$keysesionempresa=$_urlsubdomain;		
		$datosempresa = $sesion->getAll('_infoempresa_');
		$subdominioguardado=!empty($datosempresa)?$datosempresa["subdominio"]:date('YmdHis');
		if(empty($subdominioguardado)||$subdominioguardado!=$_urlsubdomain){
			JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
			$oNegEmpresa = new Negempresa;
			$hayempresa=array();
			if(!empty($_haysubdomain)){				
				$hayempresa=$oNegEmpresa->configuracion(array('subdominio'=>$_urlsubdomain));
				//if(empty($hayempresa)) $rec='noexiste';
			}

			if(empty($hayempresa)){
				$hayempresa=$oNegEmpresa->configuracion(array('idempresa'=>IDEMPRESA));
			}
			$datosempresa = $sesion->getAll('_infoempresa_');
		}
		$documento =&JrInstancia::getDocumento();
		$idioma=!empty($datosempresa["idioma"])?$datosempresa["idioma"]:'ES';
		$documento->setIdioma($idioma);
		NegSesion::set('idioma',$idioma,'idioma__');
		//var_dump($rec,$ax);
		if(_sitio_==$rec && !empty($ax)){
			$rec=$ax;
			$url__=substr($_SERVER['REQUEST_URI'], 1);	
			$url__=explode('?', $url__);
			$url__=$url__[0];
			if(!empty($url__)){
				$url__=explode('/', $url__);
				if(!empty($url__[2])) {
					$ax=$url__[2];
				}else $ax='index';
			}else $ax='index';
		}
		$empresa=NegSesion::getEmpresa();		
		if(IS_LOGIN===true&&!NegSesion::existeSesion()&&!in_array($rec,$permitenologueado)&&!in_array($rec, $permiteempresa)){
			$empresa=NegSesion::setEmpresa('returnmodulo',!empty($rec)?$rec:_sitio_);
			parent::redir('json/sesion');
		}else{
			$ax=explode('?', $ax);
			$ax=$ax[0];			
			parent::enrutar($rec, $ax);
		}
	}
	public static function &getInstancia()
	{
		if(empty(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}

	public function error($msj, $plantilla = null)
	{
		//echo "error sitio";
		JrCargador::clase('WebExcepcion', RUTA_SITIO);
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->error($msj, $plantilla);
	}
	public function noencontrado()
	{
		JrCargador::clase('WebExcepcion', RUTA_SITIO);
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->noencontrado();
	}
}
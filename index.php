<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2020
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
define('SD', DIRECTORY_SEPARATOR);
require_once(dirname(__FILE__) . SD . "config.php");
require_once(dirname(__FILE__).SD .DEFAULTMODULE.SD.'index.php');
$aplicacion->iniciar();
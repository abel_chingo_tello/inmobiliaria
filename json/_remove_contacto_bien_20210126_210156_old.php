<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-01-2021 
 * @copyright	Copyright (C) 25-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegContacto_bien', RUTA_BASE);
class Contacto_bien extends JrWeb
{
	private $oNegContacto_bien;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegContacto_bien = new NegContacto_bien;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Contacto_bien', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id"])) $filtros["id"]=$_REQUEST["id"];

			if(isset($_REQUEST["rebi_id"])&&@$_REQUEST["rebi_id"]!='')$filtros["rebi_id"]=$_REQUEST["rebi_id"]; 
  			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"]; 
  			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"]; 
  			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"]; 
  			if(isset($_REQUEST["mensaje"])&&@$_REQUEST["mensaje"]!='')$filtros["mensaje"]=$_REQUEST["mensaje"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegContacto_bien->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($id)) {
				$this->oNegContacto_bien->id = $id;
				$accion='_edit';
			}

           	$this->oNegContacto_bien->rebi_id=@$rebi_id;
  			$this->oNegContacto_bien->email=@$email;
  			$this->oNegContacto_bien->nombre=@$nombre;
  			$this->oNegContacto_bien->telefono=@$telefono;
  			$this->oNegContacto_bien->dni=@$dni;
  			$this->oNegContacto_bien->mensaje=@$mensaje;
  				        
            if($accion=='_add') {
            	$res=$this->oNegContacto_bien->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Contacto_bien')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegContacto_bien->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Contacto_bien')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegContacto_bien->__set('id', $_REQUEST['id']);
			$res=$this->oNegContacto_bien->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegContacto_bien->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
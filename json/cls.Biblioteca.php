<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-08-2020 
 * @copyright	Copyright (C) 10-08-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBiblioteca', RUTA_BASE);
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Biblioteca extends JrWeb
{
	private $oNegBiblioteca;
	private $oNegEmpresa;
		
	public function __construct()
	{
		parent::__construct();	
		$this->oNegBiblioteca = new NegBiblioteca;
		$this->oNegEmpresa = new NegEmpresa;
				
	}

	public function index(){
		return $this->listado();
	}

	
	public function listado(){
		$this->documento->plantilla = 'blanco';
		try{

			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Biblioteca', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idbiblioteca"])&&@$_REQUEST["idbiblioteca"]!='')$filtros["idbiblioteca"]=$_REQUEST["idbiblioteca"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["archivo"])&&@$_REQUEST["archivo"]!='')$filtros["archivo"]=$_REQUEST["archivo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecha_insert"])&&@$_REQUEST["fecha_insert"]!='')$filtros["fecha_insert"]=$_REQUEST["fecha_insert"];
			if(isset($_REQUEST["fecha_update"])&&@$_REQUEST["fecha_update"]!='')$filtros["fecha_update"]=$_REQUEST["fecha_update"];						
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["tipo"])){
				if(!in_array($_REQUEST["tipo"], array('image','audio','video'))){
					$filtros["tipo"]=explode(',',$_REQUEST["tipo"]);
				}
			}	
			$this->datos=$this->oNegBiblioteca->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            $user = NegSesion::getUsuario();
            $empresa=NegSesion::getEmpresa();
            JrCargador::clase('ACHT::JrArchivo');

            $this->oNegBiblioteca->idempresa=$empresa["idempresa"];
            $datosarchivo=JrArchivo::subirarchivo($_FILES['media'],'static/media/E_'.$empresa["idempresa"]."/U".$user["idpersona"]."/biblioteca/","",$tipo);
            if($datosarchivo["code"]!=200){
            	echo json_encode($datosarchivo);
            	exit(0);
            }
			$this->oNegBiblioteca->idpersona=$user["idpersona"];
			$this->oNegBiblioteca->tipo=!empty($tipo)?$tipo:'image';			
			$this->oNegBiblioteca->estado=1;

	        if(!empty($idbiblioteca)) {
				$this->oNegBiblioteca->idbiblioteca = $idbiblioteca;
				$accion='_edit';
			}
			$this->oNegBiblioteca->nombre=@$datosarchivo["nombre"];
			$this->oNegBiblioteca->archivo=@$datosarchivo["link"];
			$datosarchivo["idpersona"]=$user["idpersona"];
			$datosarchivo["idempresa"]=$empresa["idempresa"];
            if($accion=='_add') {
            	$res=$this->oNegBiblioteca->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Biblioteca')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'data'=>$datosarchivo)); 
            }else{
            	$res=$this->oNegBiblioteca->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Biblioteca')).' '.JrTexto::_('update successfully'),'newid'=>$res,'data'=>$datosarchivo)); 
            }
            exit(0);            			
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function importar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            $jsondata=json_decode($datajson);
            $usuarioAct = NegSesion::getUsuario();
            if(!empty($jsondata)){
				$this->datos=array();
				$j=1;
				foreach($jsondata as $v){ // falta fotas las validaciones
				$this->oNegBiblioteca->idempresa=@$idempresa;
				$this->oNegBiblioteca->idpersona=@$idpersona;
				$this->oNegBiblioteca->tipo=@$tipo;
				$this->oNegBiblioteca->archivo=@$archivo;
				$this->oNegBiblioteca->nombre=@$nombre;
				$this->oNegBiblioteca->estado=@$estado;
				$this->oNegBiblioteca->fecha_insert=@$fecha_insert;
				$this->oNegBiblioteca->fecha_update=@$fecha_update;
				$newid=$this->oNegUgel->agregar();
				$this->datos[$j]=array('code'=>200,'id'=>$newid,'msj'=>JrTexto::_('saved successfully'),'data'=>$v);
				}
			}
			echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Min_dre')).' '.JrTexto::_('update successfully'),'data'=>$this->datos));  
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

		
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegBiblioteca->idbiblioteca=$_REQUEST['idbiblioteca'];
			$res=$this->oNegBiblioteca->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegBiblioteca->setCampo($_REQUEST['idbiblioteca'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
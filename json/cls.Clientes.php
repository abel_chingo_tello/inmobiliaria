<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2020 
 * @copyright	Copyright (C) 23-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegClientes', RUTA_BASE);
class Clientes extends JrWeb
{
	private $oNegClientes;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegClientes = new NegClientes;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Clientes', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["clie_id"])) $filtros["clie_id"]=$_REQUEST["clie_id"];

			if(isset($_REQUEST["tabl_tipocliente"])&&@$_REQUEST["tabl_tipocliente"]!='')$filtros["tabl_tipocliente"]=$_REQUEST["tabl_tipocliente"]; 
  			if(isset($_REQUEST["clie_apellidos"])&&@$_REQUEST["clie_apellidos"]!='')$filtros["clie_apellidos"]=$_REQUEST["clie_apellidos"]; 
  			if(isset($_REQUEST["clie_nombres"])&&@$_REQUEST["clie_nombres"]!='')$filtros["clie_nombres"]=$_REQUEST["clie_nombres"]; 
  			if(isset($_REQUEST["clie_razsocial"])&&@$_REQUEST["clie_razsocial"]!='')$filtros["clie_razsocial"]=$_REQUEST["clie_razsocial"]; 
  			if(isset($_REQUEST["clie_dni"])&&@$_REQUEST["clie_dni"]!='')$filtros["clie_dni"]=$_REQUEST["clie_dni"]; 
  			if(isset($_REQUEST["clie_direccion"])&&@$_REQUEST["clie_direccion"]!='')$filtros["clie_direccion"]=$_REQUEST["clie_direccion"]; 
  			if(isset($_REQUEST["clie_telefono"])&&@$_REQUEST["clie_telefono"]!='')$filtros["clie_telefono"]=$_REQUEST["clie_telefono"]; 
  			if(isset($_REQUEST["clie_email"])&&@$_REQUEST["clie_email"]!='')$filtros["clie_email"]=$_REQUEST["clie_email"]; 
  			if(isset($_REQUEST["clie_fregistro"])&&@$_REQUEST["clie_fregistro"]!='')$filtros["clie_fregistro"]=$_REQUEST["clie_fregistro"]; 
  			if(isset($_REQUEST["clie_estado"])&&@$_REQUEST["clie_estado"]!='')$filtros["clie_estado"]=$_REQUEST["clie_estado"]; 
  			if(isset($_REQUEST["clie_zona"])&&@$_REQUEST["clie_zona"]!='')$filtros["clie_zona"]=$_REQUEST["clie_zona"]; 
  			if(isset($_REQUEST["clie_empleado"])&&@$_REQUEST["clie_empleado"]!='')$filtros["clie_empleado"]=$_REQUEST["clie_empleado"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegClientes->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($clie_id)) {
				$this->oNegClientes->clie_id = $clie_id;
				$accion='_edit';
			}

           	$this->oNegClientes->tabl_tipocliente=@$tabl_tipocliente;
  			$this->oNegClientes->clie_apellidos=@$clie_apellidos;
  			$this->oNegClientes->clie_nombres=@$clie_nombres;
  			$this->oNegClientes->clie_razsocial=@$clie_razsocial;
  			$this->oNegClientes->clie_dni=@$clie_dni;
  			$this->oNegClientes->clie_direccion=@$clie_direccion;
  			$this->oNegClientes->clie_telefono=@$clie_telefono;
  			$this->oNegClientes->clie_email=@$clie_email;
  			$this->oNegClientes->clie_fregistro=@$clie_fregistro;
  			$this->oNegClientes->clie_estado=@$clie_estado;
  			$this->oNegClientes->clie_zona=@$clie_zona;
  			$this->oNegClientes->clie_empleado=@$clie_empleado;
  				        
            if($accion=='_add') {
            	$res=$this->oNegClientes->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Clientes')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegClientes->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Clientes')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegClientes->__set('clie_id', $_REQUEST['clie_id']);
			$res=$this->oNegClientes->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegClientes->setCampo($_REQUEST['clie_id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		05-02-2021 
 * @copyright	Copyright (C) 05-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegContacto_bien', RUTA_BASE);
class Contacto_bien extends JrWeb
{
	private $oNegContacto_bien;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegContacto_bien = new NegContacto_bien;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Contacto_bien', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id"])) $filtros["id"]=$_REQUEST["id"];

			if(isset($_REQUEST["tipo_documento"])&&@$_REQUEST["tipo_documento"]!='')$filtros["tipo_documento"]=$_REQUEST["tipo_documento"]; 
  			if(isset($_REQUEST["nro_documento"])&&@$_REQUEST["nro_documento"]!='')$filtros["nro_documento"]=$_REQUEST["nro_documento"]; 
  			if(isset($_REQUEST["apellidos"])&&@$_REQUEST["apellidos"]!='')$filtros["apellidos"]=$_REQUEST["apellidos"]; 
  			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"]; 
  			if(isset($_REQUEST["rebi_id"])&&@$_REQUEST["rebi_id"]!='')$filtros["rebi_id"]=$_REQUEST["rebi_id"]; 
  			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"]; 
  			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"]; 
  			if(isset($_REQUEST["mensaje"])&&@$_REQUEST["mensaje"]!='')$filtros["mensaje"]=$_REQUEST["mensaje"]; 
  			if(isset($_REQUEST["file_infocorp"])&&@$_REQUEST["file_infocorp"]!='')$filtros["file_infocorp"]=$_REQUEST["file_infocorp"]; 
  			if(isset($_REQUEST["file_recomendacion"])&&@$_REQUEST["file_recomendacion"]!='')$filtros["file_recomendacion"]=$_REQUEST["file_recomendacion"]; 
  			if(isset($_REQUEST["file_policial_judicial"])&&@$_REQUEST["file_policial_judicial"]!='')$filtros["file_policial_judicial"]=$_REQUEST["file_policial_judicial"]; 
  			if(isset($_REQUEST["file_extra_recomendacion"])&&@$_REQUEST["file_extra_recomendacion"]!='')$filtros["file_extra_recomendacion"]=$_REQUEST["file_extra_recomendacion"]; 
  			if(isset($_REQUEST["punataje"])&&@$_REQUEST["punataje"]!='')$filtros["punataje"]=$_REQUEST["punataje"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegContacto_bien->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public static function subirFile($name,$rutaFichero){
		if (isset($_FILES[$name]) && $_FILES[$name]['error'] === UPLOAD_ERR_OK) {
			$fileTmpPath = $_FILES[$name]['tmp_name'];
			$fileName = $_FILES[$name]['name'];
			$fileSize = $_FILES[$name]['size'];
			$fileType = $_FILES[$name]['type'];
			$fileNameCmps = explode(".", $fileName);
			$fileExtension = strtolower(end($fileNameCmps));
			$newFileName = md5(time() . $fileName) . '.' . $fileExtension;
			$allowedfileExtensions = array('xlsx', 'doc', 'docx', 'pdf');
			$uploadFileDir = $_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ.$rutaFichero;
			$dest_path = $uploadFileDir . $newFileName;
			if (in_array($fileExtension, $allowedfileExtensions)) {
				if(move_uploaded_file($fileTmpPath, $dest_path)){
				}
				else{
					throw new Exception("ERROR\n".JrTexto::_("File no pudo ser cargado")." ".JrTexto::_("Galeria_fotos").": ");
				}
			}
			return $newFileName;
		}else{
			return null;
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($id)) {
				$this->oNegContacto_bien->id = $id;
				$accion='_edit';
			}

			$ruta = "static/templates/plantillas/web/assets/img/bienes/";
			
			$new_file_recomendacion = Contacto_Bien::subirFile("file_recomendacion",$ruta);
			$new_file_policial_judicial = Contacto_Bien::subirFile("file_policial_judicial",$ruta);
			$new_file_extra_recomendacion = Contacto_Bien::subirFile("file_extra_recomendacion",$ruta);
			$new_file_infocorp = Contacto_Bien::subirFile("file_infocorp",$ruta);

           	$this->oNegContacto_bien->tipo_documento=@$tipo_documento;
  			$this->oNegContacto_bien->nro_documento=@$nro_documento;
  			$this->oNegContacto_bien->apellidos=@$apellidos;
  			$this->oNegContacto_bien->direccion=@$direccion;
  			$this->oNegContacto_bien->rebi_id=@$rebi_id;
  			$this->oNegContacto_bien->email=@$email;
  			$this->oNegContacto_bien->nombre=@$nombre;
  			$this->oNegContacto_bien->telefono=@$telefono;
  			$this->oNegContacto_bien->mensaje=@$mensaje;
			if($new_file_recomendacion!=null)
			  $this->oNegContacto_bien->file_recomendacion=@$new_file_recomendacion;
  			if($new_file_policial_judicial!=null)
			  $this->oNegContacto_bien->file_policial_judicial=@$new_file_policial_judicial;
			if($new_file_extra_recomendacion!=null)
			  $this->oNegContacto_bien->file_extra_recomendacion=@$new_file_extra_recomendacion;
  			if($new_file_infocorp!=null)
			  $this->oNegContacto_bien->file_infocorp=@$new_file_infocorp;
  			
  			$this->oNegContacto_bien->punataje=@$punataje;
  			$this->oNegContacto_bien->estado=@$estado;
  				        
            if($accion=='_add') {
            	$res=$this->oNegContacto_bien->agregar();           	
				
				echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Contacto_bien')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegContacto_bien->editar();

				$this->oNegContacto_bien->setId($res);
				$p = 0;
				if($this->oNegContacto_bien->file_recomendacion!="")
					$p+=2;
				if($this->oNegContacto_bien->file_policial_judicial!="")
					$p+=2;
				if($this->oNegContacto_bien->file_extra_recomendacion!="")
					$p+=2;
				if($this->oNegContacto_bien->file_infocorp!="")
					$p+=2;
				$this->oNegContacto_bien->setCampo($res,"punataje",$p);	
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Contacto_bien')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegContacto_bien->__set('id', $_REQUEST['id']);
			$res=$this->oNegContacto_bien->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegContacto_bien->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
<?php
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE);
class Correo extends JrWeb
{
	protected $idpersona=0; // sistema
	protected $oConfig;

	public function __construct()
	{
		$oConfig=new NegConfiguracion();
		parent::__construct();	
			
	}

	public function index(){
		try{

			JrCargador::clase('ACHT::JrCorreo');
			$oCorreo = new JrCorreo;
			$oCorreo->setRemitente('abelchingo@gmail.com','Abel'); //de 
			$oCorreo->setAsunto('tester');  // asunto
			$oCorreo->addDestinarioPhpmailer('abelchingo@gmail.com','Abel'); // para
			$this->esquema = 'correo/general';
            $oCorreo->setMensaje(parent::getEsquema());
            try{
                $oCorreo->sendPhpmailer();
                echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Correo Enviado')));
            	exit();
            }catch(Exception $ex){
                  echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error al enviar correo')));
            	exit();                    
            }

		}catch(Exception $ex){
			echo json_encode(array('code'=>'error','msj'=>'Eliminar_instalaciones'.$ex->getMessage()));
            exit(0);
		}
	} 
}
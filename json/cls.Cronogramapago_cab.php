<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021 
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegCronogramapago_cab', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
JrCargador::clase('sys_negocio::NegContacto_bien', RUTA_BASE);
JrCargador::clase('PHPMailer::class.phpmailer',RUTA_LIBS,'libs::', '');
JrCargador::clase('PHPMailer::class.pop3',RUTA_LIBS,'libs::', '');
JrCargador::clase('PHPMailer::class.smtp',RUTA_LIBS,'libs::', '');

class Cronogramapago_cab extends JrWeb
{
	private $oNegCronogramapago_cab;
	private $oNegPersona;
	private $oNegPersona_rol;
	

	public function __construct()
	{
		parent::__construct();		
		$this->oNegCronogramapago_cab = new NegCronogramapago_cab;
		$this->oNegPersona = new NegPersona;
		$this->oNegPersona_rol = new NegPersona_rol;
		$this->oNegContacto_bien = new NegContacto_bien;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Cronogramapago_cab', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id"])) $filtros["id"]=$_REQUEST["id"];

			if(isset($_REQUEST["persona_id"])&&@$_REQUEST["persona_id"]!='')$filtros["persona_id"]=$_REQUEST["persona_id"]; 
  			if(isset($_REQUEST["rebi_id"])&&@$_REQUEST["rebi_id"]!='')$filtros["rebi_id"]=$_REQUEST["rebi_id"]; 
  			if(isset($_REQUEST["garantia"])&&@$_REQUEST["garantia"]!='')$filtros["garantia"]=$_REQUEST["garantia"]; 
  			if(isset($_REQUEST["meses_contrato"])&&@$_REQUEST["meses_contrato"]!='')$filtros["meses_contrato"]=$_REQUEST["meses_contrato"]; 
  			if(isset($_REQUEST["monto_mensual"])&&@$_REQUEST["monto_mensual"]!='')$filtros["monto_mensual"]=$_REQUEST["monto_mensual"]; 
  			if(isset($_REQUEST["dia_pago"])&&@$_REQUEST["dia_pago"]!='')$filtros["dia_pago"]=$_REQUEST["dia_pago"]; 
  			if(isset($_REQUEST["archivo_contrato"])&&@$_REQUEST["archivo_contrato"]!='')$filtros["archivo_contrato"]=$_REQUEST["archivo_contrato"]; 
  			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["estado_pago"])&&@$_REQUEST["estado_pago"]!='')$filtros["estado_pago"]=$_REQUEST["estado_pago"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(isset($_REQUEST["inner"])&&@$_REQUEST["inner"]!='')$filtros["inner"]=$_REQUEST["inner"];	
			if(isset($_REQUEST["inner_id"])&&@$_REQUEST["inner_id"]!='')$filtros["inner_id"]=$_REQUEST["inner_id"];	
			
			
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegCronogramapago_cab->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public static function enviar($email, $nombres, $usuario, $password, $contrato){
		$mail = new PHPMailer(true);
		try {
			//Server settings
			$mail->SMTPDebug = false;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = MAIL_HOST;                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = MAIL_USERNAME;                     // SMTP username
			$mail->Password   = MAIL_PASSWORD;                               // SMTP password
			$mail->Port       = MAIL_PORT;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
			$mail->setFrom(MAIL_FROM_ADDRESS, MAIL_FROM_NAME);
			$mail->addAddress($email, $nombres);               // Name is optional
			
			// Attachments
			$mail->addAttachment($_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ."json/plantillas/json/contrato/".$contrato,"CONTRATO_".$nombres.".pdf");         // Add attachments
			
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'CONTRATO INMOBILIARIO';
			$mail->Body    = 'Estimado cliente(a) <b>'.strtoupper($nombres).'</b> para ingresar a su cronograma de pagos pendiente deberá ingresar con las siguientes credenciales:<br><br>
								USERNAME : <span><b>'.$usuario.'</b></span><br>
								PASSWORD : <span><b>'.$password.'</b></span><br><br>
								Ingresando en el siguiente enlace : <a href="'.URL_BASE.'admin/" >CLICK AQUÍ</a>
								<br>Además se adjunta el contrato a cumplir.';

			$mail->send();
		} catch (Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($mail->ErrorInfo)));
		}
	}
	public static function subirFile($name,$rutaFichero){
		
		if (isset($_FILES[$name]) && $_FILES[$name]['error'] === UPLOAD_ERR_OK) {
			$fileTmpPath = $_FILES[$name]['tmp_name'];
			$fileName = $_FILES[$name]['name'];
			$fileSize = $_FILES[$name]['size'];
			$fileType = $_FILES[$name]['type'];
			$fileNameCmps = explode(".", $fileName);
			$fileExtension = strtolower(end($fileNameCmps));
			$newFileName = md5(time() . $fileName) . '.' . $fileExtension;
			$allowedfileExtensions = array('pdf');
			$uploadFileDir = $_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ.$rutaFichero;
			$dest_path = $uploadFileDir . $newFileName;
			if (in_array($fileExtension, $allowedfileExtensions)) {
				if(move_uploaded_file($fileTmpPath, $dest_path)){
					return $newFileName;
				}else{
					throw new Exception("ERROR\n".JrTexto::_("File no pudo ser cargado")." ".JrTexto::_("Cronogramapago_cab").": ");
				}
			}
		}else{
			return null;
		}
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
			//echo var_dump($garantia);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($id)) {
				$this->oNegCronogramapago_cab->id = $id;
				$accion='_edit';
			}

			
			$this->oNegContacto_bien->setId($contactobien_id);

			$filtros = array("num_documento"=>$this->oNegContacto_bien->nro_documento);
			$dataPersona = $this->oNegPersona->buscar($filtros);
			
			if(count($dataPersona)<=0){
				$this->oNegPersona->tipo_documento= $this->oNegContacto_bien->tipo_documento;
				$this->oNegPersona->num_documento= $this->oNegContacto_bien->nro_documento;
				
				$arr_ape = explode(" ",$this->oNegContacto_bien->apellidos);
				if(count($arr_ape)>1){
					$this->oNegPersona->apellido_1= $arr_ape[0];
					$this->oNegPersona->apellido_2= implode(" ",array_shift($arr_ape));
				}elseif(count($arr_ape)==1){
					$this->oNegPersona->apellido_1= $arr_ape[0];
				}

				$this->oNegPersona->nombres=$this->oNegContacto_bien->nombre;

				$this->oNegPersona->usuario=$this->oNegContacto_bien->nro_documento;  			
				$this->oNegPersona->email=$this->oNegContacto_bien->email;
				
				$clave =  $this->oNegContacto_bien->nro_documento;

				if(!empty($clave)){
					$this->oNegPersona->clave=md5($clave);
					$this->oNegPersona->verclave=$clave;
				}  			
				$this->oNegPersona->tipo_usuario='n';
				$this->oNegPersona->telefono=$this->oNegContacto_bien->telefono;
				$this->oNegPersona->direccion=$this->oNegContacto_bien->direccion;
				
				$newIdPersona = $this->oNegPersona->agregar();		
			}else{
				$newIdPersona = $dataPersona[0]["idpersona"];
			}
			
			$filtros_rol = array("idpersona"=>$newIdPersona,"idrol"=>4,"idempresa"=>2);
			$dataPersonaRol = $this->oNegPersona_rol->buscar($filtros_rol);
			
			if(count($dataPersona)<=0){
				$this->oNegPersona_rol->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
				$this->oNegPersona_rol->idpersona=$newIdPersona;
				//rol cliente de la empresa activa con id 2
				$this->oNegPersona_rol->idrol=4;
				$this->oNegPersona_rol->usuario_registro=$usuarioAct["idpersona"];
				
				$this->oNegPersona_rol->agregar();
			}			
			$ruta = "json/plantillas/json/contrato/";
			$new_file_contrato = Cronogramapago_cab::subirFile("archivo_contrato",$ruta);
			
			$nombres = $this->oNegContacto_bien->nombre;
			$usuario = $this->oNegContacto_bien->nro_documento;
			$password = $this->oNegContacto_bien->nro_documento;
			
			Cronogramapago_cab::enviar($emailmodal, $nombres, $usuario, $password, $new_file_contrato);

           	$this->oNegCronogramapago_cab->persona_id=@$newIdPersona;
  			$this->oNegCronogramapago_cab->rebi_id=$this->oNegContacto_bien->rebi_id;
  			$this->oNegCronogramapago_cab->garantia=@$garantia;
  			$this->oNegCronogramapago_cab->meses_contrato=@$meses_contrato;
  			$this->oNegCronogramapago_cab->monto_mensual=@$monto_mensual;
  			$this->oNegCronogramapago_cab->dia_pago=@$dia_pago;
  			$this->oNegCronogramapago_cab->archivo_contrato=@$new_file_contrato;
  					
            if($accion=='_add') {
				
            	$res=$this->oNegCronogramapago_cab->agregar();				
				$this->oNegContacto_bien->setCampo($contactobien_id,"estado","2");
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Cronogramapago_cab')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegCronogramapago_cab->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Cronogramapago_cab')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
			
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegCronogramapago_cab->__set('id', $_REQUEST['id']);
			$res=$this->oNegCronogramapago_cab->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegCronogramapago_cab->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
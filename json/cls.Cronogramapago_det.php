<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021 
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegCronogramapago_det', RUTA_BASE);
JrCargador::clase('sys_negocio::NegPagos', RUTA_BASE);

JrCargador::clase('vendor::autoload',RUTA_LIBS,'libs::', '');

class Cronogramapago_det extends JrWeb
{
	private $oNegCronogramapago_det;
	private $oNegPagos;
		
	public function __construct()
	{
		MercadoPago\SDK::setAccessToken(ACCES_TOKEN_MERCADOPAGO);

		parent::__construct();		
		$this->oNegCronogramapago_det = new NegCronogramapago_det;
		$this->oNegPagos = new NegPagos;
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Cronogramapago_det', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id"])) $filtros["id"]=$_REQUEST["id"];

			if(isset($_REQUEST["cab_id"])&&@$_REQUEST["cab_id"]!='')$filtros["cab_id"]=$_REQUEST["cab_id"]; 
  			if(isset($_REQUEST["fecha_vencimiento"])&&@$_REQUEST["fecha_vencimiento"]!='')$filtros["fecha_vencimiento"]=$_REQUEST["fecha_vencimiento"]; 
  			if(isset($_REQUEST["cargo"])&&@$_REQUEST["cargo"]!='')$filtros["cargo"]=$_REQUEST["cargo"]; 
  			if(isset($_REQUEST["pago"])&&@$_REQUEST["pago"]!='')$filtros["pago"]=$_REQUEST["pago"]; 
  			if(isset($_REQUEST["saldo"])&&@$_REQUEST["saldo"]!='')$filtros["saldo"]=$_REQUEST["saldo"]; 
  			if(isset($_REQUEST["subtotal"])&&@$_REQUEST["subtotal"]!='')$filtros["subtotal"]=$_REQUEST["subtotal"]; 
  			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"]; 
  			if(isset($_REQUEST["fecha_pago"])&&@$_REQUEST["fecha_pago"]!='')$filtros["fecha_pago"]=$_REQUEST["fecha_pago"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegCronogramapago_det->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($id)) {
				$this->oNegCronogramapago_det->id = $id;
				$accion='_edit';
			}

           	$this->oNegCronogramapago_det->cab_id=@$cab_id;
  			$this->oNegCronogramapago_det->fecha_vencimiento=@$fecha_vencimiento;
  			$this->oNegCronogramapago_det->cargo=@$cargo;
  			$this->oNegCronogramapago_det->pago=@$pago;
  			$this->oNegCronogramapago_det->saldo=@$saldo;
  			$this->oNegCronogramapago_det->subtotal=@$subtotal;
  			$this->oNegCronogramapago_det->observacion=@$observacion;
  			$this->oNegCronogramapago_det->fecha_pago=@$fecha_pago;
  			$this->oNegCronogramapago_det->estado=@$estado;
  				        
            if($accion=='_add') {
            	$res=$this->oNegCronogramapago_det->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Cronogramapago_det')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegCronogramapago_det->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Cronogramapago_det')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegCronogramapago_det->__set('id', $_REQUEST['id']);
			$res=$this->oNegCronogramapago_det->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegCronogramapago_det->setCampo($_REQUEST['id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}
	public function createpreference(){
		$json = file_get_contents("php://input");
		$data = json_decode($json);

        $preference = new MercadoPago\Preference();

		$data_items_mercadopago = array();
		$data_items = array();
		$ids_det = array();
		$total = 0.0;

		$cab_id = -1;
		for($i=0;$i<count($data);$i++){
			

			$id_cronogramapagodet = $data[$i]->id;
			$ids_det[] = $id_cronogramapagodet;
			$this->oNegCronogramapago_det->setId($id_cronogramapagodet);
			if($cab_id==-1){
				$cab_id = $this->oNegCronogramapago_det->cab_id;	
			}
			$item = new MercadoPago\Item();
        
			$item->title = $this->oNegCronogramapago_det->concepto;
    	    $item->quantity = 1;
        	$item->unit_price = $this->oNegCronogramapago_det->cargo;
			$total += floatval( $this->oNegCronogramapago_det->cargo);
			$data_items_mercadopago[] = $item;
			$data_items[] = array("concepto"=>$this->oNegCronogramapago_det->concepto,
									"precio"=>$this->oNegCronogramapago_det->cargo);
		}
        
        $preference->items = $data_items_mercadopago;

        $preference->back_urls = array(
            "success" => URL_BASE."admin/cronogramapago_det/respuesta",
            "failure" => URL_BASE."admin/cronogramapago_det/respuesta",
            "pending" => URL_BASE."admin/cronogramapago_det/respuesta",
        );
        $preference->auto_return = "approved"; 

        $preference->save();
		$this->oNegPagos->persona_id =  NegSesion::getUsuario()["idpersona"];
		$this->oNegPagos->preference_id = $preference->id;
		$this->oNegPagos->cronogramapagodet_ids = implode(",",$ids_det);
		$this->oNegPagos->cronogramapagocab_id= $cab_id ;

		$this->oNegPagos->agregar();
		
		$data_items[] = array("concepto"=>"TOTAL : ","precio"=>number_format($total,2,".",","));
		$respuesta = array('preference' => $preference->id,"data"=> $data_items,"total"=>number_format($total,2,".",","));
		echo json_encode($respuesta); 
		exit(0);
	}  
 
}
<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Empresa extends JrWeb
{
	private $oNegEmpresa;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa = new NegEmpresa;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idempresa"])) $filtros["idempresa"]=$_REQUEST["idempresa"];

			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"]; 
  			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"]; 
  			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"]; 
  			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"]; 
  			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"]; 
  			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"]; 
  			if(isset($_REQUEST["subdominio"])&&@$_REQUEST["subdominio"]!='')$filtros["subdominio"]=$_REQUEST["subdominio"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegEmpresa->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
           	$empresaAct=NegSesion::getEmpresa();           
            if(!empty($idempresa)) {
				$this->oNegEmpresa->idempresa = $idempresa;
				$accion='_edit';
				if(!empty($logo)){
	           		JrCargador::clase('ACHT::JrArchivo');
	           		 $datosarchivo=JrArchivo::cargar_base64($logo,'static/media/E_'.$empresaAct["idempresa"]."/U".$usuarioAct["idpersona"]."/empresa/",'logo');		
	           		 $this->oNegEmpresa->logo=@$datosarchivo["link"];
	           	}
			}           

           	$this->oNegEmpresa->nombre=@$nombre;
  			$this->oNegEmpresa->direccion=@$direccion;
  			$this->oNegEmpresa->telefono=@$telefono;
  			$this->oNegEmpresa->correo=@$correo;  			
  			$this->oNegEmpresa->ruc=@$ruc;
  			$this->oNegEmpresa->idioma=@$idioma;
  			$this->oNegEmpresa->subdominio=@$subdominio;
  			$this->oNegEmpresa->estado=@$estado;
  			$this->oNegEmpresa->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$this->oNegEmpresa->logo='';
            	$res=$this->oNegEmpresa->agregar();
            	if(!empty($logo)){
	           		JrCargador::clase('ACHT::JrArchivo');
	           		$datosarchivo=JrArchivo::cargar_base64($logo,'static/media/E_'.$empresaAct["idempresa"]."/U".$usuarioAct["idpersona"]."/empresa/",'logo');	
	           		$this->oNegEmpresa->setCampo($res,'logo',@$datosarchivo["link"]); 
	           	}
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegEmpresa->editar();
            	if($res==$empresaAct["idempresa"]){
            		$this->oNegEmpresa->configuracion(array('idempresa'=>$empresaAct["idempresa"]));
            	}
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa->__set('idempresa', $_REQUEST['idempresa']);
			$res=$this->oNegEmpresa->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa->setCampo($_REQUEST['idempresa'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
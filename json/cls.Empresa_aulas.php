<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa_aulas', RUTA_BASE);
class Empresa_aulas extends JrWeb
{
	private $oNegEmpresa_aulas;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa_aulas = new NegEmpresa_aulas;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_aulas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idaula"])) $filtros["idaula"]=$_REQUEST["idaula"];

			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"]; 
  			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; 
  			if(isset($_REQUEST["capacidad"])&&@$_REQUEST["capacidad"]!='')$filtros["capacidad"]=$_REQUEST["capacidad"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegEmpresa_aulas->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idaula)) {
				$this->oNegEmpresa_aulas->idaula = $idaula;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario(); 
           	$this->oNegEmpresa_aulas->idlocal=@$idlocal;
  			$this->oNegEmpresa_aulas->nombre=@$nombre;
  			$empresaAct=NegSesion::getEmpresa();
  			$this->oNegEmpresa_aulas->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
  			$this->oNegEmpresa_aulas->estado=@$estado;
  			$this->oNegEmpresa_aulas->tipo=@$tipo;
  			$this->oNegEmpresa_aulas->capacidad=@$capacidad;
  			$this->oNegEmpresa_aulas->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$res=$this->oNegEmpresa_aulas->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_aulas')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegEmpresa_aulas->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_aulas')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa_aulas->__set('idaula', $_REQUEST['idaula']);
			$res=$this->oNegEmpresa_aulas->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa_aulas->setCampo($_REQUEST['idaula'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
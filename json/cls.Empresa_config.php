<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-10-2020 
 * @copyright	Copyright (C) 04-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa_config', RUTA_BASE);
class Empresa_config extends JrWeb
{
	private $oNegEmpresa_config;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa_config = new NegEmpresa_config;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_config', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idempresaconfig"])) $filtros["idempresaconfig"]=$_REQUEST["idempresaconfig"];

			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["parametro"])&&@$_REQUEST["parametro"]!='')$filtros["parametro"]=$_REQUEST["parametro"]; 
  			if(isset($_REQUEST["valor"])&&@$_REQUEST["valor"]!='')$filtros["valor"]=$_REQUEST["valor"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegEmpresa_config->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();  
            $idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];           
            JrCargador::clase('ACHT::JrArchivo');
            JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
            $negempresa=new NegEmpresa();
            foreach ($_POST as $k => $v){            	
				if(!empty($v)){
					$this->oNegEmpresa_config->idempresa=$idempresa;
		  			$this->oNegEmpresa_config->parametro=$k;
		  			$this->oNegEmpresa_config->valor=$v;
		  			$this->oNegEmpresa_config->estado=1;
		  			$this->oNegEmpresa_config->usuario_registro=$usuarioAct["idpersona"];
					$hay=$this->oNegEmpresa_config->buscar(array('idempresa'=>$idempresa,'parametro'=>$k,'sqlget'=>true));
					
					if($k=='imagen_fondo_login' && !empty($v)){		           		
		           		$datosarchivo=JrArchivo::cargar_base64($v,'static/media/E_'.$empresaAct["idempresa"]."/login/",'imagen_fondo_login');		
		           		$v=@$datosarchivo["link"];
		           	}
					if(!empty($hay)){
						$this->oNegEmpresa_config->idempresaconfig=$hay["idempresaconfig"];
						$this->oNegEmpresa_config->valor=$v;
						$this->oNegEmpresa_config->usuario_registro=$usuarioAct["idpersona"];						
						$res=$this->oNegEmpresa_config->editar();
						$negempresa->setconfiguracion($k,$v);
					}else{
						$this->oNegEmpresa_config->valor=$v;
						$res=$this->oNegEmpresa_config->agregar();
						$negempresa->setconfiguracion($k,$v);
					}

				}
			}
            echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_config')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa_config->__set('idempresaconfig', $_REQUEST['idempresaconfig']);
			$res=$this->oNegEmpresa_config->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa_config->setCampo($_REQUEST['idempresaconfig'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
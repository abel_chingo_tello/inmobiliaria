<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020 
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa_modulo', RUTA_BASE);
class Empresa_modulo extends JrWeb
{
	private $oNegEmpresa_modulo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa_modulo = new NegEmpresa_modulo;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_modulo', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idempresamodulo"])) $filtros["idempresamodulo"]=$_REQUEST["idempresamodulo"];
			if(isset($_REQUEST["idmodulo"])&&@$_REQUEST["idmodulo"]!='')$filtros["idmodulo"]=$_REQUEST["idmodulo"]; 
			$empresa=NegSesion::getEmpresa();
  			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(empty($filtros["idempresa"])) $idempresa=$empresa["idempresa"];
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"]; 
  			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"]; 
  			if(isset($_REQUEST["idmoduloapdre"])&&@$_REQUEST["idmoduloapdre"]!='')$filtros["idmoduloapdre"]=$_REQUEST["idmoduloapdre"]; 
  			if(isset($_REQUEST["nomtemporal"])&&@$_REQUEST["nomtemporal"]!='')$filtros["nomtemporal"]=$_REQUEST["nomtemporal"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegEmpresa_modulo->buscar($filtros);			
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {			
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($idempresamodulo)) {
				$this->oNegEmpresa_modulo->idempresamodulo = $idempresamodulo;
				$accion='_edit';
			}

           	$this->oNegEmpresa_modulo->idmodulo=@$idmodulo;
  			$this->oNegEmpresa_modulo->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
  			$this->oNegEmpresa_modulo->estado=@$estado;
  			$this->oNegEmpresa_modulo->idrol=@$idrol;
  			$this->oNegEmpresa_modulo->orden=!empty($orden)?$orden:0;
  			$this->oNegEmpresa_modulo->idmoduloapdre=@$idmoduloapdre;
  			$this->oNegEmpresa_modulo->nomtemporal=@$nomtemporal;
  			$this->oNegEmpresa_modulo->usuario_registro=$usuarioAct["idpersona"];  			
  				        
            if($accion=='_add') {
            	$res=$this->oNegEmpresa_modulo->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_modulo')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegEmpresa_modulo->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_modulo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa_modulo->__set('idempresamodulo', $_REQUEST['idempresamodulo']);
			$res=$this->oNegEmpresa_modulo->eliminar(true);			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa_modulo->setCampo($_REQUEST['idempresamodulo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function guardarorden(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa_modulo->guardarorden($_REQUEST['datos']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('El orden se ha actualizado')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-10-2020 
 * @copyright	Copyright (C) 04-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa_smtp', RUTA_BASE);
class Empresa_smtp extends JrWeb
{
	private $oNegEmpresa_smtp;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegEmpresa_smtp = new NegEmpresa_smtp;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Empresa_smtp', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idempresasmtp"])) $filtros["idempresasmtp"]=$_REQUEST["idempresasmtp"];

			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["host"])&&@$_REQUEST["host"]!='')$filtros["host"]=$_REQUEST["host"]; 
  			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"]; 
  			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"]; 
  			if(isset($_REQUEST["puerto"])&&@$_REQUEST["puerto"]!='')$filtros["puerto"]=$_REQUEST["puerto"]; 
  			if(isset($_REQUEST["cifrado"])&&@$_REQUEST["cifrado"]!='')$filtros["cifrado"]=$_REQUEST["cifrado"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegEmpresa_smtp->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();    
            $idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
           	$this->oNegEmpresa_smtp->idempresa=$idempresa;           
			$hayempresa=$this->oNegEmpresa_smtp->buscar(array('idempresa'=>$idempresa));
			if(!empty($hayempresa[0])){
				$this->oNegEmpresa_smtp->idempresasmtp=$hayempresa[0]["idempresasmtp"];
				$accion='_edit';
			}
			
           	 
  			$this->oNegEmpresa_smtp->host=@$host;
  			$this->oNegEmpresa_smtp->email=@$email;
  			$this->oNegEmpresa_smtp->clave=@$clave;
  			$this->oNegEmpresa_smtp->puerto=@$puerto;
  			$this->oNegEmpresa_smtp->cifrado=@$cifrado;
  			$this->oNegEmpresa_smtp->estado=!empty($estado)?$estado:1;
  			$this->oNegEmpresa_smtp->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$res=$this->oNegEmpresa_smtp->agregar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_smtp')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegEmpresa_smtp->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Empresa_smtp')).' '.JrTexto::_('update successfully'),'newid'=>$res));
            } 
           
        	JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
        	$negemprea=new NegEmpresa(); 
        	$smtp=$this->oNegEmpresa_smtp->buscar(array('idempresa'=>$idempresa,'sqlget'=>true));          	
        	$negemprea->setconfiguracion('smtp',$smtp);

            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegEmpresa_smtp->__set('idempresasmtp', $_REQUEST['idempresasmtp']);
			$res=$this->oNegEmpresa_smtp->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegEmpresa_smtp->setCampo($_REQUEST['idempresasmtp'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
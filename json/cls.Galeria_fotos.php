<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-01-2021 
 * @copyright	Copyright (C) 28-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGaleria_fotos', RUTA_BASE);
class Galeria_fotos extends JrWeb
{
	private $oNegGaleria_fotos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGaleria_fotos = new NegGaleria_fotos;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Galeria_fotos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["id_galeria"])) $filtros["id_galeria"]=$_REQUEST["id_galeria"];

			if(isset($_REQUEST["rebi_id"])&&@$_REQUEST["rebi_id"]!='')$filtros["rebi_id"]=$_REQUEST["rebi_id"]; 
  			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"]; 
  			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"]; 
  			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; 
  			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegGaleria_fotos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
			if (isset($_FILES['imagen']) && $_FILES['imagen']['error'] === UPLOAD_ERR_OK) {
				$fileTmpPath = $_FILES['imagen']['tmp_name'];
				$fileName = $_FILES['imagen']['name'];
				$fileSize = $_FILES['imagen']['size'];
				$fileType = $_FILES['imagen']['type'];
				$fileNameCmps = explode(".", $fileName);
				$fileExtension = strtolower(end($fileNameCmps));
				$newFileName = md5(time() . $fileName) . '.' . $fileExtension;
				$allowedfileExtensions = array('jpg', 'gif', 'png');
				$uploadFileDir = $_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ."static/templates/plantillas/web/assets/img/bienes/";
				$dest_path = $uploadFileDir . $newFileName;
				if (in_array($fileExtension, $allowedfileExtensions)) {
					if(move_uploaded_file($fileTmpPath, $dest_path)){
					}
					else{
						throw new Exception("ERROR\n".JrTexto::_("File no pudo ser cargado")." ".JrTexto::_("Galeria_fotos").": ");
					}
				}
			}
			
			if(!empty($id_galeria)) {
				$this->oNegGaleria_fotos->id_galeria = $id_galeria;
				$accion='_edit';
			}

           	$this->oNegGaleria_fotos->rebi_id=@$rebi_id;
  			$this->oNegGaleria_fotos->titulo=@$titulo;
  			$this->oNegGaleria_fotos->descripcion=@$descripcion;
  			$this->oNegGaleria_fotos->tipo=@$tipo;
  			$this->oNegGaleria_fotos->imagen=@$newFileName;
  				        
            if($accion=='_add') {
            	$res=$this->oNegGaleria_fotos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Galeria_fotos')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegGaleria_fotos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Galeria_fotos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegGaleria_fotos->__set('id_galeria', $_REQUEST['id_galeria']);
			$res=$this->oNegGaleria_fotos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegGaleria_fotos->setCampo($_REQUEST['id_galeria'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
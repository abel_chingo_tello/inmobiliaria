<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE);
class Historial_sesion extends JrWeb
{
	private $oNegHistorial_sesion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Historial_sesion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idhistorial"])) $filtros["idhistorial"]=$_REQUEST["idhistorial"];

			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"]; 
  			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"]; 
  			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["ip"])&&@$_REQUEST["ip"]!='')$filtros["ip"]=$_REQUEST["ip"]; 
  			if(isset($_REQUEST["navegador"])&&@$_REQUEST["navegador"]!='')$filtros["navegador"]=$_REQUEST["navegador"]; 
  			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"]; 
  			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"]; 
  			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"]; 
  			if(isset($_REQUEST["lugar"])&&@$_REQUEST["lugar"]!='')$filtros["lugar"]=$_REQUEST["lugar"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegHistorial_sesion->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idhistorial)) {
				$this->oNegHistorial_sesion->idhistorial = $idhistorial;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario(); 
           	$this->oNegHistorial_sesion->idpersona=@$idpersona;
  			$this->oNegHistorial_sesion->idrol=@$idrol;
  			$empresaAct=NegSesion::getEmpresa();
  			$this->oNegHistorial_sesion->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
  			$this->oNegHistorial_sesion->ip=@$ip;
  			$this->oNegHistorial_sesion->navegador=@$navegador;
  			$this->oNegHistorial_sesion->idgrupoaula=@$idgrupoaula;
  			$this->oNegHistorial_sesion->fecha_inicio=@$fecha_inicio;
  			$this->oNegHistorial_sesion->fecha_final=@$fecha_final;
  			$this->oNegHistorial_sesion->lugar=@$lugar;
  				        
            if($accion=='_add') {
            	$res=$this->oNegHistorial_sesion->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_sesion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegHistorial_sesion->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Historial_sesion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegHistorial_sesion->__set('idhistorial', $_REQUEST['idhistorial']);
			$res=$this->oNegHistorial_sesion->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegHistorial_sesion->setCampo($_REQUEST['idhistorial'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
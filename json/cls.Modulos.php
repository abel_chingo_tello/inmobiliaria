<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020 
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegModulos', RUTA_BASE);
class Modulos extends JrWeb
{
	private $oNegModulos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegModulos = new NegModulos;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Modulos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idmodulo"])) $filtros["idmodulo"]=$_REQUEST["idmodulo"];

			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"]; 
  			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["url"])&&@$_REQUEST["url"]!='')$filtros["url"]=$_REQUEST["url"]; 
  			if(isset($_REQUEST["icono"])&&@$_REQUEST["icono"]!='')$filtros["icono"]=$_REQUEST["icono"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegModulos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($idmodulo)) {
				$this->oNegModulos->idmodulo = $idmodulo;
				$accion='_edit';
			}

           	$this->oNegModulos->nombre=@$nombre;
  			$this->oNegModulos->descripcion=@$descripcion;
  			$this->oNegModulos->estado=@$estado;
  			$this->oNegModulos->url=@$url;
  			$this->oNegModulos->icono=@$icono;
  			$this->oNegModulos->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$res=$this->oNegModulos->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Modulos')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegModulos->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Modulos')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegModulos->__set('idmodulo', $_REQUEST['idmodulo']);
			$res=$this->oNegModulos->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegModulos->setCampo($_REQUEST['idmodulo'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
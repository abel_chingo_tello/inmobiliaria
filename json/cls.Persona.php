<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020 
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona', RUTA_BASE);
class Persona extends JrWeb
{
	private $oNegPersona;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona = new NegPersona;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idpersona"])) $filtros["idpersona"]=$_REQUEST["idpersona"];

			if(isset($_REQUEST["tipo_documento"])&&@$_REQUEST["tipo_documento"]!='')$filtros["tipo_documento"]=$_REQUEST["tipo_documento"]; 
  			if(isset($_REQUEST["num_documento"])&&@$_REQUEST["num_documento"]!='')$filtros["num_documento"]=$_REQUEST["num_documento"]; 
  			if(isset($_REQUEST["apellido_1"])&&@$_REQUEST["apellido_1"]!='')$filtros["apellido_1"]=$_REQUEST["apellido_1"]; 
  			if(isset($_REQUEST["apellido_2"])&&@$_REQUEST["apellido_2"]!='')$filtros["apellido_2"]=$_REQUEST["apellido_2"]; 
  			if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"]; 
  			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"]; 
  			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"]; 
  			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"]; 
  			if(isset($_REQUEST["verclave"])&&@$_REQUEST["verclave"]!='')$filtros["verclave"]=$_REQUEST["verclave"]; 
  			if(isset($_REQUEST["tocken"])&&@$_REQUEST["tocken"]!='')$filtros["tocken"]=$_REQUEST["tocken"]; 
  			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"]; 
  			if(isset($_REQUEST["genero"])&&@$_REQUEST["genero"]!='')$filtros["genero"]=$_REQUEST["genero"]; 
  			if(isset($_REQUEST["fecha_nacimiento"])&&@$_REQUEST["fecha_nacimiento"]!='')$filtros["fecha_nacimiento"]=$_REQUEST["fecha_nacimiento"]; 
  			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"]; 
  			if(isset($_REQUEST["tipo_usuario"])&&@$_REQUEST["tipo_usuario"]!='')$filtros["tipo_usuario"]=$_REQUEST["tipo_usuario"]; 
  			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"]; 
  			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"]; 
  			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegPersona->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            $usuarioAct = NegSesion::getUsuario(); 
           	$empresaAct=NegSesion::getEmpresa();

            if(!empty($idpersona)) {
				$this->oNegPersona->idpersona = $idpersona;
				$accion='_edit';
				if(!empty($foto)){
	           		JrCargador::clase('ACHT::JrArchivo');
	           		 $datosarchivo=JrArchivo::cargar_base64($foto,'static/media/E_'.$empresaAct["idempresa"]."/persona/U".$usuarioAct["idpersona"]."/",'foto');		
	           		 $this->oNegPersona->foto=@$datosarchivo["link"];
	           	}
			}  	


           	$this->oNegPersona->tipo_documento=@$tipo_documento;
  			$this->oNegPersona->num_documento=@$num_documento;
  			$this->oNegPersona->apellido_1=@$apellido_1;
  			$this->oNegPersona->apellido_2=@$apellido_2;
  			$this->oNegPersona->nombres=@$nombres;
  			$this->oNegPersona->usuario=$usuario;  			
  			$this->oNegPersona->email=@$email;
  			if(!empty($clave)){
  				$this->oNegPersona->clave=md5($clave);
  				$this->oNegPersona->verclave=$clave;
  			}  			
  			$this->oNegPersona->genero=@$genero;
  			$this->oNegPersona->fecha_nacimiento=!empty($fecha_nacimiento)?$fecha_nacimiento:date('Y-m-d');
  			$this->oNegPersona->estado_civil=@$estado_civil;
  			$this->oNegPersona->tipo_usuario=!empty($tipo_usuario)?$tipo_usuario:'n';
  			$this->oNegPersona->telefono=@$telefono;
  			$this->oNegPersona->ubigeo=@$ubigeo;
  			$this->oNegPersona->direccion=@$direccion;
  			$this->oNegPersona->estado=@$estado;
  			$this->oNegPersona->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add'){
            	$this->oNegPersona->foto='';
            	$res=$this->oNegPersona->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            	if(!empty($foto)){
	           		JrCargador::clase('ACHT::JrArchivo');
	           		 $datosarchivo=JrArchivo::cargar_base64($foto,'static/media/E_'.$empresaAct["idempresa"]."/persona/U".$usuarioAct["idpersona"]."/",'foto');	
	           		$this->oNegPersona->setCampo($res,'foto',@$datosarchivo["link"]); 
	           	}
            }else{
            	$res=$this->oNegPersona->editar();
            	if($res==$usuarioAct["idpersona"]){
            		NegSesion::refresh($usuarioAct["idpersona"]);
            	}
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona')).' '.JrTexto::_('update successfully'),'newid'=>$res));
            }
            


            if(!empty($usuario)){            	
				$this->oNegPersona->setCampo($res,'tocken',md5($res."_".$usuario)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona->__set('idpersona', $_REQUEST['idpersona']);
			$res=$this->oNegPersona->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona->setCampo($_REQUEST['idpersona'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}   
}
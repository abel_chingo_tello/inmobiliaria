<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020 
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE);
class Persona_rol extends JrWeb
{
	private $oNegPersona_rol;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersona_rol = new NegPersona_rol;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Persona_rol', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["idpersonarol"])) $filtros["idpersonarol"]=$_REQUEST["idpersonarol"];

			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"]; 
  			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"]; 
  			if(isset($_REQUEST["idrol"])&&@$_REQUEST["idrol"]!='')$filtros["idrol"]=$_REQUEST["idrol"]; 
  			if(isset($_REQUEST["usuario_registro"])&&@$_REQUEST["usuario_registro"]!='')$filtros["usuario_registro"]=$_REQUEST["usuario_registro"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegPersona_rol->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($idpersonarol)) {
				$this->oNegPersona_rol->idpersonarol = $idpersonarol;
				$accion='_edit';
			}

           	$this->oNegPersona_rol->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
  			$this->oNegPersona_rol->idpersona=@$idpersona;
  			$this->oNegPersona_rol->idrol=@$idrol;
  			$this->oNegPersona_rol->usuario_registro=$usuarioAct["idpersona"];
  				        
            if($accion=='_add') {
            	$res=$this->oNegPersona_rol->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_rol')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegPersona_rol->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Persona_rol')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegPersona_rol->__set('idpersonarol', $_REQUEST['idpersonarol']);
			$res=$this->oNegPersona_rol->eliminar(true);			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegPersona_rol->setCampo($_REQUEST['idpersonarol'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
	 
}
<?php /**
 * @autor   Generador Abel Chingo Tello, ACHT
 * @fecha   24-12-2020 
 * @copyright Copyright (C) 24-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRegistro_bien', RUTA_BASE);
class Registro_bien extends JrWeb
{
  private $oNegRegistro_bien;
    
  public function __construct()
  {
    parent::__construct();    
    $this->oNegRegistro_bien = new NegRegistro_bien;
    
  }

  public function index(){    
    $this->documento->plantilla = 'blanco';
    try{
      global $aplicacion;     
      //if(!NegSesion::tiene_acceso('Registro_bien', 'list')) {
      //  echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
      //  exit(0);
      //}
      $filtros=array();
      if(!empty($_REQUEST["rebi_id"])) $filtros["rebi_id"]=$_REQUEST["rebi_id"];

      if(isset($_REQUEST["rebi_modalidad"])&&@$_REQUEST["rebi_modalidad"]!='')$filtros["rebi_modalidad"]=$_REQUEST["rebi_modalidad"]; 
        if(isset($_REQUEST["rebi_tipo_bien"])&&@$_REQUEST["rebi_tipo_bien"]!='')$filtros["rebi_tipo_bien"]=$_REQUEST["rebi_tipo_bien"]; 
        if(isset($_REQUEST["casb_codigo"])&&@$_REQUEST["casb_codigo"]!='')$filtros["casb_codigo"]=$_REQUEST["casb_codigo"]; 
        if(isset($_REQUEST["rebi_fadquisicion"])&&@$_REQUEST["rebi_fadquisicion"]!='')$filtros["rebi_fadquisicion"]=$_REQUEST["rebi_fadquisicion"]; 
        if(isset($_REQUEST["rebi_vadquisicion"])&&@$_REQUEST["rebi_vadquisicion"]!='')$filtros["rebi_vadquisicion"]=$_REQUEST["rebi_vadquisicion"]; 
        if(isset($_REQUEST["rebi_ult_dep_acum"])&&@$_REQUEST["rebi_ult_dep_acum"]!='')$filtros["rebi_ult_dep_acum"]=$_REQUEST["rebi_ult_dep_acum"]; 
        if(isset($_REQUEST["rebi_ult_val_act"])&&@$_REQUEST["rebi_ult_val_act"]!='')$filtros["rebi_ult_val_act"]=$_REQUEST["rebi_ult_val_act"]; 
        if(isset($_REQUEST["tabl_estado"])&&@$_REQUEST["tabl_estado"]!='')$filtros["tabl_estado"]=$_REQUEST["tabl_estado"]; 
        if(isset($_REQUEST["rebi_detalles"])&&@$_REQUEST["rebi_detalles"]!='')$filtros["rebi_detalles"]=$_REQUEST["rebi_detalles"]; 
        if(isset($_REQUEST["depe_id_ubica"])&&@$_REQUEST["depe_id_ubica"]!='')$filtros["depe_id_ubica"]=$_REQUEST["depe_id_ubica"]; 
        if(isset($_REQUEST["tabl_condicion"])&&@$_REQUEST["tabl_condicion"]!='')$filtros["tabl_condicion"]=$_REQUEST["tabl_condicion"]; 
        if(isset($_REQUEST["rebi_reg_publicos"])&&@$_REQUEST["rebi_reg_publicos"]!='')$filtros["rebi_reg_publicos"]=$_REQUEST["rebi_reg_publicos"]; 
        if(isset($_REQUEST["rebi_area"])&&@$_REQUEST["rebi_area"]!='')$filtros["rebi_area"]=$_REQUEST["rebi_area"]; 
        if(isset($_REQUEST["rebi_observaciones"])&&@$_REQUEST["rebi_observaciones"]!='')$filtros["rebi_observaciones"]=$_REQUEST["rebi_observaciones"]; 
        if(isset($_REQUEST["rebi_fregistro"])&&@$_REQUEST["rebi_fregistro"]!='')$filtros["rebi_fregistro"]=$_REQUEST["rebi_fregistro"]; 
        if(isset($_REQUEST["depe_id"])&&@$_REQUEST["depe_id"]!='')$filtros["depe_id"]=$_REQUEST["depe_id"]; 
        if(isset($_REQUEST["rebi_anno"])&&@$_REQUEST["rebi_anno"]!='')$filtros["rebi_anno"]=$_REQUEST["rebi_anno"]; 
        if(isset($_REQUEST["rebi_alquiler"])&&@$_REQUEST["rebi_alquiler"]!='')$filtros["rebi_alquiler"]=$_REQUEST["rebi_alquiler"]; 
        if(isset($_REQUEST["tabl_tipo_interior"])&&@$_REQUEST["tabl_tipo_interior"]!='')$filtros["tabl_tipo_interior"]=$_REQUEST["tabl_tipo_interior"]; 
        if(isset($_REQUEST["rebi_numero_interior"])&&@$_REQUEST["rebi_numero_interior"]!='')$filtros["rebi_numero_interior"]=$_REQUEST["rebi_numero_interior"]; 
        if(isset($_REQUEST["rebi_foto_principal"])&&@$_REQUEST["rebi_foto_principal"]!='')$filtros["rebi_foto_principal"]=$_REQUEST["rebi_foto_principal"]; 
        if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"]; 
      
      if(isset($_REQUEST["precio_min"])&&@$_REQUEST["precio_min"]!='')$filtros["precio_min"]=$_REQUEST["precio_min"]; 
      if(isset($_REQUEST["precio_max"])&&@$_REQUEST["precio_max"]!='')$filtros["precio_max"]=$_REQUEST["precio_max"]; 
      if(isset($_REQUEST["area_min"])&&@$_REQUEST["area_min"]!='')$filtros["area_min"]=$_REQUEST["area_min"]; 
      if(isset($_REQUEST["area_max"])&&@$_REQUEST["area_max"]!='')$filtros["area_max"]=$_REQUEST["area_max"]; 
      
        if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
          
      $this->datos=$this->oNegRegistro_bien->buscar($filtros);
      echo json_encode(array('code'=>200,'data'=>$this->datos));
      exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
  }

  public function guardar(){
    $this->documento->plantilla = 'blanco';
    try {
      global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
            
            if (isset($_FILES['rebi_foto_principal']) && $_FILES['rebi_foto_principal']['error'] === UPLOAD_ERR_OK) {
              $fileTmpPath = $_FILES['rebi_foto_principal']['tmp_name'];
              $fileName = $_FILES['rebi_foto_principal']['name'];
              $fileSize = $_FILES['rebi_foto_principal']['size'];
              $fileType = $_FILES['rebi_foto_principal']['type'];
              $fileNameCmps = explode(".", $fileName);
              $fileExtension = strtolower(end($fileNameCmps));
              $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
              $allowedfileExtensions = array('jpg', 'gif', 'png');
              $uploadFileDir = $_SERVER['DOCUMENT_ROOT'] ."/".CARPETA_RAIZ."static/templates/plantillas/web/assets/img/bienes/";
              $dest_path = $uploadFileDir . $newFileName;
              if (in_array($fileExtension, $allowedfileExtensions)) {
                if(move_uploaded_file($fileTmpPath, $dest_path)){
                }
                else{
                  throw new Exception("ERROR\n".JrTexto::_("File no pudo ser cargado")." ".JrTexto::_("Galeria_fotos").": ");
                }
              }
            }


            if(!empty($rebi_id)) {
        $this->oNegRegistro_bien->rebi_id = $rebi_id;
        $accion='_edit';
      }

          $this->oNegRegistro_bien->rebi_modalidad=@$rebi_modalidad;
        $this->oNegRegistro_bien->rebi_tipo_bien=@$rebi_tipo_bien;
        $this->oNegRegistro_bien->casb_codigo=@$casb_codigo;
        $this->oNegRegistro_bien->rebi_fadquisicion=@$rebi_fadquisicion;
        $this->oNegRegistro_bien->rebi_vadquisicion=@$rebi_vadquisicion;
        $this->oNegRegistro_bien->rebi_ult_dep_acum=@$rebi_ult_dep_acum;
        $this->oNegRegistro_bien->rebi_ult_val_act=@$rebi_ult_val_act;
        $this->oNegRegistro_bien->tabl_estado=@$tabl_estado;
        $this->oNegRegistro_bien->rebi_detalles=@$rebi_detalles;
        $this->oNegRegistro_bien->depe_id_ubica=@$depe_id_ubica;
        $this->oNegRegistro_bien->tabl_condicion=@$tabl_condicion;
        $this->oNegRegistro_bien->rebi_reg_publicos=@$rebi_reg_publicos;
        $this->oNegRegistro_bien->rebi_area=@$rebi_area;
        $this->oNegRegistro_bien->rebi_observaciones=@$rebi_observaciones;
        $this->oNegRegistro_bien->rebi_fregistro=@$rebi_fregistro;
        $this->oNegRegistro_bien->depe_id=@$depe_id;
        $this->oNegRegistro_bien->rebi_anno=@$rebi_anno;
        $this->oNegRegistro_bien->rebi_alquiler=@$rebi_alquiler;
        $this->oNegRegistro_bien->tabl_tipo_interior=@$tabl_tipo_interior;
        $this->oNegRegistro_bien->rebi_numero_interior=@$rebi_numero_interior;
        $this->oNegRegistro_bien->rebi_foto_principal=@$newFileName;
                  
            if($accion=='_add') {
              $res=$this->oNegRegistro_bien->agregar();
               echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Registro_bien')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
              $res=$this->oNegRegistro_bien->editar();
              echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Registro_bien')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
    
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
  }
  
  public function eliminar(){
    try {
      if(empty($_REQUEST)){ 
        echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
        exit(0);
      }

      $this->oNegRegistro_bien->__set('rebi_id', $_REQUEST['rebi_id']);
      $res=$this->oNegRegistro_bien->eliminar();      
      echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
      exit(0);
    }catch(Exception $e) {
      echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
      exit(0);
    }
  }

  public function setCampo(){
    try {
      if(empty($_REQUEST)){ 
        echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
        exit(0);
      }
      $this->oNegRegistro_bien->setCampo($_REQUEST['rebi_id'],$_REQUEST['campo'],$_REQUEST['valor']);
      echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
      exit(0);
    }catch(Exception $e) {
      echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
      exit(0);
    }
  }  
 
}
<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
class Sesion extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
		$this->oNegEmpresa = new Negempresa;
	}
	
	public function index()
	{
		global $aplicacion;
		return $this->login();			
	}
	public function login()
	{
		try {
			global $aplicacion;	
			if(!empty($_REQUEST["usuario"])||!empty($_REQUEST['token'])){

				if(true === NegSesion::existeSesion() && (empty($_REQUEST['usuario']) && empty($_REQUEST['clave']) && empty($_REQUEST['token']))) {	
					return $aplicacion->redir();
				} elseif (!empty($_REQUEST['token'])||(!empty($_REQUEST['usuario']) && !empty($_REQUEST['clave']))){
					$datos=Array();
					$datos['tocken']=@$_REQUEST['tocken'];
					$datos['usuario']=@$_REQUEST['usuario'];
					$datos['clave']=@$_REQUEST['clave'];
					$empresa=$this->oNegEmpresa->getconfiguracion();
					$datos['idempresa']=$empresa["idempresa"];						
					$res=NegSesion::ingresar($datos);					
					if($res["code"] == 200 && $res["islogin"] == true) {
						if(!empty($empresa["returnmodulo"])) $res['return']=$empresa["returnmodulo"];						
					}
					echo json_encode($res);
					exit(0);
				} else {
					echo json_encode(array('code' => 'Error', 'msj' => 'Usuario no logueado'));
					exit(0);
				}
			}
			return $this->form();
		} catch (Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
		}
	}

	public function salir(){		
		try {
			global $aplicacion;
			if (true === NegSesion::existeSesion()) {
				//$this->terminarHistorialSesion('P');
				NegSesion::salir();
			}
			echo json_encode(array('code' => 200, 'msj' => JrTexto::_('Sesion cerrada,correctamente')));
			exit(0);
		} catch (Exception $e) {
			echo json_encode(array('code' => 'Error', 'msj' => 'Error al cerrar sesion<br>' . JrTexto::_($e->getMessage())));
			exit(0);
		}
	}

	public function form()
	{
		try {
			global $aplicacion;						
			if(true == NegSesion::existeSesion()) {
				$aplicacion->redir();
			}	
			$empresa=$this->oNegEmpresa->getconfiguracion();
            $this->documento->setTitulo(JrTexto::_('Login'));	
            $this->empresa=$empresa;
            $pagelogin=(!empty($empresa["pagina-login"])?$empresa["pagina-login"]:'login01');
           
			$this->documento->plantilla = 'login/'.$pagelogin."/".$pagelogin;
			$this->esquema = 'login/'.$pagelogin;
			return parent::getEsquema();
		} catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}
}
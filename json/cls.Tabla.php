<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-12-2020 
 * @copyright	Copyright (C) 22-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);
class Tabla extends JrWeb
{
	private $oNegTabla;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTabla = new NegTabla;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Tabla', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["tabl_id"])) $filtros["tabl_id"]=$_REQUEST["tabl_id"];

			if(isset($_REQUEST["tabl_codigo"])&&@$_REQUEST["tabl_codigo"]!='')$filtros["tabl_codigo"]=$_REQUEST["tabl_codigo"]; 
  			if(isset($_REQUEST["tabl_descripcion"])&&@$_REQUEST["tabl_descripcion"]!='')$filtros["tabl_descripcion"]=$_REQUEST["tabl_descripcion"]; 
  			if(isset($_REQUEST["tabl_fecharegistro"])&&@$_REQUEST["tabl_fecharegistro"]!='')$filtros["tabl_fecharegistro"]=$_REQUEST["tabl_fecharegistro"]; 
  			if(isset($_REQUEST["tabl_descripaux"])&&@$_REQUEST["tabl_descripaux"]!='')$filtros["tabl_descripaux"]=$_REQUEST["tabl_descripaux"]; 
  			if(isset($_REQUEST["tabl_codigoauxiliar"])&&@$_REQUEST["tabl_codigoauxiliar"]!='')$filtros["tabl_codigoauxiliar"]=$_REQUEST["tabl_codigoauxiliar"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegTabla->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
  			
            if(!empty($tabl_id)) {
				$this->oNegTabla->tabl_id = $tabl_id;
				$accion='_edit';
			}

           	$this->oNegTabla->tabl_codigo=@$tabl_codigo;
  			$this->oNegTabla->tabl_descripcion=@$tabl_descripcion;
  			$this->oNegTabla->tabl_fecharegistro=@$tabl_fecharegistro;
  			$this->oNegTabla->tabl_descripaux=@$tabl_descripaux;
  			$this->oNegTabla->tabl_codigoauxiliar=@$tabl_codigoauxiliar;
  				        
            if($accion=='_add') {
            	$res=$this->oNegTabla->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Tabla')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNegTabla->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('Tabla')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNegTabla->__set('tabl_id', $_REQUEST['tabl_id']);
			$res=$this->oNegTabla->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNegTabla->setCampo($_REQUEST['tabl_id'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
 
}
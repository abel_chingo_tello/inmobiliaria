<?php /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-02-2021 
 * @copyright	Copyright (C) 15-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegVistapagos', RUTA_BASE);
class Vistapagos extends JrWeb
{
	private $oNegVistapagos;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegVistapagos = new NegVistapagos;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Vistapagos', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST[""])) $filtros[""]=$_REQUEST[""];

			if(isset($_REQUEST["fecha_vencimiento"])&&@$_REQUEST["fecha_vencimiento"]!='')$filtros["fecha_vencimiento"]=$_REQUEST["fecha_vencimiento"]; 
  			if(isset($_REQUEST["vivienda"])&&@$_REQUEST["vivienda"]!='')$filtros["vivienda"]=$_REQUEST["vivienda"]; 
  			if(isset($_REQUEST["num_documento"])&&@$_REQUEST["num_documento"]!='')$filtros["num_documento"]=$_REQUEST["num_documento"]; 
  			if(isset($_REQUEST["nombres"])&&@$_REQUEST["nombres"]!='')$filtros["nombres"]=$_REQUEST["nombres"]; 
  			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"]; 
  			if(isset($_REQUEST["monto_total"])&&@$_REQUEST["monto_total"]!='')$filtros["monto_total"]=$_REQUEST["monto_total"]; 
  			if(isset($_REQUEST["monto_mensual"])&&@$_REQUEST["monto_mensual"]!='')$filtros["monto_mensual"]=$_REQUEST["monto_mensual"]; 
  			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	

			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];	
			if(isset($_REQUEST["fecha_fin"])&&@$_REQUEST["fecha_fin"]!='')$filtros["fecha_fin"]=$_REQUEST["fecha_fin"];	


			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNegVistapagos->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	  
 
}
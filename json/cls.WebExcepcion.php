<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebExcepcion extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function defecto()
	{
		global $aplicacion;
		$aplicacion->redir();		
	}
	
	public function error($msj, $plantilla = null)
	{
		try {			
			global $aplicacion;	
			echo json_encode(array('code'=>200,'msj'=>'Error '.$msj));
			exit(0);
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit();
		}
	}
	
	public function noencontrado($infopage=null)
	{
		try {
			global $aplicacion;	
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Page not found')." :: ".$infopage));
			exit(0);
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>$e->getMessage()));
			exit();
		}
	}
}
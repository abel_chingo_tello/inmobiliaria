<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/07/2012
 * @copyright	Copyright (C) 2012. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebIndex extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		global $aplicacion;		
		if(NegSesion::existeSesion()==false){			
			return $aplicacion->redir('sesion');
		} else	return $aplicacion->redir('../');
	}
}
<?php
$logo=!empty($this->empresa["logo"])?$this->empresa["logo"]:'static/media/defecto/logoempresa.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$logo))) $logo='static/media/defecto/logoempresa.png';

$imagen_fondo_login=!empty($this->empresa["imagen_fondo_login"])?$this->empresa["imagen_fondo_login"]:'static/media/defecto/fondologin.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$imagen_fondo_login))) $imagen_fondo_login='static/media/defecto/fondologin.png';
$color_fondo_login=!empty($this->empresa["color_fondo_login"])?$this->empresa["color_fondo_login"]:'#dddddd';
//$logo=!empty($this->empresa["logo"])?$this->empresa["logo"]:'empresa/sinlogo.png';
?>
<style>
    .fondologin{   
        height:99.9vh;
        width:100%;
        position:relative;
        overflow:hidden;
    }
    .contentmedia {
        position: absolute;
        height: 100%;
        width: 100%;
        /*opacity: 0.95;*/
    }
    .contentmedia img, .contentmedia video{
        margin: auto;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 100%;
        min-height: 100%;
        opacity:0.85;
    }
</style>
<div class="fondologin">
    <div class="fondo contentmedia" style=""></div>
    <div class="">
        <a class="hiddenanchor" id="torecuperar"></a>
        <a class="hiddenanchor" id="tologin"></a>
        
        <div id="wrapper">
            <div id="login" class="animate form ">
                <section class="login_content text-center">
                    <?php if(!empty($this->msjErrorLogin)):?>
                    <div class="alert alert-warning" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only"><?php echo JrTexto::_('Attention')?>:</span> <?php echo JrTexto::_('combination of email and password incorrect')?>
                    </div>
                    <?php endif;?>
                    <img src="<?php echo @$logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    
                    <form method="post" id="frmlogin">
                        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo @$frm["idempresa"];?>">                        
                        <h1><?php echo JrTexto::_('Log In');?></h1>
                        
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="text" class="form-control" placeholder="<?php echo JrTexto::_('Username')?>" required="required" name="usuario" id="usuario" value="" maxlength="80" autocomplete="off"/>
                            <span class="fa fa-user form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>
                        <div class="form-group has-feedback" style="position: relative;">
                            <input type="password" autocomplete="off" class="form-control" placeholder="<?php echo JrTexto::_('Password')?>" required="required" name="clave" id="clave"/>
                            <span class="fa fa-lock form-control-feedback" style="position: absolute; top: 1ex; left:1ex !important;"></span>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-formInfoLogin" class="btn btn-blue" style="width: 100% !important; border-radius: 5px !important;">
                            <i class="fa fa-key"></i> <?php echo JrTexto::_('login')?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link"><?php echo JrTexto::_('have you forgotten your password?')?>
                                <a href="#torecuperar" class="to_recuperar"> <?php echo JrTexto::_('Restore password')?></a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <!--div>
                                <h1></h1>

                                <p>© 2016 <?php echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div-->
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="recuperar" class="animate form">
                <section class="login_content">
                    <img src="<?php echo $logo; ?>" class="img-fluid img-responsive" alt="_logo_" id="logo">
                    <form method="post" id="formRecupClave" onsubmit="return false;">
                        <input type="hidden" name="idempresa" value="<?php echo @$frm["idempresa"];?>">
                        <input type="hidden" name="idproyecto" value="<?php echo @$frm["idproyecto"];?>">
                        <h1><?php echo JrTexto::_('Recover password')?></h1>
                        <div class="form-group has-feedback">
                            <input type="text" name="usuario" id="usuariorecover" class="form-control" placeholder="<?php echo JrTexto::_('User or email')?>" required="required" maxlength="80" />
                            <span class="glyphicon glyphicon-envelope form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div>
                            <button type="submit" id="#btn-enviar-formInforecuperarclave" class="btn btn-primary submits"> <?php echo JrTexto::_('Recover password')?></button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <p class="change_link">
                                <a href="#tologin" class="to_recuperar"><?php echo JrTexto::_('Return to login')?></a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <!-- <div>
                                <h1></h1>

                                <p>© 2016 <?php echo JrTexto::_('All rights reserved').' <br>'.$this->pasarHtml($this->oNegConfig->get('nombre_sitio'));?> </p>
                            </div> -->
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>
</div>
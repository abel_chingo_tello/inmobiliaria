<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
require_once(dirname(dirname(__FILE__)).'/config.php');
$__carpeta__ = explode(SD, dirname(__FILE__));
$__carpeta__ = end($__carpeta__);
$__config__ = json_decode(_CONFIG_, true)[$__carpeta__];
define('_mitema_', $__config__["tema"]);
define('_sitio_', $__carpeta__);
define('RUTA_PLANTILLAS', RUTA_BASE. _sitio_. SD . 'plantillas' . SD);
define('RUTA_TEMA', RUTA_PLANTILLAS. _mitema_.SD);
define('URL_SITIO',URL_BASE._sitio_."/");
define('URL_TEMA',URL_SITIO."plantillas/"._mitema_."/");
define('RUTA_SITIO', RUTA_BASE. _sitio_. SD);
define('IS_LOGIN', $__config__["_islogin_"]);
require_once(RUTA_LIBS . 'cls.JrCargador.php');
require_once(RUTA_LIBS . 'ACHT'.SD.'jrFram.php');
require_once(RUTA_LIBS . 'ACHT'.SD.'documento'.SD.'cls.JrDocumento.'.'php');
try {
	JrCargador::clase('inc::ConfigSitio',RUTA_BASE);
	JrCargador::clase('inc::Sitio',RUTA_BASE);
	$aplicacion = Sitio::getInstancia();
} catch(Exception $e){
	echo json_encode(array('code'=>'Error','msj'=>'Imposible iniciar la aplicacion: '.$e->getMessage()));
	exit();
}
$aplicacion->iniciar();

if(!empty($_REQUEST["idioma"])){	
	$documento = &JrInstancia::getDocumento();	
	$idioma=$_REQUEST["idioma"];
	$documento->setIdioma($idioma);
}

$aplicacion->enrutar(JrPeticion::getPeticion(0), JrPeticion::getPeticion(1));
echo $aplicacion->presentar();

function mostar__($rec, $ax){
	global $aplicacion;	
	$aplicacion->iniciar();
	$aplicacion->enrutar($rec, $ax);
	echo $aplicacion->presentar();
}
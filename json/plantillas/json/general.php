<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>

<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/owl-carousel/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/headereffects/css/component.css">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/headereffects/css/normalize.css" />
<link href="<?php echo URL_STATIC;?>librerias/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>

<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_STATIC;?>librerias/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->

<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo URL_STATIC;?>templates/webarch/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_STATIC;?>templates/webarch/css/magic_space.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_STATIC;?>templates/webarch/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_STATIC;?>templates/css/animate.min.css" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/v1.8/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/funciones.js?version=<?php echo _version_;?>"></script>
<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/css/settings.css" media="screen" />

</head>
<body>
<jrdoc:incluir tipo="docsCss" />
<div class="main-wrapper">
    <jrdoc:incluir tipo="recurso" />
</div>
<script src="<?php echo URL_STATIC;?>librerias/bootstrap/v3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/jquery/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="<?php echo URL_STATIC;?>librerias/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/parrallax/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-appear/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-numberAnimate/jquery.animateNumbers.js" ></script> 
<script type="text/javascript" src="<?php  echo URL_TEMA;?>/js/core.js"></script>
<jrdoc:incluir tipo="docsJs" />
</body>
</html>

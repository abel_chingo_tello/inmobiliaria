<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>

<link rel="stylesheet" type="text/css" href="<?php echo $rutastatic;?>/librerias/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $rutastatic;?>/librerias/owl-carousel/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $rutastatic;?>/librerias/headereffects/css/component.css">
<link rel="stylesheet" type="text/css" href="<?php echo $rutastatic;?>/librerias/headereffects/css/normalize.css" />
<link href="<?php echo $rutastatic;?>/librerias/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>

<!-- BEGIN CORE CSS FRAMEWORK -->
<link href="<?php echo $rutastatic;?>/librerias/bootstrap/v3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $rutastatic;?>/librerias/bootstrap/v3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $rutastatic;?>/librerias/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->

<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo $rutastatic;?>/templates/webarch/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $rutastatic;?>/templates/webarch/css/magic_space.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $rutastatic;?>/templates/webarch/css/responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $rutastatic;?>/templates/css/animate.min.css" rel="stylesheet" type="text/css"/>
<!-- END CSS TEMPLATE -->

<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/v1.8/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/templates/js/funciones.js?version=<?php echo _version_;?>"></script>
<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $rutastatic;?>/librerias/jquery/rs-plugin/css/settings.css" media="screen" />

</head>
<body>
<jrdoc:incluir tipo="docsCss" />
<div class="main-wrapper">
	<header id="ha-header" class="ha-header ha-header-hide "  >
	<div class="ha-header-perspective">
<div class="ha-header-front navbar navbar-default">		  
		  <jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
		</div>
	</div>
</header>
<div class="section ha-waypoint"  data-animate-down="ha-header-hide" data-animate-up="ha-header-hide">
<div role="navigation" class="navbar navbar-transparent navbar-top">
     <div class="container">
      <jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
    </div>
    </div>
	<!--BEGIN SLIDER -->
	<div class="tp-banner-container">
		<div class="tp-banner" id="home" >
			<ul>
				<!-- SLIDE  -->
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700" >
					<!-- MAIN IMAGE -->
					<img src="assets/img/bg/slide_one.jpg"   alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">


					<!-- LAYERS -->
					<div class="tp-caption mediumlarge_light_white_center sft tp-resizeme slider"
							data-x="center" data-hoffset="0"
							data-y="60"
							data-speed="500"
							data-start="800"
							data-easing="Power4.easeOut"
							data-endspeed="300"
							data-endeasing="Power1.easeIn"
							data-captionhidden="off"
							style="z-index: 6"><h2 class="text-white custom-font title ">WE'VE DESIGN<br> EVERYTHING JUST FOR YOU</h2>

					</div>
					<div class="tp-caption sfb slider tp-resizeme slider"
						data-x="center"
						data-hoffset="0"
						data-y="240"
						data-speed="800"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 6">	<a href="#" class="btn btn-purple btn-lg  btn-large m-r-10">BUY IT NOW</a>
					</div>
					<div class="tp-caption fade slider tp-resizeme slider"
						data-x="center"
						data-hoffset="0"
						data-y="300"
						data-speed="500"
						data-start="800"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 6">	<a href="#" class="text-white m-r-10">or subscribe to newsletter</a>
					</div>
				</li>					
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700" >
					<!-- MAIN IMAGE -->
					<img src="assets/img/bg/picture-1.jpg"   alt="slidebg2"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

					<!-- LAYERS -->
					<div class="tp-caption mediumlarge_light_white_center sft tp-resizeme slider"
							data-x="center" data-hoffset="0"
							data-y="60"
							data-speed="500"
							data-start="800"
							data-easing="Power4.easeOut"
							data-endspeed="300"
							data-endeasing="Power1.easeIn"
							data-captionhidden="off"
							style="z-index: 6"><h2 class="text-white custom-font title ">WE'VE DESIGN<br> EVERYTHING JUST FOR YOU</h2>

					</div>
					<div class="tp-caption sfb slider tp-resizeme slider"
						data-x="center"
						data-hoffset="0"
						data-y="240"
						data-speed="800"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 6">	<a href="#" class="btn btn-danger btn-lg  btn-large m-r-10">BUY IT NOW</a>
					</div>
					
				</li>	
				<!-- SLIDE  -->
			<li data-transition="parallaxtoright" data-slotamount="7" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
						<!-- MAIN IMAGE -->
						<img src="assets/video/first-frame.jpg"  alt="video_forest"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
					
						<!-- LAYERS -->

						<!-- LAYER NR. 1 -->
						<div class="tp-caption tp-fade fadeout fullscreenvideo"
							data-x="0"
							data-y="0"
							data-speed="1000"
							data-start="1100"
							data-easing="Power4.easeOut"
							data-endspeed="1500"
							data-endeasing="Power4.easeIn"
							data-autoplay="true"
							data-autoplayonlyfirsttime="false"
							data-nextslideatend="true"
							data-forceCover="1"
							data-dottedoverlay="twoxtwo"
							data-aspectratio="16:9"
							data-forcerewind="on"
							style="z-index: 2">


						 <video class="video-js vjs-default-skin" preload="none" style="width:100%;height:100%;"
						poster='assets/video/first-frame.jpg' data-setup="{}">
						<source src='assets/video/home.mp4' type='video/mp4' />
						<source src='assets/video/home.webm' type='video/webm' />
						<source src='assets/video/home.ogv' type='video/ogg' />
						</video>

						</div>

						<!-- LAYER NR. 2 -->
						<div class="tp-caption large_bold_white_25 customin customout tp-resizeme"
							data-x="center" data-hoffset="0"
							data-y="170"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1400"
							data-easing="Power4.easeOut"
							data-endspeed="600"
							data-endeasing="Power0.easeIn"
							style="z-index: 3">The clearest way into the Universe<br/>is through a forest wilderness.

						</div>

						<!-- LAYER NR. 3 -->
						<div class="tp-caption medium_text_shadow customin customout tp-resizeme"
							data-x="center" data-hoffset="0"
							data-y="bottom" data-voffset="-140"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1700"
							data-easing="Power4.easeOut"
							data-endspeed="600"
							data-endeasing="Power0.easeIn"
							style="z-index: 4">John Muir
						</div>
					</li>
			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</div>
	<!--END SLIDER -->
</div>



	
	<jrdoc:incluir tipo="recurso" />
	<jrdoc:incluir tipo="modulo" nombre="pie" /></jrdoc:incluir>
</div>
<script src="<?php echo $rutastatic;?>/librerias/bootstrap/v3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $rutastatic;?>/librerias/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo $rutastatic;?>/librerias/jquery/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="<?php echo $rutastatic;?>/librerias/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo $rutastatic;?>/librerias/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/parrallax/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/jquery-appear/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo $rutastatic;?>/librerias/jquery/jquery-numberAnimate/jquery.animateNumbers.js" ></script> 
<script type="text/javascript" src="<?php  echo URL_TEMA;?>/js/core.js"></script>
<jrdoc:incluir tipo="docsJs" />
</body>
</html>

class Login{
	btnlogin;
	btnrecover;
	frm;
	formRecupClave;
	constructor(params = null){
		this.func=new _acht_();
        /*this.section = $(document);
        this.identify();*/
    }
   /* identify() {
        if (__oCurso.hasOwnProperty('idcurso')) {
            this.curso = __oCurso;
        } else {
            this.curso = {
                avance_secuencial: 0,
                idioma: "ES"
            }
        }
    }*/
    ini(){
    	this.addEventos();
    }
    addEventos(){
    	this.frm=document.getElementById("frmlogin");
    	this.frm.addEventListener('submit', (ev)=>{this.logear(ev)}, false);
    	this.formRecupClave=document.getElementById("formRecupClave");
    	this.formRecupClave.addEventListener('submit', (ev)=>{this.recuperarclave(ev)}, false);
    }
    //eventos
    logear(event){
    	event.preventDefault();  
    	let el=event.target[0];
    	if(el.classList.contains('disabled')) return false;
    	el.classList.add('disabled');
    	this.func.postData({
            url: '',
            data: new FormData(this.frm)
        }).then(rs => { 
        	el.classList.remove('disabled');
        	Swal.close();
        	if(rs.return!='') window.location.href=_sysUrlBase_+rs.return;
        	else window.location.href=_sysUrlBase_;
        }).catch(e => {
        	let datos={text:e.msj}
        	el.classList.remove('disabled');
            this.func.swalError(datos);
        })
    }
    recuperarclave(event){
    	event.preventDefault();
    	this.func.showLoading();
    	this.func.postData({
            url: 'sesion',
            data:  new FormData(this.frmlogin)
        }).then(rs => {
            this.selectExamenes.trigger('change.select2');
            this.oHabilidadPuntajeEx = null;
            Swal.close();
        }).catch(e => {
            this.func.swalError();
        })
    }
}


window.onload = function(){
	const oLogin = new Login;
    oLogin.ini();
};
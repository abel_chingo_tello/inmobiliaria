<!DOCTYPE html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google" content="notranslate" />
	<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>

	<!-- BEGIN CORE CSS FRAMEWORK -->
	<link href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL_STATIC;?>librerias/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!-- END CORE CSS FRAMEWORK -->

	<!-- BEGIN CSS TEMPLATE -->
	<link href="<?php echo URL_STATIC;?>templates/css/animate.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL_STATIC;?>templates/css/hover.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL_TEMA ?>login/login01/login01.css?vs=<?php echo _version_; ?>" rel="stylesheet" type="text/css"-->
	<!-- END CSS TEMPLATE -->
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/v3.4/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/sweetalert/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/funciones.js?version=<?php echo _version_;?>"></script>
	<jrdoc:incluir tipo="cabecera" />
	<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
	<script src="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<jrdoc:incluir tipo="docsCss" />
<jrdoc:incluir tipo="recurso" />
<script type="text/javascript" src="<?php echo URL_BASE ?>static/librerias/abelchingo/abelchingo.js"></script>
<script type="text/javascript" src="<?php echo URL_TEMA ?>login/login01/login01.js"></script>
<jrdoc:incluir tipo="docsJs" />
</body>
</html>

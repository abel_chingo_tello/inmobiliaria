<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::NegGenerador', RUTA_BASE);
class Generador extends JrWeb
{
	//private $oNegSesion;
	private $oNegConfiguracion;
	private $oNegGenerador;
	
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGenerador = new NegGenerador;		
	}
	
	public function index()
	{
		try{
			global $aplicacion;	
			$this->tablas=$this->oNegGenerador->getTablas();
			$this->documento->setTitulo(JrTexto::_('Generador'), true);
			$this->esquema = 'generador/plantilla';
			$this->documento->plantilla = 'general';
			return  parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj(JrTexto::_($e->getMessage()), false, 'error');
			$aplicacion->redir();
		}
	}

	public function getcampos(){
		try {
			if(empty($_REQUEST["tb"])) echo json_encode(array('code'=>'error','msj'=>'tabla no selecionada'));
			$campos=$this->oNegGenerador->getCampos($_REQUEST["tb"]);
			if(!empty($campos))
				echo json_encode(array('code'=>'200','datos'=>$campos));
			else{
				echo json_encode(array('code'=>'error','msj'=>'No exiten campos'));	
			}					
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Error al obtener campos'));
		}
		exit(0);
	}

	public function generarPagina(){
		try {

			if(empty($_REQUEST)) echo json_encode(array('code'=>'error','msj'=>'datos nullos'));
			$frm=$_REQUEST;

			$this->tablaActiva=@$frm["tablaActiva"];
			$this->tabla=$frm["tablaActiva"];
			$this->pk=@$frm["pk"];
			$this->campos=@$frm["campo"];
			$this->frm=$frm;
                   
	        if(!empty($frm["createdatos"])){	                	
				$archivo=RUTA_BASE.'sys_datos'.SD.'cls.Dat'.ucfirst($this->tabla).'.php';
				if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_datos'.SD.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.php'); 						
				$fp = fopen($archivo, "a");
				$this->esquema = 'generador/datos';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}
			if(!empty($frm["createnegocio"])){
				$archivo=RUTA_BASE.'sys_negocio'.SD.'cls.Neg'.ucfirst($this->tabla).'.php';
				if(file_exists($archivo)) @rename($archivo,RUTA_BASE.'sys_negocio'.SD.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.php');
				$fp = @fopen($archivo, "a");
				$this->esquema = 'generador/negocio';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}
			if(!empty($frm["createclasewebadmin"])){
				$tpl_web=RUTA_BASE.'admin'.SD;
				$archivo=$tpl_web.'cls.'.ucfirst($this->tabla).'.php';
				if(file_exists($archivo)) @rename($archivo,$tpl_web.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.php');
				$fp = @fopen($archivo, "a");
				$this->esquema = 'generador/webadmin';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}

			if(!empty($frm["createlistadowebadmin"])){
				$tpl_web=RUTA_BASE.'admin'.SD."html".SD;
				$archivo=$tpl_web.$this->tabla.'.php';
				if(file_exists($archivo)) @rename($archivo,$tpl_web.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.php');
				$fp = @fopen($archivo, "a");
				$this->esquema = 'generador/listadoadmin';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}

			if(!empty($frm["createjs"])){
				$tpl_web=RUTA_STATIC.'librerias'.SD.'abelchingo'.SD;
				$archivo=$tpl_web.$this->tabla.'.js';
				if(file_exists($archivo)) @rename($archivo,$tpl_web.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.js');
				$fp = @fopen($archivo, "a");
				$this->esquema = 'generador/js';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}		

			if(!empty($frm["createclasewebjson"])){
				$tpl_web=RUTA_BASE.'json'.SD;
				$archivo=$tpl_web.'cls.'.ucfirst($this->tabla).'.php';
				if(file_exists($archivo)) @rename($archivo,$tpl_web.'_remove_'.$this->tabla.'_'.date("Ymd_Hms").'_old.php');
				$fp = @fopen($archivo, "a");
				$this->esquema = 'generador/json';
				$tpl_esquema=parent::getEsquema();
				$write = fputs($fp,$tpl_esquema);
				fclose($fp);
				@chmod($archivo,0777);
			}		
			echo json_encode(array('code'=>200,'msj'=>'Archivos Generados'));
		} catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Error al obtener campos'));
		}
		exit(0);
	}
}
<?php
/**
 * @autor		Chingo Tello Abel
 * @fecha		15/10/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegRegistro_bien', RUTA_BASE);
JrCargador::clase('sys_negocio::NegCatalogo_sbn', RUTA_BASE);
JrCargador::clase('sys_negocio::NegTabla', RUTA_BASE);
JrCargador::clase('sys_negocio::NegGaleria_fotos', RUTA_BASE);

class WebIndex extends JrWeb
{
	private $oNegRegistro_bien;
	private $oNegTabla_sbn;
	public function __construct()
	{
		parent::__construct();
		$this->oNegRegistro_bien = new NegRegistro_bien();
		$this->oNegCatalogo_sbn = new NegCatalogo_sbn();
		$this->oNegTabla_sbn = new NegTabla();
		$this->oNegGaleria_fotos = new NegGaleria_fotos();
	}

	public function index()
	{
		global $aplicacion;
		$filtros=array("rebi_alquiler"=>"0");
		$inner_join=array("tabla"=>"catalogo_sbn","id_tabla"=>"casb_codigo","fk_origen"=>"casb_codigo");
		$this->bienes=$this->oNegRegistro_bien->listarPrueba($filtros,$inner_join);
		//var_dump($this->bienes);exit();

		$this->catalogo=$this->oNegCatalogo_sbn->buscar();
		$this->tipos_proveedor=$this->oNegTabla_sbn->buscar(array("tabl_tipo" => "TIPO_PROVEEDOR" ));	
		$this->tipo_bien=$this->oNegTabla_sbn->buscar(array("tabl_tipo" => "TIPO_INTERIOR_INMUEBLE" ));	
		$empresa=NegSesion::getEmpresa();
		$this->documento->plantilla ="index";
		$this->esquema = 'page/inicio';
		$empresa=NegSesion::getEmpresa();
		return parent::getEsquema();
	}
	public function detalles(){

		global $aplicacion;
		$filtros_galeria=array("estado"=>"1","rebi_id"=>$_REQUEST["_v3_"]);
		$this->fotos=$this->oNegGaleria_fotos->listar($filtros_galeria);

		$filtros_rebi = array("rebi_id"=>$_REQUEST["_v3_"]);
		$this->bien=$this->oNegRegistro_bien->buscar($filtros_rebi);	
			
		$empresa=NegSesion::getEmpresa();
		$this->documento->plantilla ="detalle";
		$this->esquema = 'page/detalle';
		//echo $param;
		return parent::getEsquema();
	}

	

	

}
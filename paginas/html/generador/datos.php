<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$notomar=array('fecha_insert','fecha_update',$this->pk);
$frm=$this->frm;
$inner='';
$nuevalista='';
$ifk=1;
$aliastb='tb1';
$camposfk='';
$allcampos=array();
echo '<?php
'; ?>
<?php 
		$x='';
		$strcampos='';
		$filtros='';
		$hayorder=false;
		foreach ($campos as $campo) {
			$allcampos[]=$campo;
			if($campo=='order'||$campo=='orden') $hayorder=true;
			$verenlistado='_chk'.$campo;
			$filtros.='
			if(isset($filtros["'.$campo.'"])) {
					$cond[] = "'.$campo.' = " . $this->oBD->escapar($filtros["'.$campo.'"]);
			}'.(($campo=='estado')?('else{ $cond[] = "'.$campo.' >=0 ";}
			'):'');
			if(empty($frm[$verenlistado])) continue;
					if(@$frm["tipo_".$campo]=='fk'){
						if(!empty($frm["tipo2_".$campo])){
							$tb2=$frm["tipo2_".$campo];							
							if(!empty($frm["tipofkid_".$campo])&&!empty($frm["tipofkver_".$campo])){
								$ifk++;
								$aliastb2='tb'.$ifk;
								$fkid=$frm["tipofkid_".$campo];
								$fkver=$frm["tipofkver_".$campo];
								$inner.=' INNER JOIN '.$tb2.' '.$aliastb2.' ON '.$aliastb.'.'.$campo.'='.$aliastb2.'.'.$fkid.' ';
								$camposfk.=','.$aliastb2.'.'.$fkver.' AS _'.$fkver.' ';
								$nuevalista.='';
							}
						}

					}					
					
							if(!in_array($campo, $notomar)){
							$strcampos.=",$".$campo;
							$x.= "
							,'".$campo."'=>$".$campo."";
							}
					
	}
		$strcampos=substr($strcampos, 1);
	?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?>  
  * @copyright	Copyright (C) <?php echo date("Y") ?>. Todos los derechos reservados.
 */ 
class Dat<?php echo  $tb ?> extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT <?php echo @implode(",", $allcampos); ?> FROM <?php echo $this->tablaActiva; ?>";
			$cond = array();
			<?php echo $filtros; ?>			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM <?php echo $this->tablaActiva; ?>";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
		
	public function insertar(<?php echo $strcampos ?>)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT <?php echo $this->pk ?>+1 FROM <?php echo $this->tablaActiva; ?> ORDER BY <?php echo $this->pk ?> DESC limit 0,1 ");			
			$estados = array(<?php echo substr(trim($x),1);?>							
							);			
			return $this->oBD->insert('<?php echo $this->tablaActiva; ?>', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_<?php echo $this->tabla; ?>_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	public function actualizar($id, <?php echo $strcampos ?>)
	{
		try {			
			$estados = array(<?php echo substr(trim($x),1);?>								
							);
			$this->oBD->update('<?php echo $this->tablaActiva; ?> ', $estados, array('<?php echo $this->pk ?>' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('<?php echo $this->tablaActiva; ?>', array('<?php echo $this->pk ?>' => $id));
			else 
				return $this->oBD->update('<?php echo $this->tablaActiva; ?>', array('estado' => -1), array('<?php echo $this->pk ?>' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('<?php echo $this->tablaActiva; ?>', array($propiedad => $valor), array('<?php echo $this->pk ?>' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}  

	<?php if($hayorder) { ?>
	
	public function guardarorden($datos)
	{
		try {
			$data=json_decode($datos,true);
			foreach ($data as $key => $va){	
				$estados=array('orden' =>$va["orden"],'idpadre'=>!empty($va["idpadre"])?$va["idpadre"]:'null');
				$this->oBD->update('<?php echo $this->tablaActiva; ?>', $estados, array('<?php echo $this->pk ?>' => $va["<?php echo $this->pk ?>"]));				
			}	
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("<?php echo $this->tablaActiva; ?>").": " . $e->getMessage());
		}
	}

	<?php } ?>
	 
	<?php echo $nuevalista; ?>	
}
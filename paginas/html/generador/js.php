<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$notomar=array('usuario_registro','fecha_insert','fecha_update',$this->pk);
$hayorder=false;
$haydate=false;
$haycolor=false;
$txtbuscar_='';
foreach ($campos as $key => $cp){
	if($cp=='order'||$cp=='orden') $hayorder=true;			
	if(in_array($cp, $notomar))continue;
  	$tipo=$frm['tipo_'.$cp];	
  	if($tipo=='color') $haycolor=true;		  
  	if($tipo=='date'||$tipo=='datetime') $haydate=true;
  	if($tipo=='fk' || $tipo=='radiobutton'||$tipo=='checkbox'){ 
		$tipo2=$frm['tipo2_'.$cp];
	    if($tipo2!=''){ 
     	$txtbuscar_.="
     	.on('change','select#bus".$cp."',function(ev){ev.preventDefault(); _this.vista_tabla();})";
		}
	} 
} 
?>
class BD<?php echo $tb; ?>{
	ventana=null;
	ventanaid=null;	
	frmid='frm<?php echo $this->tabla;?>';
	tb='<?php echo $this->tabla;?>';
	$fun=null;
	bustexto='';
	<?php echo $campopk;?>=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.<?php echo $campopk;?>=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
			<?php echo $hayorder?(".on('click','#aquitable .btnordenartb',function(ev){ ev.preventDefault(); _this.vista_ordenar();})"):('');?>
			<?php echo $txtbuscar_;?>;
		}
		<?php if($hayorder){?>
		if(this.ventana.find('#vistaordenar').length){
			this.ventana.find('#vistaordenar')
			.on('click', '.btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})
			.on('click', '.btncancelar',function(ev){ ev.preventDefault(); 
				if(_this.ventana.find('#aquitable').length){
					_this.ventana.find('.vistatablaformulario').hide(0);
					_this.ventana.find('#vistaordenar').hide(0);
					_this.ventana.find('#aquitable').fadeIn(500);
				}else window.location.reload();
			});
		}
		<?php } ?>

		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
		<?php if($haydate){ ?>
		$('.input-append.date').datepicker({
				autoclose: true,
				todayHighlight: true
	   	});
	   <?php } ?>
	   <?php if($haycolor){ ?>
		$('.colorpicker').colorpicker({format: 'rgba'});
		$('.colorpicker').on('change', function(e) {
            $(this)[0].style.backgroundColor = $(this).val();
        });
	   <?php } ?>
	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
		<?php 	
			$filtroabuscar="";		
			foreach ($campos as $key => $cp){			  
			  if(in_array($cp, $notomar))continue;
			  $tipo=$frm['tipo_'.$cp];			  
			  if($tipo=='fk' || $tipo=='radiobutton'||$tipo=='checkbox'){ 
			    $tipo2=$frm['tipo2_'.$cp];
			    if($tipo2!=''){ ?>
		data.append('<?php echo $cp;?>',$('select#bus<?php echo $cp;?>').val()||'');
		<?php } } } ?>
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <?php                    
                foreach ($campos as $key => $cp){
                	if(in_array($cp, $notomar))continue;
                	$tipo=$frm['tipo_'.$cp];
                	if($tipo=='fk' || $tipo=='textArea') continue; 
                ?><th><?php echo ucfirst($cp); ?></th>
            	<?php } 
            	?><th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.<?php echo $this->pk;?>}" idpk="<?php echo $this->pk;?>">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.<?php echo $this->pk;?>}" type="checkbox" value="0">
                      <label for="checkbox{$v.<?php echo $this->pk;?>}"></label>
                    </div></div></td>
					<?php foreach ($campos as $key => $cp){
				$tipo=!empty($frm['tipo_'.$cp])?$frm['tipo_'.$cp]:false;
			  if(in_array($cp, $notomar))continue;			  
			  else if($tipo=='fk'|| $tipo=='textArea') continue; 
			  else if($tipo=='radiobutton'||$tipo=='checkbox'){ 
			  	?><td><div class="row-fluid"><div cp="<?php echo $cp;?>" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.<?php echo $cp;?>}" ${v.<?php echo $cp;?>==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.<?php echo $cp;?>==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<?php }else if($tipo=='file'){
				$tipo2=$frm["tipo2_".$cp];
      			if($tipo2=="imagen"){  
      				?><td>${v.<?php echo $cp;?>==null?'':'<img src="'+_sysUrlBase_+v.<?php echo $cp;?>+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<?php }}else{  
					?><td>${v.<?php echo $cp;?>}</td>	
					<?php } } 
					?><td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.<?php echo $this->pk;?>=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={<?php echo $this->pk;?>:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto<?php echo $hayorder?',ordenar:true':''; ?>});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		<?php 
	    $frmcampos='';
	    foreach ($campos as $key => $cp){
  			if(in_array($cp, $notomar))continue;
  			$tipo=!empty($frm['tipo_'.$cp])?$frm['tipo_'.$cp]:false; 
  			$tipotmp=array('text'=>'text','doble'=>'text','decimal'=>'text','money'=>'text','password'=>'password','email'=>'email','int'=>'number');                    
            if($tipo=='text'||$tipo=='int'||$tipo=='double'||$tipo=='decimal'||$tipo=='money' || $tipo=="password" || $tipo=="email"){
  			$frmcampos.='$frm.find(\'input#'.$cp.'\').val(rs.'.$cp.'||\'\');
  			';
  			}else if($tipo=='textArea'){
  			$frmcampos.='$frm.find(\'textarea#'.$cp.'\').val(rs.'.$cp.'||\'\');
  			';
  			}else if($tipo=='radiobutton'||$tipo=='checkbox'){
  			$frmcampos.='let '.$cp.'=rs.'.$cp.'||1;
			$frm.find(\'input#'.$cp.'\').val('.$cp.');
			if('.$cp.'==1){
				$frm.find(\'input#'.$cp.'\').attr(\'checked\',\'checked\');
				$frm.find(\'input#'.$cp.'\').siblings(\'label\').text(\'Activo\');
			}else {
				$frm.find(\'input#'.$cp.'\').removeAttr(\'checked\');
				$frm.find(\'input#'.$cp.'\').siblings(\'label\').text(\'Inactivo\');
			}
  			';
  			}else if($tipo=='combobox'||$tipo=='fk'){
  			$frmcampos.='$frm.find(\'select#'.$cp.'\').val(rs.'.$cp.'||\'\').trigger(\'change\');
  			';
  			}else if($tipo=='file'){
  			$tipo2=$frm["tipo2_".$cp];
      		if($tipo2=="imagen"){
      		$frmcampos.='let '.$cp.'default=\'static/media/defecto/nofoto.jpg\';
      		let __'.$cp.'=(rs.'.$cp.'==\'\'||rs.'.$cp.'==\'null\'||rs.'.$cp.'==null)?'.$cp.'default:rs.'.$cp.';
      		$frm.find(\'img#fileimg'.$cp.'\').attr(\'src\',_sysUrlBase_+(__'.$cp.')).attr(\'link\',__'.$cp.');
  			let container'.$cp.'=document.querySelector(\'.img-container.img-'.$cp.'\');
	        let opt'.$cp.'={
	            container:container'.$cp.',
	            image:container'.$cp.'.getElementsByTagName(\'img\').item(0),
	            imagedefault:container'.$cp.'.getElementsByTagName(\'img\').item(0).getAttribute(\'src\'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt'.$cp.');
        ';
      		?>
    	<?php }}} 
    	?>
    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#<?php echo $campopk; ?>').val(rs.<?php echo $campopk; ?>);
			<?php echo $frmcampos;?>
	    }
	    if(this.<?php echo $campopk; ?>==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('<?php echo $this->pk;?>',this.<?php echo $campopk; ?>);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

<?php if($hayorder){?>
	vista_ordenar(){
		this.ventana.find('#aquitable').hide();
		this.ventana.find('#vistaordenar').show();
		var _this=this;
		var data=new FormData();
		<?php echo $filtroabuscar; ?>
		data.append('enorden',true);
		let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let strmodulo=v.nombre||'sin nombre';
					let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="fa '+(v.icono)+'"></i>');
					let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					htmlorder+=`
					<li  class="dd-item " id="${v.<?php echo $this->pk;?>}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono">${imagen+icono} </span> 
									<span class="title">${(strmodulo||'sin nombre')}</span>
								</span>
								<span class="btnacciones" >
									<i class="fa fa-pencil btneditar"></i> 
									<i class="fa fa-trash btneliminar"></i>
								</span>
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/<?php echo $this->tabla ?>/','data':data}).then(rs =>{				
				let htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar </button>
                		<button class="btn btn-default btncancelar"> <i class="fa fa-refresh"></i> Cancelar </button>
                	</div>
                </div>
                `;				
				$('#vistaordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){},
			        beforeDragStop: function(l,e, p){			        	
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);	           
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'<?php echo $this->pk ?>':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/<?php echo $this->tabla; ?>/guardarorden','data':formData}).then(rs => {
        			window.location.reload();
        			resolve();
        		}) 
        })
	}
<?php } ?>	
}
<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$notomar=array('fecha_insert','fecha_update',$this->pk);
$addcbo='';
$hayfile='';
echo '<?php';
?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?> 
 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::Neg<?php echo $tb;?>', RUTA_BASE);
class <?php echo $tb;?> extends JrWeb
{
	private $oNeg<?php echo $tb;?>;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNeg<?php echo $tb;?> = new Neg<?php echo $tb;?>;
		
	}

	public function index(){		
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb;?>', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(!empty($_REQUEST["<?php echo $campopk;?>"])) $filtros["<?php echo $campopk;?>"]=$_REQUEST["<?php echo $campopk;?>"];

			<?php 
			$hayorden=false;
           	foreach ($campos as $key => $cp){           		
  				if(in_array($cp, $notomar))continue;
  				if($cp=='orden'||$cp=='orden') $hayorden=true;
  			?>if(isset($_REQUEST["<?php echo $cp; ?>"])&&@$_REQUEST["<?php echo $cp; ?>"]!='')$filtros["<?php echo $cp; ?>"]=$_REQUEST["<?php echo $cp; ?>"]; 
  			<?php }
  			if($hayorden){?>$filtros["enorden"]=!empty($_REQUEST["enorden"])?true:false;
  			<?php } ?>if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];	
			if(!empty($_REQUEST["sqlget"])) $filtros["sqlget"]=true;
					
			$this->datos=$this->oNeg<?php echo $tb;?>->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardar(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            $usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
<?php 
           	foreach ($campos as $key => $cp){
           		$tipo=!empty($frm['tipo_'.$cp])?$frm['tipo_'.$cp]:false;
           		$tipo2=!empty($frm["tipo2_".$cp])?$frm["tipo2_".$cp]:false;
  				if(in_array($cp, $notomar))continue;
  				else if($tipo=='file' && $tipo2=='imagen'){
  				?>
  			$this->oNeg<?php echo $tb; ?>-><?php echo $cp;?>='';<?php }} ?>  			
            if(!empty($<?php echo $campopk;?>)) {
				$this->oNeg<?php echo $tb;?>-><?php echo $campopk;?> = $<?php echo $campopk;?>;
				$accion='_edit';
			}

           	<?php 
           	$addfiles='';
           	foreach ($campos as $key => $cp){
           		$tipo=!empty($frm['tipo_'.$cp])?$frm['tipo_'.$cp]:false;
           		$tipo2=!empty($frm["tipo2_".$cp])?$frm["tipo2_".$cp]:false;
  				if(in_array($cp, $notomar))continue;
  				else if($tipo=='file' && $tipo2=='imagen'){
?>if(!empty($<?php echo $cp;?>)){
           		JrCargador::clase('ACHT::JrArchivo');
           		$datosarchivo=JrArchivo::cargar_base64($<?php echo $cp;?>,'static/media/E_'.$empresaAct["idempresa"]."/U".$usuarioAct["idpersona"]."/<?php echo $this->tabla; ?>/",'<?php echo $cp;?>');
           		$this->oNeg<?php echo $tb; ?>-><?php echo $cp;?>=@$datosarchivo["link"];
	        }
  			<?php } else if($cp=='idempresa'){
  			?>$this->oNeg<?php echo $tb;?>->idempresa=!empty($idempresa)?$idempresa:$empresaAct["idempresa"];
  			<?php }else if($cp=='usuario_registro'){
  			?>$this->oNeg<?php echo $tb;?>->usuario_registro=$usuarioAct["idpersona"];
  			<?php }else{
  			?>$this->oNeg<?php echo $tb;?>-><?php echo $cp; ?>=@$<?php echo $cp; ?>;
  			<?php }}?>
	        
            if($accion=='_add') {
            	$res=$this->oNeg<?php echo $tb;?>->agregar();
            	 echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('saved successfully'),'newid'=>$res));
            }else{
            	$res=$this->oNeg<?php echo $tb;?>->editar();
            	echo json_encode(array('code'=>200,'msj'=>ucfirst(JrTexto::_('<?php echo $tb;?>')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }      
		
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	
	public function eliminar(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}

			$this->oNeg<?php echo $tb;?>->__set('<?php echo $campopk;?>', $_REQUEST['<?php echo $campopk;?>']);
			$res=$this->oNeg<?php echo $tb;?>->eliminar();			
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('Delete Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}

	public function setCampo(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNeg<?php echo $tb;?>->setCampo($_REQUEST['<?php echo $campopk;?>'],$_REQUEST['campo'],$_REQUEST['valor']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('update Record successfully')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	}  
<?php if($hayorden){?>
	public function guardarorden(){
		try {
			if(empty($_REQUEST)){ 
				echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
				exit(0);
			}
			$this->oNeg<?php echo $tb; ?>->guardarorden($_REQUEST['datos']);
			echo json_encode(array('code'=>200,'msj'=>JrTexto::_('El orden se ha actualizado')));
			exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'error','msj'=>'Datos imcompletos'));
			exit(0);
		}
	} 
<?php }?> 
}
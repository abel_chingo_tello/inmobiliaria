<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$notomar=array('usuario_registro','fecha_insert','fecha_update',$this->pk);
$addcbo='';
$hayfile='';
$hayorder=false;
foreach ($campos as $key => $cp){
  if(in_array($cp, $notomar))continue;
  $tipo=$frm['tipo_'.$cp]; 
  if($cp=='order'||$cp=='orden') $hayorder=true;
  if($tipo=='fk'){ 
    $tipo2=$frm['tipo2_'.$cp];
    if($tipo2!=''){
      $idfk=!empty($frm["tipofkid_".$cp])?$frm["tipofkid_".$cp]:$cp;
      $idverfk=!empty($frm["tipofkver_".$cp])?$frm["tipofkver_".$cp]:$cp;
    //  var_dump($frm);
      $nomlabel=!empty($frm['tipo2_'.$cp])?$frm['tipo2_'.$cp]:$cp;

      $nomlabel=str_replace('_',' ',ucfirst($nomlabel));
        $addcbo.=(($cp=='idempresa')?'<?php if($this->user["tipo_usuario"]=="s"){ ?>':'').'
                <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label">'.$nomlabel.'</label>                      
                      <select name="bus'.$cp.'" id="bus'.$cp.'" class="select2 form-control"  >
                        '.($cp=='idempresa'?'':'<option value="">Todos</option>').'
                        <?php if(!empty($this->fk'.ucfirst($cp).'))
                              foreach($this->fk'.ucfirst($cp).' as $k=>$v){ ?>         
                          <option value="<?php echo $v["'.$idfk.'"]; ?>" '.($cp=='idempresa'?('<?php echo $v["'.$idfk.'"]==$this->idempresa?\'selected="selected"\':\'\';?>'):'').' ><?php echo ucfirst($v["'.$idverfk.'"]); ?></option>
                        <?php } ?>  
                      </select>                        
                  </div>
                </div>
      '.(($cp=='idempresa')?'<?php } ?>':'');
    }
  }else if($tipo=='radiobutton'||$tipo=='checkbox'){
    if($cp=='idempresa'){
      $addcbo.='<?php if($this->user["tipo_usuario"]=="s"){?>
        ';
    }
    $addcbo.='<div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      <label class="form-label">'.ucfirst($cp).'</label>                      
                      <select name="bus'.$cp.'" id="bus'.$cp.'" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <option value="1">Activos</option>
                        <option value="0">Inactivo</option>
                        <?php if($this->user["tipo_usuario"]=="s"){ ?><option value="-1">Eliminados</option><?php } ?>
                      </select>                        
                    </div>
                </div>
      ';
    if($cp=='idempresa'){
      $addcbo.='<?php } ?>
      ';
    }
  }else if($tipo=='file'){
     $tipo2=$frm["tipo2_".$cp];
      if($tipo2=="imagen"){
        $hayfile.='$'.$cp.'=\'static/media/defecto/nofoto.jpg\';
';
      }
  }
}
echo '<?php 
$ventana="ven".date("YmdHis");
'.$hayfile;
?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?> 
 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
<?php echo '?>
'; ?>
<div id="<?php echo '<?php echo $ventana; ?>';?>" idrol="<?php echo '<?php echo $this->user["idrol"]; ?>';?>" idpersona="<?php echo '<?php echo $this->user["idpersona"]; ?>';?>" tuser="<?php echo '<?php echo $this->user["tipo_usuario"]; ?>';?>" idempresa="<?php echo '<?php echo $this->empresa["idempresa"]; ?>';?>" >
  <div class="vistatabla">
    <ul class="breadcrumb">
      <li><a href="<?php echo'<?php echo URL_SITIO; ?>';?>" class="">Inicio</a></li>
      <li><a href="#" class="breadcrumbactive active"><?php echo str_replace('_',' ',$tb); ?></a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple">
          <?php if(!empty($addcbo)){ ?>
          <div class="grid-body">
            <form id="frmbuscar" name="frmbuscar">
              <div class="row">
                <?php echo $addcbo; ?>
              </div>
            </form>
          </div><?php } ?>
          <div class="grid-body" id="aquitable" >
          </div>
          <?php if($hayorder){ ?>
          <div class="grid-body" id="vistaordenar" style="display: none;">
            aaaaa
          </div>
          <?php } ?>
        </div>    
      </div>
    </div>
  </div>
  <div class="vistatablaformulario" style="display: none;">
    <ul class="breadcrumb">
      <li><a href="<?php echo'<?php echo URL_SITIO; ?>';?>" class="">Inicio</a></li>
      <li><a href="<?php echo'<?php echo URL_SITIO; ?>';?><?php echo $this->tabla; ?>" class=""><?php echo str_replace('_',' ',$tb); ?></a></li>
      <li><a href="#" class="breadcrumbactive active">Editar</a></li>
    </ul>
    <div class="row-fluid">
      <div class="span12">
        <div class="grid simple" >         
          <div class="grid-body" id="aquiformulario" >
            <form id="frm<?php echo $this->tabla; ?>" action="#">
            <input type="hidden" name="<?php echo $campopk;?>" id="<?php echo $campopk;?>" value="">
                <div class="row"><?php
                    $notomar=array('usuario_registro','fecha_insert','fecha_update',$this->pk);
                    foreach ($campos as $key => $cp){
                      if(in_array($cp, $notomar))continue;
                      $tipo=$frm['tipo_'.$cp]; 
                      $nomlabel=$cp;
                      if($tipo=='fk'){
                        $nomlabel=!empty($frm['tipo2_'.$cp])?$frm['tipo2_'.$cp]:$cp;
                      }
                      $nomlabel=str_replace('_',' ',ucfirst($nomlabel));
?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="form-label"><?php echo $nomlabel; ?> <span class="help"> </span> </label><?php 
                     $tipotmp=array('text'=>'text','doble'=>'text','decimal'=>'text','money'=>'text','password'=>'password','email'=>'email','int'=>'number');                    
                      if($tipo=='text'||$tipo=='int'||$tipo=='double'||$tipo=='decimal'||$tipo=='money' || $tipo=="password" || $tipo=="email"){ ?>

                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="<?php echo @$tipotmp[$tipo]; ?>" name="<?php echo $cp; ?>" id="<?php echo $cp; ?>" class="form-control" required placeholder="">
                      <span class="error"><?php echo '<?php echo JrTexto::_(\'Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9\') ?>';?></span>
                    </div>
                    <?php }else if($tipo=='textArea'){ ?>
                                       
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                       <textarea name="<?php echo $cp; ?>" id="<?php echo $cp; ?>" class="form-control" placeholder=""></textarea>
                      <span class="error"><?php echo '<?php echo JrTexto::_(\'Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9\') ?>';?></span>
                    </div>
                     <?php }else if($tipo=='color'){
                      $haycolor=true;
                    ?>                   
                    <div class="input-with-icon  " claseerror="success-control" claseok="error-control">
                      <input type="text" style="background-color: " name="<?php echo $cp; ?>" id="<?php echo $cp; ?>" class="form-control colorpicker" />                        
                    </div>
              
                    <?php }else if($tipo=='radiobutton'||$tipo=='checkbox'){ ?>

                      <div class="row-fluid">
                        <div class="cambiarestado checkbox check-primary <?php echo $tipo=='radiobutton'?'checkbox-circle':'';?>" >
                          <input type="checkbox" value="0" name="<?php echo $cp; ?>" id="<?php echo $cp; ?>" v1="Activo" v2="Inactivo">
                          <label><span>Inactivo</span></label>
                        </div>
                      </div><?php }else if($tipo=='combobox'){ 
                      ?><div class="input-with-icon  right">
                        <i class="error fa-exclamation fa"></i>
                        <i class="success fa fa-check"></i>
                        <select name="<?php echo $cp; ?>" id="<?php echo $cp; ?>" class="select2 form-control"  >
                          <option value="">Ninguno</option>                               
                        </select>                        
                      </div><?php }else if($tipo=='fk'){ 
?>
                      <select name="<?php echo $cp;?>" id="<?php echo $cp;?>" class="select2 form-control"  >
                        <option value="">Todos</option>
                        <?php 
                        $idfk=!empty($frm["tipofkid_".$cp])?$frm["tipofkid_".$cp]:$cp;
                        $idverfk=!empty($frm["tipofkver_".$cp])?$frm["tipofkver_".$cp]:$cp;
                        echo '<?php if(!empty($this->fk'.ucfirst($cp).'))
                              foreach($this->fk'.ucfirst($cp).' as $k=>$v){ ?>';
?>
                          <option value="<?php echo '<?php echo $v["'.$idfk.'"]; ?>';?>"><?php echo '<?php echo $v["'.$idverfk.'"] ?>';?></option>
                        <?php echo '<?php } ?>';?> 
                      </select>
                    <?php } else if($tipo=='file'){
                      $tipo2=$frm["tipo2_".$cp];
                      if($tipo2=="imagen"){
                    ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="img-container img-<?php echo $cp;?>">
                            <img id="fileimg<?php echo $cp; ?>" style="max-width: 100%;"  src="<?php echo'<?php echo URL_MEDIA.$'.$cp.'; ?>';?>" alt="Picture">
                            <input type="hidden" name="<?php echo $cp;?>" tipo="url" >
                        </div>
                        <div class="col-md-12 text-center">
                          <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="btn-group mr-2  btn-group-sm" role="group" aria-label="First group">
                              <button type="button" class="btn btn-secondary btnrotate0"><i class="fa fa-rotate-left"></i></button>
                              <button type="button" class="btn btn-secondary btnrotate1"><i class="fa fa-rotate-right"></i></button>
                              <button type="button" class="btn btn-secondary btnzoom0"><i class="fa fa-search-minus"></i></button>
                              <button type="button" class="btn btn-secondary btnzoom1"><i class="fa fa-search-plus"></i></button>
                            </div>
                            <div class="btn-group  btn-group-sm" role="group" aria-label="Third group">
                              <button type="button" class="btn btn-primary btnupload"><i class="fa fa-upload"></i></button>
                              <button type="button" class="btn btn-warning btnrefresh"><i class="fa fa-refresh"></i></button>
                              <button type="button" class="btn btn-danger btndefault"><i class="fa fa-trash-o"></i></button>
                              <button type="button" class="btn btn-success btndownload"><i class="fa fa-download"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div><?php  }  } ?>
                  </div>
                </div>          
              <?php } ?>

              </div>
              <div class="form-actions">  
                <div class="text-center">
                  <button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
                  <button type="button" class="btncancelar btn btn-white btn-cons"><i class="fa fa-refresh"></i> Cancelar</button>
                </div>
              </div>
            </form>
          </div>
        </div>    
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"> 
  $(function(){    
    var o<?php echo  $tb ?>=new BD<?php echo  $tb ?>('<?php echo '<?php echo $ventana; ?>';?>');
    o<?php echo  $tb ?>.vista_tabla();
  });
</script>
<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$datosver='';
$addjschk=false;
$jsaddimg=false;
?>
<?php echo'
<?php 
$pk=$this->pk;
$reg=$this->datos;
?>';?>
<div class="row" style="<?php echo '<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>';?>">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
      <div class="x_title">
        <h2><?php echo "<?php echo JrTexto::_('Ver ".$tb."'); ?>";?></h2>
        <div class="btn-group btn-group-md" style="float:right">
          <a class="btn btn-primary" href="<?php echo '<?php echo JrAplicacion::getJrUrl(array(\''.$this->tabla.'\'));?>'; ?>"><i class="fa fa-repeat"></i> <?php echo "<?php echo JrTexto::_('Ir a listado'); ?>"?></a>
          <button class="editar btn btn-success" data-reg="<?php echo '<?php echo $reg["'.$campopk.'"]; ?>'?>"  href="#"><i class="fa fa-edit"></i> <?php echo "<?php echo JrTexto::_('Editar'); ?>"?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo '<?php echo $reg["'.$campopk.'"]; ?>'?>" href="#"><i class="fa fa-remove"></i> <?php echo "<?php echo JrTexto::_('Eliminar'); ?>"?></button>
        </div>            
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="col-md-12">
        <div id="msj-interno">
        <table class="table table-striped">
         <?php
          if(!empty($campos))
          foreach ($campos as $campo) {
             $datosver.='<tr>
              ';
            $verenlistado='_chk'.$campo;
            $tipo=@$frm['tipo_'.$campo];
            if($campo!=$campopk){
              if($tipo=='fk'){
                $datosver.='<th style="max-width:150px;"><?php echo JrTexto::_("'.ucfirst(substr(str_replace('_', ' ', $frm["tipo2_".$campo]),4)).'")."(".JrTexto::_("'.ucfirst(str_replace('_', ' ', $frm["tipofkver_".$campo])).'").")"; ?> </th>
                <th style="max-width:20px;">:</th>
                <td ><?php echo !empty($reg["_'.$frm["tipofkver_".$campo].'"])?$reg["_'.$frm["tipofkver_".$campo].'"]:null; ?></td>
          ';  }else{
                $datosver.='<th style="max-width:150px;"><?php echo JrTexto::_("'.ucfirst(str_replace('_', ' ', $campo)).'") ;?> </th>
                <th style="max-width:20px;">:</th> 
          ';    if(($tipo=='textArea'||$tipo=='text')&&$frm["maximocaracteres".$campo]>100){
                  $datosver.='<td><?php echo substr($reg["'.$campo.'"],0,1000)."..."; ?></td>
          ';    }elseif($tipo=='int'){
                  $datosver.='<td><?php echo $reg["'.$campo.'"] ;?></td>
          ';    }elseif($tipo=='decimal'){
                  $datosver.='<td><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
          ';    }elseif($tipo=='money'){
                  $datosver.='<td><span style="float:left">$.</span><?php echo number_format($reg["'.$campo.'"],'.$frm["partedecimal".$campo].',"."," "); ?></td>
          ';    }elseif($tipo=='file'){
                  $tipofile=$frm["tipo2_".$campo];
                  if($tipofile=='imagen'){
                    $jsaddimg=true;
                    $datosver.='<td><img src="<?php echo $this->documento->getUrlBase().$reg["'.$campo.'"]; ?>" class="img-thumbnail" style="max-height:100px; max-width:250px;"></td>
          ';      }elseif($tipofile=='video'){
                    $datosver.='<td><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver video"); ?></a></td>
          ';      }elseif($tipofile=='documentos'){
                    $datosver.='<td><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver documento"); ?></a></td>
          ';      }else{
                    $datosver.='<td><a href="<?php echo $reg["'.$campo.'"]; ?>" target="_blank"><?php echo JrTexto::_("ver"); ?></a></td>
          ';      }
                }elseif($tipo=='combobox'||$tipo=='radiobutton'||$tipo=='checkbox'){
                  $addjschk=true;
                  if($frm["tipo2_".$campo]=='simple'||$frm["tipo2_".$campo]=='2'){
                    if($frm["dattype".$campo]=="enum"){
                      $valoresopt=@explode(",",substr($frm["coltype".$campo], 5,-1));
                      $datosver.='<td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["'.$campo.'"]; ?>"
              data-campo="'.$campo.'" data-txtid="<?php echo $reg["'.$campopk.'"]; ?>" data-value2="<?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?'.$valoresopt[1].':'.$valoresopt[0].';?>" >
              <?php echo $reg["'.$campo.'"]=='.$valoresopt[0].'?JrTexto::_('.$valoresopt[0].'):JrTexto::_('.$valoresopt[1].'); ?></a></td>
          ';        }else{
                      $datosver.='<td><a style="cursor:pointer;" class="chklistado fa  <?php echo $reg["'.$campo.'"]=="1"?"fa-check-circle":"fa-circle-o"; ?> fa-lg" data-value="<?php echo $reg["'.$campo.'"]; ?>"
              data-campo="'.$campo.'" data-txtid="<?php echo $reg["'.$campopk.'"]; ?>" data-value2="<?php echo $reg["'.$campo.'"]==1?0:1; ?>" >
              <?php echo $reg["'.$campo.'"]=="1"?JrTexto::_("Activo"):JrTexto::_("Inactivo"); ?></a></td>
          ';        }
                  }else{
                    $datosver.='<td><?php echo $reg["'.$campo.'"] ;?></td>
          ';      }
                }else{
                  $datosver.='<td><?php echo $reg["'.$campo.'"] ;?></td>
          ';    }              
              }             
            }else{
              $datosver.='<th><?php echo JrTexto::_("'.ucfirst(str_replace('_', ' ', $campo)).'") ;?> </th>
              <th>:</th> 
          ';
              $datosver.='<td><?php echo $reg["'.$campo.'"] ;?></td>
          ';} 
            $datosver.='</tr>
          ';
          }                      
          echo $datosver;
          ?>
          </table>
          </div>
          <hr><div class="text-center">
          <div class="btn-group btn-group-md">
          <a class="btn btn-primary" href="<?php echo '<?php echo JrAplicacion::getJrUrl(array(\''.$this->tabla.'\'));?>'; ?>"><i class="fa fa-repeat"></i> <?php echo "<?php echo JrTexto::_('Ir al listado'); ?>"?></a>
          <button class="editar btn btn-success" data-reg="<?php echo '<?php echo $reg["'.$campopk.'"]; ?>'?>"  href="#"><i class="fa fa-edit"></i> <?php echo "<?php echo JrTexto::_('Editar'); ?>"?></button>
          <button class="eliminar btn btn-danger" data-reg="<?php echo '<?php echo $reg["'.$campopk.'"]; ?>'?>" href="#"><i class="fa fa-remove"></i> <?php echo "<?php echo JrTexto::_('Eliminar'); ?>"?></button>
        </div>
	        </div>         
	    </div>
</div>
<br />
	</div>
	<script type="text/javascript">
  var asInitVals = new Array();
	$(document).ready(function(){
    <?php if($addjschk){?>
    $('.chklistado').click(function(){
        var id= $(this).attr('data-txtid');
        var campo_= $(this).attr('data-campo');
        var v1=$(this).attr('data-value');
        var v2=$(this).attr('data-value2');
        var data={pk:id,campo:campo_,value:v2};
        var res = xajax__('', '<?php echo $tb; ?>', 'saveEstatus',data);
        if(res) {
          $(this).attr('data-value',v2);
          $(this).attr('data-value2',v1);
          $(this).removeClass('fa-circle-o').removeClass('fa-check-circle');
          $(this).addClass(('0' == v2) ? ' fa-circle-o ' : ' fa-check-circle ');
          $(this).text(('0' == v2) ? v2 : v1);
          agregar_msj_interno('success', '<?php echo "<?php echo JrTexto::_(\"Información actualizada\");?>"; ?>');
          $('.alert').fadeOut(4000);
        }
    });
    <?php }?>

    $('.btnNuevo').click(function(){
      $('#pkaccion').val('guardar');
      $('#pk<?php echo $campopk; ?>').val('');
      $('.img-thumbnail').attr("style","display:none");
    });

    $('.editar').click(function(){
      $('.img-thumbnail').removeAttr("style");
      var id=$(this).attr("data-reg");
     <?php if($jsaddimg==false){?>
     addFancyAjax(<?php echo '"<?php echo JrAplicacion::getJrUrl(array(\''.$tb.'\', \'frm\'))?>?tpl=b&acc=Editar&id="+id'; ?>, true);
    <?php }else{ ?>
       recargarpagina('admin/<?php echo strtolower($tb);?>/frm/?tpl=g&acc=Editar&id='+id);
    <?php } ?>
      
    });

    $('.eliminar').click(function(){
     if(confirm("<?php echo '<?php echo JrTexto::_(\'¿Desea eliminar el registro seleccionado?\');?>';?>")) {
        var id=$(this).attr("data-reg");
        var res = xajax__('', '<?php  echo $tb?>', 'eliminar<?php echo $tb; ?>',id)
        if(res) {
          agregar_msj_interno('success', "<?php echo '<?php echo JrTexto::_(\'Registro eliminado\');?>';?>");
          <?php if($jsaddimg==false){?>
          recargarpagina(false);
          <?php }else{ ?>
       recargarpagina('<?php echo strtolower($tb);?>/frm/?tpl=g&acc=Editar&id='+id);
    <?php } ?>
          } 
        $('.alert').fadeOut(4000);
      }
    });
	});
<?php //echo $jsout; ?>
</script>
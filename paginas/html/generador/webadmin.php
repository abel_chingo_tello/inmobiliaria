<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$notomar=array('usuario_registro','fecha_insert','fecha_update',$this->pk);
$addclases='';
$varclases='';
$addFk='';
$addlibrerias='';
foreach ($campos as $key => $cp){
	if(in_array($cp, $notomar))continue;
	if($cp=='orden'||$cp=='order'){
		 $addlibrerias.='/// libreria para ordenar
        	$this->documento->stylesheet(\'jquery.nestable.min\',\'librerias/jquery/jquery-nestable/\');
			$this->documento->script(\'jquery.nestable.min\',\'librerias/jquery/jquery-nestable/\');
';
	}
	$tipo=$frm['tipo_'.$cp]; 
	if($tipo=='color'){
		 $addlibrerias.='/// libreria para color
        	$this->documento->stylesheet(\'bootstrap-colorpicker.min\',\'librerias/bootstrap/v3/bootstrap-colorpicker/css/\');
			$this->documento->script(\'bootstrap-colorpicker.min\',\'librerias/bootstrap/v3/bootstrap-colorpicker/js/\');
';
	}
	if($tipo=='date' || $tipo=='datetime'){
		 $addlibrerias.='/// libreria para fecha
        	$this->documento->stylesheet(\'bootstrap-datepicker.min\',\'librerias/bootstrap/v3/bootstrap-datepicker/css/\');
			$this->documento->script(\'bootstrap-datepicker.min\',\'librerias/bootstrap/v3/bootstrap-datepicker/js/\');
';
	}
	if($tipo=='fk'){ 
		$tipo2=$frm['tipo2_'.$cp];
		if($tipo2!=''){
			if($tipo2!=$this->tabla){
$addclases.='JrCargador::clase(\'sys_negocio::Neg'.ucfirst($tipo2).'\', RUTA_BASE);
';
$varclases.='$this->oNeg'.ucfirst($tipo2).' = new Neg'.ucfirst($tipo2).'; 
		';
			}
		$addFk.='$this->fk'.ucfirst($cp).'=$this->oNeg'.ucfirst($tipo2).'->buscar();
			';
		}
	}else if($tipo=='file'){
     $tipo2=$frm["tipo2_".$cp];
      if($tipo2=="imagen"){
        $addlibrerias.='/// libreria para imagen
        	$this->documento->stylesheet(\'cropper.min\',\'librerias/cropper/\');
			$this->documento->script(\'cropper.min\',\'librerias/cropper/\');
';
      }
  }
}
echo '<?php';
?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?> 
 * @copyright	Copyright (C) <?php echo date("d-m-Y") ?>. Todos los derechos reservados.
 */
JrCargador::clase('sys_negocio::Neg<?php echo  $tb ?>', RUTA_BASE);
<?php echo $addclases; ?>
class <?php echo  $tb ?> extends JrWeb
{
	private $oNeg<?php echo  $tb ?>;

	public function __construct()
	{
		parent::__construct();
		if(NegSesion::existeSesion()==false){
			return $aplicacion->redir();
		}	
		$this->oNeg<?php echo  $tb ?> = new Neg<?php echo  $tb ?>;
		<?php echo $varclases; ?>

	}

	public function index(){		
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('<?php echo $tb; ?>', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->user=NegSesion::getUsuario();
			$this->documento->stylesheet('datatables.min','librerias/dataTables/');
			$this->documento->stylesheet('select2.min','librerias/select2/css/');
			
			<?php echo $addlibrerias; ?>

			$this->documento->stylesheet('tablas','templates/css/');			
			$this->documento->script('datatables.min','librerias/dataTables/');
			$this->documento->script('main','librerias/abelchingo/');
			$this->documento->script('select2.min','librerias/select2/js/');
			$this->documento->script('<?php echo $this->tabla; ?>','librerias/abelchingo/');
			$this->documento->setTitulo(JrTexto::_('<?php echo $tb ?>'), true);
			$this->esquema = '<?php echo $this->tabla; ?>';
			<?php echo $addFk;?>

			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}
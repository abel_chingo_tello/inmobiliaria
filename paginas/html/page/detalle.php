<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="icon">
<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="apple-touch-icon">
 <!-- Menú -->
<link href="<?php echo URL_TEMA;?>css/menu.css" rel="stylesheet">
<!-- Mi estilo -->
<link href="<?php echo URL_TEMA;?>css/myStyle.css" rel="stylesheet">

<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

<style>

	
	#main {
		padding-top: 60px !important;
	}

	.panel-image{
		padding-top: 20px;
	}

	#descripcion{
		padding-bottom:20px !important;
		margin-bottom: 0px !important;
	}

	h6{
		padding-bottom: 10px !important;
		margin: 0 !important;
	}

	.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active{
		color: #fff !important;
		background-color: #1a73bd !important;
	}

	.bootstrap-select>.dropdown-toggle{
		border: 1px solid #ced4da !important;
		border-radius: 0px !important;
		background: white;
		color: #71757b;
		font-size: 15px;
	}

	.member-info{
		cursor: pointer;
	}


</style>

<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="icon">
<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="apple-touch-icon">
 <!-- Menú -->
 <link href="<?php echo URL_TEMA;?>css/menu.css" rel="stylesheet">

<!-- ======= foto principal Section ======= -->
<section id="facts" class="d-flex flex-column align-items-center" data-aos="fade-right" data-aos-delay="200" style="min-height: 500px;">
	
<div class="container" data-aos="fade-up" data-aos-delay="200">

	<div class="facts-img">

		<header class="section-header">
			<h3><?php echo $this->bien[0]["rebi_ult_dep_acum"]?></h3>
		</header>
		<div class="panel-image">
			<div class="principal-image">
				<img alt="" class="img-fluid" style="width: 70%;padding-bottom: 30px;" src="<?php echo URL_TEMA."assets/img/bienes/".$this->bien[0]['rebi_foto_principal'];?>">
				</div>
			</div>

	</div>

</div>


	
</section><!-- End foto-principal -->


<!----Ficha técnica ------>

<section id="team" style="background: #f5f5f5;" data-aos="fade-up">
	<div class="container" data-aos="fade-up">
		<header class="section-header">
				<h3>Ficha técnica</h3>
			</header>
	</div>

	<div class="row" style="padding:15px;">
		<div class="col-lg-12">
	
				<!-- Section: Live preview -->
				<section class="mx-2 pb-3" data-aos="fade-up">
	
					<ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
						<li class="nav-item waves-effect waves-light">
							<a class="nav-link active" id="home-tab-md" data-toggle="tab" href="#home-md" role="tab" aria-controls="home-md" aria-selected="true">Ubicación</a>
						</li>
						<li class="nav-item waves-effect waves-light">
							<a class="nav-link" id="profile-tab-md" data-toggle="tab" href="#profile-md" role="tab" aria-controls="profile-md" aria-selected="false">Información General del Predio</a>
						</li>
						
					</ul>
					<div class="tab-content card pt-5" id="myTabContentMD">
						<div class="tab-pane fade show active" id="home-md" role="tabpanel" aria-labelledby="home-tab-md">
							<div class="container">
								<div class="row" style="padding-left:25px;">
									<div class="col-6">
										<h6>Tipo de Vía</h6>
										Calle
										<br><br>
										<h6>Nombre de Vía</h6>
										San Jose
										<br><br>
										<h6>Número</h6>
										929
										<br><br>
									</div>
									<div class="col-6">
										<h6>Distrito</h6>
										Chiclayo
										<br><br>
										<h6>Provincia</h6>
										Chiclayo
										<br><br>
										<h6>Departamento</h6>
										Lambayeque
										<br>
									</div>
								</div>
							</div>
						</div>	
						<div class="tab-pane fade" id="profile-md" role="tabpanel" aria-labelledby="profile-tab-md">
							<div class="row" style="padding-left:25px;padding-right:20px;">
								<div class="col-sm-12 col-lg-4">
									<h6>Precio</h6>
									Actual: <?php echo "S/ ". number_format($this->bien[0]['rebi_ult_val_act'],2);?>
									<br>
									<h6>Referencia</h6>
									Actual: <?php echo $this->bien[0]['rebi_detalles'];?> 
									<br>
									<h6>N° de Pisos</h6>
									Actual: 1
									<br>
									Parámetro Municipal: 4
									<br><br>
									<h6>Tipo de Predio</h6>
									Urbano
									<br><br>
									
								</div>
								<div class="col-sm-12 col-lg-4">
									<h6>Uso del Inmueble</h6>
									Actual: Desocupado
									<br>
									Parámetro Municipal: COMERCIO - RESIDENCIAL RDM
									<br><br>
									<h6>Partida Registral</h6>
									P10165933
									<br><br>
									<h6>Estado Actual</h6>
									Desocupado
									<br><br>
									<h6>Áreas</h6>
									Área Total: <?php echo $this->bien[0]['rebi_area'];?> 
									<br>
									Área Techada: -----
									<br><br>
								</div>
								<div class="col-sm-12 col-lg-4">
									<h6>Linderos</h6>
									Frente: Con la Ca. San José, en línea recta de 5.10 ml.
									<br>
									Derecha: Con los lotes 25 y 24, en línea recta de 38.70 ml.<br>
									Izquierda: Con el sublote 26A, en línea recta de 38.67 ml<br>
									Fondo: Con el lote 4A, en línea recta de 4.45 ml.
								</div>
							</div>
						</div>
					</div>
	
				</section>
				<!-- Section: Live preview -->
	
		</div>
	</div>
</section>

<!--Fin Ficha Técnica --->

<!-- Sección Galería --->

<section id="facts" style="min-height: 500px;">
      <div class="container" data-aos="fade-up" data-aos-delay="200">

          <div class="facts-img">
            

            <div class="row">
			
              <div class="col-lg-12 box" data-aos="fade-up" data-aos-delay="100">
			  
                <h3 class="title" style="color:#137cc5;text-align: left;"><span class="iconify" data-icon="mdi:google-maps" data-inline="false"></span> Plano de Ubicación</h3>
				<br>
                <img id="foto_ubicacion" alt="" class="img-fluid" style="width: 70%;padding-bottom: 30px;" src="../../static/templates/plantillas/web/assets/img/foto-mapa-muestra.jpg">
              </div>
            </div>  
        </div>  
      </div>
    </section> 

<section id="team">
      <div class="container" data-aos="fade-up">
		<!--Hacer un for y luego validar por cada tipo de imágenes-->

        <div class="row">
			<div class="col-lg-12 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
				<h3 class="title" style="color:#137cc5"><span class="iconify" data-icon="zmdi:google-maps" data-inline="false"></span></span> Propuesta de Negocio</h3>
				<h4 class="propuesta_negocio"></h4>
			</div>
		</div>
		<br>
		<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="member" data-aos="fade-up" data-aos-delay="100">
						<img src="../../static/templates/plantillas/web/assets/img/fotoMuestra-propuesta.jpg" class="img-fluid" alt="">
						<div class="member-info" data-toggle="modal" data-target="#exampleModalCenter">
							<div class="member-info-content">
								<div class="social">
									<a href=""><i class="fa fa-search" style="font-size:30px;"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <br>


        <hr>
        <div class="row">
          <div class="col-lg-12 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
            <h3 class="title" style="color:#137cc5"><span class="iconify" data-icon="zmdi:google-maps" data-inline="false"></span></span> Fotografías del Predio</h3>
          </div>
        </div>
		
        <br>
        

        <hr>
        <div class="row">
          <div class="col-lg-12 col-md-6 box" data-aos="fade-up" data-aos-delay="100">
            <h3 class="title" style="color:#137cc5"><span class="iconify" data-icon="zmdi:google-maps" data-inline="false"></span></span> Entorno del Predio</h3>
          </div>
        </div>
        <br>


      </div>
    </section><!-- Fin Galería Fotos -->

	<!---Modal---->

	<div id="cuerpo_modal">
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">'+
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">USO: COMERCIO - RESIDENCIAL (NOTARIA, EDIFICIO PARA OFICINAS, HOTEL y OTROS)</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body text-center">'+
					<img src="../../static/templates/plantillas/web/assets/img/fotoMuestra-propuesta.jpg" class="img-fluid" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Fin Modal --->
<!---------------------->


	<!-- ======= galeria fotos ======= -->
		<section id="galeria" class="galeria" data-aos="fade-right">
			<div class="container">
			<?php $i=0; 
			foreach($this->fotos as $foto){?>
				<header class="galeria-header justify-content-center pt-5 pb-5" data-aos="fade-up">
					<h3><?php echo $foto["tipo"];?></h3>
				</header>
				<?php $fotos_tipo = explode(",",$foto["imagen"]);?>
				<div class="panel-image row dflex flex-row">
					<?php 
					foreach($fotos_tipo as $f){
						$i++;
						?>
						<div class="tipo-image col-lg-3" data-aos="fade-up">
							<img class="img-fluid" src="<?php echo URL_TEMA."assets/img/bienes/".$f;?>">
							<div class="info" data-toggle="modal" data-target="#f<?php echo $i; ?>">
								<a>
									<i class="fa fa-search" style="font-size:30px;" aria-hidden="true"></i>
								</a>
							</div>
						</div>
						
					<?php }?>
				
			</div>
			<?php }?>  
			</div>
		</section><!-- End galeria fotos -->
	
<!-- Modal -->
<?php
$i=0; 
foreach($this->fotos as $foto){
	$fotos_tipo = explode(",",$foto["imagen"]);
	$titulos_tipo = explode(",",$foto["titulo"]);
	$j=0;
	foreach($fotos_tipo as $f){
		$i++;
		?>
<div class="modal fade" id="f<?php echo $i; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-capitalize" id="exampleModalLabel"><?php echo $titulos_tipo[$j]; ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img class="img-fluid" src="<?php echo URL_TEMA."assets/img/bienes/".$f;?>">
			</div>
		</div>
	</div>
</div>

<?php $j++;}?>
<?php }?>
		 

		<!-- modal para contactar-->
		<div >
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h7 class="modal-title" id="exampleModalLabel">Mensaje al anunciante</h7>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="row">
							<div class="col-lg-4">
								<div class="row px-1 py-1 ">
									<img class="img-fluid"src="<?php echo URL_TEMA."assets/img/bienes/".$this->bien[0]['rebi_foto_principal'];?>">
								 <span>S/.<?php echo number_format($this->bien[0]['rebi_ult_val_act'],2,".",",");?></span>
								</div>
								<p id="bien_descripcion" class="text-center"></p>
							</div>
							<div class="col-lg-8">
								<h9>Contacta al anunciante</h9>
								<form id="contacto_bien" action="<?php echo URL_RAIZ;?>json/Contacto_bien/guardar" method="post" role="form" class="contactar mt-4" autocomplete="off" validate>
									<input id="bien_id" name="rebi_id" type="hidden">

									<div class="form-group">
										<select type="text" class="form-control" name="tipo_documento" id="tipo_documento" />
											<option> Seleccione Tipo de Documento</option>
											<?php 
											foreach($this->tipos_proveedor as $value){?>
												
												<option value="<?php echo $value["tabl_id"] ?>"><?php echo $value["tabl_descripcion"] ?></option>
											<?php }?>
										</select>
											<div class="validate"></div>
									</div>


									<div class="form-row">
										<div class="col-md-6 form-group">
											<input type="text" class="form-control" name="nro_documento" id="nro_documento" placeholder="Nro Documento" data-rule="minlen:8" data-msg="Ingresar un número válido" />
											<div class="validate"></div>
										</div>
										<div class="col-md-6 form-group">
											<input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono" data-rule="minlen:6" data-msg="Ingresar un telefono o celular " />
											<div class="validate"></div>  
										</div>
									</div>
									
									<div class="form-row">
										<div class="col-md-6 form-group">
											<input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
											<div class="validate"></div>
										</div>
										<div class="col-md-6 form-group">
											<input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
											<div class="validate"></div>  
										</div>
									</div>  
									<div class="form-group">
										<input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Ingresar un email válido" />
											<div class="validate"></div>
									</div>    
									<div class="form-group">
										<input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" data-rule="minlen:6" data-msg="Ingresar al menos 6 carácteres" />
											<div class="validate"></div>
									</div>            
									<div class="form-group">
										<textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Ingresar un mensaje">Hola, me interesa este inmueble y quiero que me contacten. Gracias.</textarea>
										<div class="validate"></div>
									</div>
									<div class="text-center"><input class="btn btn-primary" type="submit" value=Enviar /></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>          

<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'templates/css/detalle.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/font-awesome/css/font-awesome.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/jquery/jquery-ui/jquery-ui.min.css';?>">
<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/v3.4/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/detalle.js';?>"></script>

<script type="text/javascript">
	$("#contacto_bien").on('submit', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		
		if($("#tipo_documento").val()==""){
				alert("Seleccione el tipo de documento");
				return;
		}
		if($("#nro_documento").val()==""){
				alert("Ingrese su numero de documento");
				return;
		}
		if($("#telefono").val()==""){
				alert("Ingrese su numero de telefono o celular");
				return;
		}
		if($("#nombre").val()==""){
				alert("Ingrese su nombre");
				return;
		}
		if($("#apellidos").val()==""){
				alert("Ingrese su apellido");
				return;
		}
		if($("#email").val()==""){
				alert("Ingrese su correo");
				return;
		}

		$.ajax({
				type: $(this).prop('method'),
				url : $(this).prop('action'),
				data: $(this).serialize()
		}).done(function() {
				alert("Se contactarán contigo.");
				$("#exampleModal").modal("hide");
				$('#contacto_bien').trigger("reset");
		});
		
		
});
</script>

<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<?php
	//var_dump("dinámico",$this->bienes);exit();
	/*foreach($this->bienes as $key => $bien){
		echo $bien["rebi_detalles"]."<br>";
	}

	exit();*/
?>
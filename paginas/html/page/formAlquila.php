<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="icon">
<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="apple-touch-icon">
 <!-- Menú -->
 <link href="<?php echo URL_TEMA;?>css/menu.css" rel="stylesheet">

<!-- Content Wrapper. Contains page content -->

<div class="wrapper">
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-8">
                        <!-- Widget: user widget style 2 -->
                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header bg-warning">
                                <div class="widget-user-image">
                                <img class="img-circle elevation-2" src="../static/templates/plantillas/web/assets/img/servicios/compra-form.jpg" alt="Compra">
                                </div>
                                <!-- /.widget-user-image -->
                                <h3 class="widget-user-username">ALQUILA TU INMUEBLE!</h3>
                                <h5 class="widget-user-desc">Ingresa tus datos y nos contactaremos contigo...</h5>
                            </div>
                            <div class="card-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nombre(*)</label>
                                                <input type="text" name="nombre" class="form-control" placeholder="Ingrese su nombre completo ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email(*)</label>
                                                <input type="email" name="email" class="form-control" placeholder="Ingrese su email ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Teléfono/Celular(*)</label>
                                                <input type="text" name="telefono" class="form-control" placeholder="Ingrese su teléfono ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Dirección(*)</label>
                                                <input type="text" name="direccion" class="form-control" placeholder="Ingrese su dirección ...">
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-12">
                                            <!-- textarea -->
                                            <div class="form-group">
                                                <label>Mensaje</label>
                                                <textarea class="form-control" rows="3" placeholder="Ingrese su mensaje ..."></textarea>
                                            </div>
                                        </div>
                                        
                                    </div>

                                    <div class="col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary bg-warning" style="border:0;">Enviar</button>
                                    </div>

                                
                                </form>
                            </div>

                        
                        </div>
                        <!-- /.widget-user -->
                    </div>
                    
                    <!-- /.col -->
                    <div class="col-md-4">
                        <!-- Widget: user widget style 1 -->
                        <div class="card card-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                            <div class="widget-user-header text-white"
                                style="background: url('../static/templates/plantillas/web/assets/img/servicios/compra-aside.jpg') center center;">
                                <!--<h3 class="widget-user-username text-right">Elizabeth Pierce</h3>-->
                                <h3 class="widget-user-desc text-right">Informes</h3>
                            </div>
                        
                            <div class="card-footer">
                                <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                    <h5 class="description-header">074-123456</h5>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                    <h5 class="description-header">987654321</h5>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                    <h5 class="description-header">admin@sbch.gob.pe</h5>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <br>
                            <div class="col-sm-12 text-center">
                                <img src="../static/templates/plantillas/web/assets/img/logo_inmuebles_silueta.png" alt="" style="width:50%;">
                            </div>
                            <br>
                        </div>
                        <!-- /.widget-user -->
                    </div>
                <!-- /.col -->
                </div>
            </div>
        </section>
    </div>
</div>
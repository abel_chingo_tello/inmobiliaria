<section id="hero">
    <div class="hero-container">
    
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Bienvenidos</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#portfolio" class="btn-get-started animate__animated animate__fadeInUp scrollto btn-hero">Ver inmuebles</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-2.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
              <h2 class="animate__animated animate__fadeInDown">Bienvenidos</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#portfolio" class="btn-get-started animate__animated animate__fadeInUp scrollto btn-hero">Ver inmuebles</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                x<h2 class="animate__animated animate__fadeInDown">Bienvenidos</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#portfolio" class="btn-get-started animate__animated animate__fadeInUp scrollto btn-hero">Ver inmuebles</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

         <!--form que acompaña al hero-->

          <div id="form_search">

            <div class="logo-circular">
              <img src="<?php echo URL_TEMA;?>assets/img/logo-form.png" alt="">
            </div>

            <div class="row tit-form">
                <div class="col-lg-12">
                  <label for="">Quiero información sobre:</label>
                </div>
            </div>
            <ul class="menu-form">
              <li><a href="#" class="active">Alquiler</a></li>
              <!--<li><a href="#">Compra</a></li>-->
            </ul>

            <form action="" class="body-form">

              <div class="grupo-1">
                <div class="form-group">
                  <label for="">Busco:</label>
                  <select class="custom-select">
                    <option value="1" selected>Departamento</option>
                    <option value="2">Casa</option>
                    <option value="3">Terreno</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Ubicación:</label>
                  <select class="custom-select">
                    <option value="1" selected>Lambayeque</option>
                    <option value="2">Amazonas</option>
                    <option value="3">Loreto</option>
                  </select>
                </div>
              </div>

              <div class="grupo-2" style="display:none;">
                <div class="form-group">
                  <label for="">Busco:</label>
                  <select class="custom-select">
                    <option value="1" selected>Departamento</option>
                    <option value="2">Casa</option>
                    <option value="3">Terreno</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Ubicación:</label>
                  <select class="custom-select">
                    <option value="1" selected>Lambayeque</option>
                    <option value="2">Amazonas</option>
                    <option value="3">Loreto</option>
                  </select>
                </div>
                
              </div>

              <br>
              <div class="row">
                <div class="col-lg-12 text-center">
                  <button type="submit" class="btn btn-primary btn-submit">Buscar</button>
                </div>
              </div>
              
            </form>

          </div>


      </div>
    </div>
</section><!-- End Hero -->

  <!-- ======= SERVICIOS ======= -->

  <!-- ======= About Us ======= -->
<section id="about">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2>NUESTROS SERVICIOS</h2> 
    </div>

    <div class="row about-cols">

       <!-- <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
          <div class="about-col">
            <div class="img">
              <img src="static/templates/plantillas/web/assets/img/servicios/compra.jpg" alt="" class="img-fluid">
              <div class="icon"><i class="ion-ios-home-outline"></i></div>
            </div>
            <h2 class="title"><a href="pagina/formCompra">Compra</a></h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
          </div>
        </div>-->

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="200">
          <div class="about-col">
            <div class="img">
              <img src="static/templates/plantillas/web/assets/img/servicios/alquila.jpg" alt="" class="img-fluid">
              <div class="icon"><i class="ion-key"></i></div>
            </div>
            <h2 class="title"><a href="pagina/formAlquila">Servicios de Beneficencia</a></h2>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            </p>
          </div>
        </div>

        <div class="col-md-6" data-aos="fade-up" data-aos-delay="300">
          <div class="about-col">
            <div class="img">
              <img src="static/templates/plantillas/web/assets/img/servicios/vende.jpg" alt="" class="img-fluid">
              <div class="icon"><i class="ion-document-text"></i></div>
            </div>
            <h2 class="title"><a href="pagina/formVende">Servicios de Terceros</a></h2>
            <p>
              Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
            </p>
          </div>
        </div>

    </div>

  </div>
</section><!-- End About Us Section -->

    

  <!-- ======= ALQUILER ======= -->
  <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>ALQUILER (Últimos ingresos)</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>


        <div class="row portfolio-container">

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-lg-12 text-right">
            <a href="pagina/listado"><button type="button" class="btn btn-danger">Ver más</button></a>
          </div>
        </div>

      </div>
    </section><!-- End Our Portfolio Section -->
    
    <!------ VENTA ----------->

    <!-- ======= Our Portfolio Section ======= -->
  <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up" data-aos-delay="100">

        <div class="section-title">
          <h2>VENTA (Últimos ingresos)</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>


        <div class="row portfolio-container">

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <span class="badge badge-warning">Departamento</span>
              <img src="<?php echo URL_TEMA;?>assets/img/bienes/foto-muestra.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div class="row">

                  <div class="col-lg-10">
                    <h4>S/ 500.00</h4>
                    <p>Zarumilla 123</p>
                    <span>Chiclayo, Lambayeque</span>
                  </div>

                  <div class="col-lg-2">
                    <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-hand-sparkles icon-rojo"></i>
                      <br>
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-eye icon-azul"></i>
                      </a>
                    </div>
                  </div>

                </div>
                
                <hr>
                <div class="portfolio-links">
                      <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1">
                        <i class="fas fa-ruler-combined"></i> <span>&nbsp;120 m2 </span>
                      </a>
                  
                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-warehouse"></i> <span> &nbsp; 3 dorm. </span>
                      </a>

                      <a href="portfolio-details.html" title="More Details">
                        <i class="fas fa-toilet"></i> <span> &nbsp;2 baños </span>
                      </a>
                    </div>
                
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-lg-12 text-right">
            <button type="button" class="btn btn-danger">Ver más</button>
          </div>
        </div>

      </div>

      
    </section><!-- End Our Portfolio Section -->
    
   <!-- ======= Contact Section ======= -->
   <section id="contact" class="section-bg">
      <div class="container" data-aos="fade-up">

      <div class="section-title">
          <h2>Contáctenos</h2>
          <p>Puede dejar sus datos y nosotros nos comunicaremos a la brevedad con usted.</p>
        </div>

        <div style="height: 400px; overflow: hidden;">
          <h3>Portada Principal de la Sociedad de Beneficiencia de Chiclayo</h3>
          <img src="<?php echo URL_TEMA;?>assets/img/fondo_2.jpg" alt="" width="100%">
        </div>

        <br>

        <br><h3>Ubicación</h3>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.9863640776207!2d-79.84539868568437!3d-6.771513068100487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x904cef27838dd001%3A0xb5bd718c555dff5c!2sSociedad%20de%20Beneficencia%20de%20Chiclayo!5e0!3m2!1ses!2spe!4v1604073538752!5m2!1ses!2spe" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

        <br><br><br><h3>Deje su mensaje y nosotros nos comunicaremos con Usted</h3>
        <div class="form">
          <!--<form action="forms/contact.php" method="post" role="form" class="php-email-form">-->
            <form method="post" role="form" id="enviar_form" class="php-email-form">
            <div class="form-row">
              <div class="form-group col-md-6">
                <select name="ubicacion" id="ubicacion" class="form-control selectpicker" data-live-search="true" >
                  
                </select>
              </div>
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Ingresa tu nombre completo" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validate"></div>
              </div>
              <div class="form-group col-md-6"> 
                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Ingresa tu celular/teléfono" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validate"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Ingresa tu Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validate"></div>
              </div>
            </div>
           
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Por favor ingresa el mensaje"></textarea>
              <div class="validate"></div>
            </div>
            <!--<div class="mb-3">
              <div class="loading">Enviando</div>
              <div class="error-message"></div>
              <div class="sent-message">Su mensaje ha sido enviado, muchas gracias</div>
            </div>-->
            <div class="text-center"><button type="submit">Enviar Mensaje</button></div>
          </form>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Dirección</h3>
              <address>Elías Aguirre 248, Chiclayo</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Teléfonos</h3>
              <p><a href="tel:+74233691">(074) 233691, 233768</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:jbarragan@sbch.gob.pe">jbarragan@sbch.gob.pe</a></p>
            </div>
          </div>

        </div>


      </div>
    </section><!-- End Contact Section -->

    <div id="WAButton"></div>

    <link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'templates/css/bienes.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/font-awesome/css/font-awesome.css';?>">
<!--<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/jquery/jquery-ui/jquery-ui.min.css';?>">-->
<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/v3.4/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/funciones.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/bienes.js';?>"></script>


<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/jquery.touchSwipe.min.js';?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick.css';?>" >
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick-theme.css';?>" >
<script src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/slick.js';?>" type="text/javascript" charset="utf-8"></script>

<script>
  URL_TEMA = '<?php echo URL_TEMA; ?>';
  URL_RAIZ = '<?php echo URL_RAIZ; ?>';
</script>

<script>
  $(function() {
  $('#WAButton').floatingWhatsApp({
    phone: '+51958791562', //WhatsApp Business phone number International format-
    //Get it with Toky at https://toky.co/en/features/whatsapp.
    headerTitle: 'Envíanos un mensaje si tienes alguna consulta', //Popup Title
    popupMessage: '¿Tienes alguna consulta que realizarnos?, con gusto te atenderemos', //Popup Message
    showPopup: true, //Enables popup display
    buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
    //headerColor: 'crimson', //Custom header color
    //backgroundColor: 'crimson', //Custom background button color
    position: "right"    
  });
});
</script>

 

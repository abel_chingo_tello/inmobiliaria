<!-- ======= Hero Section ======= -->
<!--<section id="hero" class="d-flex align-items-center">
  <div class="container text-center position-relative" data-aos="fade-in" data-aos-delay="200">
    <h1>Conoce nuestros inmuebles</h1>
    <h2>La mejor atención con total garantía</h2>
    <a href="#portfolio" class="btn-get-started scrollto">Ver inmuebles</a>
  </div>
</section>--><!-- End Hero -->

<section id="hero">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <!-- Slide 1 -->
          <div class="carousel-item active" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-1.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Welcome to <span>Mamba</span></h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>

          <!-- Slide 2 -->
          <div class="carousel-item" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-2.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>

          <!-- Slide 3 -->
          <div class="carousel-item" style="background-image: url('static/templates/plantillas/web/assets/img/slide/slide-3.jpg');">
            <div class="carousel-container">
              <div class="carousel-content container">
                <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- End Hero -->

  <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row content">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
            <img src="<?php echo URL_TEMA;?>assets/img/img-1.jpg" class="img-fluid">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left" data-aos-delay="200">

            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequa</li>
              <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit</li>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in</li>
            </ul>
            <p class="font-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

     <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title" data-aos="zoom-out">
          <h2>Nuestros Inmuebles</h2>
          <p>Da clic en cualquiera de ellos para tener más detalles</p>
        </div>

        <!-- <ul id="portfolio-flters" class="d-flex justify-content-end" data-aos="fade-up">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-app">App</li>
          <li data-filter=".filter-card">Card</li>
          <li data-filter=".filter-web">Web</li>
        </ul> -->
        <div class="row">
          <div class="col-lg-3">
            <label class="font-weight-bold"> Precio mínimo: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_min" placeholder="Ejm: 2000" />
            </div>
            <label class="font-weight-bold"> Precio máximo: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_max" placeholder="Ejm: 10000" />
            </div>

            <label class="font-weight-bold"> Área mínima: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_min" placeholder="Ejm: 200" />
            </div>
            <label class="font-weight-bold"> Área máxima: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_max" placeholder="Ejm: 500" />
            </div>

            <label class="font-weight-bold">Catálogo: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="catalogo">
                  <option value="">TODOS</option>
                <?php 
                  foreach($this->catalogo as $c){
                    echo "<option value=".$c["casb_codigo"].">".$c["casb_descripcion"]."</option>";
                  }
                ?>
                </select>
            </div>

            <label class="font-weight-bold">Tipo Bien: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="tipo_bien">
                  <option value="">TODOS</option>
                <?php 
                  foreach($this->tipo_bien as $t){
                    echo "<option value=".$t["tabl_id"].">".$t["tabl_descripcion"]."</option>";
                  }
                ?>
                </select>
            </div>

             <label class="font-weight-bold">Fechas: </label> 
            <div class="col-md-12 form-group">
                <select class="form-control" id="fecha">
                  <option value="">TODO</option>
                   <option value="month">Último Mes</option>
                   <option value="fifteen">Últimos 15 días</option>
                   <option value="week">Último Semana</option>
                </select>
            </div>
            
            <!--<div class="col-md-12 mb-2 form-row">
              <div class="col-md-12">
                  <p>
                    Rango de precios:<strong style="font-size: 11px;" id="amount"></strong>
                  </p>
                <div id="slider-range"></div>
                <input type="hidden" id="amount1">
                <input type="hidden" id="amount2">
              </div>
           </div>-->

            <div class="text-center"><button class="btn btn-secondary" onclick="filtrar()">Filtrar</button></div>
          
          </div>
          <div id="portfolio-container" class="row col-lg-9" data-aos="fade-up">

            <?php $items=0;
            foreach($this->bienes as $value){
              $items++;?>
              <div onclick='detalle(<?php echo $value["rebi_id"]; ?>)'  id="item_nro<?php echo $items;?>" class="col-lg-4 col-md-6 portfolio-item  items_bienes item_nro<?php echo $items;?> " nro_item="item_nro<?php echo $items;?>">
                <div class="portfolio-img"><img src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" class="img-fluid" alt=""></div>
                <div class="portfolio-info">
                  <h4><?php echo $value["casb_descripcion"]?></h4>
                  <p><?php echo $value["rebi_detalles"]?></p>
                  <p><?php echo "S/. ".number_format($value["rebi_ult_val_act"],2,".",",")?></p>
                  <button onclick='contactar(<?php echo $value["rebi_id"].",\"".str_replace("\"","",$value["rebi_detalles"])."\",\"".$value["rebi_foto_principal"]."\",\"".URL_TEMA."\""?>);event.stopPropagation();' >CONTACTAR</button>
                  
                </div>
              </div>
            <?php }?>
          </div>
        </div>
        
        
        <div id="footer-pagination" class="row container" style="justify-content: flex-end;">
              <div id=navegacion_abajo >
              </div>
        </div>
      </div>
    </section><!-- End Portfolio Section -->

     <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-4" data-aos="fade-right">
            <div class="section-title">
              <h2>Contact</h2>
              <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
            </div>
          </div>

          <div class="col-lg-8" data-aos="fade-up" data-aos-delay="100">
            <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" allowfullscreen></iframe>
            <div class="info mt-4">
              <i class="icofont-google-map"></i>
              <h4>Location:</h4>
              <p>A108 Adam Street, New York, NY 535022</p>
            </div>
            <div class="row">
              <div class="col-lg-6 mt-4">
                <div class="info">
                  <i class="icofont-envelope"></i>
                  <h4>Email:</h4>
                  <p>info@example.com</p>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="info w-100 mt-4">
                  <i class="icofont-phone"></i>
                  <h4>Call:</h4>
                  <p>+1 5589 55488 55s</p>
                </div>
              </div>
            </div>

            <form action="forms/contact.php" method="post" role="form" class="php-email-form mt-4">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
        </div>

      </div>
    </section><!-- End Contact Section -->

    <!-- modal para contactar-->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h7 class="modal-title" id="exampleModalLabel">Mensaje al anunciante</h7>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-lg-4">
                <div class="row px-1 py-1 ">
                  <img class="img-fluid" src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" >
              
                </div>
                <p id="bien_descripcion" class="text-center"></p>
              </div>
              <div class="col-lg-8">
                <h9>Contacta al anunciante</h9>
                <form id="contacto_bien" action="<?php echo URL_RAIZ;?>json/Contacto_bien/guardar" method="post" role="form" class="contactar mt-4" autocomplete="off" validate>
                  <input id="bien_id" name="rebi_id" type="hidden">

                  <div class="form-group">
                    <select type="text" class="form-control" name="tipo_documento" id="tipo_documento" />
                      <option value=""> Seleccione Tipo de Documento</option>
                      <?php 
                      foreach($this->tipos_proveedor as $value){?>
                        
                        <option value="<?php echo $value["tabl_id"] ?>"><?php echo $value["tabl_descripcion"] ?></option>
                      <?php }?>
                    </select>
                      <div class="validate"></div>
                  </div>


                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="nro_documento" id="nro_documento" placeholder="Nro Documento" data-rule="minlen:8" data-msg="Ingresar un número válido" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono" data-rule="minlen:6" data-msg="Ingresar un telefono o celular " />
                      <div class="validate"></div>  
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>  
                    </div>
                  </div>  
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Ingresar un email válido" />
                      <div class="validate"></div>
                  </div>    
                  <div class="form-group">
                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" data-rule="minlen:6" data-msg="Ingresar al menos 6 carácteres" />
                      <div class="validate"></div>
                  </div>            
                  <div class="form-group">
                    <textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Ingresar un mensaje">Hola, me interesa este inmueble y quiero que me contacten. Gracias.</textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="text-center"><input class="btn btn-primary" type="submit" value=Enviar /></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>          

<!--<div class="g_button-container">
    <div class="g_button text-center no_rotaras no_ver" style="background-color: skyblue;">
        <span id="boton-chat" target="_blank">
            <i class="fa fa-user icon_g_button"></i>
        </span>
    </div>
    <div class="g_button text-center no_rotaras no_ver" style="background-color: green;">
        <a id="wsp_chat" target="_blank">
            <i class="fa fa-whatsapp icon_g_button"></i>
        </a>
    </div>
    <div class="g_button text-center no_rotaras no_ver" style="background-color: blue;">
        <a id="id_facpage" target="_blank">
            <i class="fa fa-facebook icon_g_button"></i>
        </a>
    </div>
    <div class="btn g_button text-center" style="background-color: red">
        <i class="fa fa-comments icon_g_button rotaras" style="line-height: 35px;"></i>
        <i class="fa fa-times no_ver icon_g_button no_rotaras no_rotaras_nunca" style="line-height: 35px;"></i>
    </div>
</div>-->




<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'templates/css/bienes.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/font-awesome/css/font-awesome.css';?>">
<!--<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/jquery/jquery-ui/jquery-ui.min.css';?>">-->
<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/v3.4/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/funciones.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/bienes.js';?>"></script>


<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/jquery.touchSwipe.min.js';?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick.css';?>" >
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick-theme.css';?>" >
<script src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/slick.js';?>" type="text/javascript" charset="utf-8"></script>

<script>
  URL_TEMA = '<?php echo URL_TEMA; ?>';
  URL_RAIZ = '<?php echo URL_RAIZ; ?>';
</script>
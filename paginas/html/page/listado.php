<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="icon">
<link href="<?php echo URL_TEMA;?>assets/img/favicon.png" rel="apple-touch-icon">
 <!-- Menú -->
<link href="<?php echo URL_TEMA;?>css/menu.css" rel="stylesheet">

<style>
  #main{
    background-color: #f6f6f7;
  }

  .portfolio .portfolio-wrap {
    height: 310px;
}

  .portfolio .portfolio-wrap .portfolio-info{
    top: 190px;
}

.portfolio-info .row{
  margin-bottom: 15px;
}

</style>

     <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title" data-aos="zoom-out">
          <h2>Nuestros Inmuebles</h2>
          <p>Da clic en cualquiera de ellos para tener más detalles</p>
        </div>

        <!-- <ul id="portfolio-flters" class="d-flex justify-content-end" data-aos="fade-up">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-app">App</li>
          <li data-filter=".filter-card">Card</li>
          <li data-filter=".filter-web">Web</li>
        </ul> -->

        <div class="row">
            <div class="col-md-3"  data-aos="fade-up">
                <!-- Widget: user widget style 2 -->
                <div class="card card-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-header-card">
                        <div class="widget-user-image">
                        <img class="img-circle elevation-2" src="../static/templates/plantillas/web/assets/img/servicios/compra-form.jpg" alt="Compra">
                        </div>
                        <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">BUSCA TU INMUEBLE!</h3>
                    </div>
                    <div class="card-body">
                        <form role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                  <h6><i class="fas fa-money-bill-wave"></i> Precio</h6x>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <input type="text" name="precio_min" id="precio_min" class="form-control" placeholder="Mínimo">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                      <input type="text" name="precio_min" id="precio_min" class="form-control" placeholder="Máximo">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                  <h6><i class="fas fa-chart-area"></i> Área</h6>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="area_min" id="area_min" class="form-control" placeholder="Mínimo">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="area_max" id="area_min" class="form-control" placeholder="Máximo">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                  <h6><i class="fas fa-map-marker-alt"></i>&nbsp;Ubicación</h6>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <select class="form-control" id="catalogo">
                                      <option value="">TODOS</option>
                                    <?php 
                                      foreach($this->catalogo as $c){
                                        echo "<option value=".$c["casb_codigo"].">".$c["casb_descripcion"]."</option>";
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                  <h6><i class="fas fa-store-alt"></i>&nbsp;Tipo de inmueble</h6>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <select class="form-control" id="tipo_bien">
                                      <option value="">TODOS</option>
                                    <?php 
                                      foreach($this->tipo_bien as $t){
                                        echo "<option value=".$t["tabl_id"].">".$t["tabl_descripcion"]."</option>";
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                  <h6><i class="far fa-calendar-alt"></i>&nbsp;Fechas</h6>
                                </div>
                                <div class="col-md-12 form-group">
                                    <select class="form-control" id="fecha">
                                      <option value="">TODO</option>
                                      <option value="month">Último Mes</option>
                                      <option value="fifteen">Últimos 15 días</option>
                                      <option value="week">Último Semana</option>
                                    </select>
                                </div>
                                
                                
                            </div>

                            <div class="col-sm-12 text-right">
                                <button class="btn btn-primary bg-header-card" onclick="filtrar();">Buscar</button>
                            </div>

                        
                        </form>
                    </div>

                
                </div>
                <!-- /.widget-user -->
            </div>


          <!--<div class="row">
          <div class="col-lg-3">
            <label class="font-weight-bold"> Precio mínimo: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_min" placeholder="Ejm: 2000" />
            </div>
            <label class="font-weight-bold"> Precio máximo: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_max" placeholder="Ejm: 10000" />
            </div>

            <label class="font-weight-bold"> Área mínima: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_min" placeholder="Ejm: 200" />
            </div>
            <label class="font-weight-bold"> Área máxima: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_max" placeholder="Ejm: 500" />
            </div>

            <label class="font-weight-bold">Catálogo: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="catalogo">
                  <option value="">TODOS</option>-->
                <?php 
                  /*foreach($this->catalogo as $c){
                    echo "<option value=".$c["casb_codigo"].">".$c["casb_descripcion"]."</option>";
                  }*/
                ?>
                <!--</select>
            </div>

            <label class="font-weight-bold">Tipo Bien: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="tipo_bien">
                  <option value="">TODOS</option>-->
                <?php 
                 /* foreach($this->tipo_bien as $t){
                    echo "<option value=".$t["tabl_id"].">".$t["tabl_descripcion"]."</option>";
                  } */
                ?>
                <!--</select>
            </div>

             <label class="font-weight-bold">Fechas: </label> 
            <div class="col-md-12 form-group">
                <select class="form-control" id="fecha">
                  <option value="">TODO</option>
                   <option value="month">Último Mes</option>
                   <option value="fifteen">Últimos 15 días</option>
                   <option value="week">Último Semana</option>
                </select>
            </div>-->
            
            <!--<div class="col-md-12 mb-2 form-row">
              <div class="col-md-12">
                  <p>
                    Rango de precios:<strong style="font-size: 11px;" id="amount"></strong>
                  </p>
                <div id="slider-range"></div>
                <input type="hidden" id="amount1">
                <input type="hidden" id="amount2">
              </div>
           </div>-->

            <!--<div class="text-center"><button class="btn btn-secondary" onclick="filtrar()">Filtrar</button></div>
          
          </div>-->
          <div id="portfolio-container" class="row col-lg-9" data-aos="fade-up">
             
            

            <?php $items=0;
            foreach($this->bienes as $value){
              $items++;?>
              <div class="col-lg-4 col-md-6 portfolio-item filter-app items_bienes item_nro<?php echo $items;?> " onclick='detalle(<?php echo $value["rebi_id"]; ?>)'  id="item_nro<?php echo $items;?>"  nro_item="item_nro<?php echo $items;?>">

                <div class="portfolio-wrap">
                  <span class="badge badge-warning"><?php echo $value["casb_descripcion"]?></span>
                  <img src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" class="img-fluid" alt="">
                  <div class="portfolio-info">
                    <div class="row">

                      <div class="col-lg-10">
                        <h4><?php echo "S/. ".number_format($value["rebi_ult_val_act"],2,".",",")?></h4>
                        <p><?php echo $value["rebi_ult_dep_acum"]?></p>
                        <span>Chiclayo, Lambayeque</span>
                      </div>

                      <div class="col-lg-2">
                        <div class="portfolio-links">
                          <a  onclick='contactar(<?php echo $value["rebi_id"].",\"".str_replace("\"","",$value["rebi_ult_dep_acum"])."\",\"".$value["rebi_foto_principal"]."\",\"".URL_TEMA."\""?>);event.stopPropagation();' style="cursor:pointer;">
                            <i class="fas fa-hand-sparkles icon-rojo"></i>
                          </a>
                          <br>
                          <a title="Más detalles">
                            <i class="fas fa-eye icon-azul"></i>
                          </a>
                        </div>
                      </div>

                    </div>
                  </div>


                </div>

              </div>
            <?php }?>
          </div>

        </div>
        
        
        <div id="footer-pagination" class="row container" style="justify-content: flex-end;">
              <div id=navegacion_abajo >
              </div>
        </div>
      </div>
    </section><!-- End Portfolio Section -->

    
    <!-- modal para contactar-->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h7 class="modal-title" id="exampleModalLabel">Mensaje al anunciante</h7>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-lg-4">
                <div class="row px-1 py-1 ">
                  <img class="img-fluid" src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" >
              
                </div>
                <p id="bien_descripcion" class="text-center"></p>
              </div>
              <div class="col-lg-8">
                <h9>Contacta al anunciante</h9>
                <form id="contacto_bien" action="<?php echo URL_RAIZ;?>json/Contacto_bien/guardar" method="post" role="form" class="contactar mt-4" autocomplete="off" validate>
                  <input id="bien_id" name="rebi_id" type="hidden">

                  <div class="form-group">
                    <select type="text" class="form-control" name="tipo_documento" id="tipo_documento" />
                      <option value=""> Seleccione Tipo de Documento</option>
                      <?php 
                      foreach($this->tipos_proveedor as $value){?>
                        
                        <option value="<?php echo $value["tabl_id"] ?>"><?php echo $value["tabl_descripcion"] ?></option>
                      <?php }?>
                    </select>
                      <div class="validate"></div>
                  </div>


                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="nro_documento" id="nro_documento" placeholder="Nro Documento" data-rule="minlen:8" data-msg="Ingresar un número válido" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono" data-rule="minlen:6" data-msg="Ingresar un telefono o celular " />
                      <div class="validate"></div>  
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>  
                    </div>
                  </div>  
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Ingresar un email válido" />
                      <div class="validate"></div>
                  </div>    
                  <div class="form-group">
                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" data-rule="minlen:6" data-msg="Ingresar al menos 6 carácteres" />
                      <div class="validate"></div>
                  </div>            
                  <div class="form-group">
                    <textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Ingresar un mensaje">Hola, me interesa este inmueble y quiero que me contacten. Gracias.</textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="text-center"><input class="btn btn-primary" type="submit" value=Enviar /></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>          

<!--<div class="g_button-container">
    <div class="g_button text-center no_rotaras no_ver" style="background-color: skyblue;">
        <span id="boton-chat" target="_blank">
            <i class="fa fa-user icon_g_button"></i>
        </span>
    </div>
    <div class="g_button text-center no_rotaras no_ver" style="background-color: green;">
        <a id="wsp_chat" target="_blank">
            <i class="fa fa-whatsapp icon_g_button"></i>
        </a>
    </div>
    <div class="g_button text-center no_rotaras no_ver" style="background-color: blue;">
        <a id="id_facpage" target="_blank">
            <i class="fa fa-facebook icon_g_button"></i>
        </a>
    </div>
    <div class="btn g_button text-center" style="background-color: red">
        <i class="fa fa-comments icon_g_button rotaras" style="line-height: 35px;"></i>
        <i class="fa fa-times no_ver icon_g_button no_rotaras no_rotaras_nunca" style="line-height: 35px;"></i>
    </div>
</div>-->




<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'templates/css/bienes.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/font-awesome/css/font-awesome.css';?>">
<!--<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/jquery/jquery-ui/jquery-ui.min.css';?>">-->
<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/v3.4/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/funciones.js';?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC.'templates/js/bienes.js';?>"></script>


<script type="text/javascript" src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/jquery.touchSwipe.min.js';?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick.css';?>" >
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC.'librerias/slick/slick-theme.css';?>" >
<script src="<?php echo URL_STATIC.'librerias/jquery/jquery-touchSwipe/slick.js';?>" type="text/javascript" charset="utf-8"></script>

<script>
  URL_TEMA = '<?php echo URL_TEMA; ?>';
  URL_RAIZ = '<?php echo URL_RAIZ; ?>';
</script>
<!-- lista de inmuebles -->

  <!-- ======= Portfolio Section ======= -->
  <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title" data-aos="zoom-out">
          <h2>Nuestros Inmuebles</h2>
          <p>Da clic en cualquiera de ellos para tener más detalles</p>
        </div>

        <!-- <ul id="portfolio-flters" class="d-flex justify-content-end" data-aos="fade-up">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-app">App</li>
          <li data-filter=".filter-card">Card</li>
          <li data-filter=".filter-web">Web</li>
        </ul> -->
        <div class="row">
          <div class="col-lg-3">
            <label class="font-weight-bold"> Precio mínimo: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_min" placeholder="Ejm: 2000" />
            </div>
            <label class="font-weight-bold"> Precio máximo: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="precio_max" placeholder="Ejm: 10000" />
            </div>

            <label class="font-weight-bold"> Área mínima: </label>  
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_min" placeholder="Ejm: 200" />
            </div>
            <label class="font-weight-bold"> Área máxima: </label>  
            
            <div class="col-md-12 form-group">
                <input type="text" class="form-control" id="area_max" placeholder="Ejm: 500" />
            </div>

            <label class="font-weight-bold">Catálogo: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="catalogo">
                  <option value="">TODOS</option>
                <?php 
                  foreach($this->catalogo as $c){
                    echo "<option value=".$c["casb_codigo"].">".$c["casb_descripcion"]."</option>";
                  }
                ?>
                </select>
            </div>

            <label class="font-weight-bold">Tipo Bien: </label>

            
            <div class="col-md-12 form-group">
                <select class="form-control" id="tipo_bien">
                  <option value="">TODOS</option>
                <?php 
                  foreach($this->tipo_bien as $t){
                    echo "<option value=".$t["tabl_id"].">".$t["tabl_descripcion"]."</option>";
                  }
                ?>
                </select>
            </div>

             <label class="font-weight-bold">Fechas: </label> 
            <div class="col-md-12 form-group">
                <select class="form-control" id="fecha">
                  <option value="">TODO</option>
                   <option value="month">Último Mes</option>
                   <option value="fifteen">Últimos 15 días</option>
                   <option value="week">Último Semana</option>
                </select>
            </div>
            
            <!--<div class="col-md-12 mb-2 form-row">
              <div class="col-md-12">
                  <p>
                    Rango de precios:<strong style="font-size: 11px;" id="amount"></strong>
                  </p>
                <div id="slider-range"></div>
                <input type="hidden" id="amount1">
                <input type="hidden" id="amount2">
              </div>
           </div>-->

            <div class="text-center"><button class="btn btn-secondary" onclick="filtrar()">Filtrar</button></div>
          
          </div>
          <div id="portfolio-container" class="row col-lg-9" data-aos="fade-up">

            <?php $items=0;
            foreach($this->bienes as $value){
              $items++;?>
              <div onclick='detalle(<?php echo $value["rebi_id"]; ?>)'  id="item_nro<?php echo $items;?>" class="col-lg-4 col-md-6 portfolio-item  items_bienes item_nro<?php echo $items;?> " nro_item="item_nro<?php echo $items;?>">
                <div class="portfolio-img"><img src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" class="img-fluid" alt=""></div>
                <div class="portfolio-info">
                  <h4><?php echo $value["casb_descripcion"]?></h4>
                  <p><?php echo $value["rebi_detalles"]?></p>
                  <p><?php echo "S/. ".number_format($value["rebi_ult_val_act"],2,".",",")?></p>
                  <button onclick='contactar(<?php echo $value["rebi_id"].",\"".str_replace("\"","",$value["rebi_detalles"])."\",\"".$value["rebi_foto_principal"]."\",\"".URL_TEMA."\""?>);event.stopPropagation();' >CONTACTAR</button>
                  
                </div>
              </div>
            <?php }?>
          </div>
        </div>
        
        
        <div id="footer-pagination" class="row container" style="justify-content: flex-end;">
              <div id=navegacion_abajo >
              </div>
        </div>
      </div>
    </section><!-- End Portfolio Section -->

    <!-- modal para contactar-->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h7 class="modal-title" id="exampleModalLabel">Mensaje al anunciante</h7>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="row">
              <div class="col-lg-4">
                <div class="row px-1 py-1 ">
                  <img class="img-fluid" src="<?php echo URL_TEMA."assets/img/bienes/".$value['rebi_foto_principal'];?>" >
              
                </div>
                <p id="bien_descripcion" class="text-center"></p>
              </div>
              <div class="col-lg-8">
                <h9>Contacta al anunciante</h9>
                <form id="contacto_bien" action="<?php echo URL_RAIZ;?>json/Contacto_bien/guardar" method="post" role="form" class="contactar mt-4" autocomplete="off" validate>
                  <input id="bien_id" name="rebi_id" type="hidden">

                  <div class="form-group">
                    <select type="text" class="form-control" name="tipo_documento" id="tipo_documento" />
                      <option value=""> Seleccione Tipo de Documento</option>
                      <?php 
                      foreach($this->tipos_proveedor as $value){?>
                        
                        <option value="<?php echo $value["tabl_id"] ?>"><?php echo $value["tabl_descripcion"] ?></option>
                      <?php }?>
                    </select>
                      <div class="validate"></div>
                  </div>


                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="nro_documento" id="nro_documento" placeholder="Nro Documento" data-rule="minlen:8" data-msg="Ingresar un número válido" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono" data-rule="minlen:6" data-msg="Ingresar un telefono o celular " />
                      <div class="validate"></div>  
                    </div>
                  </div>
                  
                  <div class="form-row">
                    <div class="col-md-6 form-group">
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                      <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" data-rule="minlen:4" data-msg="Ingresar al menos 4 carácteres" />
                      <div class="validate"></div>  
                    </div>
                  </div>  
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Ingresar un email válido" />
                      <div class="validate"></div>
                  </div>    
                  <div class="form-group">
                    <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Direccion" data-rule="minlen:6" data-msg="Ingresar al menos 6 carácteres" />
                      <div class="validate"></div>
                  </div>            
                  <div class="form-group">
                    <textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Ingresar un mensaje">Hola, me interesa este inmueble y quiero que me contacten. Gracias.</textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="text-center"><input class="btn btn-primary" type="submit" value=Enviar /></div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
class BDContacto_bien{
	ventana=null;
	ventanaid=null;	
	frmid='frmcontacto_bien';
	tb='contacto_bien';
	$fun=null;
	bustexto='';
	id=null;
	constructor(ventanaid,rebi_id){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.rebi_id= rebi_id;
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.id=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						;
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Rebi_id</th>
            	<th>Email</th>
            	<th>Nombre</th>
            	<th>Telefono</th>
            	<th>Dni</th>
            	<th>Mensaje</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		data.append("rebi_id",this.rebi_id);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.id}" idpk="id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.id}" type="checkbox" value="0">
                      <label for="checkbox{$v.id}"></label>
                    </div></div></td>
					<td>${v.rebi_id}</td>	
					<td>${v.email}</td>	
					<td>${v.nombre}</td>	
					<td>${v.telefono}</td>	
					<td>${v.dni}</td>	
					<td>${v.mensaje}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#id').val(rs.id);
			$frm.find('input#rebi_id').val(rs.rebi_id||'');
  			$frm.find('input#email').val(rs.email||'');
  			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#telefono').val(rs.telefono||'');
  			$frm.find('input#dni').val(rs.dni||'');
  			$frm.find('input#mensaje').val(rs.mensaje||'');
  				    }
	    if(this.id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('id',this.id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
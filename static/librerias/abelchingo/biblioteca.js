requirecss('biblioteca.css');
var nbiblioteca=0;
class mybilioteca{
	endonde='';		
	conf={
		elemento:null,
		ventanaid:'',
		image:false,
		video:false,
		documento:false,
		audio:false,
		funcionretorno:null,
	};

	constructor(el,params={}){
		this.func=new _acht_();
		this.endonde=selector(el);
		this.conf=assign(this.conf,params);
		this.template();
	}
	template(){	
		nbiblioteca++;
        this.conf.ventanaid='biblioteca'+'_'+nbiblioteca;
        let html = /*html*/`<div class="container-fluid biblioteca " id="${this.conf.ventanaid}">
          <form method="post" id="frm${this.conf.ventanaid}" enctype="multipart/form-data"></form>
		  <div class="row biblioteca-header">
		    <div class="col-md-6 col-sm-6 col-xs-12 text-center">
		    	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		    	${this.conf.image?`
		      	<div class="btn-group btnsubirfile" tipo="image">
		      		<div class="btn btn-lg btn btn-primary istooltip" title="selecciona un archivo desde su computadora" data-placement="bottom">
		          		<i class="fa fa-image"></i> 
		          		<span>upload Image</span>		          		
		        	</div>
		        </div>`:``}
		        ${this.conf.video?`
		      	<div class="btn-group btnsubirfile" tipo="video" >
		      		<div class="btn btn-lg  btn btn-primary istooltip" title="selecciona un archivo desde su computadora" data-placement="bottom">
		          		<i class="fa fa-video-camera"></i> 
		          		<span>upload video</span>		          		
		        	</div>
		        </div>`:``}
		        ${this.conf.pdf?`
		      	<div class="btn-group btnsubirfile" tipo="documento">
		      		<div class="btn btn-lg btn btn-primary istooltip" title="selecciona un archivo desde su computadora" data-placement="bottom">
		          		<i class="fa fa-file"></i> 
		          		<span>upload PDF</span>		          		
		        	</div>
		        </div>`:``}
		        ${this.conf.audio?`
		      	<div class="btn-group btnsubirfile" tipo="audio">
		      		<div class="btn btn-lg btn btn-primary istooltip" title="selecciona un archivo desde su computadora" data-placement="bottom">
		          		<i class="fa fa-music"></i> 
		          		<span>upload Audio</span>		          		
		        	</div>
		        </div>`:``}
		        </div>
		    </div>
		    <div class="col-md-6 col-sm-6 col-xs-12 text-center">     
		      <div class="form-group col-sm-12" style="margin:0">  
		          <div class="input-group " style="margin:0">
		            <input type="hidden" id="tipobiblioteca" value="video">
		            <input type="text" id="buscarbiblioteca" class="form-control" placeholder="Search...">
		            <span class="btn input-group-addon btnsearch"><i class="fa fa-search"></i></span>
		          </div>
		      </div>
		    </div>   
		  </div>
		  	<div class="row addfile" id="${this.conf.ventanaid}-content">
		      	<div class="col-md-3 col-sm-4 col-xs-6 text-center pnllisbilioteca" fileid="1" filelink="N1-U1-A1-20200304102958.mp4" filename="VIDEO PARA EJERCICIO.mp4" title="Select">
		            <span class="file" data-tipo="video">
		         		<i class="iconfilebibli fa fa-file-video-o btnselected"></i>         
		        	</span>       
		             <div class="text-center ">VIDEO PARA EJERCICIO1.mp4</div>
			        <div>
			          <span class="btn btn-xs btn istooltip btnver" title="Preview"><i class="fa fa-eye"></i></span>
			          <span class="btn btn-xs istooltip btnselected" title="Select"><i class="fa fa-hand-o-down"></i></span>
			          <span class="btn btn-xs btnremove istooltip" title="Remove" data-id="17397"><i class="fa fa-trash-o"></i></span>
			        </div>
		    	</div>
		    	<div class="col-md-3 col-sm-4 col-xs-6 text-center pnllisbilioteca" fileid="2" filelink="N1-U1-A1-20200304102958.mp4" filename="VIDEO PARA EJERCICIO.mp4" title="Select">
		            <span class="file" data-tipo="video">
		         		<i class="iconfilebibli fa fa-file-video-o btnselected"></i>         
		        	</span>       
		             <div class="text-center ">VIDEO PARA EJERCICIO2.mp4</div>
			        <div>
			          <span class="btn btn-xs btn istooltip btnver" title="Preview"><i class="fa fa-eye"></i></span>
			          <span class="btn btn-xs istooltip btnselected" title="Select"><i class="fa fa-hand-o-down"></i></span>
			          <span class="btn btn-xs btnremove istooltip" title="Remove" data-id="17397"><i class="fa fa-trash-o"></i></span>
			        </div>
		    	</div>         
		    </div>
		</div>
        `;
        if(this.endonde.length>0) addAttr(this.endonde,{'innerHTML':html}); 
        this.btnbuscar();
        this.eventos();
	}
	eventos(){
		let _this=this;
		let $ven=$(`#${this.conf.ventanaid}`);
		$ven.on('click','.btnsearch',function(ev){
			ev.preventDefault();
			_this.btnbuscar();
		}).on('keypress','#buscarbiblioteca',function(ev){
			if(ev.which === 13){ _this.btnbuscar();}
		}).on('click','.btnsubirfile',function(ev){
			ev.preventDefault();
			_this.subirfile(ev);
		}).on('click','.btnselected',function(ev){
			ev.preventDefault();
			let pnl=$(this).closest('.pnllisbilioteca');
			_this.btnselected(pnl);
		}).on('click','.btnremove',function(ev){
			ev.preventDefault();
			let pnl=$(this).closest('.pnllisbilioteca');
			_this.btnremove(pnl);
		}).on('click','.btnver',function(ev){			
			ev.preventDefault();
			let pnl=$(this).closest('.pnllisbilioteca');
			_this.btnver(pnl);
		})
	}	
	btnbuscar(){
		let _this=this;
		let ven=$(this.endonde);
		//let el=ev.target;
		var data=new FormData()		
			data.append('texto',ven.find('#buscarbiblioteca').val()||'');
			data.append('tipo','image');
		_this.func.postData({url:_sysUrlBase_+'json/biblioteca/','data':data,'headers':{'Content-Type': 'multipart/form-data'}}).then(rs => { 
			$.each(rs,function(i,v){
				_this.addfile({
					fileid:v.idbiblioteca,
					tipo:v.tipo,
					nombre:v.nombre,
					link:_sysUrlBase_+v.archivo,
					autorid:v.idpersona
				});							
			})
        }).catch(e => { console.log(e)});
		
	}
	addfile(datos={}){
		return new Promise((resolve, reject) => {
			let dt=assign({fileid:Date.now(),tipo:'image',link:'',nombre:'sinnombre',cargando:false,donde:'affter',autorid:''},datos);
			let html = /*html*/`
		      	<div class="col-md-3 col-sm-4 col-6 col-xs-6 text-center pnllisbilioteca" idautor="${dt.autorid}" 
		      	fileid="${dt.fileid}" id="biblioteca${dt.fileid}" link="${dt.link}" nombre="${dt.nombre}" title="Select">
		      		${dt.cargando==true?`
		      		<div class="progress">
					  <div class="progress-bar progress-bar-striped" role="progressbar" style="width:0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
					</div>`:``}				
		            <div class="file">
		            	${dt.tipo=='image'?`<img src="${dt.link}" class="img-fluid">`:(
		            	dt.tipo=='audio'?`<audio src="${dt.link}" controls=true/>`:(
		            	dt.tipo=='video'?`<video src="${dt.link}" controls=true/>`:
		            	`<i class="fa fa-file btnselected"></i>`))}
		        	</div>       
		             <div class="text-center txtnombre">${dt.nombre}</div>
			        <div>
			        ${(dt.tipo!='image' &&  dt.tipo!='video' && dt.tipo!='audio')?`<span class="btn btn-xs btn istooltip btnver" title="Preview"><i class="fa fa-eye"></i></span>`:``}
			          <span class="btn btn-xs istooltip btnselected" title="Select"><i class="fa fa-hand-o-down"></i></span>
			          <span class="btn btn-xs btnremove istooltip" title="Remove"><i class="fa fa-trash-o"></i></span>
			        </div>
		    	</div>		    	
	        `;
	        let content=selector(`#${this.conf.ventanaid}-content`)[0];
	        if(dt.donde=='affter') content.insertAdjacentHTML('afterbegin',html)
	        else content.insertAdjacentHTML('beforeend',html);
	    	let pnltmp=$('#biblioteca'+dt.fileid);
	    	resolve(pnltmp);
    	})
	}
	subirfile(ev){
		let $this=this;
		let pnl=ev.target.closest('.btnsubirfile');
		let tipo=pnl.getAttribute('tipo');
		let nombre=pnl.getAttribute('nombre')||'';
		if(pnl.classList.contains('disabled')) return false;
        pnl.classList.add('disabled');
        let acepta='image/x-png, image/gif, image/jpeg, image/*, audio/mp3, video/mp4, application/pdf, application/zip, .html, .htm, audio/mp3, application/zip';
        if(tipo=='image'){acepta='image/*';}
		else if(tipo=='enlacesinteres') acepta='text/plain';
		else if(tipo=='audio') acepta='audio/// ';
		else if(tipo=='video') acepta='video/mp4';
		else if(tipo=='html') acepta='.html , .htm , application/zip';		
		else if(tipo=='documento') acepta='application/pdf , .doc, .docx, .xls, .xlsx, .ppt, .pptx';		
		var file=document.createElement('input');
		file.id='file_'+Date.now();		
		file.type='file';
		file.accept=acepta;
		file.addEventListener('change',function(ev){
			let tmppnlfile=null;
			if(tipo=="image"||tipo=="video"||tipo=="audio"){
				var rd = new FileReader();
				rd.onload = function(filetmp){
					var filelocal = filetmp.target.result;
					$this.addfile({'tipo':tipo,'link':filelocal,cargando:true}).then(pnlfile=>{						
						tmppnlfile=pnlfile;						
						pnl.classList.remove('disabled');
					})
				}
				rd.readAsDataURL(this.files[0]);
			}	
			//let frm=$this.conf.elemento.querySelector('#frm'+$this.conf.ventanaid);
			var data=new FormData()		
			data.append('media',this.files[0]);
			data.append('tipo',tipo);
			$this.func.postData({url:_sysUrlBase_+'json/biblioteca/guardar','data':data,'headers':{'Content-Type': 'multipart/form-data'}}).then(rs => { 
				$(tmppnlfile).attr('link',_sysUrlBase_+rs.link);
				$(tmppnlfile).attr('fileid',rs.newid);
				$(tmppnlfile).attr('id',rs.newid);				
				$(tmppnlfile).attr('idautor',rs.idpersona);
				$(tmppnlfile).attr('nombre',rs.nombre);
				if(tipo=="video"||tipo=="audio"){
					$(tmppnlfile).find(tipo).attr('src',_sysUrlBase_+rs.link);
				}else if(tipo=="image"){
					$(tmppnlfile).attr('link',_sysUrlBase_+rs.link);
					$(tmppnlfile).find('img').attr('src',_sysUrlBase_+rs.link);
				}
				$(tmppnlfile).find('.txtnombre').text(rs.nombre);
	        }).catch(e => { console.log(e)});
		})
		file.click();
	}
	btnver(pnl){
		
	}
	btnselected(pnl){
		console.log(pnl);
		let dt={
			link:pnl.attr('link')||'',
			nombre:pnl.attr('nombre')||'',
			id:pnl.attr('fileid')||-1
		}
		this.conf.funcionretorno(dt);
	}
	btnremove(pnl){		
		var data=new FormData()		
			data.append('idbiblioteca',pnl.attr('fileid'));
		this.func.eliminar({url:'biblioteca',data:data,ele:pnl,remove:true});
	}
}  

class BDClientes{
	ventana=null;
	ventanaid=null;	
	frmid='frmclientes';
	tb='clientes';
	$fun=null;
	bustexto='';
	clie_id=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.clie_id=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						
     	.on('change','select#bustabl_tipocliente',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busclie_estado',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busclie_empleado',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
				$('.input-append.date').datepicker({
				autoclose: true,
				todayHighlight: true
	   	});
	   	   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('tabl_tipocliente',$('select#bustabl_tipocliente').val()||'');
				data.append('clie_estado',$('select#busclie_estado').val()||'');
				data.append('clie_empleado',$('select#busclie_empleado').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Clie_apellidos</th>
            	<th>Clie_nombres</th>
            	<th>Clie_razsocial</th>
            	<th>Clie_dni</th>
            	<th>Clie_direccion</th>
            	<th>Clie_telefono</th>
            	<th>Clie_email</th>
            	<th>Clie_fregistro</th>
            	<th>Clie_estado</th>
            	<th>Clie_zona</th>
            	<th>Clie_empleado</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.clie_id}" idpk="clie_id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.clie_id}" type="checkbox" value="0">
                      <label for="checkbox{$v.clie_id}"></label>
                    </div></div></td>
					<td>${v.clie_apellidos}</td>	
					<td>${v.clie_nombres}</td>	
					<td>${v.clie_razsocial}</td>	
					<td>${v.clie_dni}</td>	
					<td>${v.clie_direccion}</td>	
					<td>${v.clie_telefono}</td>	
					<td>${v.clie_email}</td>	
					<td>${v.clie_fregistro}</td>	
					<td><div class="row-fluid"><div cp="clie_estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.clie_estado}" ${v.clie_estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.clie_estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.clie_zona}</td>	
					<td><div class="row-fluid"><div cp="clie_empleado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.clie_empleado}" ${v.clie_empleado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.clie_empleado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.clie_id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={clie_id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#clie_id').val(rs.clie_id);
			$frm.find('select#tabl_tipocliente').val(rs.tabl_tipocliente||'').trigger('change');
  			$frm.find('input#clie_apellidos').val(rs.clie_apellidos||'');
  			$frm.find('input#clie_nombres').val(rs.clie_nombres||'');
  			$frm.find('input#clie_razsocial').val(rs.clie_razsocial||'');
  			$frm.find('input#clie_dni').val(rs.clie_dni||'');
  			$frm.find('input#clie_direccion').val(rs.clie_direccion||'');
  			$frm.find('input#clie_telefono').val(rs.clie_telefono||'');
  			$frm.find('input#clie_email').val(rs.clie_email||'');
  			let clie_estado=rs.clie_estado||1;
			$frm.find('input#clie_estado').val(clie_estado);
			if(clie_estado==1){
				$frm.find('input#clie_estado').attr('checked','checked');
				$frm.find('input#clie_estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#clie_estado').removeAttr('checked');
				$frm.find('input#clie_estado').siblings('label').text('Inactivo');
			}
  			$frm.find('input#clie_zona').val(rs.clie_zona||'');
  			let clie_empleado=rs.clie_empleado||1;
			$frm.find('input#clie_empleado').val(clie_empleado);
			if(clie_empleado==1){
				$frm.find('input#clie_empleado').attr('checked','checked');
				$frm.find('input#clie_empleado').siblings('label').text('Activo');
			}else {
				$frm.find('input#clie_empleado').removeAttr('checked');
				$frm.find('input#clie_empleado').siblings('label').text('Inactivo');
			}
  				    }
	    if(this.clie_id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('clie_id',this.clie_id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
class BDContacto_bien{
	ventana=null;
	ventanaid=null;	
	frmid='frmcontacto_bien';
	tb='contacto_bien';
	$fun=null;
	bustexto='';
	id=null;
	constructor(ventanaid,rebi_id){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.rebi_id=rebi_id;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.id=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						
     	.on('change','select#bustipo_documento',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busrebi_id',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});

		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});

		//generar contrato	
		$("#contacto_bien").on('submit',function(ev){
			ev.preventDefault();			
			let recarga = true;
			return new Promise((resolve, reject) => {
				let _frmtmp=document.getElementById("contacto_bien");
				var data=new FormData(_frmtmp);
				_this.$fun.postData({url:_sysUrlBase_+'json/Cronogramapago_cab/guardar','data':data,'headers':{'Content-Type': 'multipart/form-data'}}).then(rs => { 
				if(recarga=='return'){
					resolve(rs);
				}else if(recarga==true)window.location.reload();
				
				else{                    
					Swal.fire({
						type: 'success',//warning (!)//success
						icon: 'success',//warning (!)//success
						// title: rs.msj,
						text: 'Se generó el cronograma de pagos',
						allowOutsideClick: false,
						closeOnClickOutside: false
					}).then(rs=>{
						ventana.ventana.children('.vistatablaformulario').hide(0);
						ventana.ventana.children('.vistatabla').fadeIn(500);
						$("#exampleModal").modal("hide");
						$("#contacto_bien").trigger("reset");	
						resolve(true);
					})	          		
				}	          	
				}).catch(e =>{
					resolve(false);	            
				})
			})
		});
		
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('tipo_documento',$('select#bustipo_documento').val()||'');
				data.append('rebi_id',$('select#busrebi_id').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Tipo Usuario</th>	
                <th>Nro_documento</th>				
            	<th>Nombre</th>
            	<th>Apellidos</th>				
            	<th>Email</th>				
            	<th>Telefono</th>
            	<th>Direccion</th>
            	<th>File_infocorp</th>
            	<th>File_recomendacion</th>
            	<th>File_policial_judicial</th>
            	<th>File_extra_recomendacion</th>
            	<th>Puntaje</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		// linea 79 puede ser alternativa href="./contacto_bien/download/${v.file_infocorp}.pdf" y faltaria implementar el metodo download  
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			
			forEach(rs,function(v,i){
				switch(v.estado){
					case "1":
						v.estado = '<i class="fa fa-arrow-right btncontrato"></i>';
						v.accions = '<i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i>';
						break;
					case "2":
						v.estado = 'Con cronograma';
						v.accions = '<i class="fa fa-thumbs-up"></i>';
						break;
				}

				v.file_infocorp = v.file_infocorp =="" ? "" : `<a target="_blank" href="https://localhost/inmobiliaria/static/templates/plantillas/web/assets/img/bienes/${v.file_infocorp}" download><i class="fa fa-file" ></i>` ;
				v.file_recomendacion = v.file_recomendacion =="" ? "" : `<a target="_blank" href="https://localhost/inmobiliaria/static/templates/plantillas/web/assets/img/bienes/${v.file_recomendacion}" download><i class="fa fa-file" ></i>` ;
				v.file_policial_judicial = v.file_policial_judicial =="" ? "" : `<a target="_blank" href="https://localhost/inmobiliaria/static/templates/plantillas/web/assets/img/bienes/${v.file_policial_judicial}" download><i class="fa fa-file" ></i>` ;
				v.file_extra_recomendacion = v.file_extra_recomendacion =="" ? "" : `<a target="_blank" href="https://localhost/inmobiliaria/static/templates/plantillas/web/assets/img/bienes/${v.file_extra_recomendacion}" download><i class="fa fa-file" ></i>` ;

				html+=`<tr id="${v.id}" idpk="id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.id}" type="checkbox" value="0">
                      <label for="checkbox{$v.id}"></label>
                    </div></div></td>
					<td>${v.tipo_documento2}</td>	
					<td>${v.nro_documento}</td>					
					<td>${v.nombre}</td>	
					<td>${v.apellidos}</td>						
					<td>${v.email}</td>						
					<td>${v.telefono}</td>	
					<td>${v.direccion}</td>	
					<td>${v.file_infocorp}</td>	
					<td>${v.file_recomendacion}</td>	
					<td>${v.file_policial_judicial}</td>	
					<td>${v.file_extra_recomendacion}</td>	
					<td>${v.punataje}</td>	
					<td>${v.accions}  ${v.estado}  </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
			
			$tabla.on('click','.btncontrato',function(ev){
				ev.preventDefault();
				_this.id=$(this).closest('tr').attr('id');
				_this.vista_modal();
			});
			$(".btnnuevo").hide();
        }).catch(e => {
        	console.log(e);
        })
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#id').val(rs.id);
			$frm.find('select#tipo_documento').val(rs.tipo_documento||'').trigger('change');
  			$frm.find('input#nro_documento').val(rs.nro_documento||'');
  			$frm.find('input#apellidos').val(rs.apellidos||'');
  			$frm.find('input#direccion').val(rs.direccion||'');
  			$frm.find('select#rebi_id').val(rs.rebi_id||'').trigger('change');
  			$frm.find('input#email').val(rs.email||'');
  			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#telefono').val(rs.telefono||'');
  			$frm.find('textarea#mensaje').val(rs.mensaje||'');
  			$frm.find('input#punataje').val(rs.punataje||'');
  			$frm.find('input#estado').val(rs.estado||'');
  				    }
	    if(this.id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('id',this.id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);
				
				$(".ocultar").hide();	
	        }).catch(e => {console.log(e); });

	}

	//open modal to generate a contract
	vista_modal(){
		let _this=this;
		//set id en el modal
		$("#exampleModal").modal("show");
		var data = new FormData();
		data.append('id',this.id);
		data.append('sqlget',true);			
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
			$(".modal-title").html("Generar contrato a <strong class='text-uppercase'>"+ (rs.nombre)+" "+(rs.apellidos)+"</strong>");
			
			setTimeout(function() { $('input#garantia').focus() }, 1000);
			$("input#emailmodal").val(rs.email);	
			$("input#usuario").val(rs.nro_documento);	
			$("input#password").val(rs.nro_documento);
			$("input#contactobien_id").val(rs.id);						
		}).catch(e => {console.log(e); });

	}
}
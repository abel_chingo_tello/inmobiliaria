class BDCronogramapago_cab{
	ventana=null;
	ventanaid=null;	
	frmid='frmcronogramapago_cab';
	tb='cronogramapago_cab';
	$fun=null;
	bustexto='';
	id=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.id=false;	_this.vista_formulario(); })
			.on('keyup','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Persona</th>
            	<th>Registro Bien</th>
            	<th>Observacion</th>
            	<th>Estado</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		data.append("inner","1");
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				if(v.nombres == null){
					v.nombres = "";
				}
				if(v.apellido_1 ==null){
					v.apellido_1 = "";
				}
				if(v.apellido_2 ==null){
					v.apellido_2 = "";
				}

				switch(v.estado){
					case "0":
						v.estado = "Sin aceptar";
						break;
					case "1":
						v.estado = "Aceptó contrato";
						break;
				}

				html+=`<tr id="${v.id}" idpk="id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.id}" type="checkbox" value="0">
                      <label for="checkbox{$v.id}"></label>
                    </div></div></td>
					<td>${v.nombres} ${v.apellido_1} ${v.apellido_2}</td>	
					<td>${v.rebi_detalles}</td>	
					<td>${v.observacion}</td>	
					<td>${v.estado}</td>	
					<td><i class="fa fa-search btneditar"></i> <i class="fa fa-calendar btncalendar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.id=$(this).closest('tr').attr('id');
				_this.vista_modal();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); });

			$tabla.on('click','.btncalendar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={id:tr.attr('id')}
				location.href = "./cronogramapago_det/index/"+data.id;
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
			

			$(".btnnuevo").hide();
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#id').val(rs.id);
			$frm.find('input#persona_id').val(rs.persona_id||'');
  			$frm.find('input#rebi_id').val(rs.rebi_id||'');
  			$frm.find('input#garantia').val(rs.garantia||'');
  			$frm.find('input#meses_contrato').val(rs.meses_contrato||'');
  			$frm.find('input#monto_mensual').val(rs.monto_mensual||'');
  			$frm.find('input#dia_pago').val(rs.dia_pago||'');
  			$frm.find('input#archivo_contrato').val(rs.archivo_contrato||'');
  			$frm.find('input#observacion').val(rs.observacion||'');
  			$frm.find('input#estado').val(rs.estado||'');
  			$frm.find('input#estado_pago').val(rs.estado_pago||'');
  				    }
	    if(this.id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('id',this.id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}
	vista_modal(){
		let _this=this;

		$("#exampleModal").modal("show");


		var llenardatos=function(rs){
	    	$('#m_garantia').val(rs.garantia||'');
  			$('#m_meses_contrato').val(rs.meses_contrato||'');
  			$('#m_monto_mensual').val(rs.monto_mensual||'');
  			$('#m_dia_pago').val(rs.dia_pago||'');
		}
	    var data=new FormData()		
		data.append('id',this.id);
		data.append('sqlget',true);			
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
			llenardatos(rs);				
		}).catch(e => {console.log(e); });

	}

	
}
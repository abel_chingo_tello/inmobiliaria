class BDCronogramapago_det{
	ventana=null;
	ventanaid=null;	
	frmid='frmcronogramapago_det';
	tb='cronogramapago_det';
	$fun=null;
	bustexto='';
	id=null;
	constructor(ventanaid,cab_id,tipo_usuario){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.cab_id = cab_id;
		this.tipo = tipo_usuario;
		
		this.cuotas = [];
		this.cuotas_a_pagar = [];
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault(); _this.pagar()})
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
			.on('change','select#bienes',function(ev){ev.preventDefault(); _this.vista_select_usuario();})
			.on('change','.input-lg',function(ev){ 
				ev.preventDefault();
				var checkbox = $(this);
				_this.cargo = checkbox.val();
				_this.checkboxid = checkbox[0].id;
				_this.actualizarTotal();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		

		//crear resumen de pago
		document.getElementById("checkout-btn").addEventListener("click", function() {
			$('#checkout-btn').attr("disabled", true);
			var orderData = _this.cuotas_a_pagar;		
			
			fetch(_sysUrlBase_+'json/'+_this.tb+'/createpreference', {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(orderData),
			  })
				.then(function(response) {
					return response.json();
				})
				.then(function(preference) {
					let html = "";
					for(var i=0;i<preference.data.length; i++){
						var v = preference.data[i];
						html+=`<tr>
							<td>${v.concepto}</td>
							<td>${v.precio}</td>		
						</tr>`;
					};	
					$("#resumen_pago").html(html);
					createCheckoutButton(preference.preference);
					$(".shopping-cart").fadeOut(500);
					setTimeout(() => {
						$(".container_payment").show(500).fadeIn();
					}, 500);
				})
				.catch(function() {
					alert("Unexpected error");
					$('#checkout-btn').attr("disabled", false);
				});
		  });
		  
		  //Create preference when click on checkout button
		  function createCheckoutButton(preference) {
			var script = document.createElement("script");
			
			// The source domain must be completed according to the site for which you are integrating.
			// For example: for Argentina ".com.ar" or for Brazil ".com.br".
			script.src="https://www.mercadopago.com.pe/integrations/v1/web-payment-checkout.js"
			script.type = "text/javascript";
			script.dataset.preferenceId = preference;
			document.getElementById("button-checkout").innerHTML = "";
			document.querySelector("#button-checkout").appendChild(script);
		  }
		  
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
		data.append("cab_id",this.cab_id);

				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                
            	<th>Fecha Vencimiento</th>
            	<th>Concepto</th>
            	<th>Estado</th>
            	
				<th>Cargo</th>
            	<th>Pago</th>
            	<th>Saldo</th>
            	<th>Subtotal</th>
            	<th>Observacion</th>
            	
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);

		
		
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			var repeat = false;
			var j = rs.findIndex((ele)=> ele.estado == "1");
			if(j==-1){
				j = rs.length-1;
			}else{
				j = j-1;
			}

			for(var i=j;i<rs.length; i++){
				var v = rs[i];
				switch(v.estado){
					case "0":
						v.estado = "Pendiente";
						repeat = true;
						break;
					case "1":
						v.estado = "Cancelado";
						break;
				}
				html+=`<tr id="${v.id}" idpk="id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.id}" type="checkbox" value="0">
                      <label for="checkbox{$v.id}"></label>
                    </div></div></td>
					
					<td>${v.fecha_vencimiento}</td>	
					<td>${v.concepto}</td>
					<td>${v.estado}</td>	
					<td>${v.cargo}</td>	
					<td>${v.pago}</td>	
					<td>${v.saldo}</td>	
					<td>${v.subtotal}</td>	
					<td>${v.observacion}</td>	
				</tr>`;
			};		
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
			$(".input-group").hide();
			if(this.tipo=="s")
				$(".btnnuevo").hide();		
			if(this.tipo=="n"){
				$(".input-group").hide();
			
				$(".btnnuevo i").removeClass("fa-file");
				$(".btnnuevo").html("PAGAR <i class='fa fa-usd'></i>");
			}

        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#id').val(rs.id);
			$frm.find('input#cab_id').val(rs.cab_id||'');
  			$frm.find('input#fecha_vencimiento').val(rs.fecha_vencimiento||'');
  			$frm.find('input#cargo').val(rs.cargo||'');
  			$frm.find('input#pago').val(rs.pago||'');
  			$frm.find('input#saldo').val(rs.saldo||'');
  			$frm.find('input#subtotal').val(rs.subtotal||'');
  			$frm.find('input#observacion').val(rs.observacion||'');
  			$frm.find('input#fecha_pago').val(rs.fecha_pago||'');
  			$frm.find('input#estado').val(rs.estado||'');
  				    }
	    if(this.id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('id',this.id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}
	//actualizar on change del select
	vista_select_usuario(){
		let _this=this;	

		var dataCab=new FormData();
		dataCab.append("id",$("#bienes option:selected").val());

		this.$fun.postData({url:_sysUrlBase_+'json/cronogramapago_cab/',data:dataCab}).then(rs => {
			if(rs.length>0){
				var estado = rs[0].estado_pago;

				switch(estado){
					case "0":
						estado = "Pendiente";
						break;
					case "1":
						estado = "Al día";
						break;	
				}

				$("#estado").html(estado);
			}
		}).catch(e => {
        	console.log(e);
        })

		var data=new FormData();

		data.append("cab_id",$("#bienes option:selected").val());

		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                
            	<th>Fecha Vencimiento</th>
            	<th>Concepto</th>
            	<th>Estado</th>
            	
				<th>Cargo</th>
            	<th>Pago</th>
            	<th>Saldo</th>
            	<th>Subtotal</th>
            	<th>Observacion</th>
            	
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			var repeat = false;
			var j = rs.findIndex((ele)=> ele.estado == "1");
			if(j==-1){
				j = rs.length-1;
			}else{
				j = j-1;
			}

			for(var i=j;i<rs.length; i++){
				var v = rs[i];
				switch(v.estado){
					case "0":
						v.estado = "Pendiente";
						repeat = true;
						break;
					case "1":
						v.estado = "Cancelado";
						break;
				}
				html+=`<tr id="${v.id}" idpk="id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.id}" type="checkbox" value="0">
                      <label for="checkbox{$v.id}"></label>
                    </div></div></td>
					
					<td>${v.fecha_vencimiento}</td>	
					<td>${v.concepto}</td>
					<td>${v.estado}</td>	
					<td>${v.cargo}</td>	
					<td>${v.pago}</td>	
					<td>${v.saldo}</td>	
					<td>${v.subtotal}</td>	
					<td>${v.observacion}</td>	
				</tr>`;
			};		
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			_this.$fun.dtTable('#table'+this.ventanaid,{});
			$(".input-group").hide();
			
			$(".btnnuevo i").removeClass("fa-file");
			$(".btnnuevo").html("PAGAR <i class='fa fa-usd'></i>");
			
			$(".btnnuevo").show();
		}).catch(e => {
        	console.log(e);
        })
	}
	//muestra una tabla con checkbox para pagar
	pagar(){
		
		let _this=this;	
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablapagar').fadeIn(500);
		let html =`<table class="table table-hover table-condensed" id="">
            <thead>
              <tr>
            	<th>Fecha Vencimiento</th>
            	<th>Concepto</th>
            	<th>Cargo</th>
            	<th>Subtotal</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		var data=new FormData();
		data.append("cab_id",$("#bienes option:selected").val());

		this.ventana.find('#aquitablapagar').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			var repeat = false;
			var j = rs.findIndex((ele)=> ele.estado == "1");
			if(j==-1){
				j = rs.length-1;
			}else{
				j = j-1;
			}
			var k=0;
			for(var i=k;i<rs.length; i++){
				var v = rs[i];
				switch(v.estado){
					case "0":
						v.estado = "Pendiente";
						repeat = true;
						v.checked = "";
						v.disabled = "";
						break;
					case "1":

						v.estado = "Cancelado";
						v.checked = "checked";
						v.disabled = "disabled";
						
						break;
				}
				
				if(i==j){
					v.checked = "checked";	
					$("#monto_pagar").html(v.cargo);
					v.disabled = "disabled";
					this.cuotas_a_pagar.push({"id":v.id});
				}
				if(v.estado == "Cancelado"){
					continue;
				}
				
				this.cuotas.push(v.id);
				html+=`<tr id="${v.id}" idpk="id">					
					<td>${v.fecha_vencimiento}</td>	
					<td>${v.concepto}</td>	
					<td>${v.cargo}</td>	
					<td>${v.subtotal}</td>	
					<td><div class="row-fluid"><div class="	">
                      <input id="checkbox${v.id}" class="input-lg" style="width:30px" type="checkbox" value="${v.cargo}" ${v.checked} ${v.disabled}> 
                    </div></div></td>	
				</tr>`;
			};
			$("#aquitablapagar tbody").html(html);
		});		
	}
	actualizarTotal(){
		let _this=this;	
		console.log(_this.cargo,_this.checkboxid);
		var total = parseFloat($("#monto_pagar").html());
		   
		let id_cuota = $("#"+_this.checkboxid).closest('tr').attr('id');

		if($("#"+_this.checkboxid).is(':checked')) {

			var j = this.cuotas.findIndex(ele => ele == id_cuota);
			for(var i=j;i<this.cuotas.length-1;i++){				
				$("#checkbox"+this.cuotas[i]).prop('checked', true);
				var indice = this.cuotas_a_pagar.findIndex(ele=> ele.id==this.cuotas[i]);
				if(indice==-1){
					this.cuotas_a_pagar.push({"id":this.cuotas[i]});
				}				
			}
			this.cuotas_a_pagar.sort(function compare(a,b){ return parseInt(a.id)-parseInt(b.id) });
		}else{
			
			var indice = this.cuotas_a_pagar.findIndex(ele=>ele.id==id_cuota);
			var max_indice = this.cuotas_a_pagar.length;
			
			for(var j=indice;j<max_indice;j++){
				$("#checkbox"+this.cuotas_a_pagar[j].id).prop("checked",false);
			}

			this.cuotas_a_pagar.splice(indice,max_indice-indice);

					
		}
		total = 0;
		for(var i=0;i<this.cuotas_a_pagar.length;i++){
			var cargo=$("#checkbox"+this.cuotas_a_pagar[i].id).val();
			total+=parseFloat(cargo);
		}	
		$("#monto_pagar").html(number_format(total,2,'.',''));
	}
}
function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
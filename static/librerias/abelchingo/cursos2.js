class BDCursos{
	ventana=null;
	ventanaid=null;	
	frmid='frmcursos';
	tb='cursos';
	$fun=null;
	bustexto='';
	idcurso=false;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		let _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idcurso=false;	_this.vista_formulario(); })
			.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.vista_tabla();})
			.on('change','select#busestado',function(ev){ev.preventDefault(); _this.vista_tabla();})
			.on('keypress','input#bustexto',function(ev){if(ev.which === 13){ _this.vista_tabla();}})
			.on('click','.btnordenartb',function(ev){ ev.preventDefault(); _this.vista_ordenar();})			
		}	
		if(this.ventana.find('#vistaordenar').length){
			this.ventana.find('#vistaordenar')
			.on('click', '.btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})
			.on('click', '.btncancelar',function(ev){ ev.preventDefault(); 
				if(_this.ventana.find('#aquitable').length){
					_this.ventana.find('.vistatablaformulario').hide(0);
					_this.ventana.find('#vistaordenar').hide(0);
					_this.ventana.find('#aquitable').fadeIn(500);
				}else window.location.reload();
			});
		}	
		this.ini_librerias();
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault();
			if(_this.ventana.find('#aquitable').length){
				_this.ventana.find('.vistatablaformulario').hide(0);
				_this.ventana.find('.vistatabla').fadeIn(500);
			}else window.location.reload();
		});
	}

	ini_librerias(){
		$('.colorpicker').colorpicker({format: 'rgba'});
		$('.colorpicker').on('change', function(e) {
            $(this)[0].style.backgroundColor = $(this).val();
        });
	   	$('select.select2').select2();
	}

	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
			data.append('idempresa',$('select#busidempresa').val());
			data.append('estado',$('select#busestado').val());			
			data.append('texto',$('input#bustexto').val()||_this.bustexto);
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Nombre</th>
                	<th>Autor</th>
                	<th>Imagen</th>
                	<th>Color_fondo</th>
                	<th>Estado</th>
                	<th>Idioma</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);		
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idcurso}" idpk="idcurso">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox${v.idcurso}" type="checkbox" value="0">
                      <label for="checkbox${v.idcurso}"></label>
                    </div></div></td>
					<td>${v.nombre}</td>	
					<td>${v.autor}</td>	
					<td>${v.imagen==null?'':'<img src="'+_sysUrlBase_+v.imagen+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<td class="text-center"><div style="background-color:${v.color_fondo}; width:50px; height:20px;"></div></td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.idioma}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idcurso=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idcurso:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto,'ordenar':false});
        }).catch(e => {
        	console.log(e);
        })
	}

	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idcurso').val(rs.idcurso);
			$frm.find('select#idempresa').val(rs.idempresa||_this.ventana.attr('idempresa')||'').trigger('change');
  			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#autor').val(rs.autor||'');
  			let imagedefault='static/media/defecto/nofoto.jpg';
      		let __imagen=(rs.imagen==''||rs.imagen=='null'||rs.imagen==null)?imagedefault:rs.imagen;
      		$frm.find('img#fileimgimagen').attr('src',_sysUrlBase_+(__imagen)).attr('link',__imagen);
  			let containerimg=document.querySelector('.img-container.img-imagen');
	        let opt={
	            container:containerimg,
	            image:containerimg.getElementsByTagName('img').item(0),
	            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,
	            	minContainerHeight: 150, minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt);
        	$frm.find('textarea#descripcion').val(rs.descripcion||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  			$frm.find('select#idioma').val(rs.idioma||'ES').trigger('change');
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }	
	    if(this.idcurso==false) return llenardatos({});	
		var data=new FormData()		
			data.append('idcurso',this.idcurso);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}
}
class BDCursos_categorias{
	ventana=null;
	ventanaid=null;	
	frmid='frmcursos_categorias';
	tb='cursos_categorias';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.templatetabla();
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busidcategoria',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busidcurso',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}})
		;
			   	$('select.select2').select2();
	}
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Orden</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
data.append('idcategoria',$('select#busidcategoria').val());
			data.append('idcurso',$('select#busidcurso').val());
			
		data.append('texto',$('input#bustexto').val()||_this.bustexto);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idcursocategoria}" idpk="idcursocategoria">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idcursocategoria}" type="checkbox" value="0">
                      <label for="checkbox{$v.idcursocategoria}"></label>
                    </div></div></td>
					<td>${v.orden}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault();  _this.vista_editar(this)});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idcursocategoria:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	       

	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idcursocategoria').val(rs.idcursocategoria);
			$frm.find('select#idcategoria').val(rs.idcategoria||'').trigger('change');
  			$frm.find('select#idcurso').val(rs.idcurso||'').trigger('change');
  			$frm.find('input#orden').val(rs.orden||'');
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idcursocategoria',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}

	/************************** Formulario */		
	seleccionar(ev){

	}
	ordenar(ev){

	}
}
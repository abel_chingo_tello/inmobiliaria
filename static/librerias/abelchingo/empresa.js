class BDEmpresa{
	ventana=null;
	ventanaid=null;	
	frmid='frmempresa';
	tb='empresa';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
	   	$('select.select2').select2();
	   	var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busestado',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}});
		$('.input-append.date').datepicker({
				autoclose: true,
				todayHighlight: true
	   	});
	}	
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Nombre</th>
                	<th>Direccion</th>
                	<th>Telefono</th>
                	<th>Correo</th>
                	<th>Logo</th>
                	<th>Ruc</th>
                	<th>Idioma</th>
                	<th>Subdominio</th>
                	<th>Estado</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
			data.append('estado',$('select#busestado').val());
			data.append('texto',$('input#bustexto').val()||_this.bustexto);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idempresa}" idpk="idempresa">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idempresa}" type="checkbox" value="0">
                      <label for="checkbox{$v.idempresa}"></label>
                    </div></div></td>
					<td>${v.nombre}</td>	
					<td>${v.direccion}</td>	
					<td>${v.telefono}</td>	
					<td>${v.correo}</td>	
					<td>${v.logo==null?'':'<img src="'+_sysUrlBase_+v.logo+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<td>${v.ruc}</td>	
					<td>${v.idioma}</td>	
					<td>${v.subdominio}</td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault(); 
				let tr=$(this).closest('tr');
				window.location=_sysUrlBase_+'admin/empresa/configuracion/?idempresa='+tr.attr('id');
			});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idempresa:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	        	   

	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idempresa').val(rs.idempresa);
			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#direccion').val(rs.direccion||'');
  			$frm.find('input#telefono').val(rs.telefono||'');
  			$frm.find('input#correo').val(rs.correo||'');
  			let imagedefault='static/nofoto.jpg';
      		$frm.find('img#fileimglogo').attr('src',_sysUrlBase_+(rs.logo||imagedefault));
  			let containerimg=document.querySelector('.img-container.img-logo');
	        let opt={
	            container:containerimg,
	            image:containerimg.getElementsByTagName('img').item(0),
	            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt);
        	$frm.find('input#ruc').val(rs.ruc||'');
  			$frm.find('select#idioma').val(rs.idioma||'');
  			$frm.find('input#subdominio').val(rs.subdominio||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idempresa',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}
}
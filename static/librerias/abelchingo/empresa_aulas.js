class BDEmpresa_aulas{
	ventana=null;
	ventanaid=null;	
	frmid='frmempresa_aulas';
	tb='empresa_aulas';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.templatetabla();
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busidlocal',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busestado',function(ev){ev.preventDefault(); _this.templatetabla(false);})
					.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}});
	}
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Nombre</th>
                	<th>Estado</th>
                	<th>Tipo</th>
                	<th>Capacidad</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
			data.append('idlocal',$('select#busidlocal').val());
			data.append('idempresa',$('select#busidempresa').val());
			data.append('estado',$('select#busestado').val());
			data.append('texto',$('input#texto').val()||'');
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idaula}" idpk="idaula">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox2" type="checkbox" value="0">
                      <label for="checkbox2"></label>
                    </div></div></td>
					<td>${v.nombre}</td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.tipo}</td>	
					<td>${v.capacidad}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault();  _this.vista_editar(this)});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idaula:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	       

	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idaula').val(rs.idaula);
			$frm.find('select#idlocal').val(rs.idlocal||'');
  			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('select#idempresa').val(rs.idempresa||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  			$frm.find('select#tipo').val(rs.tipo||'');
  			$frm.find('input#capacidad').val(rs.capacidad||'');
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idaula',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}

	vista_ordenar(){
		
	}
	
	/************************** Formulario */		
	seleccionar(ev){

	}
	ordenar(ev){

	}
}
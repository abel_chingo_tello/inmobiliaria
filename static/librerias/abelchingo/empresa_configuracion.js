class BDEmpresa_configuracion{
	ventana=null;
	ventanaid=null;	
	frmid='frmempresa';
	frmsmtp='frmempresasmtp';
	frmconfig='frmempresaconfig';
	tb='empresa';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar({frmid:_this.frmid,tb:'empresa'},true);});
		let $frm1=$('#'+this.frmsmtp);			
		$frm1.on('submit',function(ev){ev.preventDefault(); 
			let idempresa=$frm.find('input#idempresa').val();
			if($frm1.children('input#idempresa').length){
				$frm1.children('input#idempresa').val(idempresa);
			}else $frm1.append('<input type="hidden" name="idempresa" id="idempresa" value="'+idempresa+'">');
			_this.$fun.guardar({frmid:_this.frmsmtp,tb:'empresa_smtp'},true);
		});
		let $frm2=$('#'+this.frmconfig);			
		$frm2.on('submit',function(ev){ev.preventDefault(); 
			let idempresa=$frm.find('input#idempresa').val();
			if($frm2.children('input#idempresa').length){
				$frm2.children('input#idempresa').val(idempresa);
			}else $frm2.append('<input type="hidden" name="idempresa" id="idempresa" value="'+idempresa+'">');
			_this.$fun.guardar({frmid:_this.frmconfig,tb:'empresa_config'},true);
		});

		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});	
		$('.colorpicker').colorpicker({format: 'rgba'});
		$('.colorpicker').on('change', function(e) {
            $(this)[0].style.backgroundColor = $(this).val();
        });
		$('select.select2').select2();	
		let containerimg=document.querySelector('.img-container.img-logo');
        let opt={
            container:containerimg,
            image:containerimg.getElementsByTagName('img').item(0),
            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
    	}
        _this.$fun.crop(opt);

        let containerimg1=document.querySelector('.img-container.img-imagen_fondo_login');
        let opt1={
            container:containerimg1,
            image:containerimg1.getElementsByTagName('img').item(0),
            imagedefault:containerimg1.getElementsByTagName('img').item(0).getAttribute('src'),
            options:{aspectRatio: 16/9,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
    	}
        _this.$fun.crop(opt1); 
	}	
}
class BDEmpresa_modulo{
	ventana=null;
	ventanaid=null;	
	frmid='frmempresa_modulo';
	tb='empresa_modulo';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.templatetabla();
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busidmodulo',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('change','select#busestado',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('change','select#busidrol',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('change','select#busidmoduloapdre',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}});
		
	   	$('select.select2').select2();
	}
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Estado</th>
                	<th>Orden</th>
                	<th>Nomtemporal</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
			data.append('idmodulo',$('select#busidmodulo').val());
			data.append('idempresa',$('select#busidempresa').val());
			data.append('estado',$('select#busestado').val());
			data.append('idrol',$('select#busidrol').val());
			data.append('idmoduloapdre',$('select#busidmoduloapdre').val());
			data.append('texto',$('input#bustexto').val()||_this.bustexto);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idempresamodulo}" idpk="idempresamodulo">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idempresamodulo}" type="checkbox" value="0">
                      <label for="checkbox{$v.idempresamodulo}"></label>
                    </div></div></td>
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.orden}</td>	
					<td>${v.nomtemporal}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault();  _this.vista_editar(this)});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idempresamodulo:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	       

	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idempresamodulo').val(rs.idempresamodulo);
			$frm.find('select#idmodulo').val(rs.idmodulo||'');
  			$frm.find('select#idempresa').val(rs.idempresa||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  			$frm.find('select#idrol').val(rs.idrol||'');
  			$frm.find('input#orden').val(rs.orden||'');
  			$frm.find('select#idmoduloapdre').val(rs.idmoduloapdre||'');
  			$frm.find('input#nomtemporal').val(rs.nomtemporal||'');
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idempresamodulo',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}

	vista_ordenar(){
		
	}
	
	/************************** Formulario */		
	seleccionar(ev){

	}
	ordenar(ev){

	}
}
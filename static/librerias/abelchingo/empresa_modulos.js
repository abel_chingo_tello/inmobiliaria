class BDEmpresa_modulos{
	ventana=null;
	ventanaid=null;
	idempresa=null;
	idrol=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		let _this=this;
		$('select.select2').select2();
		$('#aquiformulario').hide();
		this.ventana.on('change','select#busidempresa',function(ev){ev.preventDefault();_this.cambiarempresa(this)})
		.on('change','select#busidrol',function(ev){ev.preventDefault(); _this.vista_ordenar(this)});
		this.cambiarempresa('');
		$('select.select2icon').select2({templateResult:function(state){
			if (!state.id) { return state.text; }
			var $state = $('<span><i class="fa '+ state.element.value + '"/></i> ' + state.text.replace(/fa-/,'') + '</span>');
  			$state.find("span").text('<span><i class="fa '+ state.text + '"/></i></span>');
  			return $state;
		}});
		this.ventana.on('submit','#frmmodulo',function(ev){
			ev.preventDefault();
			let idmodulo=$('#frmmodulo').find('select#idmodulo').val()||'';
			if(idmodulo==''){				
				let dt={
					nombre:$('#frmmodulo').find('input#nomtemporal').val()||'sin nombre',
					icono:$('#frmmodulo').find('select#icono').val()||'fa-folder-open',
				}
				dt=btoa(JSON.stringify(dt));
				$('#frmmodulo').find('input#nomtemporal').val(dt);
			}else $('#frmmodulo').find('input#nomtemporal').val('');
			_this.$fun.guardar({frmid:'frmmodulo',tb:'empresa_modulo'},true).then(rs=>{
				console.log(rs);
			});
			$('#aquiformulario').hide();
			$('#aquiordenar').show();
		}).on('click','.btncancelar',function(ev){
			ev.preventDefault();
			$('#aquiformulario').hide();
			$('#aquiordenar').show();
		}).on('change','select#idmodulo',function(ev){
			ev.preventDefault();
			_this.cambiarmodulo();
		}).on('click','.btnAgregar',function(ev){
			ev.preventDefault();
			let $frm=$('form#frmmodulo');
			$frm.children('input#idempresa').val($('select#busidempresa').val());
			$frm.children('input#idrol').val($('select#busidrol').val());
			_this.cambiarmodulo();
			$('#aquiformulario').show();
			$('#aquiordenar').hide();
		}).on('click', '#aquiordenar .btnguardar',function(ev){
			ev.preventDefault();
			_this.guardarorden();
		}).on('click', '#aquiordenar .btneditar',function(ev){
			ev.preventDefault();
			let li_=$(this).closest('li');
			let $frm=$('form#frmmodulo');
			$frm.children('input#idempresa').val($('select#busidempresa').val());
			$frm.children('input#idrol').val($('select#busidrol').val());
			$frm.children('input#idempresamodulo').val(li_.attr('id'));
			let idmodulo=li_.attr('idmodulo')||'';
			if(idmodulo=='null') idmodulo='';
			$frm.find('select#idmodulo').val(idmodulo).trigger('change');
			idempresamodulo
			let nomtemporal=li_.attr('nomtemporal')||'';
			if(nomtemporal==''||nomtemporal=='null'||nomtemporal==null)nomtemporal={nombre:'sin nombre',icono:'fa-folder-open'};
			else{ nomtemporal=JSON.parse(atob(nomtemporal));}
			$frm.find('input#nomtemporal').val((idmodulo==''?nomtemporal.nombre:''));
			$frm.find('select#icono').val(idmodulo==''?nomtemporal.icono:li_.attr('icono')).trigger('change');

			_this.cambiarmodulo();
			$('#aquiformulario').show();
			$('#aquiordenar').hide();
		}).on('click', '#aquiordenar .btneliminar',function(ev){
			ev.preventDefault();
			let li_=$(this).closest('li');
			let tienehijos=li_.find('li').length;
			if(tienehijos>0){
				_this.$fun.swalError({text:'Este Menu tiene submenus, elimine los submenus primero',footer:''});
				return 0;
			}
			_this.$fun.eliminar({
				url:'empresa_modulo',
				remove:true,
				ele:li_,
				data:{'idempresamodulo':li_.attr('id')}
			})
		})
	}
	cambiarempresa(el){
		this.idempresa=$('select#busidempresa').val()||'';
		let _this=this;
		var data=new FormData()		
			data.append('idempresa',this.idempresa);
			this.$fun.postData({url:_sysUrlBase_+'json/rol/','data':data}).then(rs =>{
				$('#busidrol').html('');
				$.each(rs,function(i,v){
					$("#busidrol").append($('<option>', {value: v.idrol, text:v.nombre}));				
				})				
				$('#busidrol').trigger('change');
	        }).catch(e => {console.log(e); });
	}
	vista_ordenar(el){
		let _this=this;
		this.idrol=$('#busidrol').val();		
		let htmlorder='';
		var data=new FormData()
			data.append('idempresa',this.idempresa);
			data.append('idrol',this.idrol);
			let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let iscontainer=(v.idmodulo==''||v.idmodulo==null)?true:false;
					let dt=(v.nomtemporal==null||v.nomtemporal=='')?{nombre:'sinnombre',icono:'fa-folder-open'}:JSON.parse(atob(v.nomtemporal));
					let strmodulo=(iscontainer==false?v.strmodulo:dt.nombre);
					let icono=(iscontainer==false)?v.icono:dt.icono;
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					htmlorder+=`
					<li  class="dd-item ${(iscontainer==true?'escontenedor':'')}" icono="${v.icono}" nomtemporal="${v.nomtemporal}"  id="${v.idempresamodulo}" idmodulo="${v.idmodulo||''}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono"><i class="fa ${icono}"></i> </span> 
									<span class="title">${(strmodulo||'sinnombre')}</span>
								</span>
								<span class="btnacciones" >
									<i class="fa fa-pencil btneditar"></i> 
									<i class="fa fa-trash btneliminar"></i>
								</span>
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/empresa_modulo/','data':data}).then(rs =>{
				let haymodulos=false;
				htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar </button>
                	</div>
                </div>
                `;
				if(htmlorder==false){
					htmlorder='<h1>Opss ..! </h1><h3>Lo sentimos no se encontraron modulos asignados</h3>';
				}
				$('#aquiordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){ 
					},
			        beforeDragStop: function(l,e, p){
			        	let li=p.parent('li');		        	
			            if((li.length>0 && li.hasClass('escontenedor'))||li.length==0){
			                return true;
			            }
			            else {
			                Swal.fire({
			                	type:'warning',
			                    icon: 'warning',
			                    title: 'Oops...',
			                    text: 'No se puede mover En este lugar, no es un contenedor',			                    
			                    }) 
			                return false;
			            }
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);
	            let escontenedor=_li.hasClass('escontenedor')?true:false;
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'idempresamodulo':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'escontendor':escontenedor,
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/empresa_modulo/guardarorden','data':formData}).then(rs => {
        			window.location.reload();
        			resolve();
        		}) 
        })
	}	
	cambiarmodulo(){
		let idmodulo=this.ventana.find('select#idmodulo').val()||'';
		if(idmodulo!=''){
			this.ventana.find('.sinmodulo').hide();
		}else{
			this.ventana.find('.sinmodulo').show();
		}
	}
}
class BDHistorial_bitacora_alumno_cursos{
	ventana=null;
	ventanaid=null;	
	frmid='frmhistorial_bitacora_alumno_cursos';
	tb='historial_bitacora_alumno_cursos';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.templatetabla();
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busidgrupoaula',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busidpersona',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busidrol',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busmenu_idtemapadre',function(ev){ev.preventDefault(); _this.templatetabla(false);})
			.on('change','select#busmenu_idtema',function(ev){ev.preventDefault(); _this.templatetabla(false);})
					.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}});
	}
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>
                    <th></th>
                    <th>Menus_total</th>
                	<th>Progreso</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
			data.append('idgrupoaula',$('select#busidgrupoaula').val());
			data.append('idpersona',$('select#busidpersona').val());
			data.append('idrol',$('select#busidrol').val());
			data.append('idempresa',$('select#busidempresa').val());
			data.append('menu_idtemapadre',$('select#busmenu_idtemapadre').val());
			data.append('menu_idtema',$('select#busmenu_idtema').val());
			data.append('texto',$('input#texto').val()||'');
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idbitacora}" idpk="idbitacora">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox2" type="checkbox" value="0">
                      <label for="checkbox2"></label>
                    </div></div></td>
					<td>${v.menus_total}</td>	
					<td>${v.progreso}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault();  _this.vista_editar(this)});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idbitacora:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	       

	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idbitacora').val(rs.idbitacora);
			$frm.find('select#idgrupoaula').val(rs.idgrupoaula||'');
  			$frm.find('select#idpersona').val(rs.idpersona||'');
  			$frm.find('select#idrol').val(rs.idrol||'');
  			$frm.find('select#idempresa').val(rs.idempresa||'');
  			$frm.find('input#menus_total').val(rs.menus_total||'');
  			$frm.find('select#menu_idtemapadre').val(rs.menu_idtemapadre||'');
  			$frm.find('select#menu_idtema').val(rs.menu_idtema||'');
  			$frm.find('input#progreso').val(rs.progreso||'');
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idbitacora',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}

	vista_ordenar(){
		
	}
	
	/************************** Formulario */		
	seleccionar(ev){

	}
	ordenar(ev){

	}
}
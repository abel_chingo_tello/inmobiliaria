var IS_BROWSER = typeof window !== 'undefined' && typeof window.document !== 'undefined';
var WINDOW = IS_BROWSER ? window : {};
var IS_TOUCH_DEVICE = IS_BROWSER && WINDOW.document.documentElement ? 'ontouchstart' in WINDOW.document.documentElement : false;
var HAS_POINTER_EVENT = IS_BROWSER ? 'PointerEvent' in WINDOW : false;
var REGEXP_SPACES = /\s\s*/;
function requirejs(file,p={}){
	var _body = document.body;
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = _sysUrlBase_+'static/librerias/abelchingo/'+file;
  if(p.callback){
    script.onreadystatechange = p.callback;
    script.onload = p.callback;
  }
  try{ /*console.log(script);*/ _body.appendChild(script);}catch(er){console.log(er);}
}
function requirecss(file,p={}){
  var _head = document.head;
  var link = document.createElement('link');    
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = _sysUrlBase_+'static/librerias/abelchingo/'+file;
  if(p.callback){
      script.onreadystatechange = p.callback;
      script.onload = p.callback;
  }
  try{ _head.appendChild(link); }catch(er){console.log(er);}
}
function _typeof(obj){
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof=function(obj){return typeof obj;};
  }else{
    _typeof = function(obj){
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }
  return _typeof(obj);
}
function isNumber(value){ return typeof value === 'number' && !isNaN(value); }
function isObject(value){ return _typeof(value) === 'object' && value !== null; }
function isFunction(value){ return typeof value === 'function'; }
function toArray(value){ return Array.from ? Array.from(value) : slice.call(value); }
function forEach(data, callback){
  if(data && isFunction(callback)){
    if(Array.isArray(data) || isNumber(data.length) /* array-like */){
        toArray(data).forEach(function(value, key){callback.call(data, value, key, data); });
    }else if(isObject(data)){
      Object.keys(data).forEach(function(key){callback.call(data, data[key], key, data); });
    }
  }
  return data;
}
function selector(_selector,donde){
	var donde=donde||false;
	if(donde==false) return document.querySelectorAll(_selector);
	let en=null;	
	donde.forEach(function(ele,i){ // falta cuando son de varios lugares
		en = donde[i].querySelectorAll(_selector);
	})
	return en;
}
var assign = Object.assign || function assign(target){	
	for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
	  args[_key - 1] = arguments[_key];
	}
	if (isObject(target) && args.length > 0) {
	  args.forEach(function (arg) {
	    if (isObject(arg)) {
	      Object.keys(arg).forEach(function (key) {
	        target[key] = arg[key];
	      });
	    }
	  });
	}
	return target; 
};
	//Manejo de eventos
var onceSupported = function () {
  var supported = false;
  if(IS_BROWSER){
    var once = false;
    var listener = function listener() {};
    var options = Object.defineProperty({}, 'once', {
      get: function get() {
        supported = true;
        return once;
      },
      set: function set(value) {
        once = value;
      }
    });
    WINDOW.addEventListener('test', listener, options);
    WINDOW.removeEventListener('test', listener, options);
  }
  return supported;
}();		   
function addListener(element, type, listener){
	if(element==undefined) return;
	if((element.length||undefined)!==undefined){
		element.forEach(function(ele,i){
			addListener(element[i], type, listener);
		})
		return;
	}
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var _handler = listener;
  type.trim().split(this.REGEXP_SPACES).forEach(function(event){
    if (options.once && !onceSupported) {
      var _element$listeners = element.listeners,
          listeners = _element$listeners === void 0 ? {} : _element$listeners;

      _handler = function handler() {
        delete listeners[event][listener];
        element.removeEventListener(event, _handler, options);
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        listener.apply(element, args);
      };

      if (!listeners[event]) {
        listeners[event] = {};
      }

      if (listeners[event][listener]) {
        element.removeEventListener(event, listeners[event][listener], options);
      }

      listeners[event][listener] = _handler;
      element.listeners = listeners;
    }
    try{
      if(element.length==0) return;
      element.addEventListener(event, _handler, options);
    }catch(ex){console.log(ex)}
  });
};	
function removeListener(element, type, listener) {
  var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var handler = listener;
  type.trim().split(REGEXP_SPACES).forEach(function (event) {
    if (!onceSupported) {
      var listeners = element.listeners;
      if (listeners && listeners[event] && listeners[event][listener]) {
        handler = listeners[event][listener];
        delete listeners[event][listener];
        if (Object.keys(listeners[event]).length === 0) {
          delete listeners[event];
        }
        if (Object.keys(listeners).length === 0) {
          delete element.listeners;
        }
      }
    }
    element.removeEventListener(event, handler, options);
  });
};
function dispatchEvent(element, type, data) {
	if(element==undefined) return;
    var event; // Event and CustomEvent on IE9-11 are global objects, not constructors
    if (isFunction(Event) && isFunction(CustomEvent)) {
      event = new CustomEvent(type, {
        detail: data,
        bubbles: true,
        cancelable: true
      });
    } else {
      event = document.createEvent('CustomEvent');
      event.initCustomEvent(type, true, true, data);
    }
   
    if(element.length!=undefined){
    	element.forEach(function(ele,i){
    		dispatchEvent(element[i], type, data)			
		  })
		  return ;	//element=element[0]; // solo ejecuta en un elemento el evento
    }
    return element.dispatchEvent(event);
};
   //Manejo de clases
function hasClass(element, value){return element.classList ? element.classList.contains(value) : element.className.indexOf(value) > -1;}
function addClass(element, value) {
	if(!value) return;
	if(isNumber(element.length)){
	  forEach(element, function(elem){
	    addClass(elem, value);
	  });
	  return;
	}
	let newclases=value.trim().split(REGEXP_SPACES);
	if(newclases.length>1){
		newclases.forEach(function(_clase){
			if (element.classList){ element.classList.add(_clase);}
		})
		return;
	}else if (element.classList){ element.classList.add(value); return;}

	var className = element.className.trim();
	if(!className) {
	  element.className = value;
	}else if(className.indexOf(value) < 0) {
	  element.className = "".concat(className, " ").concat(value);
	}}
function addcss(element,p={}){
  if(p=={}) return;
  if(isNumber(element.length)){
    forEach(element, function(elem){
      addcss(elem, p);
    });
    return;
  }
  try{
    forEach(p, function(v,k){
       element.style[k]=v;
    });    
  }catch(ex){ console.log(ex);}
}
function addAttr(element,p={}){
  if(p=={}) return;
  if(isNumber(element.length)){
    forEach(element, function(elem){
      addAttr(elem, p);
    });
    return;
  }
  try{
    forEach(p, function(v,k){
       element[k]=v;
    });    
  }catch(ex){ console.log(ex);}
}
function removeClass(element, value) {
	if (!value) { return;}
	if (isNumber(element.length)){
	  forEach(element, function (elem) {
	    removeClass(elem, value);
	  });
	  return;
	}
	let newclases=value.trim().split(REGEXP_SPACES);
	if(newclases.length>1){
		newclases.forEach(function(_clase){
			if (element.classList){ element.remove(_clase);}
		})
		return;
	}else if (element.classList){ element.remove(value); return;}
	if (element.className.indexOf(value) >= 0) {
	  element.className = element.className.replace(value, '');
	}
}
function toggleClass(element, value, added) {
  if(!value) { return; }

  if(isNumber(element.length)){
    forEach(element, function(elem){
      toggleClass(elem, value, added);
    });
    return;
  } // IE10-11 doesn't support the second parameter of `classList.toggle`

  if(added){ addClass(element, value);
  }else{ removeClass(element, value); }
}
function ready(fn){var d=document;(d.readyState=='loading')?d.addEventListener('DOMContentLoaded',fn):fn();}
requirejs('modal.js');
requirejs('biblioteca.js');
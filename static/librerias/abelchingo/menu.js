class BDMenu{
	ventana=null;
	ventanaid=null;	
	frmid='frmmenu';
	tb='menu';
	$fun=null;
	bustexto='';
	idmenu=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idmenu=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
		}

		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});
	}
	ini_librerias(){
		$('select.select2').select2();
	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
               
                <th>Nombre</th>
            	<th>Url</th>
            	<th>Descripción</th>
            	<th>Estado</th>
            	<th>Icono</th>            	
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idmenu}" idpk="idmenu">					
					<td>${v.nombre}</td>
					<td><a href="${sysUrlBase_+v.url}">${v.url}</a></td>	
					<td>${v.descripcion}</td>
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
					
                      <input type="hidden" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>
					<td>${v.icono}</td>							
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idmenu=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idmenu:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idmenu').val(rs.idmenu);
			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#descripcion').val(rs.descripcion||'');
  			$frm.find('input#estado').val(rs.estado||'');
  			$frm.find('input#url').val(rs.url||'');
  			$frm.find('input#icono').val(rs.icono||'');
  			$frm.find('input#fecha_registro').val(rs.fecha_registro||'');
  				    }
	    if(this.idmenu==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idmenu',this.idmenu);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}	
}
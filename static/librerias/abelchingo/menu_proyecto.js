class BDMenu_proyecto{
	ventana=null;
	ventanaid=null;	
	frmid='frmmenu_proyecto';
	tb='menu_proyecto';
	$fun=null;
	bustexto='';
	idmenuproyecto=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idmenuproyecto=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
			.on('click','#aquitable .btnordenartb',function(ev){ ev.preventDefault(); _this.vista_ordenar();})			
     	.on('change','select#busidmenu',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busidproyecto',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busidrol',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#buseliminar',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
				if(this.ventana.find('#vistaordenar').length){
			this.ventana.find('#vistaordenar')
			.on('click', '.btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})
			.on('click', '.btncancelar',function(ev){ ev.preventDefault(); 
				if(_this.ventana.find('#aquitable').length){
					_this.ventana.find('.vistatablaformulario').hide(0);
					_this.ventana.find('#vistaordenar').hide(0);
					_this.ventana.find('#aquitable').fadeIn(500);
				}else window.location.reload();
			});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('idmenu',$('select#busidmenu').val()||'');
				data.append('idproyecto',$('select#busidproyecto').val()||'');
				data.append('idrol',$('select#busidrol').val()||'');
				data.append('eliminar',$('select#buseliminar').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Orden</th>
            	<th>Idpadre</th>
            	<th>Insertar</th>
            	<th>Modificar</th>
            	<th>Eliminar</th>
            	<th>Fecha_registro</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idmenuproyecto}" idpk="idmenuproyecto">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idmenuproyecto}" type="checkbox" value="0">
                      <label for="checkbox{$v.idmenuproyecto}"></label>
                    </div></div></td>
					<td>${v.orden}</td>	
					<td>${v.idpadre}</td>	
					<td>${v.insertar}</td>	
					<td>${v.modificar}</td>	
					<td><div class="row-fluid"><div cp="eliminar" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.eliminar}" ${v.eliminar==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.eliminar==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td>${v.fecha_registro}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idmenuproyecto=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idmenuproyecto:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto,ordenar:true});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idmenuproyecto').val(rs.idmenuproyecto);
			$frm.find('select#idmenu').val(rs.idmenu||'').trigger('change');
  			$frm.find('select#idproyecto').val(rs.idproyecto||'').trigger('change');
  			$frm.find('select#idrol').val(rs.idrol||'').trigger('change');
  			$frm.find('input#orden').val(rs.orden||'');
  			$frm.find('input#idpadre').val(rs.idpadre||'');
  			$frm.find('input#insertar').val(rs.insertar||'');
  			$frm.find('input#modificar').val(rs.modificar||'');
  			let eliminar=rs.eliminar||1;
			$frm.find('input#eliminar').val(eliminar);
			if(eliminar==1){
				$frm.find('input#eliminar').attr('checked','checked');
				$frm.find('input#eliminar').siblings('label').text('Activo');
			}else {
				$frm.find('input#eliminar').removeAttr('checked');
				$frm.find('input#eliminar').siblings('label').text('Inactivo');
			}
  			$frm.find('input#fecha_registro').val(rs.fecha_registro||'');
  				    }
	    if(this.idmenuproyecto==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idmenuproyecto',this.idmenuproyecto);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	vista_ordenar(){
		this.ventana.find('#aquitable').hide();
		this.ventana.find('#vistaordenar').show();
		var _this=this;
		var data=new FormData();
				data.append('enorden',true);
		let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let strmodulo=v.nombre||'sin nombre';
					let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="fa '+(v.icono)+'"></i>');
					let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					htmlorder+=`
					<li  class="dd-item " id="${v.idmenuproyecto}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono">${imagen+icono} </span> 
									<span class="title">${(strmodulo||'sin nombre')}</span>
								</span>
								<span class="btnacciones" >
									<i class="fa fa-pencil btneditar"></i> 
									<i class="fa fa-trash btneliminar"></i>
								</span>
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/menu_proyecto/','data':data}).then(rs =>{				
				let htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar </button>
                		<button class="btn btn-default btncancelar"> <i class="fa fa-refresh"></i> Cancelar </button>
                	</div>
                </div>
                `;				
				$('#vistaordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){},
			        beforeDragStop: function(l,e, p){			        	
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);	           
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'idmenuproyecto':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/menu_proyecto/guardarorden','data':formData}).then(rs => {
        			window.location.reload();
        			resolve();
        		}) 
        })
	}
	
}
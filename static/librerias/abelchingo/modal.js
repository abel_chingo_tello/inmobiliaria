/**Autor Abel chingo Tello */
var nmodal=0;
requirecss('modal.css');
class mymodal extends _acht_{    
    loading = false;
    el_modal=null; 
    conf= {
	        name: 'modal',
	        titulo: '',
            tamanio:'modal-lg',
	       // section: document.body,
	        content: '',
	        pie:'',            
	        onSubmit: () => { },
	        onClose: () => { } 	        
	    }  
	constructor(el,params={}){		
		super(el);
		let param=assign(this.conf,params)
	}
    loadModal() {
    	nmodal++;
        this.conf.name='modal'+'_'+nmodal;
        let html = /*html*/`
        <div class="modal fade" id="${this.conf.name}" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog ${this.conf.tamanio}">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">${this.conf.titulo}</h5>
                <button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="${this.conf.name}-body">
                ${this.conf.content != '' ? this.conf.content : ''}
              </div>
              ${this.conf.pie==false||this.conf.pie==''?'':`
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Understood</button>
              </div> `
                }
            </div>
          </div>
        </div>`;
        document.body.insertAdjacentHTML('beforeend',html);      
        this.el_modal =  selector(`#${this.conf.name}`);
			addListener(this.el_modal,'showmodal',(ev)=>this.showmodal(ev));        
        	dispatchEvent(this.el_modal,'showmodal');
			let btnclose= selector('.closemodal',this.el_modal);
        	addListener(btnclose,'click',(ev)=>this.closemodal(ev));
        
    }
    showmodal(ev){
        $(this.el_modal).modal();
    	//addClass(this.el_modal,'in show');
        //addcss(this.el_modal,{'display':'block'});
        //this.el_modal.style.padding-right: 17px;"
    	//console.log('aaa');
    	//this.el_modal.classList.add('show');
    }
    closemodal(ev){
        let _this=this;
        $('#'+this.conf.name).modal('hide')
        $('#'+this.conf.name).on('hidden.bs.modal', function (e) {
          setTimeout(function(){ $('#'+_this.conf.name).remove();},350);
        })
    	//removeClass(this.el_modal,'in show');
    	//console.log('close');
    }
    getIdModal(){ return '#'+this.conf.name}
   /* addEvents() {
        let $this = this;
        //Evento al cerrar (limpiar form)
        this.md.on('hidden.bs.modal', function (e) {
            $this.loading = false;
            $this.clearForm(e);
        })
        //evento submit
        this.md.find('form').submit((event) => {
            event.preventDefault();
            if (!$this.loading) {
                $this.loading = true;
                let formData = new FormData(this.md.find('form').get()[0]);
                $this.config.onSubmit(formData);
            }
            $this.hide();
        })
        //evento a botones cerrar
        this.md.find('button[name="btn-eliminar"]').on('click', (event) => {
            Mine.swalConfirm({
                fx: this.config.onDelete
            })

        })
        // this.md.find('button[name="close-md"]').on('click', (event) => {
        //     // let button=$(event.currentTarget);
        //     // $this.hide();
        // })
    }
    clearForm(event) {
        let arrInput = this.md.find('input');
        for (let index = 0; index < arrInput.length; index++) {
            arrInput[index].value = '';
            $(arrInput[index]).prop('disabled', false)
        }
        let arrTextarea = this.md.find('textarea');
        for (let index = 0; index < arrTextarea.length; index++) {
            arrTextarea[index].value = '';
            $(arrTextarea[index]).prop('disabled', false)
        }
        let arrButton = this.md.find('button');
        for (let index = 0; index < arrButton.length; index++) {
            arrButton[index].value = '';
            $(arrButton[index]).prop('disabled', false)
        }
        this.md.find('div[name="modo-editar"]').addClass('my-hide');
        this.md.find('div[name="modo-ver"]').addClass('my-hide');
        this.md.find('div[name="modo-crear"]').addClass('my-hide');
    }*/
   /* show(mode = 'crear') {
       // this.md.find(`div[name="modo-${mode}"]`).removeClass('my-hide');
        this.md.modal('show');
    }*/
    hide() {
        this.md.modal('hide');
    }
}
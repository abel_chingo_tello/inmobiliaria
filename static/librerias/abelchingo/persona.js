class BDPersona{
	ventana=null;
	ventanaid=null;	
	frmid='frmpersona';
	tb='persona';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		this.templatetabla();
		var _this=this;	
		let $frm=$('#'+this.frmid);			
		$frm.on('submit',function(ev){ev.preventDefault(); 
			let haytabla=_this.ventana.find('.vistatabla').length>0?true:false;
			_this.$fun.guardar(_this,haytabla).then(rs=>{
				if(haytabla==false) window.location.reload();
			});
		});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			let haytabla=_this.ventana.find('.vistatabla').length>0?true:false;
			if(haytabla==false) window.location.reload();
			else{
				_this.ventana.find('.vistatablaformulario').hide(0);
				_this.ventana.find('.vistatabla').fadeIn(500);
			}
		});
		this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();	_this.vista_editar(false); })
		.on('change','select#busestado',function(ev){ev.preventDefault(); _this.templatetabla(false);})
		.on('keypress','input#bustexto',function(ev){if(ev.which === 13){_this.bustexto=$(this).val(); _this.templatetabla();}});
		$('.input-append.date').datepicker({
				format: "yyyy/mm/dd",
				autoclose: true,
				todayHighlight: true
	   	});
	   	$('select.select2').select2();
	}
	templatetabla(){
		this.ventana.find('#aquitable').html('');
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
                <thead>
                  <tr>                    
                    <th>Persona</th>
                	<th>Cuenta</th>
                	<th>Foto</th>
                	<th>Genero</th>
                	<th>Fecha_nacimiento</th>
                	<th>Estado_civil</th>
                	<th>Telefono</th>
                	<th>Estado</th>
                	<th></th>            
                	</tr>
                </thead>
                <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.ventana.find('.vistatablaformulario').hide(0);
		this.ventana.find('.vistatabla').fadeIn(500);
		this.buscar();
	}
	buscar(){
		var _this=this;
		var data=new FormData();
			data.append('estado',$('select#busestado').val());
			data.append('texto',$('input#bustexto').val()||_this.bustexto);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				let _tipodoc=v.tipo_documento=='P'?'Pasaporte':'DNI';
				let _estadocivil=v.estado_civil=='C'?'Casado':(v.estado_civil=='V'?'Viudo':(v.estado_civil=='D'?'Divorciado':'Soltero'));
				html+=`<tr id="${v.idpersona}" idpk="idpersona">
					<td>${_tipodoc+': '+v.num_documento+'<br>'+v.apellido_1+' '+v.apellido_2+', '+v.nombres}</td>
					<td>User: ${v.usuario+'<br>'+v.email}</td>
					<td>${v.foto==null?'':'<img src="'+_sysUrlBase_+v.foto+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<td>${v.genero=='M'?'Masculino':'Femenino'}</td>	
					<td>${v.fecha_nacimiento}</td>	
					<td>${_estadocivil}</td>
					<td>${v.telefono}</td>
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){ev.preventDefault();  _this.vista_editar(this)});
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idpersona:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{bustexto:_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })		
	}
	vista_editar(el){
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
	    let _this=this;	
	    var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idpersona').val(rs.idpersona);
			$frm.find('select#tipo_documento').val(rs.tipo_documento||'D');
  			$frm.find('input#num_documento').val(rs.num_documento||'');
  			$frm.find('input#apellido_1').val(rs.apellido_1||'');
  			$frm.find('input#apellido_2').val(rs.apellido_2||'');
  			$frm.find('input#nombres').val(rs.nombres||'');
  			$frm.find('input#usuario').val(rs.usuario||'');
  			$frm.find('input#email').val(rs.email||'');
  			$frm.find('input#clave').val(rs.verclave||'');
  			$frm.find('input#verclave').val(rs.verclave||'');
  			$frm.find('input#tocken').val(rs.tocken||'');
  			let imagedefault='static/nofoto.jpg';
  			$frm.find('img#fileimgfoto').attr('src',_sysUrlBase_+(rs.foto||imagedefault));
  			let containerimg=document.querySelector('.img-container.img-foto');
	        let opt={
	            container:containerimg,
	            image:containerimg.getElementsByTagName('img').item(0),
	            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt);
	        var f=new Date();
	        let date=f.getFullYear()+'-'+(f.getMonth() +1)+'-'+f.getDate();
	        $frm.find('input#fecha_nacimiento').val(rs.fecha_nacimiento||date);
        	$frm.find('input#genero').val(rs.genero||'M');
  			$frm.find('input#estado_civil').val(rs.estado_civil||'S');
  			$frm.find('input#tipo_usuario').val(rs.tipo_usuario||'n');
  			$frm.find('input#telefono').val(rs.telefono||'');
  			$frm.find('input#ubigeo').val(rs.ubigeo||'');
  			$frm.find('input#direccion').val(rs.direccion||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  						
			$frm.find('input#idpadre').val(rs.idpadre||'');
	    }
	    if(el==false) return llenardatos({});
		let tr=$(el).closest('tr');		
		var data=new FormData()		
			data.append('idpersona',tr.attr('id'));
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}

	vista_ordenar(){
		
	}
	
	/************************** Formulario */		
	seleccionar(ev){

	}
	ordenar(ev){

	}

	soloeditar(idpersona){
		this.ventana.find('.vistatablaformulario').show(0);
		console.log('asad',idpersona);
	}
}
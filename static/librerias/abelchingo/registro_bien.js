class BDRegistro_bien{
	ventana=null;
	ventanaid=null;	
	frmid='frmregistro_bien';
	tb='registro_bien';
	$fun=null;
	bustexto='';
	rebi_id=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.rebi_id=false;	_this.vista_formulario(); })
			.on('keyup','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						
     	.on('change','select#buscasb_codigo',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busdepe_id_ubica',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busdepe_id',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#bustabl_tipo_interior',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
				$('.input-append.date').datepicker({
				autoclose: true,
				todayHighlight: true
	   	});
	   	   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('casb_codigo',$('select#buscasb_codigo').val()||'');
				data.append('depe_id_ubica',$('select#busdepe_id_ubica').val()||'');
				data.append('depe_id',$('select#busdepe_id').val()||'');
				data.append('tabl_tipo_interior',$('select#bustabl_tipo_interior').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
            	<th>Catálogo</th>
            	<th>Tipo Interior</th>
                <!--<th>Rebi_modalidad</th>
            	<th>Rebi_tipo_bien</th>
            	<th>Rebi_fadquisicion</th>-->
            	<th>Valor Adquisición </th>
            	<!--<th>Rebi_ult_dep_acum</th>-->
            	<th>Valor Actual</th>
            	<!--<th>Tabl_estado</th>
            	<th>Tabl_condicion</th>-->
            	<th>Cod Registros Públicos</th>
            	<th>Área</th>
            	<!--<th>Rebi_fregistro</th>-->
            	<th>Año</th>
            	<!--<th>Rebi_alquiler</th>-->
            	<th>Número Interior</th>
            	<th>Imagen Principal</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.rebi_id}" idpk="rebi_id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.rebi_id}" type="checkbox" value="0">
                      <label for="checkbox{$v.rebi_id}"></label>
                    </div></div></td>	
					<td>${v.casb_descripcion}</td>	
					<td>${v.tabl_descripcion}</td>		
					<!--<td>${v.rebi_modalidad}</td>	
					<td>${v.rebi_tipo_bien}</td>	
					<td>${v.rebi_fadquisicion}</td>-->	
					<td>${v.rebi_vadquisicion}</td>	
					<!--<td>${v.rebi_ult_dep_acum}</td>	-->
					<td>${v.rebi_ult_val_act}</td>	
					<!--<td>${v.tabl_estado}</td>	
					<td>${v.tabl_condicion}</td>	-->
					<td>${v.rebi_reg_publicos}</td>	
					<td>${v.rebi_area}</td>	
					<!--<td>${v.rebi_fregistro}</td>-->	
					<td>${v.rebi_anno}</td>	
					<!--<td>${v.rebi_alquiler}</td>	-->
					<td>${v.rebi_numero_interior}</td>	
					<td><img src="https://localhost/inmobiliaria/static/templates/plantillas/web/assets/img/bienes/${v.rebi_foto_principal}" class="img-fluid"  ></td>
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i><i class="fa fa-image btnimage"></i><i class="fa fa-user btncontactar"></i>  </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.rebi_id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={rebi_id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			$tabla.on('click','.btnimage',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={rebi_id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				location.href= _sysUrlBase_+"admin/galeria_fotos/index/"+data.rebi_id;
			});
			$tabla.on('click','.btncontactar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={rebi_id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				location.href= _sysUrlBase_+"admin/contacto_bien/index/"+data.rebi_id;
			});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#rebi_id').val(rs.rebi_id);
			$frm.find('select#rebi_modalidad').val(rs.rebi_modalidad||'').trigger('change');
  			$frm.find('select#rebi_tipo_bien').val(rs.rebi_tipo_bien||'').trigger('change');
  			$frm.find('select#casb_codigo').val(rs.casb_codigo||'').trigger('change');
  			$frm.find('input#rebi_vadquisicion').val(rs.rebi_vadquisicion||'');
  			$frm.find('input#rebi_ult_dep_acum').val(rs.rebi_ult_dep_acum||'');
  			$frm.find('input#rebi_ult_val_act').val(rs.rebi_ult_val_act||'');
  			$frm.find('select#tabl_estado').val(rs.tabl_estado||'').trigger('change');
  			$frm.find('textarea#rebi_detalles').val(rs.rebi_detalles||'');
  			$frm.find('select#depe_id_ubica').val(rs.depe_id_ubica||'').trigger('change');
  			$frm.find('select#tabl_condicion').val(rs.tabl_condicion||'').trigger('change');
  			$frm.find('input#rebi_reg_publicos').val(rs.rebi_reg_publicos||'');
  			$frm.find('input#rebi_area').val(rs.rebi_area||'');
  			$frm.find('textarea#rebi_observaciones').val(rs.rebi_observaciones||'');
  			$frm.find('select#depe_id').val(rs.depe_id||'').trigger('change');
  			$frm.find('input#rebi_anno').val(rs.rebi_anno||'');
  			$frm.find('input#rebi_alquiler').val(rs.rebi_alquiler||'');
  			$frm.find('select#tabl_tipo_interior').val(rs.tabl_tipo_interior||'').trigger('change');
  			$frm.find('input#rebi_numero_interior').val(rs.rebi_numero_interior||'');
  				    }
	    if(this.rebi_id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('rebi_id',this.rebi_id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
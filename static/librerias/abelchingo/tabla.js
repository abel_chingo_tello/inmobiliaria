class BDTabla{
	ventana=null;
	ventanaid=null;	
	frmid='frmtabla';
	tb='tabla';
	$fun=null;
	bustexto='';
	tabl_id=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.tabl_id=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						
     	.on('change','select#bustabl_codigoauxiliar',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
				$('.input-append.date').datepicker({
				autoclose: true,
				todayHighlight: true
	   	});
	   	   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('tabl_codigoauxiliar',$('select#bustabl_codigoauxiliar').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Tabl_codigo</th>
            	<th>Tabl_descripcion</th>
            	<th>Tabl_fecharegistro</th>
            	<th>Tabl_descripaux</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.tabl_id}" idpk="tabl_id">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.tabl_id}" type="checkbox" value="0">
                      <label for="checkbox{$v.tabl_id}"></label>
                    </div></div></td>
					<td>${v.tabl_codigo}</td>	
					<td>${v.tabl_descripcion}</td>	
					<td>${v.tabl_fecharegistro}</td>	
					<td>${v.tabl_descripaux}</td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.tabl_id=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={tabl_id:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#tabl_id').val(rs.tabl_id);
			$frm.find('input#tabl_codigo').val(rs.tabl_codigo||'');
  			$frm.find('input#tabl_descripcion').val(rs.tabl_descripcion||'');
  			$frm.find('input#tabl_descripaux').val(rs.tabl_descripaux||'');
  			$frm.find('select#tabl_codigoauxiliar').val(rs.tabl_codigoauxiliar||'').trigger('change');
  				    }
	    if(this.tabl_id==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('tabl_id',this.tabl_id);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
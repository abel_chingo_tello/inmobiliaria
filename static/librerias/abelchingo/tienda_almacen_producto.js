class BDTienda_almacen_producto{
	ventana=null;
	ventanaid=null;	
	frmid='frmtienda_almacen_producto';
	tb='tienda_almacen_producto';
	$fun=null;
	bustexto='';
	idtiendaalmacen=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idtiendaalmacen=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
						
     	.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busidalmacen',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busidproducto',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busestado',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('idempresa',$('select#busidempresa').val()||'');
				data.append('idalmacen',$('select#busidalmacen').val()||'');
				data.append('idproducto',$('select#busidproducto').val()||'');
				data.append('estado',$('select#busestado').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Unidad_medida</th>
            	<th>Cantidad_ingreso</th>
            	<th>Stock</th>
            	<th>Precio_venta</th>
            	<th>Precio_oferta</th>
            	<th>Precio_antes</th>
            	<th>Precio_compra</th>
            	<th>Fecha_vencimiento</th>
            	<th>Moneda</th>
            	<th>Color</th>
            	<th>Tamanio</th>
            	<th>Estado</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idtiendaalmacen}" idpk="idtiendaalmacen">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idtiendaalmacen}" type="checkbox" value="0">
                      <label for="checkbox{$v.idtiendaalmacen}"></label>
                    </div></div></td>
					<td>${v.unidad_medida}</td>	
					<td>${v.cantidad_ingreso}</td>	
					<td>${v.stock}</td>	
					<td>${v.precio_venta}</td>	
					<td>${v.precio_oferta}</td>	
					<td>${v.precio_antes}</td>	
					<td>${v.precio_compra}</td>	
					<td>${v.fecha_vencimiento}</td>	
					<td>${v.moneda}</td>	
					<td>${v.color}</td>	
					<td>${v.tamanio}</td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idtiendaalmacen=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idtiendaalmacen:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idtiendaalmacen').val(rs.idtiendaalmacen);
			$frm.find('select#idempresa').val(rs.idempresa||'').trigger('change');
  			$frm.find('select#idalmacen').val(rs.idalmacen||'').trigger('change');
  			$frm.find('select#idproducto').val(rs.idproducto||'').trigger('change');
  			$frm.find('input#unidad_medida').val(rs.unidad_medida||'');
  			$frm.find('input#cantidad_ingreso').val(rs.cantidad_ingreso||'');
  			$frm.find('input#stock').val(rs.stock||'');
  			$frm.find('input#precio_venta').val(rs.precio_venta||'');
  			$frm.find('input#precio_oferta').val(rs.precio_oferta||'');
  			$frm.find('input#precio_antes').val(rs.precio_antes||'');
  			$frm.find('input#precio_compra').val(rs.precio_compra||'');
  			$frm.find('input#fecha_vencimiento').val(rs.fecha_vencimiento||'');
  			$frm.find('select#moneda').val(rs.moneda||'').trigger('change');
  			$frm.find('input#color').val(rs.color||'');
  			$frm.find('input#tamanio').val(rs.tamanio||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  			$frm.find('textarea#observacion').val(rs.observacion||'');
  				    }
	    if(this.idtiendaalmacen==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idtiendaalmacen',this.idtiendaalmacen);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
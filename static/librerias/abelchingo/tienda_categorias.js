class BDTienda_categorias{
	ventana=null;
	ventanaid=null;	
	frmid='frmtienda_categorias';
	tb='tienda_categorias';
	$fun=null;
	bustexto='';
	idcategoria=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idcurso=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
			.on('click','.btnordenartb',function(ev){ ev.preventDefault(); _this.vista_ordenar();})			
			.on('change','select#busidpadre',function(ev){ev.preventDefault(); _this.vista_tabla();})
			.on('change','select#busestado',function(ev){ev.preventDefault(); _this.vista_tabla();})
		}
		if(this.ventana.find('#vistaordenar').length){
			this.ventana.find('#vistaordenar')
			.on('click', '.btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})
			.on('click', '.btncancelar',function(ev){ ev.preventDefault(); 
				if(_this.ventana.find('#aquitable').length){
					_this.ventana.find('.vistatablaformulario').hide(0);
					_this.ventana.find('#vistaordenar').hide(0);
					_this.ventana.find('#aquitable').fadeIn(500);
				}else window.location.reload();
			});
		}


		this.ini_librerias();
		let $frm=$('#'+this.frmid);
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		}).on('blur','input#nombre',function(ev){
			let nombre=$(this).val();
			nombre.replace(' ','_');
			nombre=_this.$fun.normalizetourl(nombre);
			$('input#permalink').val(nombre);
		})

	}

	ini_librerias(){
		$('select.select2').select2();
		$('select.select2icon').select2({templateResult:function(state){
			if (!state.id) { return state.text; }
			var $state = $('<span><i class="fa '+ state.element.value + '"/></i> ' + state.text.replace(/fa-/,'') + '</span>');
  			$state.find("span").text('<span><i class="fa '+ state.text + '"/></i></span>');
  			return $state;
		}});
		$('.colorpicker').colorpicker({format: 'rgba'});
		$('.colorpicker').on('change', function(e) {
            $(this)[0].style.backgroundColor = $(this).val();
        });
	}

	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
			data.append('idpadre',$('select#busidpadre').val());
			data.append('estado',$('select#busestado').val());
			data.append('texto',$('input#bustexto').val()||_this.bustexto);
		let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Nombre</th>
            	<th>Permalink</th>
            	<th>Imagen</th>
            	<th>Icono</th>
            	<th>Color</th>            	
            	<th>Estado</th>
            	<th></th>            
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idcategoria}" idpk="idcategoria">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idcategoria}" type="checkbox" value="0">
                      <label for="checkbox{$v.idcategoria}"></label>
                    </div></div></td>
					<td>${v.nombre.ucfirst()}</td>	
					<td><a href="${_sysUrlBase_+'categoria/'+v.permalink}" target="_blank">${_sysUrlBase_+'categoria/'+v.permalink}</a></td>	
					<td>${v.imagen==null?'':'<img src="'+_sysUrlBase_+v.imagen+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<td>${v.icono==null?'':('<i class="fa '+v.icono+'"></i> ')}</td>	
					<td>${v.color_fondo==null?'':'<span style="background-color:'+v.color_fondo+'">'+v.color_fondo+'</span>'}</td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idcategoria=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idcategoria:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto,'ordenar':true});
        }).catch(e => {
        	console.log(e);
        })

	}

	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idcategoria').val(rs.idcategoria);
			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#permalink').val(rs.permalink||'');
  			$frm.find('textarea#descripcion').val(rs.descripcion||'');
  			let imagedefault='static/media/defecto/nofoto.jpg';
      		let __imagen=(rs.imagen==''||rs.imagen=='null'||rs.imagen==null)?imagedefault:rs.imagen;
      		$frm.find('img#fileimgimagen').attr('src',_sysUrlBase_+(__imagen)).attr('link',__imagen);
  			let containerimg=document.querySelector('.img-container.img-imagen');
	        let opt={
	            container:containerimg,
	            image:containerimg.getElementsByTagName('img').item(0),
	            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        	minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt);
        	$frm.find('select#icono').val(rs.icono||'').trigger('change');
  			$frm.find('input#orden').val(rs.orden||'');
  			$frm.find('input#color_fondo').val(rs.color_fondo||'');
  			$frm.find('select#idpadre').val(rs.idpadre||'').trigger('change');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
	    }
	    if(this.idcategoria==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idcategoria',this.idcategoria);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	vista_ordenar(){
		this.ventana.find('#aquitable').hide();
		this.ventana.find('#vistaordenar').show();
		var _this=this;
		var data=new FormData();
		data.append('idpadre',$('select#busidpadre').val());
			data.append('estado',$('select#busestado').val());
					data.append('enorden',true);
		let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let strmodulo=v.nombre||'sin nombre';
					let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="fa '+(v.icono)+'"></i>');
					let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					htmlorder+=`
					<li  class="dd-item " id="${v.idcategoria}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono">${imagen+icono} </span> 
									<span class="title">${(strmodulo||'sin nombre')}</span>
								</span>								
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias/','data':data}).then(rs =>{				
				let htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar </button>
                		<button class="btn btn-default btncancelar"> <i class="fa fa-refresh"></i> Cancelar </button>
                	</div>
                </div>
                `;				
				$('#vistaordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){},
			        beforeDragStop: function(l,e, p){			        	
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);	           
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'idcategoria':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias/guardarorden','data':formData}).then(rs => {
        			window.location.reload();
        			resolve();
        		}) 
        })
	}
}
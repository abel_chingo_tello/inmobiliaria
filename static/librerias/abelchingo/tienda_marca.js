class BDTienda_marca{
	ventana=null;
	ventanaid=null;	
	frmid='frmtienda_marca';
	tb='tienda_marca';
	$fun=null;
	bustexto='';
	idmarca=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idmarca=false;	_this.vista_formulario(); })
		.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})						
     	.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.vista_tabla();})
     	.on('change','select#busestado',function(ev){ev.preventDefault(); _this.vista_tabla();});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		}).on('blur','input#nombre',function(ev){
			let nombre=$(this).val();
			nombre.replace(' ','_');
			nombre=_this.$fun.normalizetourl(nombre);
			$('input#permalink').val(nombre);
		})		
	}
	ini_librerias(){
		$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('idempresa',$('select#busidempresa').val()||'');
				data.append('estado',$('select#busestado').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Nombre</th>
            	<th>Permalink</th>
            	<th>Estado</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idmarca}" idpk="idmarca">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idmarca}" type="checkbox" value="0">
                      <label for="checkbox{$v.idmarca}"></label>
                    </div></div></td>
					<td>${v.nombre.ucfirst()}</td>	
					<td><a href="${_sysUrlBase_+'marca/'+v.permalink}" target="_blank">${_sysUrlBase_+'marca/'+v.permalink}</a></td>	
					<td><div class="row-fluid"><div cp="estado" class="cambiarestado checkbox check-primary checkbox-circle">
                      <input type="checkbox" value="${v.estado}" ${v.estado==1?'checked="checked"':''} v1="Activo" V2="Inactivo">
                      <label><span>${v.estado==1?'Activo':'Inactivo'}</span></label></div>
                    </div></td>	
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idmarca=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idmarca:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idmarca').val(rs.idmarca);
			$frm.find('select#idempresa').val(rs.idempresa||_this.ventana.attr('idempresa')||'').trigger('change');
  			$frm.find('input#nombre').val(rs.nombre||'');
  			$frm.find('input#permalink').val(rs.permalink||'');
  			let estado=rs.estado||1;
			$frm.find('input#estado').val(estado);
			if(estado==1){
				$frm.find('input#estado').attr('checked','checked');
				$frm.find('input#estado').siblings('label').text('Activo');
			}else {
				$frm.find('input#estado').removeAttr('checked');
				$frm.find('input#estado').siblings('label').text('Inactivo');
			}
  		}
	    if(this.idmarca==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idmarca',this.idmarca);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
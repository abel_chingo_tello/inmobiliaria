class BDTienda_productos{
	ventana=null;
	ventanaid=null;	
	frmid='frmtienda_productos';
	tb='tienda_productos';
	$fun=null;
	bustexto='';
	idproducto=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idproducto=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){ev.preventDefault(); if(ev.which === 13){ _this.vista_tabla();}})
			.on('click','#aquitable .btnordenartb',function(ev){ ev.preventDefault(); _this.vista_ordenar();})			
     		.on('change','select#busidempresa',function(ev){ev.preventDefault(); _this.templatetabla(false);})
     		.on('change','select#busidmarca',function(ev){ev.preventDefault(); _this.templatetabla(false);});
		}
		if(this.ventana.find('#vistaordenar').length){
			this.ventana.find('#vistaordenar')
			.on('click', '.btnguardar',function(ev){ev.preventDefault(); _this.guardarorden();})
			.on('click', '.btncancelar',function(ev){ ev.preventDefault(); 
				if(_this.ventana.find('#aquitable').length){
					_this.ventana.find('.vistatablaformulario').hide(0);
					_this.ventana.find('#vistaordenar').hide(0);
					_this.ventana.find('#aquitable').fadeIn(500);
				}else window.location.reload();
			});
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); 
			_this.$fun.guardar(_this,'return').then(rs=>{
				if(rs.code==200){
					let idproducto=$frm.find('input#idproducto').val();
					$frm.find('input#idproducto').val(rs.newid);	
					$('.sihayproducto').show();	
					if(idproducto==''){
						Swal.fire({
						  title: rs.msj,
						  text: "Desea agregar categorías y stock de este producto",
						  icon: 'success',
						  type:'success',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Si',
						  cancelButtonText: 'No',
						}).then((result) => {
						  if (result.value==true) {
						    $frm.find('[tabid="tabcategorias"]').trigger('click');
						  }else
						   window.location.reload();
						})
					}													
				}
			});
		});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.vista_tabla();
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		}).on('blur','input#nombre',function(ev){
			let nombre=$(this).val();
			nombre.replace(' ','_');
			nombre=_this.$fun.normalizetourl(nombre);
			$('input#permalink').val(nombre);
		}).on('blur','input#precio_compra',function(ev){
			let p=parseFloat($(this).val());
			//if($('input#precio_venta').val()=='0'||$('input#precio_venta').val()==''){
				let pv=((p*0.25)+p)-0.01;
				$('input#precio_venta').val(pv);
			//}
			//if($('input#precio_oferta').val()=='0'||$('input#precio_oferta').val()==''){
				 pv=(p*0.1)+p;
				$('input#precio_oferta').val(pv);
			//}
			//ifinput#precio_antes').val()=='0'||$('input#precio_antes').val()==''){
				 pv=(p*0.9)+p;
				$('input#precio_antes').val(pv);
			//}					
		}).on('change','select#unidad_medida',function(ev){
			$('.changemodena').text(_this.$fun._seltipomodena($(this).val()).prefijo);
		}).on('click','ul#tabsshow a.nav-link ',function(ev){
			ev.preventDefault();
			let ul=$(this).closest('ul');
			ul.find('a').removeClass('active');
			let el=$(this).attr('tabid');
			$(this).addClass('active');
			ul.siblings().children().hide();
			ul.siblings().children('.'+el).fadeIn(50);
			if(el=='tabcategorias'){
				_this.mostrarcategorias();
			}else if(el=='tababastecimientos'){
				_this.mostrarabastecimientos();
			}
		}).on('click','.pnlagregarimagen',function(ev){
			ev.preventDefault();
			$(this).html($(this).attr('text'));
			$(this).next().show();
			$(this).closest('.pnlimagen').next().show();
		}).on('click','.activarcampo',function(ev){
			let input=$(this).children('input');
	    	if(input.val()==1){
	    		input.val(0);
	    		input.removeAttr('checked');
	    		$(this).children('label').text(input.attr('v2'));
	    		$(this).closest('div.form-group').children('.form-control').hide();
	    	}else{
	    		input.val(1);
	    		input.attr('checked',true);
	    		$(this).children('label').text(input.attr('v1'));
	    		$(this).closest('div.form-group').children('.form-control').show();
	    	}
		}).on('iniciar','.activarcampo',function(ev){
			let input=$(this).children('input');
			if(input.attr('checked')){
				$(this).closest('div.form-group').children('.form-control').show();
			}else{
				$(this).closest('div.form-group').children('.form-control').hide();
			}
		}).on('click','.btnagregarmarca',function(ev){						
			let oModal=new mymodal('',{titulo:'Marca'});
            oModal.loadModal();
            let idfrm="frmmarca"+_this.$fun._idgui();
            let html=`
            <form id="${idfrm}">
            	<div class="col-md-12 col-sm-12 col-xs-12 col-12">
                  <div class="form-group">
                    <label class="form-label">Nombre <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nombre" id="nombre" class="form-control" required placeholder="">
                      <span class="error"><?php echo JrTexto::_('Este campo es : Obligatorio , requerido y solo texto de A-Z ó 1-9') ?></span>
                    </div>
                  </div>
                </div>          
	            <div class="col-md-12 text-center">
	            	<button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
	            </div>
	        </form>
            `
            $(oModal.getIdModal()+'-body').html(html);
            $(oModal.getIdModal()+'-body').on('submit','#'+idfrm,function(ev){
            	ev.preventDefault();
            	let _frmtmp=document.getElementById(idfrm);            	
            	let nombre=$(_frmtmp).find('input#nombre').val()
	    		var data=new FormData(_frmtmp);
	    		let permalink=_this.$fun.normalizetourl(nombre);
	    		data.append('permalink',permalink)
			   	_this.$fun.postData({url:_sysUrlBase_+'json/tienda_marca/guardar','data':data,'headers':{'Content-Type': 'multipart/form-data'}}).then(rs => { 
			   		if(rs.code==200){			   			
		   			 	$frm.find("select#idmarca")
					       .append($('<option>',{
					       	value:rs.newid,
					       	text:nombre
					       }))
					    $frm.find("select#idmarca").val(rs.newid).trigger('change');
					     oModal.closemodal();
			   		}            	
        		}) 
            })
		}).on('click','.btnagregarcategoria',function(ev){
		    let idnivel=$(this).siblings('span.help').text();		    			
			let oModal=new mymodal('',{titulo:'Categoria nivel '+idnivel});
            oModal.loadModal();
            let idfrm="frmcategoria"+_this.$fun._idgui();
            let html=`
            <form id="${idfrm}">
            	<div class="col-md-12 col-sm-12 col-xs-12 col-12">
                  <div class="form-group">
                    <label class="form-label">Nombre <span class="help"> </span> </label>
                    <div class="input-with-icon  right " claseerror="success-control" claseok="error-control">
                      <i class="error fa-exclamation fa"></i>
                      <i class="success fa fa-check"></i>
                      <input type="text" name="nombre" id="nombre" class="form-control" required placeholder="">                     
                    </div>
                  </div>
                </div>          
	            <div class="col-md-12 text-center">
	            	<button type="submit" class="btn btn-success btn-cons"><i class="fa fa-save"></i> Guardar</button>
	            </div>
	        </form>
            `
            $(oModal.getIdModal()+'-body').html(html);
            $(oModal.getIdModal()+'-body').on('submit','#'+idfrm,function(ev){
            	ev.preventDefault();
            	let _frmtmp=document.getElementById(idfrm);
            	let nombre=$(_frmtmp).find('input#nombre').val()
	    		var data=new FormData(_frmtmp);
	    		let permalink=_this.$fun.normalizetourl(nombre);
	    		data.append('permalink',permalink);
	    		if(parseInt(idnivel)>1){
	    			data.append('idpadre',$frm.find('select#idcategoria'+parseInt(idnivel-1)).val());
	    		}
			   	_this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias/guardar','data':data}).then(rs => { 
			   		if(rs.code==200){			   			
		   			 	$frm.find('select#idcategoria'+idnivel)
					       .append($('<option>',{
					       	value:rs.newid,
					       	text:nombre
					       }))
					    $frm.find('select#idcategoria'+idnivel).val(rs.newid).trigger('change');
					    oModal.closemodal();
			   		}
        		}) 
            })
		}).on('change','select.selectcategoria',function(ev){
			//ev.preventDefault();
			let div=$(this).closest('.form-group').parent();
			let nivel=parseInt($(this).attr('nivel'));
			let valor=$(this).val()||'';
			if(nivel<3 && valor!=''){
				var data=new FormData();	    		
	    		data.append('idpadre',$(this).val());
				_this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias/','data':data}).then(rs => { 
					$frm.find('select#idcategoria'+(nivel+1)).empty();
					let hay=false;
					$.each(rs,function(i,v){
						hay=true;
						$frm.find('select#idcategoria'+(nivel+1))
					       .append($('<option>',{
					       	value:v.idcategoria,
					       	text:v.nombre
					    }));
					})
					$frm.find('select#idcategoria'+(nivel+1)).trigger('change');
					if(hay==false){
						$frm.find('select#idcategoria'+(nivel+1))
					       .append($('<option>',{
					       	value:'',
					       	text:'Ninguno'
					    }));
					}
        		}) 
				div.next().show();
			}else {
				div.nextAll().hide();
				div.nextAll().last().show();
			}
		}).on('click','.btnaddcategoriaaproducto',function(ev){
			ev.preventDefault();
			let idproducto=$frm.find('input#idproducto').val()||'';
			let idcat=$frm.find('select#idcategoria3').val()||'';
			let nomcat=$frm.find('select#idcategoria3').text()||'';
			if(idcat==''){
				idcat=$frm.find('select#idcategoria2').val()||'';
				nomcat=$frm.find('select#idcategoria2').text()||'';
			}
			if(idcat==''){
				idcat=$frm.find('select#idcategoria1').val()||'';
				nomcat=$frm.find('select#idcategoria1').text()||'';
			}
			if(idcat=='') {
				 Swal.fire({
		        	type: 'error',//warning (!)//success
		            icon: 'error',//warning (!)//success
		            title: 'Oops...',
		            text: 'No hay una categoria valida seleccionada',			            
		            allowOutsideClick: false,
		            closeOnClickOutside: false
		        })
				return;
			}			
			var data=new FormData();	    		
    		data.append('idcategoria',idcat);
    		data.append('idproducto',idproducto);
			_this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias_producto/guardar','data':data}).then(rs => { 
				_this.mostrarcategorias();
    		})
		}).on('click','.btnguardarabastecimiento',function(ev){
			ev.preventDefault();
			var data=new FormData();
			let $frmaba=$('#frmabastecimientos');
			data.append('idalmacen',$frmaba.find('select#idalmacen').val()||0);
			data.append('cantidad_ingreso',$frmaba.find('input#cantidad_ingreso').val()||0);
			data.append('idproducto',$('#'+_this.frmid).find('input#idproducto').val()||'');
			data.append('unidad_medida',$frmaba.find('select#unidad_medida').val()||0);
			data.append('stock',$frmaba.find('input#cantidad_ingreso').val()||0);
			let precio_venta=parseInt($frmaba.find('input#precio_venta').val()||0);
			if(precio_venta<1){
				Swal.fire({
		        	type: 'error',//warning (!)//success
		            icon: 'error',//warning (!)//success
		            title: 'Oops...',
		            text: 'El precio de venta no puede ser 0',
		            //footer: config.footer,
		            //allowOutsideClick: false,
		            //closeOnClickOutside: false
		        })
		        return false;
			}
			data.append('precio_venta',precio_venta);
			data.append('precio_oferta',$frmaba.find('input#precio_oferta').val()||0);
			data.append('precio_antes',$frmaba.find('input#precio_antes').val()||0);
			data.append('precio_compra',$frmaba.find('input#precio_compra').val()||0);
			data.append('moneda',$frmaba.find('select#moneda').val()||0);
			data.append('color',$frmaba.find('input#color').val()||0);
			data.append('tamanio',$frmaba.find('input#tamanio').val()||0);
			if($frmaba.find('input#activarfechavencimiento').val()==1)
				data.append('fecha_vencimiento',$frmaba.find('input#').val()||0);
			else 
				data.append('fecha_vencimiento','null');
    		_this.$fun.postData({url:_sysUrlBase_+'json/tienda_almacen_producto/guardar','data':data}).then(rs => { 
		   		if(rs.code==200){			   			
	   			 	_this.mostrarabastecimientos();
		   		}
    		})
		})
	}
	ini_librerias(){
		$('select.select2').select2();
	    tinymce.init({
          selector: '#descripcion',
          plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          toolbar_mode: 'floating',
        });      
        tinymce.init({
          selector: '#caracteristicas',
          plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          toolbar_mode: 'floating',
        });
	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				data.append('idempresa',$('select#busidempresa').val()||'');
				data.append('idmarca',$('select#busidmarca').val()||'');
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Nombre</th>
            	<th>Permalink</th>            	
            	<th>Imagen1</th>            	
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idproducto}" idpk="idproducto">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idproducto}" type="checkbox" value="0">
                      <label for="checkbox{$v.idproducto}"></label>
                    </div></div></td>
					<td>${v.nombre.ucfirst()}</td>	
					<td><a href="${_sysUrlBase_+'producto/'+v.permalink}" target="_blank">${_sysUrlBase_+'producto/'+v.permalink}</a></td>						
					<td>${v.imagen1==null?'':'<img src="'+_sysUrlBase_+v.imagen1+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>					
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idproducto=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idproducto:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto,ordenar:true});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		$('a[tabid="tabdescripcion"]').trigger('click');
		var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	let frmidproducto=rs.idproducto||'';
	    	$frm.find('input#idproducto').val(frmidproducto);
	    	if(frmidproducto!=''){	    		
	    		$('.sihayproducto').show();
	    	}else $('.sihayproducto').hide();
			$frm.find('select#idempresa').val(rs.idempresa||_this.ventana.attr('idempresa')||'').trigger('change');
			if(rs.idmarca==null||rs.idmarca==''||rs.idmarca==undefined) rs.idmarca=$frm.find('select#idmarca').children('option').first().attr('value');
  			$frm.find('select#idmarca').val(rs.idmarca).trigger('change');

  			$frm.find('input#nombre').val((rs.nombre||''));
  			//$frm.find('input#nombre').focus()
  			$frm.find('input#permalink').val(rs.permalink||'');  		
  			$frm.find('textarea#descripcion').val(rs.descripcion||'');  		
  			tinymce.EditorManager.get('descripcion').execCommand('mceSetContent', false, rs.descripcion||'');  			
  			$frm.find('textarea#caracteristicas').val(rs.caracteristicas||'');
  			tinymce.EditorManager.get('caracteristicas').execCommand('mceSetContent', false, rs.caracteristicas||'');
  			$frm.find('input#orden').val(rs.orden||'');
  			let imagen1default='static/media/defecto/nofoto.jpg';
      		let __imagen1=(rs.imagen1==''||rs.imagen1=='null'||rs.imagen1==null)?imagen1default:rs.imagen1;      		
      		$frm.find('img#fileimgimagen1').attr('src',_sysUrlBase_+(__imagen1)).attr('link',__imagen1);
  			let containerimagen1=document.querySelector('.img-container.img-imagen1');
	        let optimagen1={
	            container:containerimagen1,
	            image:containerimagen1.getElementsByTagName('img').item(0),
	            imagedefault:containerimagen1.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:true,cropBoxResizable:true, autoCropArea: 1,center: true,restore: false,zoomOnWheel: true,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,scalable:true}
	    	}
	        _this.$fun.crop(optimagen1);
        	let imagen2default='static/media/defecto/nofoto.jpg';
      		let __imagen2=(rs.imagen2==''||rs.imagen2=='null'||rs.imagen2==null)?imagen2default:rs.imagen2;
      		$frm.find('img#fileimgimagen2').attr('src',_sysUrlBase_+(__imagen2)).attr('link',__imagen2);
  			let containerimagen2=document.querySelector('.img-container.img-imagen2');
	        let optimagen2={
	            container:containerimagen2,
	            image:containerimagen2.getElementsByTagName('img').item(0),
	            imagedefault:containerimagen2.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:true,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(optimagen2);
        	let imagen3default='static/media/defecto/nofoto.jpg';
      		let __imagen3=(rs.imagen3==''||rs.imagen3=='null'||rs.imagen3==null)?imagen3default:rs.imagen3;
      		$frm.find('img#fileimgimagen3').attr('src',_sysUrlBase_+(__imagen3)).attr('link',__imagen3);
  			let containerimagen3=document.querySelector('.img-container.img-imagen3');
	        let optimagen3={
	            container:containerimagen3,
	            image:containerimagen3.getElementsByTagName('img').item(0),
	            imagedefault:containerimagen3.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(optimagen3);
        	let imagen4default='static/media/defecto/nofoto.jpg';
      		let __imagen4=(rs.imagen4==''||rs.imagen4=='null'||rs.imagen4==null)?imagen4default:rs.imagen4;
      		$frm.find('img#fileimgimagen4').attr('src',_sysUrlBase_+(__imagen4)).attr('link',__imagen4);
  			let containerimagen4=document.querySelector('.img-container.img-imagen4');
	        let optimagen4={
	            container:containerimagen4,
	            image:containerimagen4.getElementsByTagName('img').item(0),
	            imagedefault:containerimagen4.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(optimagen4);
        	let imagen5default='static/media/defecto/nofoto.jpg';
      		let __imagen5=(rs.imagen5==''||rs.imagen5=='null'||rs.imagen5==null)?imagen5default:rs.imagen5;
      		$frm.find('img#fileimgimagen5').attr('src',_sysUrlBase_+(__imagen5)).attr('link',__imagen5);
  			let containerimagen5=document.querySelector('.img-container.img-imagen5');
	        let optimagen5={
	            container:containerimagen5,
	            image:containerimagen5.getElementsByTagName('img').item(0),
	            imagedefault:containerimagen5.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(optimagen5);
	        $('.pnlimagen').first().show();
	        $('.pnlimagen').nextAll().hide();
	        $('.pnlimagen').nextAll().find('.pnlagregarimagen').siblings('div.row').hide();
	        $('.activarcampo').trigger('iniciar');	        
       }
	    if(this.idproducto==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idproducto',this.idproducto);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });
	}
	vista_ordenar(){
		this.ventana.find('#aquitable').hide();
		this.ventana.find('#vistaordenar').show();
		var _this=this;
		var data=new FormData();
				data.append('enorden',true);
		let htmlhijos_=function(rs){
				let htmlorder=``;
				$.each(rs,function(i,v){
					let strmodulo=v.nombre||'sin nombre';
					let icono=(v.icono==undefined||v.icono==null||v.icono=='')?'':('<i class="fa '+(v.icono)+'"></i>');
					let imagen=(v.imagen==undefined||v.imagen==null||v.imagen=='')?'':('<img src="'+_sysUrlBase_+v.imagen+'" width="25px" height="25px" >');
					let htmlhijo=(v.hijos==undefined||v.hijos==null)?'':('<ol class="dd-list">'+htmlhijos_(v.hijos)+'</ol>');
					htmlorder+=`
					<li  class="dd-item " id="${v.idproducto}" >
						<div class="dd-li">
							<div class="dd-border">						
								<span class="dd-handle">
									<span class="icono">${imagen+icono} </span> 
									<span class="title">${(strmodulo||'sin nombre')}</span>
								</span>
								<span class="btnacciones" >
									<i class="fa fa-pencil btneditar"></i> 
									<i class="fa fa-trash btneliminar"></i>
								</span>
							</div>
							<div>${htmlhijo}</div>
						</div>					
					</li>`;
				})
				return htmlorder;
			}

			this.$fun.postData({url:_sysUrlBase_+'json/tienda_productos/','data':data}).then(rs =>{				
				let htmlorder=`
				<div class="row">
					<div class="accordion col-md-12" id="accordion1" role="tablist" aria-multiselectable="true">     
                        <div class="dd nestable">
                        	<ol class="dd-list">${htmlhijos_(rs)}</ol>
                        </div>
                    </div>
                </div>                
                <div class="row"><div class="col-md-12 col-sm-12 col-12"><hr></div></div>
                <div class="row">
                	<div class="col-md-12 col-sm-12 col-12 text-center">
                		<button class="btn btn-primary btnguardar"> <i class="fa fa-save"></i> Guardar </button>
                		<button class="btn btn-default btncancelar"> <i class="fa fa-refresh"></i> Cancelar </button>
                	</div>
                </div>
                `;				
				$('#vistaordenar').html(htmlorder);
				$('.dd').nestable({
			        onDragStart: function (l, e){},
			        beforeDragStop: function(l,e, p){			        	
			        }
			    })
	        }).catch(e => {console.log(e); });
	}
	guardarorden(){
		var datos=[];
		let _this=this;
		return new Promise((resolve, reject) => {
	        $('.dd').children('ol').find('li').each(function(i,li){        	
	            let _li=$(li);	           
	            let _lipadre=_li.closest('ol').closest('li');
	            let idpadre=_lipadre.length==0?'':_lipadre.attr('id');
	            datos.push({	            	
	            	'idproducto':_li.attr('id'),
	            	'orden':(_li.index()+1),
	            	'idpadre':idpadre
	            });
	        })
        	var formData = new FormData();        
        		formData.append('datos', JSON.stringify(datos));
        		_this.$fun.postData({url:_sysUrlBase_+'json/tienda_productos/guardarorden','data':formData}).then(rs => {
        			window.location.reload();
        			resolve();
        		}) 
        })
	}
	mostrarcategorias(){
		if(!$('select#idcategoria1').hasClass('yacargo'))
			$('select#idcategoria1').addClass('yacargo').trigger('change');
		let _this=this;
		let idproducto_=$('#'+this.frmid).find('input#idproducto').val();
		var formData = new FormData();        
		formData.append('idproducto', idproducto_);
		let tbid='tbcat'+_this.$fun._idgui();		
		let html =`<table class="table table-hover table-condensed" id="table${tbid}">
            <thead>
              	<tr>               
                	<th>Categoria Asignada</th>
            		<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		$('#aquitablaventanacategorias').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/tienda_categorias_producto/','data':formData}).then(rs => {		
			html=''	;
			forEach(rs,function(v,i){				
				html+=`<tr id="${v.idcategoriaproducto}" idpk="idcategoriaproducto">					
					<td>${v.strcategoria}</td>
					<td><i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})
			let $tb=$('#aquitablaventanacategorias').find('#table'+tbid+ ' tbody');
			$tb.html(html);
			$tb.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idcategoriaproducto:tr.attr('id')}
				let datos={url:'tienda_categorias_producto',data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			})
			_this.$fun.dtTable('#table'+tbid,{ordenar:false});
		})
	}
	mostrarabastecimientos(){
		let _this=this;
		let idproducto_=$('#'+this.frmid).find('input#idproducto').val();
		var formData = new FormData();        
		formData.append('idproducto', idproducto_);
		let tbid='tbabas'+_this.$fun._idgui();		
		let html =`<table class="table table-hover table-condensed" id="table${tbid}">
            <thead>
              <tr>               
                <th>Almacen</th>
            	<th>Cantidad</th>            	
            	<th>Precio compra</th>
            	<th>Precio Venta</th>
            	<th>Precio Oferta</th>            	
            	<th>Color y Tamaño</th>
            	<th>Fecha Vencimiento</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		$('#aquitablaventanaAbastecimiento').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/tienda_almacen_producto/','data':formData}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				let compra=(v.precio_compra==null||v.precio_compra=='')?0:v.precio_compra;
				let venta=(v.precio_venta==null||v.precio_venta=='')?0:v.precio_venta;
				let oferta=(v.precio_oferta==null||v.precio_oferta=='')?0:v.precio_oferta;				
				let color=(v.color||'');
				let fechaven=(v.fecha_vencimiento||'');
				let tamanio=(v.tamanio||'');
				let moneda=_this.$fun._seltipomodena(v.moneda).prefijo;
				html+=`<tr id="${v.idtiendaalmacen}" idpk="idtiendaalmacen">					
					<td>${v.idalmacen+' - '+v.stralmacen}</td>	
					<td>${v.stock+' de '+v.cantidad_ingreso}</td>						
					<td>${moneda+' '+compra}</td>
					<td>${moneda+' '+venta}</td>
					<td>${moneda+' '+oferta}</td>
					<td>${color+' '+tamanio}</td>
					<td>${fechaven}</td>
					<td><i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})
			let $tb=$('#aquitablaventanaAbastecimiento').find('#table'+tbid+ ' tbody');
			$tb.html(html);
			$tb.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idtiendaalmacen:tr.attr('id')}
				let datos={url:'tienda_almacen_producto',data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			})
			_this.$fun.dtTable('#table'+tbid,{ordenar:false});
		})
	}	
}
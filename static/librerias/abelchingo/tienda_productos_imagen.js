class BDTienda_productos_imagen{
	ventana=null;
	ventanaid=null;	
	frmid='frmtienda_productos_imagen';
	tb='tienda_productos_imagen';
	$fun=null;
	bustexto='';
	idproductoimagen=null;
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			this.ventana.on('click','.btnnuevo',function(ev){ ev.preventDefault();_this.idproductoimagen=false;	_this.vista_formulario(); })
			.on('keypress','input#bustexto',function(ev){if(ev.preventDefault(); ev.which === 13){ _this.vista_tabla();}})
						;
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.frmid);	
		$frm.on('submit',function(ev){ev.preventDefault(); _this.$fun.guardar(_this,true);});
		$frm.on('click','div.cambiarestado',function(ev){ ev.preventDefault(); _this.$fun.cambiarestado($(this))});
		$frm.on('click','.btncancelar',function(ev){ ev.preventDefault(); 
			_this.ventana.find('.vistatablaformulario').hide(0);
			_this.ventana.find('.vistatabla').fadeIn(500);
		});		
	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Idproducto</th>
            	<th>Imagen</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			let html='';
			forEach(rs,function(v,i){
				html+=`<tr id="${v.idproductoimagen}" idpk="idproductoimagen">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.idproductoimagen}" type="checkbox" value="0">
                      <label for="checkbox{$v.idproductoimagen}"></label>
                    </div></div></td>
					<td>${v.idproducto}</td>	
					<td>${v.imagen==null?'':'<img src="'+_sysUrlBase_+v.imagen+'" class="img-fluid img-responsive" style="max-width: 75px; max-height: 50px;">'}</td>
					<td><i class="fa fa-pencil btneditar"></i> <i class="fa fa-trash btneliminar"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			$tabla.on('click','.btneditar',function(ev){
				ev.preventDefault();
				_this.idproductoimagen=$(this).closest('tr').attr('id');
				_this.vista_formulario();
			});			
			$tabla.on('click','.btneliminar',function(ev){
				ev.preventDefault();
				let tr=$(this).closest('tr');
				let data={idproductoimagen:tr.attr('id')}
				let datos={url:_this.tb,data:data,remove:true,ele:tr};
				_this.$fun.eliminar(datos);
			}).on('click','.cambiarestado',function(ev){ev.preventDefault(); _this.$fun.tablacambiarestado(this,_this.tb)});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_formulario(){
		let _this=this;
		this.ventana.find('.vistatabla').hide(0);
		this.ventana.find('.vistatablaformulario').fadeIn(500);
		    	    	var llenardatos=function(rs){
	    	let $frm=_this.ventana.find('#'+_this.frmid);
	    	$frm.find('input#idproductoimagen').val(rs.idproductoimagen);
			$frm.find('input#idproducto').val(rs.idproducto||'');
  			let imagedefault='static/media/defecto/nofoto.jpg';
      		let __imagen=(rs.imagen==''||rs.imagen=='null'||rs.imagen==null)?imagedefault:rs.imagen;
      		$frm.find('img#fileimgimagen').attr('src',_sysUrlBase_+(__imagen)).attr('link',__imagen);
  			let containerimg=document.querySelector('.img-container.img-imagen');
	        let opt={
	            container:containerimg,
	            image:containerimg.getElementsByTagName('img').item(0),
	            imagedefault:containerimg.getElementsByTagName('img').item(0).getAttribute('src'),
	            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
	        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}
	    	}
	        _this.$fun.crop(opt);
        	    }
	    if(this.idproductoimagen==false) return llenardatos({});	
	    var data=new FormData()		
			data.append('idproductoimagen',this.idproductoimagen);
			data.append('sqlget',true);			
			this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/','data':data}).then(rs =>{
				llenardatos(rs);				
	        }).catch(e => {console.log(e); });

	}

	
}
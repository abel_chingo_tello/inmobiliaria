class BDVistapagos{
	ventana=null;
	ventanaid=null;	
	frmid='frmvistapagos';
	tb='vistapagos';
	$fun=null;
	bustexto='';
	constructor(ventanaid){
		this.$fun=new _acht_();
		this.ventanaid=ventanaid;
		this.ventana=$('#'+ventanaid);
		var _this=this;
		if(this.ventana.find('#aquitable').length){
			
		}
		
		this.ini_librerias();
		let $frm=$('#'+this.ventanaid);	
		$frm.on('click','.buscar',function(ev){ ev.preventDefault(); 
			_this.vista_tabla();

		});		

	}
	ini_librerias(){
			   	   	$('select.select2').select2();

	}	
	vista_tabla(){
		let _this=this;	
		_this.bustexto=$('input#bustexto').val()||'';
		var data=new FormData();
				let html =`<table class="table table-hover table-condensed" id="table${this.ventanaid}">
            <thead>
              <tr>
                <th></th>
                <th>Fecha_vencimiento</th>
            	<th>Vivienda</th>
            	<th>Nombres</th>
            	<th>Estado</th>
            	<th>Monto_total</th>
            	<th></th>
            	</tr>
            </thead>
            <tbody></tbody></table>
		`;
		this.ventana.find('#aquitable').html(html);
		var data=new FormData();
		let nombres = $("#nombres").val();
		let estado = $("#estado option:selected").val(); 
		let fecha_inicio = $("#fecha_inicio").val();
		let fecha_fin = $("#fecha_fin").val();
		

		if(nombres!=""){
			data.append("nombres",nombres);	
		}
		if(estado!=""){
			data.append("estado",estado);	
		}
		if(fecha_inicio!="" && fecha_fin!=""){
			data.append("fecha_inicio",fecha_inicio);
			data.append("fecha_fin",fecha_fin);
		}
		if(fecha_inicio!="" && fecha_fin==""){
			data.append("fecha_inicio",fecha_inicio);
			//data.append("fecha_fin","maxfecha");
		}
		if(fecha_inicio=="" && fecha_fin!=""){
			//data.append("fecha_inicio","minfecha");
			data.append("fecha_fin",fecha_fin);
		} 
		
		this.$fun.postData({url:_sysUrlBase_+'json/'+_this.tb+'/',data:data}).then(rs => {
			html='';
			forEach(rs,function(v,i){
				switch(v.estado){
					case "0":
						v.estado = "Moroso";
						break;
					case "1":
						v.estado = "Al día";
						break;
				}

				html+=`<tr mensual=${v.monto_mensual} total=${v.monto_total} fecha=${v.fecha_vencimiento} idpk="">
					<td><div class="row-fluid"><div class="seleccionar checkbox check-success 	">
                      <input id="checkbox{$v.}" type="checkbox" value="0">
                      <label for="checkbox{$v.}"></label>
                    </div></div></td>
					<td>${v.fecha_vencimiento}</td>	
					<td>${v.vivienda}</td>	
					<td>${v.nombres}</td>	
					<td>${v.estado}</td>	
					<td>${v.monto_total}</td>	
					<td><i class="fa fa-search btnview"></i> </td>
				</tr>`;
			})				
			let $tabla=this.ventana.find('#table'+this.ventanaid+' tbody');
			$tabla.html(html);
			
			$tabla.on('click','.btnview',function(ev){
				ev.preventDefault();
				
				_this.mensual = $(this).closest('tr').attr('mensual');
				_this.total = $(this).closest('tr').attr('total');
				_this.fecha = $(this).closest('tr').attr('fecha');
				
				_this.vista_modal();
			});
			_this.$fun.dtTable('#table'+this.ventanaid,{'bustexto':_this.bustexto});
			$(".input-group").hide();
			$(".btnnuevo").hide();

        }).catch(e => {
        	console.log(e);
        })
		
	}
	vista_modal(){
		let _this = this;
		$("#m_fecha").val(_this.fecha);
		$("#m_mensual").val(_this.mensual);
		$("#m_total").val(_this.total);

		$("#exampleModal").modal("show");
	}
	
}
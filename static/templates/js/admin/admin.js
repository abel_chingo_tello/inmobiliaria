class Admin{
	btnsalir;
	constructor(params = null){
		this.func=new _acht_();       
    }
    addEventos(){
    	this.btnsalir=document.querySelectorAll(".btnsalirdelsistema");
    	this.func.addEventos(this.btnsalir,'click',(ev)=>this.salirdelsistema(ev));    		
    }
    ini(){
    	this.addEventos();
    }
    salirdelsistema(event){
    	let el=event.target;
    	if(el.classList.contains('disabled')) return false;
    	el.classList.add('disabled');
    	this.func.postData({
            url: _sysUrlBase_+'json/sesion/salir',
            data: {}
        }).then(rs => { 
        	el.classList.remove('disabled');
        	Swal.close();
        	window.location.href=_sysUrlSitio_;
        }).catch(e => {
        	let datos={text:e.msj}
        	el.classList.remove('disabled');
            this.func.swalError(datos);
        })
    }
}

window.onload = function(){
	const oAdmin = new Admin;
    oAdmin.ini();
};
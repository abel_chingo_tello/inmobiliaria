class Persona{	
    frm;
	constructor(params = null){
		this.func=new _acht_();       
    }
    addEventos(){
    	this.btncancelar=document.getElementsByClassName("btncancelar");
        this.btncancelar[0].addEventListener('click', (ev)=>{this.eventcancelar(ev)}, false);
        this.frm=document.getElementById("frmpersona");
        this.frm.addEventListener('submit', (ev)=>{this.eventguardar(ev)}, false);        
    }
    ini(){        
        $(".select2").select2();
    	this.addEventos();
        let containerimg=document.querySelector('.img-container');
        var opt={
            container:containerimg,
            image:containerimg.getElementsByTagName('img').item(0),
            //options:{aspectRatio: 1,viewmode:3 , initialAspectRatio:1.1,autoCrop:true,autoCropArea:false}
            options:{aspectRatio: 1/1,viewMode: 2,cropBoxMovable:false,cropBoxResizable:false, autoCropArea: 1,center: true,restore: false,zoomOnWheel: false,minContainerHeight: 150,
        minContainerWidth: 150,movable: true,cropBoxResizable:false, zoomable: true,  minCanvasHeight: 150, minCanvasWidth: 150,}

        }
        this.func.crop(opt);
    }
    imprimir(event){
        let el=event.target;
        if(el.classList.contains('disabled')) return false;
        el.classList.add('disabled');
    }
    editar(event){
        let el=event.target;
        if(el.classList.contains('disabled')) return false;
        el.classList.add('disabled');
    }
    eventguardar(event){
        event.preventDefault();  
        let el=event.target[0];
        if(el.classList.contains('disabled')) return false;
        el.classList.add('disabled');
        this.func.postData({
            url: _sysUrlBase_+'persona/guardar',
            data: new FormData(this.frm)
        }).then(rs => { 
            el.classList.remove('disabled');
            Swal.close();
            //if(rs.return!='') window.location.href=_sysUrlBase_+rs.return;
            //else window.location.href=_sysUrlBase_;
        }).catch(e => {
            let datos={text:e.msj}
            el.classList.remove('disabled');
            this.func.swalError(datos);
        })
    }
    eventcancelar(event){
        event.preventDefault();
        window.location.href=_sysUrlSitio_+'persona';
        //window.history.back();
    }
}
function ready(fn){var d=document;(d.readyState=='loading')?d.addEventListener('DOMContentLoaded',fn):fn();}
ready(function(){
    const oPersona = new Persona;
    oPersona.ini();
});

cantidad_maxima_navegacion=9;
cantidad_botones_crear=0;
cantidad_bienes_lectura=0;
var botones_navegacion="";
var paginaActual=1;
cantidad_maxima_mostrar=5;
//var classList = $(this).attr("class").split(/\s+/);// me jala todas clases clases  separadas por space+ en array
//var classList = $(this).attr("nro_item");// me jala todas clases clases  separadas por space+ en array
fix_carrito();
function fix_carrito(){
	botones_navegacion="";
	var listaBienes =document.getElementsByClassName("items_bienes");
	cantidad_bienes_lectura=listaBienes.length;
	for(var i=1;i<=listaBienes.length;i++){
		var nro_item=listaBienes[i-1].getAttribute("nro_item").split("item_nro")[1].trimStart().trimEnd();
		if(nro_item>cantidad_maxima_navegacion){
			//listaBienes[i].setAttribute("hidden","hidden");
			$("#item_nro"+i).hide();
		}
	}
	cantidad_botones_crear=Math.ceil(cantidad_bienes_lectura/cantidad_maxima_navegacion);// x.5=>(x+1)
	for(var l=1;l<=cantidad_botones_crear;l++){
        switch(true){
            case l==1:
                botones_navegacion+="<input type=button id=\"nroBoton"+l+"\" onclick=\"muestraIteracion("+l+")\" class=\"muestra_boton selectedBoton\" value="+l+" />";break;
            case l>=cantidad_maxima_mostrar && l!=cantidad_botones_crear:
                botones_navegacion+="<input type=button id=\"nroBoton"+l+"\" onclick=\"muestraIteracion("+l+")\" class=oculta_boton value="+l+" />";break;
            case l==cantidad_maxima_mostrar-1 && cantidad_botones_crear>cantidad_maxima_mostrar:
                botones_navegacion+="<input type=button id=\"nroBoton"+l+"\" onclick=\"muestraIteracion("+l+")\"class=\"muestra_boton iteraAdelante\" value="+l+" />";break;
            //case l==2 && cantidad_botones_crear>cantidad_maxima_mostrar:
            //    botones_navegacion+="<input type=button id=\"nroBoton"+l+"\" onclick=\"muestraIteracion("+l+")\"class=\"muestra_boton iteraAtras\" value="+l+" />";break;
            default:
                botones_navegacion+="<input type=button id=\"nroBoton"+l+"\" onclick=\"muestraIteracion("+l+")\" class=muestra_boton value="+l+" />";break;
        }
    }
    $("#navegacion_abajo").html("");
    $("#navegacion_abajo").append("<button class=\"btn-pagination\"  type=button value=Anterior  onclick=\"anteriorPagina()\"   disabled=disabled id=anteriorPag><i class=\"fa fa-arrow-left\"  aria-hidden=\"true\"></i></button>"+botones_navegacion+
                                           "<button class=\"btn-pagination\"  type=button value=Siguiente  onclick=\"siguientePagina()\" id=siguientePag><i class=\"fa fa-arrow-right\"  aria-hidden=\"true\"></i></button>");
    $(".oculta_boton").hide();  
    cantidad_botones_crear== 1 ? $("#siguientePag").attr("disabled","disabled") : true;
}

function muestraIteracion(nroBoton){
    //actualizo la pagina actual
    paginaActual=nroBoton;
    //alert(cantidad_botones_crear);
    //reviso si es el penultimo item para quitarle el iterador adelante; el segundo parametro no vale, en el switch de arriba ya esta validado
    if(nroBoton==cantidad_botones_crear-1 && cantidad_botones_crear>cantidad_maxima_mostrar){
        $("#nroBoton"+nroBoton).removeClass("iteraAdelante");
    }
    //reviso si es el segundo item para quitarle el iterador atras ; lo mismo.. el 2do paraetro no vale.. ya que igual dependerá de que exista la clase iteraAdelante
    if(nroBoton==2 && cantidad_botones_crear>cantidad_maxima_mostrar){
        $("#nroBoton"+nroBoton).removeClass("iteraAtras");
    }
    //este for es para poner el style  de seleccionado al boton que corresponde
    for(var i=1;i<=cantidad_botones_crear;i++){
        if(i==nroBoton){
            $("#nroBoton"+i).removeClass("selectedBoton");
            $("#nroBoton"+i).addClass("selectedBoton");
        }else{
            $("#nroBoton"+i).removeClass("selectedBoton");
        }
        $("#nroBoton"+i).removeClass("oculta_boton");
        $("#nroBoton"+i).addClass("muestra_boton");
    }
    //este for es para mostrar u ocultar los items que corresponde para cada pagina
    for(var i=1;i<=cantidad_bienes_lectura;i++){
        if( i<=cantidad_maxima_navegacion*nroBoton && i>=cantidad_maxima_navegacion*nroBoton-(cantidad_maxima_navegacion-1)){
            $("#item_nro"+i).show();
        }else{
            $("#item_nro"+i).hide();
        }
    }
    //compruebo si los botones tienen que rotar hacia adelante con el numero magico=3 jajaja (que funciona para 5 botones) o sea deberia restar nrobotones-2 creo ..(2 seria el magico) jaja ..  xd alv
    if($("#nroBoton"+nroBoton).hasClass("iteraAdelante")){
        iteraHaciaAdelante(nroBoton);
    }
    //compruebo si los botones tienen que rotar hacia atras con el numero magico=3 jajaja
    if($("#nroBoton"+nroBoton).hasClass("iteraAtras")){
        iteraHaciaAtras(nroBoton);
    }
    //compruebo si tengo que desactivar el boton atras o adelante
    nroBoton==1 ? $("#anteriorPag").attr("disabled","disabled") : $("#anteriorPag").removeAttr("disabled");
    nroBoton==cantidad_botones_crear ? $("#siguientePag").attr("disabled","disabled") : $("#siguientePag").removeAttr("disabled");
    

}
function siguientePagina(){
    if(paginaActual!=cantidad_botones_crear){
        muestraIteracion(paginaActual+1)
    }
}
function anteriorPagina(){
    if(paginaActual!=1){
        muestraIteracion(paginaActual-1)
    }
}
function iteraHaciaAdelante(nroBoton){
    var nroBotonOcultar=cantidad_maxima_mostrar-3;
    $("#nroBoton"+(nroBoton+1)).show();
    $("#nroBoton"+(nroBoton-nroBotonOcultar)).hide();
    $("#nroBoton"+nroBoton).removeClass("iteraAdelante");
    $("#nroBoton"+(nroBoton+1)).addClass("iteraAdelante");
    
    //reasigno el clase itera Atras
    $("#nroBoton"+(nroBoton-(nroBotonOcultar))).removeClass("iteraAtras");
    $("#nroBoton"+(nroBoton-(nroBotonOcultar)+1)).addClass("iteraAtras");//agrego clase para iterar atras
}
function iteraHaciaAtras(nroBoton){
    var nroBotonOcultar=cantidad_maxima_mostrar-3;
    $("#nroBoton"+(nroBoton-1)).show();
    $("#nroBoton"+(nroBoton+nroBotonOcultar)).hide();
    $("#nroBoton"+nroBoton).removeClass("iteraAtras");
    if((nroBoton-1)!=2){
        $("#nroBoton"+(nroBoton-1)).addClass("iteraAtras")
    }
    //reasigno el clase itera Adelante
    $("#nroBoton"+(nroBoton+(nroBotonOcultar))).removeClass("iteraAdelante")
    $("#nroBoton"+(nroBoton+(nroBotonOcultar)-1)).addClass("iteraAdelante");//agrego clase para iterar adelante
}

//modal para contactar
function contactar(id,descripcion,img,tema){
    //alert("en construccion")
    $("#exampleModal").modal("show");
    $("#bien_id").val(id);
    $("#bien_descripcion").html(descripcion);
    $("#img_detalle").attr("src",tema+"assets/img/bienes/"+img);
}
$("#contacto_bien").on('submit', function(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    
    if($("#tipo_documento").val()==""){
        alert("Seleccione el tipo de documento");
        return;
    }
    if($("#nro_documento").val()==""){
        alert("Ingrese su numero de documento");
        return;
    }
    if($("#telefono").val()==""){
        alert("Ingrese su numero de telefono o celular");
        return;
    }
    if($("#nombre").val()==""){
        alert("Ingrese su nombre");
        return;
    }
    if($("#apellidos").val()==""){
        alert("Ingrese su apellido");
        return;
    }
    if($("#email").val()==""){
        alert("Ingrese su correo");
        return;
    }

    $.ajax({
        type: $(this).prop('method'),
        url : $(this).prop('action'),
        data: $(this).serialize()
    }).done(function() {
        alert("Se contactarán contigo.");
        $("#exampleModal").modal("hide");
        $('#contacto_bien').trigger("reset");
    });
    
    
});
function filtrar(){
    precio_min = $("#precio_min").val();
    precio_max = $("#precio_max").val();
    
    area_min = $("#area_min").val();
    area_max = $("#area_max").val();
        
    catalogo =  $("#catalogo option:selected").val();
    tipo_bien =  $("#tipo_bien option:selected").val();

    fecha =  $("#fecha option:selected").val();
    
    if(precio_min==""){
        precio_min = -1;
    }
    if(precio_max==""){
        precio_max =Number.MAX_VALUE;
    }

    if(area_min==""){
        area_min = -1;
    }
    if(area_max==""){
        area_max =Number.MAX_VALUE;
    }
    if (catalogo==""){
        data_enviar = {
            "precio_min": precio_min, 
            "precio_max" : precio_max,
            "area_min": area_min, 
            "area_max" : area_max, 
            "tabl_tipo_interior" : tipo_bien, 
            "rebi_fadquisicion" : fecha     
        }
    }else{
        data_enviar = {
            "casb_codigo" : catalogo,
            "precio_min": precio_min, 
            "precio_max" : precio_max,
            "area_min": area_min, 
            "area_max" : area_max, 
            "tabl_tipo_interior" : tipo_bien, 
            "rebi_fadquisicion" : fecha 
          }
    }


    $.ajax({
        url : URL_RAIZ+"json/Registro_bien/",
        dataType : "json",
        method:"POST",
        data : data_enviar,
        success : function(data){
          limpiar();
          $.when(llenarBienes(data["data"])).then(fix_carrito());
        }
    })    

}
//elimina la lista de bienes
function limpiar(){
    $(".portfolio-item").remove();
}
//actualiza los bienes filtrados
function llenarBienes(data){
    
    if(data.length > 0){
        a="";
        for(var items=0;items<data.length;items++){
            // item["rebi_detalles"] +'
            if(data[items]["rebi_detalles"]==null){
                data[items]["rebi_detalles"] = "INMUEBLE";
            }
            data_fixed = (data[items]["rebi_detalles"]).replace(/(\r\n|\n|\r|\")/gm,"");
            a+=('<div  onclick="detalle('+data[items]["rebi_id"]+')" id="item_nro'+(items+1)+'" class="col-lg-4 col-md-6 portfolio-item  items_bienes item_nro'+  (items+1)+' " nro_item="item_nro'+  (items+1)+'">'+
            '<div class="portfolio-img"><img src="'+  URL_TEMA+'assets/img/bienes/'+data[items]['rebi_foto_principal']+'" class="img-fluid" alt=""></div>'+
            '<div class="portfolio-info">'+
                '<h4>'+  data[items]["casb_descripcion"]+'</h4>'+
                '<p>'+  data[items]["rebi_detalles"]+'</p>'+
                '<p> S/.'+ number_format(data[items]["rebi_ult_val_act"],2,".",",")+'</p>'+
                '<button onclick=\'contactar("'+  data[items]['rebi_id']+'","'+data_fixed+'","'+data[items]["rebi_foto_principal"]+'","'+URL_TEMA+'"'+');event.stopPropagation();\' >CONTACTAR</button>'+
            '</div>'+
            '</div>');
        }
    }else{
        a = "<div class='row pl-5'><h3>No encontramos resultados.<h3></div>";
    }
    
    $("#portfolio-container").html(a);
}
function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
function detalle(id){
     //location.href =  URL_RAIZ+"pagina/detalle/"+id;
     var url = URL_RAIZ+"pagina/detalle/"+ id;
     window.open(url , '_blank');
}
$(document).ready(function(){
});	




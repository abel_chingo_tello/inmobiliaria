var _sysUrlBase_='//'+window.location.host+'/';
var _sysUrlStatic_=_sysUrlBase_+'static/';
var _sysUrlLibs_=_sysUrlStatic_+'librerias/';
var _sysUrlSitio_=_sysUrlBase_;
function actualizar_rutas(rutabase,rutasitio){
	_sysUrlBase_=rutabase;
	_sysUrlStatic_=_sysUrlBase_+'static/';
	_sysUrlLibs_=_sysUrlStatic_+'librerias/';
	_sysUrlSitio_=_sysUrlBase_+(rutasitio!=''?(rutasitio+'/'):'');
}

var __sysAyax = function (infodata) {
	try {
		var opt = { donde: false, url: false, type: 'json', mostrarcargando: false, method: 'POST', fromdata: {}, msjatencion: '' };
		$.extend(opt, infodata);
		var datasend = dts = opt.fromdata;

		var showmsjok = opt.showmsjok == false ? false : true;
		var callback = opt.callback || false;
		var callbackerror = opt.callbackerror || false;

		var typesend = (dts.constructor.name || 'object').toString().toLowerCase();
		var _prd = (typesend == "formdata") ? false : true;
		var $progressavance = $(opt.iduploadtmp).length ? $(opt.iduploadtmp + ' #progressavance ') : '';
		var $divuploadp = $progressavance.length ? $progressavance.find('span#cantidadupload') : '';
		var $iduploadtmp = $(opt.iduploadtmp) || '';
		var hayprogress = $progressavance.length ? true : false;
		var _donde = opt.donde || false;

		//console.log(mostrarcargando,_donde);
		if (!opt.url) return;
		$.ajax({
			url: opt.url,
			type: opt.method,
			dataType: opt.type,
			data: datasend,
			processData: _prd,
			contentType: _prd == true ? 'application/x-www-form-urlencoded; charset=UTF-8' : false,
			cache: false,
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				try {
					xhr.upload.addEventListener("progress", function (evt) { //Upload progress
						if (evt.lengthComputable && hayprogress) {
							var percentComplete = Math.floor((evt.loaded * 100) / evt.total);
							$progressavance.width(percentComplete + '%');
							$divuploadp.text(percentComplete + '%');
							//console.log(percentComplete);
						}
					}, false);

					xhr.addEventListener("progress", function (evt) {//Download progress
						if (evt.lengthComputable) {
							var percentComplete = Math.floor((evt.loaded * 100) / evt.total);
							//console.log(percentComplete);
						}
					}, false);
				} catch (err) {
					console.log(err);
				}
				return xhr;
			},
			beforeSend: function (XMLHttpRequest) {
				if ((_donde != false && opt.type != 'json') && opt.mostrarcargando == true) {
					$cl = $('#cargando_clone').clone();
					$cl.removeAttr('style');
					_donde.html($cl.html());
				}
			},
			success: function (data) {
				if (opt.type == 'json') {
					if (data.code == 'Error') {
						mostrar_notificacion(opt.msjatencion, data.msj, 'warning');
						if (_isFunction(callbackerror)) callbackerror(data);
					} else {
						if (showmsjok) mostrar_notificacion(opt.msjatencion, data.msj, 'success');
						if (_isFunction(callback)) callback(data);
					}
				} else {
					if (_isFunction(callback)) callback(data);
				}
				if ($iduploadtmp.length) $iduploadtmp.remove();
			},
			error: function (e) { if ($iduploadtmp.length) $iduploadtmp.remove(); },
			complete: function (xhr) { if ($iduploadtmp.length) $iduploadtmp.remove(); }
		});
	} catch (err) {
		return false;
	}
}
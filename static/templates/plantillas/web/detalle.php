<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo URL_TEMA;?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo URL_TEMA;?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo URL_TEMA;?>assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Bethany - v2.2.0
  * Template URL: https://bootstrapmade.com/bethany-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <jrdoc:incluir tipo="docsCss" />

</head>

<body>

	<jrdoc:incluir tipo="modulo" nombre="menuestatico" /></jrdoc:incluir>

 

  <main id="main">
  	<jrdoc:incluir tipo="recurso" />

  </main>

  <jrdoc:incluir tipo="modulo" nombre="pie" /></jrdoc:incluir>

  <!--<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>-->

  <!-- Vendor JS Files -->
  <script src="<?php echo URL_TEMA;?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?php echo URL_TEMA;?>assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo URL_TEMA;?>assets/js/main.js"></script>

  <jrdoc:incluir tipo="docsJs" />
  <jrdoc:incluir tipo="modulo" nombre="modspeach" /></jrdoc:incluir>

</body>

<?php 
$version=!empty(_version_)?_version_:'1.0';
$rutastatic=$documento->getUrlStatic();
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/headereffects/css/component.css">
	<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/headereffects/css/normalize.css" />
	<link href="<?php echo URL_STATIC;?>librerias/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>

	<!-- BEGIN CORE CSS FRAMEWORK -->
	<link href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL_STATIC;?>librerias/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!-- END CORE CSS FRAMEWORK -->

	<!-- BEGIN CSS TEMPLATE -->
	<link href="<?php echo URL_TEMA;?>css/styles.css" rel="stylesheet" type="text/css"/>
	<!--link href="<?php echo URL_TEMA;?>css/responsive.css" rel="stylesheet" type="text/css"/-->
	<link href="<?php echo URL_STATIC;?>templates/css/animate.min.css" rel="stylesheet" type="text/css"/>
	<!-- END CSS TEMPLATE -->

	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/v3.4/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/funciones.js?version=<?php echo _version_;?>"></script>
	<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/jquery/rs-plugin/css/settings.css" media="screen" />
</head>
<body>
<jrdoc:incluir tipo="docsCss" />
	<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
<div class="main-wrapper">	
    <jrdoc:incluir tipo="recurso" />
</div>
<jrdoc:incluir tipo="modulo" nombre="pie" /></jrdoc:incluir>
<script src="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/pace/pace.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/jquery/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script> 
<script src="<?php echo URL_STATIC;?>librerias/sliders/owl.carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo URL_STATIC;?>librerias/waypoints/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/parrallax/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-appear/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-numberAnimate/jquery.animateNumbers.js" ></script> 
<!--cript type="text/javascript" src="<?php  echo URL_TEMA;?>/js/core.js"></script-->
<jrdoc:incluir tipo="docsJs" />
</body>
</html>

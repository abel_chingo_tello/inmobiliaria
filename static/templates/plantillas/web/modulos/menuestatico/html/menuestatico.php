  <style>

    div.fixed{
      position: fixed;
      top: 10px;
      left: 100px;
    }
  </style>

  <!-- ======= PARTE 01: Header ======= -->

  

  <header id="headerc" class="fixed-top d-flex align-items-center">
    <div class="container">

      <div class="fixed">
        <a href="index.php"><img class="logo1" src="<?php echo URL_TEMA;?>assets/img/logo_inmuebles_silueta.png" alt="" style="height:95px;"></a>
      </div>

      <div class="header-container d-flex align-items-center">
        
        <div class="logo mr-auto">

          <h1 class="text-light" style="visibility: hidden"><a href="index.php"><span>Bethany</span></a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!---->
        </div>

        <nav class="nav-menu d-none d-lg-block">
          <ul>
            <li class="active"><a href="<?php echo URL_BASE."paginas"?>">Inicio</a></li>
            <li><a href="<?php echo URL_BASE."paginas#portfolio"; ?>">Regresar a Bienes</a></li>
            <li><a href="#contact">Contáctenos</a></li>

            <!--<li class="get-started"><a href="#about">Get Started</a></li>-->
          </ul>
        </nav><!-- .nav-menu -->
      </div><!-- End Header Container -->
    </div>
  </header><!-- End Header -->
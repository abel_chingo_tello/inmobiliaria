<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Modspeach extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();		
		$this->modulo = 'modspeach';		
	}
	
	public function mostrar($html=null)
	{
		try{
			//$this->empresa=NegSesion::getEmpresa();
			$this->esquema = 'speach';
			/*JrCargador::clase('sys_negocio::NegCategoria_servicio', RUTA_BASE);
            $this->oNegCategoriaServicio = new NegCategoria_servicio;
            $this->oNegCategoriaServicio->setLimite(0,6); 
			$this->categorias=$this->oNegCategoriaServicio->buscar(array('estado'=>'1'));*/
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}
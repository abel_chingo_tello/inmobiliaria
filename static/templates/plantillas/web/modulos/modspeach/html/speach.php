<style type="text/css">
  .btnspeach {
    font-size: 18px;
    display: inline-block;
    background-color: red;
    /* color: #eee; */
    line-height: 1;
    padding: 9px 0;
    margin-right: 4px;
    border-radius: 50%;
    text-align: center;
    width: 38px;
    height: 38px;;
    transition: 0.3s;
    position: fixed;
    bottom: 0px;
    color: #fff;
    margin-left: 1ex;
    margin-bottom: 1ex;
    cursor: pointer;
        z-index: 10;
  }
  .btnspeach.active{
      background-color: green;
  }
</style>
<span class="btnspeach">
  <i class="fa fa-microphone-slash animate__animated" aria-hidden="true"></i>
</span>
<script type="text/javascript">
$(function(){
var speechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.oSpeechRecognition || window.msSpeechRecognition;
var speachdetetenermanual=false;
var frasespeach='ir a';
if (!String.prototype.trim) {
  (function() {
    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    String.prototype.trim = function() {
      return this.replace(rtrim, '');
    };
  })();
}

var speachlimpiartxt=function(txt){
  txt=txt||'';
  let txt1=txt.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
  txt1=txt1.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

  //txt1=txt1.replace(/(áéíóú)/)
  //rtxt1=txt1.normalize('NFD').replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1").normalize();

  let txt2=txt1.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
  let txt3=txt2.replace(/(\r\n|\n|\r)/gm,"")
  return txt3;
}

var speachalternative=function(){
  var txt=[];
  var txtbtn=[];
  $('a').each(function(i,v){
    let txta=$(v).text()||''; 
    let txt_=speachlimpiartxt(frasespeach+txta);
    if(txt_!='' && txt.includes(txt_)==false){
     txt.push(txt_);
     txtbtn.push({'texto':txt_,'a' : v });
    }
  })
  return txtbtn;
}
let speachtextos=speachalternative();
//console.log(speachtextos);
var promisifiedOldGUM = function(constraints, successCallback, errorCallback){ // function a soporte a multiples navegadores.
  var getUserMedia = (navigator.getUserMedia ||  navigator.webkitGetUserMedia || navigator.mozGetUserMedia);
  if(!getUserMedia){ return Promise.reject(new Error('getUserMedia is not implemented in this browser'));  }
  return new Promise(function(successCallback, errorCallback){ getUserMedia.call(navigator, constraints, successCallback, errorCallback); });
}
if(navigator.mediaDevices === undefined) { navigator.mediaDevices = {}; }
if(navigator.mediaDevices.getUserMedia === undefined){ navigator.mediaDevices.getUserMedia = promisifiedOldGUM;}


//var grammar = '#JSGF V1.0; grammar colors; public <color> ='+speachtextos+';'
var constraints={audio:true};
var iniciospeach=false;
  var escuchar=function(btni){
    if(iniciospeach==false)
    navigator.mediaDevices.getUserMedia(constraints)
    .then(function(stream){
      //var speechRecognitionList = new SpeechGrammarList();
      //speechRecognitionList.addFromString(grammar, 1);
      recognizeronline = new speechRecognition();
      //recognition.grammars = speechRecognitionList;
      recognizeronline.continuous = false;
      recognizeronline.lang = "es-US";
      recognizeronline.interimResults = true;

      recognizeronline.onstart = function(){ // iniciar speach 
        btni.removeClass('fa-microphone-slash').addClass('fa-microphone animate__zoomIn animate__infinite');
        btni.parent('.btnspeach').css('background-color','green');  
        iniciospeach=true;        
      }
      recognizeronline.onend = function(){ //detetener speach
        if(speachdetetenermanual==false)
          setTimeout(function(){escuchar(btni)},1000);
        btni.removeClass('fa-microphone  animate__zoomIn animate__infinite').addClass('fa-microphone-slash');
        btni.parent('.btnspeach').css('background-color','red');
        iniciospeach=false;
      }   

      recognizeronline.onresult = function(event){ // resultado;
          if(event.results &&  event.results.length )
          {
            let text = event.results[0][0].transcript;
            let txtresult=speachlimpiartxt(text);
            let txt1=text;
            $.each(speachtextos,function(i,v){
             let texto=speachlimpiartxt(v.texto);
              if(texto==txtresult && txtresult!='ir a'){
                let href=$(v.a).attr('href')||'';
                if(href==_sysUrlBase_|| href=='#' || href=='' || href==' ' ) window.location.href=_sysUrlBase_;
                else if(href.charAt(0)=='#') $(v.a).trigger('click');
                else window.location.href=href;
                return;
              }
            })
            //console.log(speachtextos);
          }            
      }
      recognizeronline.start();
    }).catch(function(err){console.log('Error de Speach : '+err.name + ": " + err.message);});
  }


    $('.btnspeach').on('click',function(ev){
      let btn=$(this);
      let i=$(this).children('i');
      if(i.hasClass('fa-microphone-slash')){
        speachdetetenermanual=false;
        escuchar(i);
      }else{
        i.removeClass('fa-microphone  animate__zoomIn animate__infinite').addClass('fa-microphone-slash');
        btn.css('background-color','red');
        speachdetetenermanual=true;
      }
    }).trigger('click');

  })
</script>
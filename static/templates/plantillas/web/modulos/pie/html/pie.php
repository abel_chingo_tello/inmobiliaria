  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <h3><img src="<?php echo URL_TEMA;?>assets/img/logo_cuadrado.png"> SBCH</h3>
            <p>Las actividades comerciales de la Sociedad de Beneficencia de Chiclayo se rigen exclusivamente por
el Código Civil y demás normas del sector privado. </p>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Links Actuales</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="index.php">Inicio</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#about">Quienes somos</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#portfolio">Locales</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#contact">Contáctanos</a></li>
              <li><i class="sistema/vistas/login.php"></i> <a href="#">Login</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-contact">
            <h4>Contáctanos</h4>
            <p>
              Elías Aguirre 248 - Chiclayo <br>
              Lambayeque<br>
              Perú<br>
              <strong>Celular</strong>(074) 233691, 233768<br>
              <strong>Email:</strong>jbarragan@sbch.gob.pe<br>
            </p>

            <div class="social-links">
              <a href="https://twitter.com/benef_chiclayo" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/beneficenciadechiclayo/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/SOCBENEFICENCIACHICLAYO/" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="https://www.youtube.com/channel/UCgAki2wHc7H3CVqsWZfOjMQ" class="youtube"><i class="fab fa-youtube"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright. 2020
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
      -->
        <!--Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>-->
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

    <!--Floating WhatsApp javascript-->
<script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <script src="sistema/public/js/bootbox.js"></script>
<script src="sistema/public/js/bootstrap-select.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <script>
  $(function() {
  // Sidebar toggle behavior
  $('#sidebarCollapse').on('click', function() {
    $('#sidebar, #content').toggleClass('active');
  });
});

</script>


  <script>
    $(function() {
     $("#enviar_form").on("submit", function(e) {
         e.preventDefault();
         var f = $(this);
         var metodo = f.attr("method");
         var url = "enviar_form.php";
         var formData = new FormData(this);
         formData.append("dato", "valor");
         $.ajax({
             url: url,
             type: metodo,
             dataType: "html",
             data: formData,
             cache: false,
             contentType: false,
             processData: false,
             beforeSend: function() {},
             success: function(data) {
                 if (data == "1") {
                     alert("El correo ha sido enviado");
                 }else{
                     console.log(data);
                     alert("Lo sentimos, no se pudo enviar el correo");
                 }
                 
             },
             error: function() {},
         });
     });
 });
  </script>

</body>

</html>
<?php
/**
 * @autor		Alvaro Alonso Cajusol Vallejos
 * @fecha		10/01/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('ACHT::JrModulo', RUTA_LIBS);
JrCargador::clase('modulos::pie::Pie', RUTA_TEMA);
$oMod = new Pie;
echo $oMod->mostrar(@$atributos['posicion']);
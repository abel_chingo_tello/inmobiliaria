<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- BEGIN PLUGIN CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/pace/pace-theme-flash.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-select2/select2.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-datepicker/css/datepicker.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-timepicker/css/bootstrap-timepicker.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-checkbox/css/bootstrap-checkbox.css" media="screen"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL_STATIC;?>librerias/ios-switch/ios7-switch.css" media="screen">
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/bootstrap/v3/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>templates/css/animate.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/jquery/jquery-scrollbar/jquery.scrollbar.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/cropper/cropper.min.css" rel="stylesheet">
<link rel="stylesheet/less" href="<?php echo URL_TEMA;?>css/estilo.less" rel="stylesheet">
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>templates/webarch/dashboard/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>templates/webarch/dashboard/css/responsive.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>templates/webarch/dashboard/css/custom-icon-set.css"/>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/v3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/popper/popper.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/sweetalert/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/funciones.js?version=<?php echo _version_;?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/cropper/cropper.min.js"></script>
<jrdoc:incluir tipo="cabecera" />
<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
<!-- END CSS TEMPLATE -->
</head>
<body>
	<jrdoc:incluir tipo="docsCss" />
	<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
	<div class="page-container row-fluid">
	  <!-- BEGIN SIDEBAR -->
	  	<div class="page-sidebar" id="main-menu"> 
		    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		    	<jrdoc:incluir tipo="modulo" nombre="perfil" /></jrdoc:incluir>  
		   		<jrdoc:incluir tipo="modulo" nombre="menu" /></jrdoc:incluir>
		  	</div>
	  	</div>
	  	<jrdoc:incluir tipo="modulo" nombre="menufooter" /></jrdoc:incluir>	  
		<div class="page-content"> 
		    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		    <div id="portlet-config" class="modal hide">
		      	<div class="modal-header">
		        	<button data-dismiss="modal" class="close" type="button"></button>
		        	<h3>Widget Settings</h3>
		      	</div>
		      	<div class="modal-body"> Widget settings form goes here </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="content"> 
				<jrdoc:incluir tipo="recurso" />
		    </div>
		</div>
		<jrdoc:incluir tipo="modulo" nombre="chat" /></jrdoc:incluir>
	</div>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/breakpoints.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-unveil/jquery.unveil.min.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-block-ui/jqueryblockui.js" ></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/pace/pace.min.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-numberAnimate/jquery.animateNumbers.js" ></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/bootstrap/v3/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/validator/validator.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/webarch/dashboard/js/core.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/webarch/dashboard/js/chat.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/abelchingo/main.js"></script> 
	<script type="text/javascript" src="<?php echo URL_TEMA;?>js/admin.js"></script> 
	<jrdoc:incluir tipo="docsJs" />
</body>
</html>

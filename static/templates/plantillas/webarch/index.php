<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<jrdoc:incluir tipo="modulo" nombre="empresa" /></jrdoc:incluir>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- BEGIN PLUGIN CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/pace/pace-theme-flash.css" media="screen"/>
<!-- END PLUGIN CSS -->
<!-- BEGIN CORE CSS FRAMEWORK -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap.min.css"/>
<!--link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/css/bootstrap-theme.min.css"/-->
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>templates/css/animate.min.css"/>


<link rel="stylesheet" type="text/css" href="<?php echo URL_STATIC;?>librerias/jquery/jquery-scrollbar/jquery.scrollbar.css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link rel="stylesheet" type="text/css" href="<?php echo URL_TEMA;?>dashboard/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_TEMA;?>dashboard/css/responsive.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL_TEMA;?>dashboard/css/custom-icon-set.css"/>
<!--script src="https://unpkg.com/@popperjs/core@2"></script-->
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/popper/popper.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/v3.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/sweetalert/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/funciones.js?version=<?php echo _version_;?>"></script>
<jrdoc:incluir tipo="cabecera" />
<script type="text/javascript">actualizar_rutas('<?php echo URL_BASE; ?>','<?php echo _sitio_ ?>')</script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/abelchingo/abelchingo.js?version=<?php echo _version_;?>"></script>
<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/abelchingo/ventanas.js?version=<?php echo _version_;?>"></script>
<!-- END CSS TEMPLATE -->
</head>
<body>
	<jrdoc:incluir tipo="docsCss" />
	<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
	<div class="page-container row-fluid">
	  <!-- BEGIN SIDEBAR -->	  
	  	<div class="page-sidebar" id="main-menu"> 
		    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
		    	<jrdoc:incluir tipo="modulo" nombre="perfil" /></jrdoc:incluir>  
		   		<jrdoc:incluir tipo="modulo" nombre="menu" /></jrdoc:incluir>
		  	</div>
	  	</div>
	  	<jrdoc:incluir tipo="modulo" nombre="menufooter" /></jrdoc:incluir>	  
		<div class="page-content"> 
		    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		    <div id="portlet-config" class="modal hide">
		      	<div class="modal-header">
		        	<button data-dismiss="modal" class="close" type="button"></button>
		        	<h3>Widget Settings</h3>
		      	</div>
		      	<div class="modal-body"> Widget settings form goes here </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="content"> 
				<jrdoc:incluir tipo="recurso" />
		    </div>
		</div>
		<jrdoc:incluir tipo="modulo" nombre="chat" /></jrdoc:incluir>
	</div>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/bootstrap/v4.5.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/breakpoints.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-unveil/jquery.unveil.min.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/pace/pace.min.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>librerias/jquery/jquery-numberAnimate/jquery.animateNumbers.js" ></script>
	<script type="text/javascript" src="<?php echo URL_TEMA;?>dashboard/js/core.js"></script> 
	<script type="text/javascript" src="<?php echo URL_TEMA;?>dashboard/js/chat.js"></script> 
	<script type="text/javascript" src="<?php echo URL_STATIC;?>templates/js/admin/admin.js"></script> 
	<jrdoc:incluir tipo="docsJs" />
</body>
</html>

<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class ModEmpresa extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();		
		$this->modulo = 'empresa';		
	}
	
	public function mostrar($html=null)
	{
		try{		
			$this->empresa=NegSesion::getEmpresa();
			$this->esquema = 'empresa';
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}
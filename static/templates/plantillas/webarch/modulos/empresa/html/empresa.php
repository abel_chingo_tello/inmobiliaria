<?php 
	$logoempresa=!empty($this->empresa["logo"])?$this->empresa["logo"]:'static/media/defecto/logoempresa.png';
	if(!is_file(RUTA_BASE.str_replace('',SD,$logoempresa))) $logoempresa='static/media/defecto/fotouser.png';
?>
<title><?php echo @$this->empresa["nombre"]; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="<?php echo @$this->empresa["descripcion"]; ?>" name="description" />
<meta content="<?php echo @$this->empresa["nombre"]; ?>" name="author" />
<link rel="icon" href="<?php echo URL_BASE.$logoempresa; ?>">
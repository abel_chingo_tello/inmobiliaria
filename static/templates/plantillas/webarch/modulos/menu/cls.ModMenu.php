<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class ModMenu extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();		
		$this->modulo = 'menu';		
	}
	
	public function mostrar($html=null)
	{
		try{
			JrCargador::clase('sys_negocio::NegEmpresa_modulo', RUTA_BASE);	
			$oNegEmpresa_modulo = new NegEmpresa_modulo;
			$usuarioAct = NegSesion::getUsuario(); 
            $empresaAct=NegSesion::getEmpresa();
            $this->idrol=$usuarioAct["idrol"];
            $this->idempresa=$empresaAct["idempresa"];
            $datos=$oNegEmpresa_modulo->buscar(array('idempresa'=>$this->idempresa,'idrol' =>$this->idrol));           
            $this->htmlmenus=$this->menus($datos,true);
			$this->esquema = 'menu';	
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}

	private function menus($menu,$ini=false){
		if(empty($menu))return '';
		$htmmenu='';
		if($ini) $htmmenu.='<ul>';
		foreach($menu as $mk => $v){			
			$iscontainer=is_null($v["idmodulo"])?true:false;
			$dt=empty($v["nomtemporal"])?array('nombre'=>'sinnombre','icono'=>'fa-folder-open'):json_decode(utf8_encode(base64_decode($v["nomtemporal"])),true);			
			$strmodulo=$iscontainer==false?$v["strmodulo"]:$dt["nombre"];
			$icono=$iscontainer==false?$v["icono"]:$dt["icono"];
			$htmlhijo=!empty($v["hijos"])?('<ul class="sub-menu">'.$this->menus($v["hijos"],false).'</ul>'):'';
			$htmmenu.='<li> <a href="'.(empty($v["url"])?'javascript:;':(URL_BASE.$v["url"])).'"> <i class="fa '.$icono.'"></i> ';
			$htmmenu.=' <span class="title">'.ucfirst($strmodulo).'</span>';
			if($iscontainer) $htmmenu.=' <span class="arrow "></span>';
			$htmmenu.='</a>'.$htmlhijo.'</li>';
		}
		if($ini) $htmmenu.='</ul>';
		return $htmmenu;
	}
}
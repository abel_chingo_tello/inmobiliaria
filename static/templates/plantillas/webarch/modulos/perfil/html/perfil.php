<?php 
$foto=!empty($this->persona["foto"])?$this->persona["foto"]:'static/media/defecto/fotouser.png';
if(!is_file(RUTA_BASE.str_replace('',SD,$foto))) $foto='static/media/defecto/fotouser.png';
?>
<div class="user-info-wrapper"> 
  <div class="profile-wrapper">
    <img src="<?php echo URL_BASE.$foto; ?>"  alt="" data-src="<?php echo URL_BASE.$foto; ?>" data-src-retina="<?php echo URL_BASE.$foto; ?>" width="69" height="69" />
  </div>
  <div class="user-info">
    <div class="greeting"><?php echo JrTexto::_('Bienvenido');?></div>
    <div class="username"><?php echo @$this->persona["apellido_1"]." ".@$this->persona["apellido_2"].",".@$this->persona["nombres"];?></div>
    <!--div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div-->
  </div>
</div>
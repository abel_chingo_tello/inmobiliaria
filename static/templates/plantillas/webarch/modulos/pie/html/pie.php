<div class="section white footer">
		<div class="container">
			<div class="p-t-30 p-b-50">
				<div class="row">
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 xs-m-b-20">
					<img src="assets/img/logo_condensed.png" alt="" data-src="assets/img/logo_condensed.png" data-src-retina="assets/img/logo2x.png" width="119" height="22"/>
					</div>
					<div class="col-md-4 col-lg-3 col-sm-4  col-xs-12 xs-m-b-20">
						<address class="xs-no-padding  col-md-6 col-lg-6 col-sm-6  col-xs-12">	
							<?php echo @$this->empresa["direccion"]; ?>
						</address>
						<div class="xs-no-padding col-md-6 col-lg-6 col-sm-6">
							<div><?php echo @$this->empresa["telefono"]; ?></div>
							<?php echo @$this->empresa["email"]; ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 xs-m-b-20">
						Copyright © <?php echo date('Y-m-d'); ?><br>
						Privacy Policy<br>
						Design by ING&TAL
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 xs-m-b-20">
					<div class="bold">RECRUITMENT</div>
					We are frequently on the lookout for new talent!
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2  col-xs-12 ">
					<div class="bold">FOLLOW US</div>
					We are frequently on the lookout for new talent!
					</div>
				</div>
			</div>
		</div>
</div>
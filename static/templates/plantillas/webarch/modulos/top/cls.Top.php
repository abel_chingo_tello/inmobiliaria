<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class Top extends JrModulo
{
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();		
		$this->modulo = 'top';		
	}
	
	public function mostrar($html=null)
	{
		try{			
			JrCargador::clase('sys_negocio::NegEmpresa', RUTA_BASE);
			$oNegEmpresa = new Negempresa;	
			$this->empresa=$oNegEmpresa->getconfiguracion();
			$this->persona=NegSesion::getUsuario();			
			$this->esquema = 'top';	
			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}
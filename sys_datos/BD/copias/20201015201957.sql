/*
MySQL Backup
Database: abel_all
Backup Time: 2020-10-15 20:19:58
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `abel_all`.`biblioteca`;
DROP TABLE IF EXISTS `abel_all`.`cursos`;
DROP TABLE IF EXISTS `abel_all`.`cursos_capacidades`;
DROP TABLE IF EXISTS `abel_all`.`cursos_categorias`;
DROP TABLE IF EXISTS `abel_all`.`cursos_categorias_empresa`;
DROP TABLE IF EXISTS `abel_all`.`cursos_grupoaula`;
DROP TABLE IF EXISTS `abel_all`.`cursos_matriculas`;
DROP TABLE IF EXISTS `abel_all`.`cursos_temas`;
DROP TABLE IF EXISTS `abel_all`.`empresa`;
DROP TABLE IF EXISTS `abel_all`.`empresa_aulas`;
DROP TABLE IF EXISTS `abel_all`.`empresa_config`;
DROP TABLE IF EXISTS `abel_all`.`empresa_local`;
DROP TABLE IF EXISTS `abel_all`.`empresa_modulo`;
DROP TABLE IF EXISTS `abel_all`.`empresa_smtp`;
DROP TABLE IF EXISTS `abel_all`.`historial_bitacora_alumno_cursos`;
DROP TABLE IF EXISTS `abel_all`.`historial_sesion`;
DROP TABLE IF EXISTS `abel_all`.`modulos`;
DROP TABLE IF EXISTS `abel_all`.`paginas`;
DROP TABLE IF EXISTS `abel_all`.`persona`;
DROP TABLE IF EXISTS `abel_all`.`persona_rol`;
DROP TABLE IF EXISTS `abel_all`.`rol`;
DROP TABLE IF EXISTS `abel_all`.`sys_configuracion`;
DROP TABLE IF EXISTS `abel_all`.`tienda_almacen`;
DROP TABLE IF EXISTS `abel_all`.`tienda_almacen_producto`;
DROP TABLE IF EXISTS `abel_all`.`tienda_categorias`;
DROP TABLE IF EXISTS `abel_all`.`tienda_categorias_producto`;
DROP TABLE IF EXISTS `abel_all`.`tienda_marca`;
DROP TABLE IF EXISTS `abel_all`.`tienda_productos`;
DROP TABLE IF EXISTS `abel_all`.`tienda_productos_especificaciones`;
CREATE TABLE `biblioteca` (
  `idbiblioteca` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Audio,video,pdf,imagen',
  `archivo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbiblioteca`),
  KEY `fk_biblioteca_empresa` (`idempresa`),
  KEY `fk_biblioteca_persona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos` (
  `idcurso` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `autor` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_fondo` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idioma` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '	0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcurso`),
  KEY `fk_cursos_empresa` (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_capacidades` (
  `idcapacidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` tinyint(4) NOT NULL COMMENT '1=capacidad, 2=competencia\r\n3=criterio',
  `idpadre` bigint(20) DEFAULT NULL,
  `idcurso` bigint(20) DEFAULT NULL,
  `orden` tinyint(4) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcapacidad`),
  KEY `fk_cursos_capacidades_empresa` (`idempresa`),
  KEY `fk_cursos_capacidades_cursos` (`idcurso`),
  KEY `index_tipo` (`tipo`) USING BTREE,
  KEY `index_idpadre` (`idpadre`) USING BTREE,
  CONSTRAINT `fk_cursos_capacidades_cursos` FOREIGN KEY (`idcurso`) REFERENCES `cursos` (`idcurso`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_categorias` (
  `idcursocategoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `idcurso` bigint(20) NOT NULL,
  `orden` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcursocategoria`),
  KEY `fk_cursos_categorias_categorias_empresa` (`idcategoria`),
  KEY `fk_cursos_categorias_cursos` (`idcurso`),
  CONSTRAINT `fk_cursos_categorias_categorias_empresa` FOREIGN KEY (`idcategoria`) REFERENCES `cursos_categorias_empresa` (`idcategoria`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_cursos_categorias_cursos` FOREIGN KEY (`idcurso`) REFERENCES `cursos` (`idcurso`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_categorias_empresa` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `idpadre` int(11) DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `orden` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcategoria`),
  KEY `fk_cursos_categorias_empresas` (`idempresa`),
  KEY `index_idpadre` (`idpadre`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_grupoaula` (
  `idgrupoaula` bigint(20) NOT NULL AUTO_INCREMENT,
  `anio` int(4) NOT NULL,
  `tipo` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'P=Presencial\r\nV=virtual\r\nM=Mixto',
  `idcurso` bigint(20) NOT NULL,
  `iddocente` bigint(20) DEFAULT NULL,
  `idlocal` int(11) DEFAULT NULL,
  `idaula` int(11) DEFAULT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `nivel` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgrado` int(11) DEFAULT NULL,
  `idseccion` int(11) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idgrupoaula`),
  KEY `index_tipo` (`tipo`) USING BTREE,
  KEY `fk_grupoaula_cursos` (`idcurso`),
  KEY `fk_docente_persona` (`iddocente`),
  CONSTRAINT `fk_grupoaula_cursos` FOREIGN KEY (`idcurso`) REFERENCES `cursos` (`idcurso`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_matriculas` (
  `idmatricula` bigint(20) NOT NULL AUTO_INCREMENT,
  `idgrupoaula` bigint(20) NOT NULL,
  `idalumno` bigint(20) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_termino` date NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmatricula`),
  KEY `fk_matricula_grupoaula` (`idgrupoaula`),
  KEY `fk_alumno_persona` (`idalumno`),
  CONSTRAINT `fk_matricula_grupoaula` FOREIGN KEY (`idgrupoaula`) REFERENCES `cursos_grupoaula` (`idgrupoaula`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `cursos_temas` (
  `idtema` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcurso` bigint(20) NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idpadre` bigint(20) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `recurso_id` bigint(20) DEFAULT NULL,
  `recurso_tipo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recurso_url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtema`),
  KEY `fk_temas_cursos` (`idcurso`),
  KEY `fk_temas_subtemas` (`idpadre`),
  CONSTRAINT `fk_temas_cursos` FOREIGN KEY (`idcurso`) REFERENCES `cursos` (`idcurso`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_temas_subtemas` FOREIGN KEY (`idpadre`) REFERENCES `cursos_temas` (`idtema`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa` (
  `idempresa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idioma` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdominio` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '	0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa_aulas` (
  `idaula` bigint(20) NOT NULL AUTO_INCREMENT,
  `idlocal` bigint(20) NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `tipo` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capacidad` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idaula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa_config` (
  `idempresaconfig` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `parametro` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresaconfig`),
  KEY `idempresa` (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa_local` (
  `idlocal` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlocal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa_modulo` (
  `idempresamodulo` bigint(20) NOT NULL AUTO_INCREMENT,
  `idmodulo` int(11) DEFAULT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idrol` int(11) NOT NULL,
  `orden` int(11) DEFAULT '0',
  `idmodulopadre` int(11) DEFAULT NULL,
  `nomtemporal` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresamodulo`),
  KEY `fk_empresa_modulo_empresa` (`idempresa`),
  KEY `fkempresa_modulos_modulos` (`idmodulo`),
  CONSTRAINT `fkempresa_modulos_modulos` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`idmodulo`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `empresa_smtp` (
  `idempresasmtp` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `host` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puerto` int(11) NOT NULL,
  `cifrado` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresasmtp`),
  KEY `fk_modulos_empresa_smtp` (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `historial_bitacora_alumno_cursos` (
  `idbitacora` bigint(20) NOT NULL AUTO_INCREMENT,
  `idgrupoaula` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `menus_total` tinyint(4) NOT NULL DEFAULT '1',
  `menu_idtemapadre` bigint(20) DEFAULT NULL,
  `menu_idtema` bigint(20) DEFAULT NULL,
  `progreso` int(3) NOT NULL DEFAULT '0',
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbitacora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `historial_sesion` (
  `idhistorial` bigint(20) NOT NULL AUTO_INCREMENT,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navegador` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgrupoaula` bigint(20) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lugar` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `modulos` (
  `idmodulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `estado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '	0=Inactivo,1,Activo,-1=eliminado',
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icono` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmodulo`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `paginas` (
  `idpagina` int(11) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `titulo_enmenu` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `titulo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(11) NOT NULL DEFAULT '1',
  `enmenu` tinyint(11) NOT NULL DEFAULT '0',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpagina`),
  KEY `fk_paginas_empresa` (`idempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `persona` (
  `idpersona` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_documento` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'D',
  `num_documento` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verclave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tocken` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_usuario` enum('s','n') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 's=superadmin\r\nn: normal',
  `telefono` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ubigeo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) DEFAULT NULL,
  `fecha_insert` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `persona_rol` (
  `idpersonarol` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpersonarol`),
  KEY `fk_persona_rol_persona` (`idpersona`),
  KEY `fk_persona_rol_rol` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idempresa` bigint(20) DEFAULT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `sys_configuracion` (
  `config` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autocargar` enum('si','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_almacen` (
  `idalmacen` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dirección` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idalmacen`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_almacen_producto` (
  `idtiendaalmacen` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `idalmacen` bigint(20) NOT NULL,
  `idproducto` bigint(20) NOT NULL,
  `unidad_medida` int(11) NOT NULL,
  `cantidad_ingreso` double NOT NULL,
  `stock` int(11) NOT NULL,
  `precio_venta` double NOT NULL,
  `precio_oferta` double NOT NULL,
  `precio_antes` double NOT NULL,
  `precio_compra` double NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `observacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtiendaalmacen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_categorias` (
  `idcategoria` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `imagen` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icono` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` tinyint(4) DEFAULT '1',
  `idpadre` bigint(20) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_categorias_producto` (
  `idcategoriaproducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcategoria` bigint(20) NOT NULL,
  `idproducto` bigint(20) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcategoriaproducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_marca` (
  `idmarca` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmarca`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_productos` (
  `idproducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `idmarca` bigint(11) DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permalink` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `características` text COLLATE utf8mb4_unicode_ci,
  `orden` int(11) DEFAULT NULL,
  `imagen1` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen2` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen3` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen4` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen5` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(11) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idproducto`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tienda_productos_especificaciones` (
  `idproductoespecificacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `idproducto` bigint(20) NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` tinyint(4) DEFAULT NULL,
  `estado` tinyint(4) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idproductoespecificacion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
BEGIN;
LOCK TABLES `abel_all`.`biblioteca` WRITE;
DELETE FROM `abel_all`.`biblioteca`;
INSERT INTO `abel_all`.`biblioteca` (`idbiblioteca`,`idempresa`,`idpersona`,`tipo`,`archivo`,`nombre`,`estado`,`fecha_insert`,`fecha_update`) VALUES (72, 1, 1, 'image', 'static/media/E_1/U1//biblioteca.jpg', '/biblioteca.jpg', -1, '2020-10-03 17:10:30', '2020-10-03 20:40:05'),(73, 1, 1, 'image', 'static/media/E_1/U1//biblioteca.jpg', '/biblioteca.jpg', -1, '2020-10-03 20:33:15', '2020-10-03 20:40:07'),(74, 1, 1, 'image', 'static/media/E_1/U1//biblioteca.jpg', '/biblioteca.jpg', 1, '2020-10-03 20:33:35', '2020-10-03 20:33:35'),(75, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//asd20201003203925.png', 'asd20201003203925.png', 1, '2020-10-03 20:39:25', '2020-10-03 20:39:25'),(76, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//user_avatar20201003204042.jpg', 'user_avatar20201003204042.jpg', -1, '2020-10-03 20:40:42', '2020-10-03 20:40:47'),(77, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//user_avatar20201003204101.jpg', 'user_avatar20201003204101.jpg', 1, '2020-10-03 20:41:01', '2020-10-03 20:41:01'),(78, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a220201004133638.png', 'a220201004133638.png', 1, '2020-10-04 13:36:38', '2020-10-04 13:36:38'),(79, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a120201005211852.png', 'a120201005211852.png', 1, '2020-10-05 21:18:52', '2020-10-05 21:18:52'),(80, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a120201007175308.png', 'a120201007175308.png', 1, '2020-10-07 17:53:08', '2020-10-07 17:53:08'),(81, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a120201007185305.png', 'a120201007185305.png', 1, '2020-10-07 18:53:05', '2020-10-07 18:53:05'),(82, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a120201007211255.png', 'a120201007211255.png', 1, '2020-10-07 21:12:55', '2020-10-07 21:12:55'),(83, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a220201007214632.png', 'a220201007214632.png', 1, '2020-10-07 21:46:32', '2020-10-07 21:46:32'),(84, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//c120201007214643.png', 'c120201007214643.png', 1, '2020-10-07 21:46:43', '2020-10-07 21:46:43'),(85, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//a120201007214747.png', 'a120201007214747.png', 1, '2020-10-07 21:47:47', '2020-10-07 21:47:47'),(86, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//nofoto20201007215215.jpg', 'nofoto20201007215215.jpg', 1, '2020-10-07 21:52:15', '2020-10-07 21:52:15'),(87, 1, 1, 'image', 'static/media/E_1/U1/biblioteca//b120201007215233.png', 'b120201007215233.png', 1, '2020-10-07 21:52:33', '2020-10-07 21:52:33'),(88, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/b220201007220549.png', 'b220201007220549.png', -1, '2020-10-07 22:05:49', '2020-10-07 22:42:20'),(89, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/a120201007220917.png', 'a120201007220917.png', -1, '2020-10-07 22:09:17', '2020-10-07 22:42:22'),(90, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/a220201007221328.png', 'a220201007221328.png', 1, '2020-10-07 22:13:28', '2020-10-07 22:13:28'),(91, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/nofoto20201007221621.jpg', 'nofoto20201007221621.jpg', -1, '2020-10-07 22:16:21', '2020-10-07 22:40:47');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos` WRITE;
DELETE FROM `abel_all`.`cursos`;
INSERT INTO `abel_all`.`cursos` (`idcurso`,`idempresa`,`nombre`,`autor`,`imagen`,`color_fondo`,`idioma`,`descripcion`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 'asdasd', 'EDUMAX', 'static/media/E_1/U1/cursos/imagen20201012145949.png', 'rgba(247,114,96,1)', 'ES', 'asdasd', 1, 1, '2020-10-12 14:34:56', '2020-10-12 17:03:37'),(2, 1, 'asdasd', 'asdasd', 'static/media/E_1/U1/cursos/imagen20201012150033.png', 'rgba(242,123,36,1)', 'ES', 'asdasd', 0, 1, '2020-10-12 14:50:18', '2020-10-12 15:02:26');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_capacidades` WRITE;
DELETE FROM `abel_all`.`cursos_capacidades`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_categorias` WRITE;
DELETE FROM `abel_all`.`cursos_categorias`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_categorias_empresa` WRITE;
DELETE FROM `abel_all`.`cursos_categorias_empresa`;
INSERT INTO `abel_all`.`cursos_categorias_empresa` (`idcategoria`,`idempresa`,`idpadre`,`nombre`,`imagen`,`descripcion`,`estado`,`orden`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, NULL, NULL, 'Libres', 'static/media/E_1/categorias/U1/imagen20201010142410.png', '', 1, 0, 1, '2020-10-03 08:33:48', '2020-10-10 15:41:52'),(2, 1, NULL, 'Ofimática', 'static/media/E_1/categorias/U1/imagen20201010154211.png', '', 1, 1, 1, '2020-10-10 14:19:21', '2020-10-10 15:42:11');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_grupoaula` WRITE;
DELETE FROM `abel_all`.`cursos_grupoaula`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_matriculas` WRITE;
DELETE FROM `abel_all`.`cursos_matriculas`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`cursos_temas` WRITE;
DELETE FROM `abel_all`.`cursos_temas`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa` WRITE;
DELETE FROM `abel_all`.`empresa`;
INSERT INTO `abel_all`.`empresa` (`idempresa`,`nombre`,`direccion`,`telefono`,`correo`,`logo`,`ruc`,`idioma`,`subdominio`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 'INGYTAL', 'chiclayo2', '987654321', 'info@ingytal.com', 'static/media/E_1/U1/empresa/logo20201008095907.png', '20145698713', 'ES', 'all', 1, 1, '2020-08-08 20:52:07', '2020-10-08 09:59:07'),(2, 'empresa1', 'chiclayo', '987654321', 'info@ingytal.com', 'static/media/E_2/U1/empresa/logo.png', '20145698713', 'ES', 'empresa1', 1, 1, '2020-08-08 20:52:07', '2020-10-13 16:18:58');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa_aulas` WRITE;
DELETE FROM `abel_all`.`empresa_aulas`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa_config` WRITE;
DELETE FROM `abel_all`.`empresa_config`;
INSERT INTO `abel_all`.`empresa_config` (`idempresaconfig`,`idempresa`,`parametro`,`valor`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, NULL, 'color_fondo_login', '#f6f7f8', 1, 1, '2020-10-04 09:17:36', '2020-10-04 09:21:13'),(2, NULL, 'imagen_fondo_login', '-', 1, 1, '2020-10-04 09:19:12', '2020-10-04 09:47:35'),(3, 1, 'color_fondo_login', '#dddddd', 1, 1, '2020-10-04 13:26:23', '2020-10-08 11:22:43'),(4, 1, 'imagen_fondo_login', 'static/media/E_1/login/imagen_fondo_login20201008111334.png', 1, 1, '2020-10-04 13:29:21', '2020-10-08 11:13:34'),(5, 2, 'color_fondo_login', 'rgba(221,221,221,1)', 1, 1, '2020-10-08 11:23:48', '2020-10-08 11:23:48'),(6, 2, 'idempresa', '2', 1, 1, '2020-10-08 11:23:48', '2020-10-08 11:23:48'),(7, 2, 'imagen_fondo_login', 'static/media/E_2/login/imagen_fondo_login20201013161851.png', 1, 1, '2020-10-08 11:24:44', '2020-10-13 16:18:51'),(8, 1, 'tipo_empresa', 'tienda', 1, 1, '2020-10-13 15:25:39', '2020-10-13 15:27:38'),(9, 1, 'valor', 'tienda', 1, 1, '2020-10-13 15:25:39', '2020-10-13 15:26:15'),(10, 1, 'estado', '1', 1, 1, '2020-10-13 15:25:39', '2020-10-13 15:25:39'),(11, 1, 'idempresa', '1', 1, 1, '2020-10-13 15:34:00', '2020-10-13 15:34:00'),(12, 2, 'tipo_empresa', 'page', 1, 1, '2020-10-13 16:18:51', '2020-10-13 16:18:51');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa_local` WRITE;
DELETE FROM `abel_all`.`empresa_local`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa_modulo` WRITE;
DELETE FROM `abel_all`.`empresa_modulo`;
INSERT INTO `abel_all`.`empresa_modulo` (`idempresamodulo`,`idmodulo`,`idempresa`,`estado`,`idrol`,`orden`,`idmodulopadre`,`nomtemporal`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 1, 1, 1, 1, NULL, NULL, 1, '2020-08-08 21:50:07', '2020-10-10 11:45:28'),(3, 17, 1, 1, 1, 3, NULL, NULL, 1, '2020-10-09 09:15:29', '2020-10-12 13:52:39'),(4, NULL, 1, 1, 1, 4, NULL, 'eyJub21icmUiOiJBZG1pbmlzdHJhY2nzbiIsImljb25vIjoiZmEtZm9sZGVyLW9wZW4ifQ==', 1, '2020-10-09 11:50:27', '2020-10-12 13:52:39'),(8, 20, 1, 1, 1, 1, 14, NULL, 1, '2020-10-10 08:57:57', '2020-10-10 12:32:43'),(9, 21, 1, 1, 1, 2, 14, NULL, 1, '2020-10-10 08:58:20', '2020-10-10 12:32:43'),(11, NULL, 1, 1, 1, 1, 4, 'eyJub21icmUiOiJjdXJzb3MiLCJpY29ubyI6ImZhLWZvbGRlci1vcGVuIn0=', 1, '2020-10-10 11:35:22', '2020-10-12 13:50:08'),(12, 23, 1, 1, 1, 1, 11, NULL, 1, '2020-10-10 12:28:07', '2020-10-12 13:50:03'),(13, 24, 1, 1, 1, 2, 11, NULL, 1, '2020-10-10 12:28:13', '2020-10-12 13:52:11'),(14, NULL, 1, 1, 1, 2, 4, 'eyJub21icmUiOiJN82R1bG9zIiwiaWNvbm8iOiJmYS1mb2xkZXItb3BlbiJ9', 1, '2020-10-10 12:31:43', '2020-10-12 13:52:11'),(16, 25, 1, 1, 1, 2, NULL, NULL, 1, '2020-10-12 13:52:06', '2020-10-12 13:52:10'),(17, NULL, 1, 1, 1, 5, NULL, 'eyJub21icmUiOiJUaWVuZGFzIiwiaWNvbm8iOiJmYS1zaG9wcGluZy1iYWcifQ==', 1, '2020-10-13 10:29:17', '2020-10-13 10:29:17'),(18, 26, 1, 1, 1, 1, 17, NULL, 1, '2020-10-13 10:29:36', '2020-10-13 10:30:07'),(19, 17, 2, 1, 1, 999, NULL, NULL, 1, '2020-10-13 16:18:12', '2020-10-13 16:18:12'),(20, 27, 1, 1, 1, 3, 17, NULL, 1, '2020-10-15 12:38:15', '2020-10-15 12:38:46'),(21, 28, 1, 1, 1, 2, 17, NULL, 1, '2020-10-15 12:38:24', '2020-10-15 12:38:46'),(22, 29, 1, 1, 1, 4, 17, NULL, 1, '2020-10-15 12:40:39', '2020-10-15 12:40:49');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`empresa_smtp` WRITE;
DELETE FROM `abel_all`.`empresa_smtp`;
INSERT INTO `abel_all`.`empresa_smtp` (`idempresasmtp`,`idempresa`,`host`,`email`,`clave`,`puerto`,`cifrado`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, '179.6.46.227', 'abelchingo@gmail.com', 'asdasd', 6, 'tls', 1, 1, '2020-10-04 13:45:02', '2020-10-04 14:02:26'),(2, 2, '179.6.46.227', 'abelchingo@gmail.com', 'asdasd', 1, 'tls', 1, 1, '2020-10-08 11:29:42', '2020-10-08 11:29:42');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`historial_bitacora_alumno_cursos` WRITE;
DELETE FROM `abel_all`.`historial_bitacora_alumno_cursos`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`historial_sesion` WRITE;
DELETE FROM `abel_all`.`historial_sesion`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`modulos` WRITE;
DELETE FROM `abel_all`.`modulos`;
INSERT INTO `abel_all`.`modulos` (`idmodulo`,`nombre`,`descripcion`,`estado`,`url`,`icono`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 'Mis cursos', 'Pagina Web principal asasd', 1, 'admin/cursos', 'fa-book', 1, '2020-08-08 21:48:45', '2020-10-12 13:38:38'),(13, 'Reportes', '', 0, '#', 'fa-chart-line', 1, '2020-10-03 23:39:19', '2020-10-07 17:23:30'),(14, 'Exámenes', NULL, -1, '#', 'fa-list', 1, '2020-10-03 23:40:43', '2020-10-05 23:09:50'),(15, 'Académico', NULL, -1, '#', 'fa-graduation-cap', 1, '2020-10-03 23:41:28', '2020-10-05 23:08:40'),(16, 'Empresa', '', 0, '#', 'fa-config', 1, '2020-10-04 08:41:00', '2020-10-07 17:23:28'),(17, 'Configuracion', 'Parámetros de configuración de empresa', 1, 'admin/empresa/configuracion', 'fa-building-o', 1, '2020-10-04 08:43:41', '2020-10-07 17:23:24'),(18, 'asdasd', 'asdasdasd', -1, 'asdasd', 'fa-book', 1, '2020-10-07 16:32:31', '2020-10-07 17:23:07'),(19, 'asd', 'asdas', 1, 'asd', 'asd', 1, '2020-10-08 15:32:34', '2020-10-08 15:32:34'),(20, 'módulos', '', 1, 'admin/modulos', 'fa-pencil', 1, '2020-10-10 08:56:24', '2020-10-10 12:31:14'),(21, 'Asignación de módulos', '', 1, 'admin/empresa/modulos', 'fa-pencil', 1, '2020-10-10 08:56:49', '2020-10-10 12:31:08'),(22, 'perfil', NULL, 1, 'admin/persona/perfil', 'fa-pencil', 1, '2020-10-10 12:01:13', '2020-10-10 12:01:13'),(23, 'Categorias', '', 1, 'admin/cursos_categorias_empresa', 'fa-pencil', 1, '2020-10-10 12:26:54', '2020-10-10 12:48:19'),(24, 'Asignación de categorías', NULL, 1, 'admin/curso_categorias', 'fa-pencil', 1, '2020-10-10 12:27:34', '2020-10-10 12:27:34'),(25, 'Crear', NULL, 1, 'admin/cursos/formulario', 'fa-book', 1, '2020-10-12 13:44:11', '2020-10-12 13:44:11'),(26, 'Categorias', '', 1, '/admin/tienda_categorias', 'fa-address-book', 1, '2020-10-13 10:28:48', '2020-10-13 10:30:55'),(27, 'Marcas', '', 1, 'admin/tienda_marca', 'fa-address-book', 1, '2020-10-15 12:36:48', '2020-10-15 12:42:57'),(28, 'Almacenes', '', 1, 'admin/tienda_almacen', 'fa-address-book', 1, '2020-10-15 12:37:34', '2020-10-15 12:43:35'),(29, 'Productos', '', 1, 'admin/tienda_productos', 'fa-address-book', 1, '2020-10-15 12:40:21', '2020-10-15 12:43:52');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`paginas` WRITE;
DELETE FROM `abel_all`.`paginas`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`persona` WRITE;
DELETE FROM `abel_all`.`persona`;
INSERT INTO `abel_all`.`persona` (`idpersona`,`tipo_documento`,`num_documento`,`apellido_1`,`apellido_2`,`nombres`,`usuario`,`email`,`clave`,`verclave`,`tocken`,`foto`,`genero`,`fecha_nacimiento`,`estado_civil`,`tipo_usuario`,`telefono`,`ubigeo`,`direccion`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 'D', '14253645', 'Abel', 'Chingo', 'Tello', 'abelchingo', 'abelchingo@gmail.com', 'abf333f2019d70ac7da2ccbcb08a489d', 'abelchingo@', 'dc51d5fbcbf47927901cf3cc11ee2f28', 'static/media/E_1/persona/U1/foto.png', 'M', '2013-07-01', 'S', 's', '942171837', '010101', 'los lirios Mz. D Lote 6', 1, 1, '2020-08-08 20:55:39', '2020-10-12 19:33:05'),(2, 'D', '100000', 'demo', 'demo', 'demo', 'demo', 'abelchingo@gmail.com', '62cc2d8b4bf2d8728120d052163a77df', 'demo123', 'b9d02ff52acc9f0e2707bd616f3ac676', 'static/media/E_1/U1/persona/foto20201003175507.png', 'M', '2020-10-01', 'S', 'n', '979709503', '', 'direccion 01', 1, 1, '2020-10-03 17:24:53', '2020-10-03 21:27:50');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`persona_rol` WRITE;
DELETE FROM `abel_all`.`persona_rol`;
INSERT INTO `abel_all`.`persona_rol` (`idpersonarol`,`idempresa`,`idpersona`,`idrol`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 1, 1, 1, '2020-08-09 11:25:51', '2020-08-09 11:25:51');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`rol` WRITE;
DELETE FROM `abel_all`.`rol`;
INSERT INTO `abel_all`.`rol` (`idrol`,`nombre`,`idempresa`,`imagen`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 'Administrador', 0, 'static/media/E_1/U1/rol/imagen20201004152420.png', 1, 1, '2020-08-09 11:24:39', '2020-10-04 15:24:21'),(2, 'Docente', 1, NULL, 1, 1, '2020-10-03 12:54:17', '2020-10-04 14:49:06'),(3, 'Alumno', 1, NULL, 1, 1, '2020-10-03 12:54:34', '2020-10-04 14:49:13'),(4, 'yyy', 1, NULL, 1, 1, '2020-10-04 15:17:29', '2020-10-04 15:17:29');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`sys_configuracion` WRITE;
DELETE FROM `abel_all`.`sys_configuracion`;
INSERT INTO `abel_all`.`sys_configuracion` (`config`,`valor`,`autocargar`) VALUES ('multiidioma', 'SI', 'si');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_almacen` WRITE;
DELETE FROM `abel_all`.`tienda_almacen`;
INSERT INTO `abel_all`.`tienda_almacen` (`idalmacen`,`idempresa`,`nombre`,`dirección`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 'unico', '.', 1, 1, '2020-10-15 13:00:08', '2020-10-15 13:00:08');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_almacen_producto` WRITE;
DELETE FROM `abel_all`.`tienda_almacen_producto`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_categorias` WRITE;
DELETE FROM `abel_all`.`tienda_categorias`;
INSERT INTO `abel_all`.`tienda_categorias` (`idcategoria`,`idempresa`,`nombre`,`permalink`,`descripcion`,`imagen`,`icono`,`orden`,`idpadre`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, NULL, 'Tecnólogia', 'tecnologia', '', 'static/media/E_1/U1/tienda_categorias/imagen.png', 'fa-tv', 1, NULL, 1, 1, '2020-10-13 10:34:45', '2020-10-13 20:27:38'),(2, NULL, 'Electrohogar', 'electrohogar', NULL, NULL, 'fa-adjust', 2, NULL, 1, 1, '2020-10-13 12:56:41', '2020-10-13 20:27:53'),(3, NULL, 'Muebles', 'muebles', NULL, NULL, NULL, 3, NULL, 1, 1, '2020-10-13 12:56:54', '2020-10-13 20:27:50'),(4, NULL, 'Televisores', 'televisores', NULL, NULL, NULL, 1, 1, 1, 1, '2020-10-13 12:57:43', '2020-10-13 20:27:47'),(5, NULL, 'Cómputo', 'computo', NULL, NULL, NULL, 2, 1, 1, 1, '2020-10-13 12:58:15', '2020-10-13 20:27:44'),(6, NULL, 'Smart TV', 'smart-tv', NULL, NULL, NULL, 1, 4, 1, 1, '2020-10-13 12:59:10', '2020-10-13 20:27:42');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_categorias_producto` WRITE;
DELETE FROM `abel_all`.`tienda_categorias_producto`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_marca` WRITE;
DELETE FROM `abel_all`.`tienda_marca`;
INSERT INTO `abel_all`.`tienda_marca` (`idmarca`,`idempresa`,`nombre`,`permalink`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 'unica', 'unica', 1, 1, '2020-10-15 15:17:20', '2020-10-15 15:17:20');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_productos` WRITE;
DELETE FROM `abel_all`.`tienda_productos`;
INSERT INTO `abel_all`.`tienda_productos` (`idproducto`,`idempresa`,`idmarca`,`nombre`,`permalink`,`descripcion`,`características`,`orden`,`imagen1`,`imagen2`,`imagen3`,`imagen4`,`imagen5`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, NULL, 'sdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 1, NULL, NULL, NULL, NULL, NULL, 1, '2020-10-15 15:40:23', '2020-10-15 15:40:23');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `abel_all`.`tienda_productos_especificaciones` WRITE;
DELETE FROM `abel_all`.`tienda_productos_especificaciones`;
INSERT INTO `abel_all`.`tienda_productos_especificaciones` (`idproductoespecificacion`,`idproducto`,`nombre`,`valor`,`orden`,`estado`,`usuario_registro`,`fecha_insert`,`fecha_update`) VALUES (1, 1, 'color', 'negro', 1, 1, 1, '2020-10-15 16:00:30', '2020-10-15 16:00:30');
UNLOCK TABLES;
COMMIT;

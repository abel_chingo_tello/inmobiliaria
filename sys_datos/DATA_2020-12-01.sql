-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2020 a las 04:58:54
-- Versión del servidor: 5.7.11
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abel_sbch`
--
CREATE DATABASE IF NOT EXISTS `abel_sbch` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `abel_sbch`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biblioteca`
--

DROP TABLE IF EXISTS `biblioteca`;
CREATE TABLE IF NOT EXISTS `biblioteca` (
  `idbiblioteca` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `tipo` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Audio,video,pdf,imagen',
  `archivo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbiblioteca`),
  KEY `fk_biblioteca_empresa` (`idempresa`),
  KEY `fk_biblioteca_persona` (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `biblioteca`
--

INSERT INTO `biblioteca` (`idbiblioteca`, `idempresa`, `idpersona`, `tipo`, `archivo`, `nombre`, `estado`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 'image', 'static/media/E_1/U1/biblioteca/buscar20201201231408.png', 'buscar20201201231408.png', 1, '2020-12-01 23:14:08', '2020-12-01 23:14:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `idempresa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruc` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idioma` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdominio` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '	0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`idempresa`, `nombre`, `direccion`, `telefono`, `correo`, `logo`, `ruc`, `idioma`, `subdominio`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Serfin', 'chiclayo2', '987654321', 'info@ingytal.com', 'static/media/E_1/U1/empresa/logo20201008095907.png', '20145698713', 'ES', 'all', 1, 1, '2020-08-08 20:52:07', '2020-12-01 23:48:41'),
(2, 'InMobiliario', '....', '9999999', 'asdasd@correo.com', '', '31235456', 'ES', '.', 1, 1, '2020-12-01 23:49:59', '2020-12-01 23:49:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_config`
--

DROP TABLE IF EXISTS `empresa_config`;
CREATE TABLE IF NOT EXISTS `empresa_config` (
  `idempresaconfig` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `parametro` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresaconfig`),
  KEY `idempresa` (`idempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_config`
--

INSERT INTO `empresa_config` (`idempresaconfig`, `idempresa`, `parametro`, `valor`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'color_fondo_login', '#f6f7f8', 1, 1, '2020-10-04 09:17:36', '2020-10-04 09:21:13'),
(2, 1, 'imagen_fondo_login', '', 1, 1, '2020-10-04 09:19:12', '2020-10-04 09:47:35'),
(3, 1, 'tipo_empresa', 'tienda', 1, 2, '2020-10-13 15:25:39', '2020-10-23 20:52:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_modulo`
--

DROP TABLE IF EXISTS `empresa_modulo`;
CREATE TABLE IF NOT EXISTS `empresa_modulo` (
  `idempresamodulo` bigint(20) NOT NULL AUTO_INCREMENT,
  `idmodulo` int(11) DEFAULT NULL,
  `idempresa` bigint(20) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idrol` int(11) NOT NULL,
  `orden` int(11) DEFAULT '0',
  `idmodulopadre` int(11) DEFAULT NULL,
  `nomtemporal` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresamodulo`),
  KEY `fk_empresa_modulo_empresa` (`idempresa`),
  KEY `fkempresa_modulos_modulos` (`idmodulo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa_modulo`
--

INSERT INTO `empresa_modulo` (`idempresamodulo`, `idmodulo`, `idempresa`, `estado`, `idrol`, `orden`, `idmodulopadre`, `nomtemporal`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, NULL, 1, 1, 1, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2dzIn0=', 1, '2020-12-01 23:27:23', '2020-12-01 23:27:46'),
(2, 3, 1, 1, 1, 2, 1, NULL, 1, '2020-12-01 23:27:36', '2020-12-01 23:30:02'),
(3, 4, 1, 1, 1, 3, 1, NULL, 1, '2020-12-01 23:27:57', '2020-12-01 23:30:02'),
(4, 2, 1, 1, 1, 1, 1, NULL, 1, '2020-12-01 23:29:54', '2020-12-01 23:30:02'),
(5, NULL, 2, 1, 2, 1, NULL, 'eyJub21icmUiOiJDb25maWd1cmFjafNuIiwiaWNvbm8iOiJmYS1jb2cifQ==', 1, '2020-12-01 23:54:48', '2020-12-01 23:55:48'),
(6, 2, 2, 1, 2, 1, 5, NULL, 1, '2020-12-01 23:54:57', '2020-12-01 23:55:48'),
(7, 3, 2, 1, 2, 2, 5, NULL, 1, '2020-12-01 23:55:18', '2020-12-01 23:55:48'),
(8, 4, 2, 1, 2, 3, 5, NULL, 1, '2020-12-01 23:55:26', '2020-12-01 23:55:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_smtp`
--

DROP TABLE IF EXISTS `empresa_smtp`;
CREATE TABLE IF NOT EXISTS `empresa_smtp` (
  `idempresasmtp` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `host` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puerto` int(11) NOT NULL,
  `cifrado` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado	',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idempresasmtp`),
  KEY `fk_modulos_empresa_smtp` (`idempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_sesion`
--

DROP TABLE IF EXISTS `historial_sesion`;
CREATE TABLE IF NOT EXISTS `historial_sesion` (
  `idhistorial` bigint(20) NOT NULL AUTO_INCREMENT,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idempresa` bigint(20) NOT NULL,
  `ip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navegador` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idgrupoaula` bigint(20) DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_final` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lugar` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE IF NOT EXISTS `modulos` (
  `idmodulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `estado` tinyint(1) NOT NULL DEFAULT '1' COMMENT '	0=Inactivo,1,Activo,-1=eliminado',
  `url` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icono` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmodulo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`idmodulo`, `nombre`, `descripcion`, `estado`, `url`, `icono`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'Empresa', '', 0, '#', 'fa-config', 1, '2020-10-04 08:41:00', '2020-10-07 17:23:28'),
(2, 'Configuracion', 'Parámetros de configuración de empresa', 1, 'admin/empresa/configuracion', 'fa-building-o', 1, '2020-10-04 08:43:41', '2020-10-07 17:23:24'),
(3, 'Módulos', '', 1, 'admin/modulos', 'fa-tags', 1, '2020-10-10 08:56:24', '2020-12-01 23:29:09'),
(4, 'Asignación de módulos', '', 1, 'admin/empresa/modulos', 'fa-tags', 1, '2020-10-10 08:56:49', '2020-12-01 23:29:29'),
(5, 'Perfil', '', 1, 'admin/persona/perfil', 'fa-pencil', 2, '2020-10-10 12:01:13', '2020-10-17 02:40:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `idpersona` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_documento` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'D',
  `num_documento` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verclave` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tocken` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado_civil` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_usuario` enum('s','n') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 's=superadmin\r\nn: normal',
  `telefono` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ubigeo` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(4) DEFAULT '1' COMMENT '0=Inactivo,1,Activo,-1=eliminado',
  `usuario_registro` bigint(20) DEFAULT NULL,
  `fecha_insert` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpersona`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `tipo_documento`, `num_documento`, `apellido_1`, `apellido_2`, `nombres`, `usuario`, `email`, `clave`, `verclave`, `tocken`, `foto`, `genero`, `fecha_nacimiento`, `estado_civil`, `tipo_usuario`, `telefono`, `ubigeo`, `direccion`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 'D', '14253645', 'Abel', 'Chingo', 'Tello', 'abelchingo', 'abelchingo@gmail.com', 'abf333f2019d70ac7da2ccbcb08a489d', 'abelchingo@', 'dc51d5fbcbf47927901cf3cc11ee2f28', 'static/media/E_1/persona/U1/foto.png', 'M', '2013-07-01', 'S', 's', '942171837', '010101', 'los lirios Mz. D Lote 6', 1, 1, '2020-08-08 20:55:39', '2020-10-12 19:33:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona_rol`
--

DROP TABLE IF EXISTS `persona_rol`;
CREATE TABLE IF NOT EXISTS `persona_rol` (
  `idpersonarol` bigint(20) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) NOT NULL,
  `idpersona` bigint(20) NOT NULL,
  `idrol` int(11) NOT NULL,
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpersonarol`),
  KEY `fk_persona_rol_persona` (`idpersona`),
  KEY `fk_persona_rol_rol` (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona_rol`
--

INSERT INTO `persona_rol` (`idpersonarol`, `idempresa`, `idpersona`, `idrol`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 1, 1, 1, '2020-08-09 11:25:51', '2020-08-09 11:25:51'),
(2, 2, 1, 2, 1, '2020-12-01 23:56:51', '2020-12-01 23:56:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

DROP TABLE IF EXISTS `rol`;
CREATE TABLE IF NOT EXISTS `rol` (
  `idrol` int(11) NOT NULL AUTO_INCREMENT,
  `idempresa` bigint(20) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `usuario_registro` bigint(20) NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idrol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `idempresa`, `nombre`, `imagen`, `estado`, `usuario_registro`, `fecha_insert`, `fecha_update`) VALUES
(1, 1, 'Administrador', 'static/media/E_1/U1/rol/imagen20201004152420.png', 1, 1, '2020-08-09 11:24:39', '2020-12-01 23:53:48'),
(2, 2, 'Administrador', NULL, 1, 1, '2020-12-01 23:54:12', '2020-12-01 23:54:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sys_configuracion`
--

DROP TABLE IF EXISTS `sys_configuracion`;
CREATE TABLE IF NOT EXISTS `sys_configuracion` (
  `config` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autocargar` enum('si','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sys_configuracion`
--

INSERT INTO `sys_configuracion` (`config`, `valor`, `autocargar`) VALUES
('multiidioma', 'SI', 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tester_demo`
--

DROP TABLE IF EXISTS `tester_demo`;
CREATE TABLE IF NOT EXISTS `tester_demo` (
  `idtest` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `textarea` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idpadre` int(11) DEFAULT NULL,
  `orden` tinyint(4) NOT NULL DEFAULT '1',
  `imagen` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_insert` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtest`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tester_demo`
--

INSERT INTO `tester_demo` (`idtest`, `nombre`, `textarea`, `color`, `estado`, `idpadre`, `orden`, `imagen`, `fecha_insert`, `fecha_update`) VALUES
(1, 'asdasd', 'asdasdasd', 'rgba(198,38,38,0.59)', 1, NULL, 1, 'static/media/E_1/U1/tester_demo/imagen.png', '2020-12-01 23:14:38', '2020-12-01 23:14:38');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `empresa_modulo`
--
ALTER TABLE `empresa_modulo`
  ADD CONSTRAINT `fkempresa_modulos_modulos` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`idmodulo`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

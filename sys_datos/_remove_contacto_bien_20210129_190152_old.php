<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatContacto_bien extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT id,rebi_id,email,nombre,telefono,dni,mensaje,fecha_insert,fecha_update FROM contacto_bien";
			$cond = array();
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(isset($filtros["mensaje"])) {
					$cond[] = "mensaje = " . $this->oBD->escapar($filtros["mensaje"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM contacto_bien";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";	
			//echo $sql;		
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
		
	public function insertar($rebi_id,$email,$nombre,$telefono,$dni,$mensaje)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM contacto_bien ORDER BY id DESC limit 0,1 ");			
			$estados = array('rebi_id'=>$rebi_id
							,'email'=>$email
							,'nombre'=>$nombre
							,'telefono'=>$telefono
							,'dni'=>$dni
							,'mensaje'=>$mensaje							
							);			
			return $this->oBD->insert('contacto_bien', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_contacto_bien_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $rebi_id,$email,$nombre,$telefono,$dni,$mensaje)
	{
		try {			
			$estados = array('rebi_id'=>$rebi_id
							,'email'=>$email
							,'nombre'=>$nombre
							,'telefono'=>$telefono
							,'dni'=>$dni
							,'mensaje'=>$mensaje								
							);
			$this->oBD->update('contacto_bien ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('contacto_bien', array('id' => $id));
			else 
				return $this->oBD->update('contacto_bien', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('contacto_bien', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}  

		 
		
}
<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::BD::BDMySQLI',RUTA_BASE);
class DatBase
{ 
	protected $oBD;
	protected $num_regs;
	protected $usuario;
	public function conectar($BD='')
	{
		try {
			if(empty($BD)) $BD=NAME_BD;
			$this->oBD = BDMySQLI::getInstancia(HOST_BD, $BD, USER_BD, PWD_BD);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	
	public function getUser(){
		return $this->oBD->getUser();
	}
	public function getClave(){
		return $this->oBD->getClave();
	}
	public function getHost(){
		return $this->oBD->getHost();
	}
	public function iniciarTransaccion($id_transaccion = '')
	{
		$this->oBD->iniciarTransaccion($id_transaccion);
	}

	public function terminarTransaccion($id_transaccion = '')
	{
		$this->oBD->terminarTransaccion($id_transaccion);
	}

	public function cancelarTransaccion($id_transaccion = '')
	{
		$this->oBD->truncarTransaccion($id_transaccion);
	}

	public function setLimite($desde, $desplazamiento)
	{
		$this->oBD->setLimite($desde, $desplazamiento);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
}

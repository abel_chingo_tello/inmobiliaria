<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatBiblioteca extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idbiblioteca,idempresa,idpersona,tipo,archivo,nombre,estado,fecha_insert,fecha_update FROM biblioteca";
			$cond = array();
			
			if(isset($filtros["idbiblioteca"])) {
					$cond[] = "idbiblioteca = " . $this->oBD->escapar($filtros["idbiblioteca"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["archivo"])) {
					$cond[] = "archivo = " . $this->oBD->escapar($filtros["archivo"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM biblioteca";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$idpersona,$tipo,$archivo,$nombre,$estado)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idbiblioteca+1 FROM biblioteca ORDER BY idbiblioteca DESC limit 0,1 ");						
			$estados = array(							
							'idempresa'=>$idempresa
							,'idpersona'=>$idpersona
							,'tipo'=>$tipo
							,'archivo'=>!empty($archivo)?$archivo:'aa'
							,'nombre'=>!empty($nombre)?$nombre:'SN'
							,'estado'=>$estado					
							);

			return $this->oBD->insert('biblioteca', $estados,true);
		} catch(Exception $e){		
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$idpersona,$tipo,$archivo,$nombre,$estado)
	{
		try {			
			$estados = array('idempresa'=>$idempresa
							,'idpersona'=>$idpersona
							,'tipo'=>$tipo
							,'archivo'=>$archivo
							,'nombre'=>$nombre
							,'estado'=>$estado						
							);
			$this->oBD->update('biblioteca ', $estados, array('idbiblioteca' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('biblioteca', array('idbiblioteca' => $id));
			else 
				return $this->oBD->update('biblioteca', array('estado' => -1), array('idbiblioteca' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('biblioteca', array($propiedad => $valor), array('idbiblioteca' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Biblioteca").": " . $e->getMessage());
		}
	}  
	 
		
}
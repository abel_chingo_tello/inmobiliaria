<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatCatalogo_sbn extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT casb_codigo,casb_descripcion,casb_fecharegistro FROM catalogo_sbn";
			$cond = array();
			
			if(isset($filtros["casb_codigo"])) {
					$cond[] = "casb_codigo = " . $this->oBD->escapar($filtros["casb_codigo"]);
			}
			if(isset($filtros["casb_descripcion"])) {
					$cond[] = "casb_descripcion = " . $this->oBD->escapar($filtros["casb_descripcion"]);
			}
			if(isset($filtros["casb_fecharegistro"])) {
					$cond[] = "casb_fecharegistro = " . $this->oBD->escapar($filtros["casb_fecharegistro"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM catalogo_sbn";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}
		
	public function insertar($casb_descripcion)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT casb_codigo+1 FROM catalogo_sbn ORDER BY casb_codigo DESC limit 0,1 ");			
			$estados = array('casb_descripcion'=>$casb_descripcion							
							);			
			return $this->oBD->insert('catalogo_sbn', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_catalogo_sbn_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $casb_descripcion)
	{
		try {			
			$estados = array('casb_descripcion'=>$casb_descripcion								
							);
			$this->oBD->update('catalogo_sbn ', $estados, array('casb_codigo' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('catalogo_sbn', array('casb_codigo' => $id));
			else 
				return $this->oBD->update('catalogo_sbn', array('estado' => -1), array('casb_codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('catalogo_sbn', array($propiedad => $valor), array('casb_codigo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Catalogo_sbn").": " . $e->getMessage());
		}
	}  

		 
		
}
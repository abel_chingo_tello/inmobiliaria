<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatClientes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT clie_id,tabl_tipocliente,clie_apellidos,clie_nombres,clie_razsocial,clie_dni,clie_direccion,clie_telefono,clie_email,clie_fregistro,clie_estado,clie_zona,clie_empleado FROM clientes";
			$cond = array();
			
			if(isset($filtros["clie_id"])) {
					$cond[] = "clie_id = " . $this->oBD->escapar($filtros["clie_id"]);
			}
			if(isset($filtros["tabl_tipocliente"])) {
					$cond[] = "tabl_tipocliente = " . $this->oBD->escapar($filtros["tabl_tipocliente"]);
			}
			if(isset($filtros["clie_apellidos"])) {
					$cond[] = "clie_apellidos = " . $this->oBD->escapar($filtros["clie_apellidos"]);
			}
			if(isset($filtros["clie_nombres"])) {
					$cond[] = "clie_nombres = " . $this->oBD->escapar($filtros["clie_nombres"]);
			}
			if(isset($filtros["clie_razsocial"])) {
					$cond[] = "clie_razsocial = " . $this->oBD->escapar($filtros["clie_razsocial"]);
			}
			if(isset($filtros["clie_dni"])) {
					$cond[] = "clie_dni = " . $this->oBD->escapar($filtros["clie_dni"]);
			}
			if(isset($filtros["clie_direccion"])) {
					$cond[] = "clie_direccion = " . $this->oBD->escapar($filtros["clie_direccion"]);
			}
			if(isset($filtros["clie_telefono"])) {
					$cond[] = "clie_telefono = " . $this->oBD->escapar($filtros["clie_telefono"]);
			}
			if(isset($filtros["clie_email"])) {
					$cond[] = "clie_email = " . $this->oBD->escapar($filtros["clie_email"]);
			}
			if(isset($filtros["clie_fregistro"])) {
					$cond[] = "clie_fregistro = " . $this->oBD->escapar($filtros["clie_fregistro"]);
			}
			if(isset($filtros["clie_estado"])) {
					$cond[] = "clie_estado = " . $this->oBD->escapar($filtros["clie_estado"]);
			}
			if(isset($filtros["clie_zona"])) {
					$cond[] = "clie_zona = " . $this->oBD->escapar($filtros["clie_zona"]);
			}
			if(isset($filtros["clie_empleado"])) {
					$cond[] = "clie_empleado = " . $this->oBD->escapar($filtros["clie_empleado"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM clientes";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}
		
	public function insertar($tabl_tipocliente,$clie_apellidos,$clie_nombres,$clie_razsocial,$clie_dni,$clie_direccion,$clie_telefono,$clie_email,$clie_estado,$clie_zona,$clie_empleado)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT clie_id+1 FROM clientes ORDER BY clie_id DESC limit 0,1 ");			
			$estados = array('tabl_tipocliente'=>$tabl_tipocliente
							,'clie_apellidos'=>$clie_apellidos
							,'clie_nombres'=>$clie_nombres
							,'clie_razsocial'=>$clie_razsocial
							,'clie_dni'=>$clie_dni
							,'clie_direccion'=>$clie_direccion
							,'clie_telefono'=>$clie_telefono
							,'clie_email'=>$clie_email
							,'clie_estado'=>$clie_estado
							,'clie_zona'=>$clie_zona
							,'clie_empleado'=>$clie_empleado							
							);			
			return $this->oBD->insert('clientes', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_clientes_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tabl_tipocliente,$clie_apellidos,$clie_nombres,$clie_razsocial,$clie_dni,$clie_direccion,$clie_telefono,$clie_email,$clie_estado,$clie_zona,$clie_empleado)
	{
		try {			
			$estados = array('tabl_tipocliente'=>$tabl_tipocliente
							,'clie_apellidos'=>$clie_apellidos
							,'clie_nombres'=>$clie_nombres
							,'clie_razsocial'=>$clie_razsocial
							,'clie_dni'=>$clie_dni
							,'clie_direccion'=>$clie_direccion
							,'clie_telefono'=>$clie_telefono
							,'clie_email'=>$clie_email
							,'clie_estado'=>$clie_estado
							,'clie_zona'=>$clie_zona
							,'clie_empleado'=>$clie_empleado								
							);
			$this->oBD->update('clientes ', $estados, array('clie_id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('clientes', array('clie_id' => $id));
			else 
				return $this->oBD->update('clientes', array('estado' => -1), array('clie_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('clientes', array($propiedad => $valor), array('clie_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Clientes").": " . $e->getMessage());
		}
	}  

		 
		
}
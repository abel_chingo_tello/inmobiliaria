<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		05-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatContacto_bien extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT id,tipo_documento,nro_documento,apellidos,direccion,rebi_id,email,nombre,telefono,mensaje,file_infocorp,file_recomendacion,file_policial_judicial,file_extra_recomendacion,punataje,estado,fecha_insert,fecha_update,
			tabla.tabl_descripcion as tipo_documento2 FROM contacto_bien inner join tabla on tabla.tabl_id=contacto_bien.tipo_documento";
			$cond = array();
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["tipo_documento"])) {
					$cond[] = "tipo_documento = " . $this->oBD->escapar($filtros["tipo_documento"]);
			}
			if(isset($filtros["nro_documento"])) {
					$cond[] = "nro_documento = " . $this->oBD->escapar($filtros["nro_documento"]);
			}
			if(isset($filtros["apellidos"])) {
					$cond[] = "apellidos = " . $this->oBD->escapar($filtros["apellidos"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["mensaje"])) {
					$cond[] = "mensaje = " . $this->oBD->escapar($filtros["mensaje"]);
			}
			if(isset($filtros["file_infocorp"])) {
					$cond[] = "file_infocorp = " . $this->oBD->escapar($filtros["file_infocorp"]);
			}
			if(isset($filtros["file_recomendacion"])) {
					$cond[] = "file_recomendacion = " . $this->oBD->escapar($filtros["file_recomendacion"]);
			}
			if(isset($filtros["file_policial_judicial"])) {
					$cond[] = "file_policial_judicial = " . $this->oBD->escapar($filtros["file_policial_judicial"]);
			}
			if(isset($filtros["file_extra_recomendacion"])) {
					$cond[] = "file_extra_recomendacion = " . $this->oBD->escapar($filtros["file_extra_recomendacion"]);
			}
			if(isset($filtros["punataje"])) {
					$cond[] = "punataje = " . $this->oBD->escapar($filtros["punataje"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM contacto_bien";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY id DESC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
		
	public function insertar($tipo_documento,$nro_documento,$apellidos,$direccion,$rebi_id,$email,$nombre,$telefono,$mensaje,$file_infocorp,$file_recomendacion,$file_policial_judicial,$file_extra_recomendacion,$punataje)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM contacto_bien ORDER BY id DESC limit 0,1 ");			
			$estados = array('tipo_documento'=>$tipo_documento
							,'nro_documento'=>$nro_documento
							,'apellidos'=>$apellidos
							,'direccion'=>$direccion
							,'rebi_id'=>$rebi_id
							,'email'=>$email
							,'nombre'=>$nombre
							,'telefono'=>$telefono
							,'mensaje'=>$mensaje
							,'file_infocorp'=>$file_infocorp
							,'file_recomendacion'=>$file_recomendacion
							,'file_policial_judicial'=>$file_policial_judicial
							,'file_extra_recomendacion'=>$file_extra_recomendacion
							,'punataje'=>$punataje							
							);			
			return $this->oBD->insert('contacto_bien', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_contacto_bien_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipo_documento,$nro_documento,$apellidos,$direccion,$rebi_id,$email,$nombre,$telefono,$mensaje,$file_infocorp,$file_recomendacion,$file_policial_judicial,$file_extra_recomendacion,$punataje)
	{
		try {			
			$estados = array('tipo_documento'=>$tipo_documento
							,'nro_documento'=>$nro_documento
							,'apellidos'=>$apellidos
							,'direccion'=>$direccion
							,'rebi_id'=>$rebi_id
							,'email'=>$email
							,'nombre'=>$nombre
							,'telefono'=>$telefono
							,'mensaje'=>$mensaje
							,'file_infocorp'=>$file_infocorp
							,'file_recomendacion'=>$file_recomendacion
							,'file_policial_judicial'=>$file_policial_judicial
							,'file_extra_recomendacion'=>$file_extra_recomendacion
							,'punataje'=>$punataje								
							);
			$this->oBD->update('contacto_bien ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('contacto_bien', array('id' => $id));
			else 
				return $this->oBD->update('contacto_bien', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('contacto_bien', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Contacto_bien").": " . $e->getMessage());
		}
	}  

		 
		
}
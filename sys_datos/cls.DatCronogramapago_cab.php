<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatCronogramapago_cab extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {

			if(isset($filtros["inner"])){
				$sql= "SELECT c.id ,c.persona_id,p.tipo_documento,p.num_documento,p.nombres,p.apellido_1,p.apellido_2, r.rebi_detalles, c.observacion,c.estado,c.estado_pago FROM `cronogramapago_cab` c ";
			}else{
				$sql = "SELECT id,persona_id,rebi_id,garantia,meses_contrato,monto_mensual,dia_pago,archivo_contrato,observacion,estado,estado_pago,fecha_insert,fecha_update FROM cronogramapago_cab";
			}
			$cond = array();
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["persona_id"])) {
					$cond[] = "persona_id = " . $this->oBD->escapar($filtros["persona_id"]);
			}
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["garantia"])) {
					$cond[] = "garantia = " . $this->oBD->escapar($filtros["garantia"]);
			}
			if(isset($filtros["meses_contrato"])) {
					$cond[] = "meses_contrato = " . $this->oBD->escapar($filtros["meses_contrato"]);
			}
			if(isset($filtros["monto_mensual"])) {
					$cond[] = "monto_mensual = " . $this->oBD->escapar($filtros["monto_mensual"]);
			}
			if(isset($filtros["dia_pago"])) {
					$cond[] = "dia_pago = " . $this->oBD->escapar($filtros["dia_pago"]);
			}
			if(isset($filtros["archivo_contrato"])) {
					$cond[] = "archivo_contrato = " . $this->oBD->escapar($filtros["archivo_contrato"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(isset($filtros["estado_pago"])) {
					$cond[] = "estado_pago = " . $this->oBD->escapar($filtros["estado_pago"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["inner"])){//inner join persona y bien 
				$sql .= "inner join persona p on p.idpersona = c.persona_id inner join registro_bien r on r.rebi_id=c.rebi_id ";
			}	
			if(isset($filtros["inner_id"])){//inner join persona y bien 
				$cond[] = "c.id = " . $this->oBD->escapar($filtros["inner_id"]);
			}
			if(isset($filtros["inner_personaid"])){//inner join persona y bien 
				$cond[] = "c.persona_id = " . $this->oBD->escapar($filtros["inner_personaid"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM cronogramapago_cab";
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(isset($filtros["inner"])){
				$sql .='order by c.id desc';
			}			
			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}
		
	public function insertar($persona_id,$rebi_id,$garantia,$meses_contrato,$monto_mensual,$dia_pago,$archivo_contrato,$observacion,$estado,$estado_pago)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM cronogramapago_cab ORDER BY id DESC limit 0,1 ");			
			$estados = array('persona_id'=>$persona_id
							,'rebi_id'=>$rebi_id
							,'garantia'=>$garantia
							,'meses_contrato'=>$meses_contrato
							,'monto_mensual'=>$monto_mensual
							,'dia_pago'=>$dia_pago
							,'archivo_contrato'=>$archivo_contrato
							,'observacion'=>$observacion
							,'estado'=>$estado
							,'estado_pago'=>$estado_pago							
							);			
			return $this->oBD->insert('cronogramapago_cab', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_cronogramapago_cab_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $persona_id,$rebi_id,$garantia,$meses_contrato,$monto_mensual,$dia_pago,$archivo_contrato,$observacion,$estado,$estado_pago)
	{
		try {			
			$estados = array('persona_id'=>$persona_id
							,'rebi_id'=>$rebi_id
							,'garantia'=>$garantia
							,'meses_contrato'=>$meses_contrato
							,'monto_mensual'=>$monto_mensual
							,'dia_pago'=>$dia_pago
							,'archivo_contrato'=>$archivo_contrato
							,'observacion'=>$observacion
							,'estado'=>$estado
							,'estado_pago'=>$estado_pago								
							);
			$this->oBD->update('cronogramapago_cab ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('cronogramapago_cab', array('id' => $id));
			else 
				return $this->oBD->update('cronogramapago_cab', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('cronogramapago_cab', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cronogramapago_cab").": " . $e->getMessage());
		}
	}  

		 
		
}
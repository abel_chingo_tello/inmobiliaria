<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		13-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatCronogramapago_det extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT id,cab_id,DATE_FORMAT(fecha_vencimiento,'%d-%m-%Y') as fecha_vencimiento,concepto,cargo,pago,saldo,subtotal,observacion,fecha_pago,estado,fecha_insert,fecha_update FROM cronogramapago_det";
			$cond = array();
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["cab_id"])) {
					$cond[] = "cab_id = " . $this->oBD->escapar($filtros["cab_id"]);
			}
			if(isset($filtros["fecha_vencimiento"])) {
					$cond[] = "fecha_vencimiento = " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if(isset($filtros["concepto"])) {
					$cond[] = "concepto = " . $this->oBD->escapar($filtros["concepto"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["pago"])) {
					$cond[] = "pago = " . $this->oBD->escapar($filtros["pago"]);
			}
			if(isset($filtros["saldo"])) {
					$cond[] = "saldo = " . $this->oBD->escapar($filtros["saldo"]);
			}
			if(isset($filtros["subtotal"])) {
					$cond[] = "subtotal = " . $this->oBD->escapar($filtros["subtotal"]);
			}
			if(isset($filtros["observacion"])) {
					$cond[] = "observacion = " . $this->oBD->escapar($filtros["observacion"]);
			}
			if(isset($filtros["fecha_pago"])) {
					$cond[] = "fecha_pago = " . $this->oBD->escapar($filtros["fecha_pago"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM cronogramapago_det";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY id DESC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}
		
	public function insertar($cab_id,$fecha_vencimiento,$concepto,$cargo,$pago,$saldo,$subtotal,$observacion,$fecha_pago,$estado)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM cronogramapago_det ORDER BY id DESC limit 0,1 ");			
			$estados = array('cab_id'=>$cab_id
							,'fecha_vencimiento'=>$fecha_vencimiento
							,'concepto'=>$concepto
							,'cargo'=>$cargo
							,'pago'=>$pago
							,'saldo'=>$saldo
							,'subtotal'=>$subtotal
							,'observacion'=>$observacion
							,'fecha_pago'=>$fecha_pago
							,'estado'=>$estado							
							);			
			return $this->oBD->insert('cronogramapago_det', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_cronogramapago_det_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $cab_id,$fecha_vencimiento,$concepto,$cargo,$pago,$saldo,$subtotal,$observacion,$fecha_pago,$estado)
	{
		try {			
			$estados = array('cab_id'=>$cab_id
							,'fecha_vencimiento'=>$fecha_vencimiento
							,'concepto'=>$concepto
							,'cargo'=>$cargo
							,'pago'=>$pago
							,'saldo'=>$saldo
							,'subtotal'=>$subtotal
							,'observacion'=>$observacion
							,'fecha_pago'=>$fecha_pago
							,'estado'=>$estado								
							);
			$this->oBD->update('cronogramapago_det ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('cronogramapago_det', array('id' => $id));
			else 
				return $this->oBD->update('cronogramapago_det', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('cronogramapago_det', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Cronogramapago_det").": " . $e->getMessage());
		}
	}  

		 
		
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idempresa,nombre,direccion,telefono,correo,logo,ruc,idioma,subdominio,estado,usuario_registro,fecha_insert,fecha_update FROM empresa";
			$cond = array();

			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}			
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["logo"])) {
					$cond[] = "logo = " . $this->oBD->escapar($filtros["logo"]);
			}
			if(isset($filtros["ruc"])) {
					$cond[] = "ruc = " . $this->oBD->escapar($filtros["ruc"]);
			}
			if(isset($filtros["idioma"])) {
					$cond[] = "idioma = " . $this->oBD->escapar($filtros["idioma"]);
			}
			if(isset($filtros["subdominio"])) {
					$cond[] = "subdominio = " . $this->oBD->escapar($filtros["subdominio"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado = 1 ";
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			//var_dump($sql);		
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}
		
	public function insertar($nombre,$direccion,$telefono,$correo,$logo,$ruc,$idioma,$subdominio,$estado,$usuario_registro)
	{
		try {			
			$id = $this->oBD->consultarEscalarSQL("SELECT idempresa+1 FROM empresa ORDER BY empresa DESC limit 0,1 ");			
			$estados = array('idempresa' => $id
							
							,'nombre'=>$nombre
							,'direccion'=>$dirección
							,'telefono'=>$telefono
							,'correo'=>$correo
							,'logo'=>$logo
							,'ruc'=>$ruc
							,'idioma'=>$idioma
							,'subdominio'=>$subdominio
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			$this->oBD->insert('empresa', $estados);
			return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$direccion,$telefono,$correo,$logo,$ruc,$idioma,$subdominio,$estado)
	{
		try {			
			$estados = array('nombre'=>$nombre
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'correo'=>$correo
							,'logo'=>$logo
							,'ruc'=>$ruc
							,'idioma'=>$idioma
							,'subdominio'=>$subdominio
							,'estado'=>$estado
							//,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('empresa ', $estados, array('idempresa' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa', array('idempresa' => $id));
			else 
				return $this->oBD->update('empresa', array('estado' => -1), array('idempresa' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa', array($propiedad => $valor), array('idempresa' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa").": " . $e->getMessage());
		}
	}  
	 
		
}
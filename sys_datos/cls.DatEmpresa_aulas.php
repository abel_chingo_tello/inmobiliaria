<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa_aulas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idaula,idlocal,nombre,idempresa,estado,tipo,capacidad,usuario_registro,fecha_insert,fecha_update FROM empresa_aulas";
			$cond = array();
			
			if(isset($filtros["idaula"])) {
					$cond[] = "idaula = " . $this->oBD->escapar($filtros["idaula"]);
			}
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["capacidad"])) {
					$cond[] = "capacidad = " . $this->oBD->escapar($filtros["capacidad"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa_aulas";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}
		
	public function insertar($idlocal,$nombre,$idempresa,$estado,$tipo,$capacidad,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idaula+1 FROM empresa_aulas ORDER BY idaula DESC limit 0,1 ");			
			$estados = array('idaula' => $id
							
							,'idlocal'=>$idlocal
							,'nombre'=>$nombre
							,'idempresa'=>$idempresa
							,'estado'=>$estado
							,'tipo'=>$tipo
							,'capacidad'=>$capacidad
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('empresa_aulas', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_aulas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idlocal,$nombre,$idempresa,$estado,$tipo,$capacidad,$usuario_registro)
	{
		try {			
			$estados = array('idlocal'=>$idlocal
							,'nombre'=>$nombre
							,'idempresa'=>$idempresa
							,'estado'=>$estado
							,'tipo'=>$tipo
							,'capacidad'=>$capacidad
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('empresa_aulas ', $estados, array('idaula' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa_aulas', array('idaula' => $id));
			else 
				return $this->oBD->update('empresa_aulas', array('estado' => -1), array('idaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa_aulas', array($propiedad => $valor), array('idaula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_aulas").": " . $e->getMessage());
		}
	}  
	 
		
}
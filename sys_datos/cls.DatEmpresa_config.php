<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa_config extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idempresaconfig,idempresa,parametro,valor,estado,usuario_registro,fecha_insert,fecha_update FROM empresa_config";
			$cond = array();
			
			
			if(isset($filtros["idempresaconfig"])) {
					$cond[] = "idempresaconfig = " . $this->oBD->escapar($filtros["idempresaconfig"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["parametro"])) {
					$cond[] = "parametro = " . $this->oBD->escapar($filtros["parametro"]);
			}
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado = 1 ";
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa_config";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$parametro,$valor,$estado,$usuario_registro)
	{
		try {
			$estados = array('idempresa'=>$idempresa
							,'parametro'=>$parametro
							,'valor'=>$valor
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('empresa_config', $estados,true);			
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_config_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}
	public function actualizar($idempresaconfig,$idempresa,$parametro,$valor,$estado,$usuario_registro)
	{
		try {			
			$estados = array(
							'idempresa'=>$idempresa
							,'parametro'=>$parametro
							,'valor'=>$valor
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro
							);
			$this->oBD->update('empresa_config ', $estados, array('idempresaconfig'=>$idempresaconfig));		   
		    return $idempresaconfig;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa_config', array('idempresaconfig' => $id));
			else 
				return $this->oBD->update('empresa_config', array('estado' => -1), array('idempresaconfig' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa_config', array($propiedad => $valor), array('idempresaconfig' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_config").": " . $e->getMessage());
		}
	}  
	 
		
}
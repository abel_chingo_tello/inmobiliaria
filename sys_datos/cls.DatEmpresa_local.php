<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa_local extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idlocal,nombre,idempresa,estado,usuario_registro,fecha_insert,fecha_update FROM empresa_local";
			$cond = array();
			
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa_local";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}
		
	public function insertar($nombre,$idempresa,$estado,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idlocal+1 FROM empresa_local ORDER BY idlocal DESC limit 0,1 ");			
			$estados = array('idlocal' => $id
							
							,'nombre'=>$nombre
							,'idempresa'=>$idempresa
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('empresa_local', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_local_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$idempresa,$estado,$usuario_registro)
	{
		try {			
			$estados = array('nombre'=>$nombre
							,'idempresa'=>$idempresa
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('empresa_local ', $estados, array('idlocal' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa_local', array('idlocal' => $id));
			else 
				return $this->oBD->update('empresa_local', array('estado' => -1), array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa_local', array($propiedad => $valor), array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_local").": " . $e->getMessage());
		}
	}  
	 
		
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa_modulo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idempresamodulo,em.idmodulo,nombre as strmodulo,url,icono,em.idempresa,em.estado,em.idrol,em.orden,em.idmodulopadre,em.nomtemporal,em.usuario_registro,em.fecha_insert,em.fecha_update FROM empresa_modulo em LEFT JOIN modulos mo on em.idmodulo=mo.idmodulo";
			$cond = array();
			
			if(isset($filtros["idempresamodulo"])) {
					$cond[] = "idempresamodulo = " . $this->oBD->escapar($filtros["idempresamodulo"]);
			}
			if(isset($filtros["idmodulo"])) {
					$cond[] = "em.idmodulo = " . $this->oBD->escapar($filtros["idmodulo"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "em.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "em.estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "em.estado >=0 ";}
			
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "em.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idmodulopadre"])) {
					$cond[] = "em.idmodulopadre = " . $this->oBD->escapar($filtros["idmodulopadre"]);
			}
			if(isset($filtros["nomtemporal"])) {
					$cond[] = "em.nomtemporal = " . $this->oBD->escapar($filtros["nomtemporal"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "em.usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "em.fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "em.fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "mo.modulo " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa_modulo";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY em.idmodulopadre,em.orden ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	}
		
	public function insertar($idmodulo,$idempresa,$estado,$idrol,$orden,$idmodulopadre,$nomtemporal,$usuario_registro)
	{
		try {			
			$orden = $this->oBD->consultarEscalarSQL("SELECT orden+1 FROM empresa_modulo WHERE idempresa='".$idempresa."' ORDER BY orden DESC limit 0,1 ");			
			$estados = array('idmodulo'=>empty($idmodulo)?'null':$idmodulo
							,'idempresa'=>$idempresa
							,'estado'=>!empty($estado)?$estado:1
							,'idrol'=>$idrol
							,'orden'=>empty($orden)?999:$orden
							,'usuario_registro'=>$usuario_registro							
							);
			if(!empty($idmodulopadre))	$estados["idmodulopadre"]=$idmodulopadre;
			if(!empty($nomtemporal))	$estados["nomtemporal"]=$nomtemporal;

			return $this->oBD->insert('empresa_modulo', $estados,true);
			var_dump($estados);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_modulo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idmodulo,$idempresa,$estado,$idrol,$orden,$idmodulopadre,$nomtemporal,$usuario_registro)
	{
		try {			
			$estados = array('idmodulo'=>empty($idmodulo)?'null':$idmodulo
							,'idempresa'=>$idempresa							
							,'idrol'=>$idrol							
							,'usuario_registro'=>$usuario_registro								
							);
			if(!empty($orden))	$estados["orden"]=$orden;
			if(!empty($estado))	$estados["estado"]=$estado;
			if(!empty($idmodulopadre))	$estados["idmodulopadre"]=$idmodulopadre;
			if(!empty($nomtemporal))	$estados["nomtemporal"]=$nomtemporal;
			$this->oBD->update('empresa_modulo ', $estados, array('idempresamodulo' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa_modulo', array('idempresamodulo' => $id));
			else 
				return $this->oBD->update('empresa_modulo', array('estado' => -1), array('idempresamodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa_modulo', array($propiedad => $valor), array('idempresamodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	} 

	public function guardarorden($datos)
	{
		try {
			$data=json_decode($datos,true);
			foreach ($data as $key => $va){	
				$estados=array('orden' =>$va["orden"],'idmodulopadre'=>!empty($va["idpadre"])?$va["idpadre"]:'null');
				$this->oBD->update('empresa_modulo', $estados, array('idempresamodulo' => $va["idempresamodulo"]));				
			}	
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_modulo").": " . $e->getMessage());
		}
	} 
}
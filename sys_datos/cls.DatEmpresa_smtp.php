<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatEmpresa_smtp extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idempresasmtp,idempresa,host,email,clave,puerto,cifrado,estado,usuario_registro,fecha_insert,fecha_update FROM empresa_smtp";
			$cond = array();
			
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["host"])) {
					$cond[] = "host = " . $this->oBD->escapar($filtros["host"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["puerto"])) {
					$cond[] = "puerto = " . $this->oBD->escapar($filtros["puerto"]);
			}
			if(isset($filtros["cifrado"])) {
					$cond[] = "cifrado = " . $this->oBD->escapar($filtros["cifrado"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado = 1 ";
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM empresa_smtp";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$host,$email,$clave,$puerto,$cifrado,$estado,$usuario_registro)
	{
		try {			
					
			$estados = array('idempresa'=>$idempresa
							,'host'=>$host
							,'email'=>$email
							,'clave'=>$clave
							,'puerto'=>$puerto
							,'cifrado'=>$cifrado
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('empresa_smtp', $estados,true);
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_empresa_smtp_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$host,$email,$clave,$puerto,$cifrado,$estado,$usuario_registro)
	{
		try {			
			$estados = array('idempresa'=>$idempresa
							,'host'=>$host
							,'email'=>$email
							,'clave'=>$clave
							,'puerto'=>$puerto
							,'cifrado'=>$cifrado
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro									
							);
			$this->oBD->update('empresa_smtp ', $estados, array('idempresasmtp' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('empresa_smtp', array('idempresasmtp' => $id));
			else 
				return $this->oBD->update('empresa_smtp', array('estado' => -1), array('idempresasmtp' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('empresa_smtp', array($propiedad => $valor), array('idempresasmtp' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Empresa_smtp").": " . $e->getMessage());
		}
	}  
	 
		
}
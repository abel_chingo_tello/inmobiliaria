<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-01-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatGaleria_fotos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}
	
	public function listar($filtros=null)
	{
		try {
			$sql = "SELECT tipo,GROUP_CONCAT(imagen) as imagen,GROUP_CONCAT(titulo) as titulo   FROM galeria_fotos";
			$cond = array();
			
			if(isset($filtros["id_galeria"])) {
					$cond[] = "id_galeria = " . $this->oBD->escapar($filtros["id_galeria"]);
			}
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			$cond[] = "estado = 1";
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM galeria_fotos";
			}						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " GROUP BY tipo ORDER BY tipo ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT *  FROM galeria_fotos";
			$cond = array();
			
			if(isset($filtros["id_galeria"])) {
					$cond[] = "id_galeria = " . $this->oBD->escapar($filtros["id_galeria"]);
			}
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			$cond[] = "estado = 1";
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM galeria_fotos";
			}						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			$sql .= " ORDER BY tipo ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}
	public function insertar($rebi_id,$titulo,$descripcion,$tipo,$imagen)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id_galeria+1 FROM galeria_fotos ORDER BY id_galeria DESC limit 0,1 ");			
			//move image file to specific route
			$estados = array('rebi_id'=>$rebi_id
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'tipo'=>$tipo
							,'imagen'=>$imagen							
							);			
			return $this->oBD->insert('galeria_fotos', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_galeria_fotos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $rebi_id,$titulo,$descripcion,$tipo,$imagen)
	{
		try {		
			if($imagen==""){
				$estados = array('rebi_id'=>$rebi_id
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'tipo'=>$tipo
							);
			
			}else{
				$estados = array('rebi_id'=>$rebi_id
				,'titulo'=>$titulo
				,'descripcion'=>$descripcion
				,'tipo'=>$tipo
				,'imagen'=>$imagen								
				);
			}
			$this->oBD->update('galeria_fotos ', $estados, array('id_galeria' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('galeria_fotos', array('id_galeria' => $id));
			else 
				return $this->oBD->update('galeria_fotos', array('estado' => -1), array('id_galeria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('galeria_fotos', array($propiedad => $valor), array('id_galeria' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Galeria_fotos").": " . $e->getMessage());
		}
	}  

		 
		
}
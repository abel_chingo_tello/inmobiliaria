<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatHistorial_sesion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idhistorial,idpersona,idrol,idempresa,ip,navegador,idgrupoaula,fecha_inicio,fecha_final,lugar FROM historial_sesion";
			$cond = array();
			
			if(isset($filtros["idhistorial"])) {
					$cond[] = "idhistorial = " . $this->oBD->escapar($filtros["idhistorial"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["ip"])) {
					$cond[] = "ip = " . $this->oBD->escapar($filtros["ip"]);
			}
			if(isset($filtros["navegador"])) {
					$cond[] = "navegador = " . $this->oBD->escapar($filtros["navegador"]);
			}
			if(isset($filtros["idgrupoaula"])) {
					$cond[] = "idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["lugar"])) {
					$cond[] = "lugar = " . $this->oBD->escapar($filtros["lugar"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM historial_sesion";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
		
	public function insertar($idpersona,$idrol,$idempresa,$ip,$navegador,$idgrupoaula,$fecha_inicio,$fecha_final,$lugar)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idhistorial+1 FROM historial_sesion ORDER BY idhistorial DESC limit 0,1 ");			
			$estados = array('idhistorial' => $id
							
							,'idpersona'=>$idpersona
							,'idrol'=>$idrol
							,'idempresa'=>$idempresa
							,'ip'=>$ip
							,'navegador'=>$navegador
							,'idgrupoaula'=>$idgrupoaula
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'lugar'=>$lugar							
							);			
			return $this->oBD->insert('historial_sesion', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_historial_sesion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$idrol,$idempresa,$ip,$navegador,$idgrupoaula,$fecha_inicio,$fecha_final,$lugar)
	{
		try {			
			$estados = array('idpersona'=>$idpersona
							,'idrol'=>$idrol
							,'idempresa'=>$idempresa
							,'ip'=>$ip
							,'navegador'=>$navegador
							,'idgrupoaula'=>$idgrupoaula
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'lugar'=>$lugar								
							);
			$this->oBD->update('historial_sesion ', $estados, array('idhistorial' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('historial_sesion', array('idhistorial' => $id));
			else 
				return $this->oBD->update('historial_sesion', array('estado' => -1), array('idhistorial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('historial_sesion', array($propiedad => $valor), array('idhistorial' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Historial_sesion").": " . $e->getMessage());
		}
	}  
	 
		
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatModulos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idmodulo,nombre,descripcion,estado,url,icono,usuario_registro,fecha_insert,fecha_update FROM modulos";
			$cond = array();
			
			if(isset($filtros["idmodulo"])) {
					$cond[] = "idmodulo = " . $this->oBD->escapar($filtros["idmodulo"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["icono"])) {
					$cond[] = "icono = " . $this->oBD->escapar($filtros["icono"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM modulos";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
		
	public function insertar($nombre,$descripcion,$estado,$url,$icono,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idmodulo+1 FROM modulos ORDER BY idmodulo DESC limit 0,1 ");			
			$estados = array('nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'url'=>$url
							,'icono'=>$icono
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('modulos', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_modulos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$descripcion,$estado,$url,$icono,$usuario_registro)
	{
		try {			
			$estados = array('nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'url'=>$url
							,'icono'=>$icono
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('modulos ', $estados, array('idmodulo' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('modulos', array('idmodulo' => $id));
			else 
				return $this->oBD->update('modulos', array('estado' => -1), array('idmodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('modulos', array($propiedad => $valor), array('idmodulo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Modulos").": " . $e->getMessage());
		}
	}  
	 
		
}
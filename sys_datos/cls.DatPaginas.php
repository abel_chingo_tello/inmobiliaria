<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatPaginas extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idpagina,idempresa,titulo_enmenu,titulo,descripcion,imagen,estado,enmenu,usuario_registro,fecha_insert,fecha_update FROM paginas";
			$cond = array();
			
			if(isset($filtros["idpagina"])) {
					$cond[] = "idpagina = " . $this->oBD->escapar($filtros["idpagina"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["titulo_enmenu"])) {
					$cond[] = "titulo_enmenu = " . $this->oBD->escapar($filtros["titulo_enmenu"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["enmenu"])) {
					$cond[] = "enmenu = " . $this->oBD->escapar($filtros["enmenu"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM paginas";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$titulo_enmenu,$titulo,$descripcion,$imagen,$estado,$enmenu,$usuario_registro)
	{
		try {			
			$id = $this->oBD->consultarEscalarSQL("SELECT idpagina+1 FROM paginas ORDER BY paginas DESC limit 0,1 ");			
			$estados = array('idpagina' => $id
							
							,'idempresa'=>$idempresa
							,'titulo_enmenu'=>$titulo_enmenu
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'imagen'=>$imagen
							,'estado'=>$estado
							,'enmenu'=>$enmenu
							,'usuario_registro'=>$usuario_registro					
							);			
			$this->oBD->insert('paginas', $estados);
			return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_paginas_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}
	public function actualizar($id,$titulo_enmenu,$titulo,$descripcion,$imagen,$estado,$enmenu)
	{
		try {			
			$estados = array('titulo_enmenu'=>$titulo_enmenu
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'imagen'=>$imagen
							,'estado'=>$estado
							,'enmenu'=>$enmenu							
							);
			$this->oBD->update('paginas ', $estados, array('idpagina' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('paginas', array('idpagina' => $id));
			else 
				return $this->oBD->update('paginas', array('estado' => -1), array('idpagina' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('paginas', array($propiedad => $valor), array('idpagina' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Paginas").": " . $e->getMessage());
		}
	}  
	 
		
}
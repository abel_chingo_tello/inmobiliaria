<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatPagos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT id,persona_id,preference_id,cronogramapagocab_id,cronogramapagodet_ids,estado FROM pagos";
			$cond = array();
			
			if(isset($filtros["id"])) {
					$cond[] = "id = " . $this->oBD->escapar($filtros["id"]);
			}
			if(isset($filtros["persona_id"])) {
					$cond[] = "persona_id = " . $this->oBD->escapar($filtros["persona_id"]);
			}
			if(isset($filtros["preference_id"])) {
					$cond[] = "preference_id = " . $this->oBD->escapar($filtros["preference_id"]);
			}
			if(isset($filtros["cronogramapagocab_id"])) {
					$cond[] = "cronogramapagocab_id = " . $this->oBD->escapar($filtros["cronogramapagocab_id"]);
			}
			if(isset($filtros["cronogramapagodet_ids"])) {
					$cond[] = "cronogramapagodet_ids = " . $this->oBD->escapar($filtros["cronogramapagodet_ids"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
						
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM pagos";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}
		
	public function insertar($persona_id,$preference_id,$cronogramapagocab_id,$cronogramapagodet_ids,$estado)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT id+1 FROM pagos ORDER BY id DESC limit 0,1 ");			
			$estados = array('persona_id'=>$persona_id
							,'preference_id'=>$preference_id
							,'cronogramapagocab_id'=>$cronogramapagocab_id
							,'cronogramapagodet_ids'=>$cronogramapagodet_ids
							,'estado'=>$estado							
							);			
			return $this->oBD->insert('pagos', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_pagos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $persona_id,$preference_id,$cronogramapagocab_id,$cronogramapagodet_ids,$estado)
	{
		try {			
			$estados = array('persona_id'=>$persona_id
							,'preference_id'=>$preference_id
							,'cronogramapagocab_id'=>$cronogramapagocab_id
							,'cronogramapagodet_ids'=>$cronogramapagodet_ids
							,'estado'=>$estado								
							);
			$this->oBD->update('pagos ', $estados, array('id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('pagos', array('id' => $id));
			else 
				return $this->oBD->update('pagos', array('estado' => -1), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('pagos', array($propiedad => $valor), array('id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Pagos").": " . $e->getMessage());
		}
	}  

		 
		
}
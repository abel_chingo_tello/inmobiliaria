<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-08-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatPersona extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idpersona,tipo_documento,num_documento,apellido_1,apellido_2,nombres,usuario,email,clave,verclave,tocken,foto,genero,fecha_nacimiento,estado_civil,tipo_usuario,telefono,ubigeo,direccion,estado,usuario_registro,fecha_insert,fecha_update FROM persona";
			$cond = array();
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			
			if(isset($filtros["tipo_documento"])) {
					$cond[] = "tipo_documento = " . $this->oBD->escapar($filtros["tipo_documento"]);
			}
			if(isset($filtros["num_documento"])) {
					$cond[] = "num_documento = " . $this->oBD->escapar($filtros["num_documento"]);
			}
			if(isset($filtros["apellido_1"])) {
					$cond[] = "apellido_1 = " . $this->oBD->escapar($filtros["apellido_1"]);
			}
			if(isset($filtros["apellido_2"])) {
					$cond[] = "apellido_2 = " . $this->oBD->escapar($filtros["apellido_2"]);
			}
			if(isset($filtros["nombres"])) {
					$cond[] = "nombres = " . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			if(isset($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(isset($filtros["clave"])) {
					$cond[] = "clave = " . $this->oBD->escapar($filtros["clave"]);
			}
			if(isset($filtros["verclave"])) {
					$cond[] = "verclave = " . $this->oBD->escapar($filtros["verclave"]);
			}
			if(isset($filtros["tocken"])) {
					$cond[] = "tocken = " . $this->oBD->escapar($filtros["tocken"]);
			}
			if(isset($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(isset($filtros["tipo_usuario"])) {
					$cond[] = "tipo_usuario = " . $this->oBD->escapar($filtros["tipo_usuario"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["ubigeo"])) {
					$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else $cond[] = "estado = 1 ";
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM persona";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";		
			//echo $sql;	
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}
		
	public function insertar($tipo_documento,$num_documento,$apellido_1,$apellido_2,$nombres,$usuario,$email,$clave,$verclave,$tocken,$foto,$genero,$fecha_nacimiento,$estado_civil,$tipo_usuario,$telefono,$ubigeo,$direccion,$estado,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idpersona+1 FROM persona ORDER BY persona DESC limit 0,1 ");			
			$estados = array('tipo_documento'=>$tipo_documento
							,'num_documento'=>$num_documento
							,'apellido_1'=>$apellido_1
							,'apellido_2'=>$apellido_2
							,'nombres'=>$nombres
							,'usuario'=>$usuario
							,'email'=>$email
							,'clave'=>$clave
							,'verclave'=>$verclave
							,'tocken'=>$tocken
							,'foto'=>$foto
							,'genero'=>$genero
							,'fecha_nacimiento'=>$fecha_nacimiento
							,'estado_civil'=>$estado_civil
							,'tipo_usuario'=>$tipo_usuario
							,'telefono'=>$telefono
							,'ubigeo'=>$ubigeo
							,'direccion'=>$direccion
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('persona', $estados,true);			
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_persona_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tipo_documento,$num_documento,$apellido_1,$apellido_2,$nombres,$usuario,$email,$clave,$verclave,$tocken,$foto,$genero,$fecha_nacimiento,$estado_civil,$tipo_usuario,$telefono,$ubigeo,$direccion,$estado)
	{
		try {			
			$estados = array('tipo_documento'=>$tipo_documento
							,'num_documento'=>$num_documento
							,'apellido_1'=>$apellido_1
							,'apellido_2'=>$apellido_2
							,'nombres'=>$nombres
							,'usuario'=>$usuario
							,'email'=>$email
							,'clave'=>$clave
							,'verclave'=>$verclave
							,'tocken'=>$tocken
							,'foto'=>$foto
							,'genero'=>$genero
							,'fecha_nacimiento'=>$fecha_nacimiento
							,'estado_civil'=>$estado_civil
							,'tipo_usuario'=>$tipo_usuario
							,'telefono'=>$telefono
							,'ubigeo'=>$ubigeo
							,'direccion'=>$direccion
							,'estado'=>$estado
							//,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('persona ', $estados, array('idpersona' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('persona', array('idpersona' => $id));
			else 
				return $this->oBD->update('persona', array('estado' => -1), array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('persona', array($propiedad => $valor), array('idpersona' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona").": " . $e->getMessage());
		}
	}  	 
		
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatPersona_rol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idpersonarol,pr.idempresa,idpersona,pr.idrol,pr.usuario_registro,pr.fecha_insert,pr.fecha_update,rol.nombre as rol FROM persona_rol pr INNER JOIN rol  ON pr.idrol=rol.idrol ";
			$cond = array();
			
			if(isset($filtros["idpersonarol"])) {
					$cond[] = "idpersonarol = " . $this->oBD->escapar($filtros["idpersonarol"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "pr.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "pr.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "pr.usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "pr.fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "pr.fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM persona_rol";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";	
			//echo $sql;		
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$idpersona,$idrol,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idpersonarol+1 FROM persona_rol ORDER BY idpersonarol DESC limit 0,1 ");			
			$estados = array('idempresa'=>$idempresa
							,'idpersona'=>$idpersona
							,'idrol'=>$idrol
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('persona_rol', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_persona_rol_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$idpersona,$idrol,$usuario_registro)
	{
		try {			
			$estados = array('idempresa'=>$idempresa
							,'idpersona'=>$idpersona
							,'idrol'=>$idrol
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('persona_rol ', $estados, array('idpersonarol' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('persona_rol', array('idpersonarol' => $id));
			else 
				return $this->oBD->update('persona_rol', array('estado' => -1), array('idpersonarol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('persona_rol', array($propiedad => $valor), array('idpersonarol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_rol").": " . $e->getMessage());
		}
	}  

		 
		
}
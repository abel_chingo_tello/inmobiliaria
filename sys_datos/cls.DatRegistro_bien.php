<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRegistro_bien extends DatBase
{

	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}

	public function listarPrueba($filtros=array(),$inner_join=array())
	{
		try {
			$sql = "SELECT * FROM registro_bien";
			$cond = array();
			
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["rebi_modalidad"])) {
					$cond[] = "rebi_modalidad = " . $this->oBD->escapar($filtros["rebi_modalidad"]);
			}
			if(isset($filtros["rebi_tipo_bien"])) {
					$cond[] = "rebi_tipo_bien = " . $this->oBD->escapar($filtros["rebi_tipo_bien"]);
			}
			if(isset($filtros["casb_codigo"])) {
					$cond[] = "casb_codigo = " . $this->oBD->escapar($filtros["casb_codigo"]);
			}
			if(isset($filtros["rebi_fadquisicion"])) {
					$cond[] = "rebi_fadquisicion = " . $this->oBD->escapar($filtros["rebi_fadquisicion"]);
			}
			if(isset($filtros["rebi_vadquisicion"])) {
					$cond[] = "rebi_vadquisicion = " . $this->oBD->escapar($filtros["rebi_vadquisicion"]);
			}
			if(isset($filtros["rebi_ult_dep_acum"])) {
					$cond[] = "rebi_ult_dep_acum = " . $this->oBD->escapar($filtros["rebi_ult_dep_acum"]);
			}
			if(isset($filtros["rebi_ult_val_act"])) {
					$cond[] = "rebi_ult_val_act = " . $this->oBD->escapar($filtros["rebi_ult_val_act"]);
			}
			if(isset($filtros["tabl_estado"])) {
					$cond[] = "tabl_estado = " . $this->oBD->escapar($filtros["tabl_estado"]);
			}
			if(isset($filtros["rebi_detalles"])) {
					$cond[] = "rebi_detalles = " . $this->oBD->escapar($filtros["rebi_detalles"]);
			}
			if(isset($filtros["depe_id_ubica"])) {
					$cond[] = "depe_id_ubica = " . $this->oBD->escapar($filtros["depe_id_ubica"]);
			}
			if(isset($filtros["tabl_condicion"])) {
					$cond[] = "tabl_condicion = " . $this->oBD->escapar($filtros["tabl_condicion"]);
			}
			if(isset($filtros["rebi_reg_publicos"])) {
					$cond[] = "rebi_reg_publicos = " . $this->oBD->escapar($filtros["rebi_reg_publicos"]);
			}
			if(isset($filtros["rebi_area"])) {
					$cond[] = "rebi_area = " . $this->oBD->escapar($filtros["rebi_area"]);
			}
			if(isset($filtros["rebi_observaciones"])) {
					$cond[] = "rebi_observaciones = " . $this->oBD->escapar($filtros["rebi_observaciones"]);
			}
			if(isset($filtros["rebi_fregistro"])) {
					$cond[] = "rebi_fregistro = " . $this->oBD->escapar($filtros["rebi_fregistro"]);
			}
			if(isset($filtros["depe_id"])) {
					$cond[] = "depe_id = " . $this->oBD->escapar($filtros["depe_id"]);
			}
			if(isset($filtros["rebi_anno"])) {
					$cond[] = "rebi_anno = " . $this->oBD->escapar($filtros["rebi_anno"]);
			}
			if(isset($filtros["rebi_alquiler"])) {
					$cond[] = "rebi_alquiler = " . $this->oBD->escapar($filtros["rebi_alquiler"]);
			}
			if(isset($filtros["tabl_tipo_interior"])) {
					$cond[] = "tabl_tipo_interior = " . $this->oBD->escapar($filtros["tabl_tipo_interior"]);
			}
			if(isset($filtros["rebi_numero_interior"])) {
					$cond[] = "rebi_numero_interior = " . $this->oBD->escapar($filtros["rebi_numero_interior"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			//filtrar maximo precio 
			if(isset($filtros["precio_max"])) {
				$cond[] = "rebi_ult_val_act <=" . $this->oBD->escapar($filtros["precio_max"],false);
			}
			//filtrar minimo precio
			if(isset($filtros["precio_min"])) {
				$cond[] = "rebi_ult_val_act >=" . $this->oBD->escapar($filtros["precio_min"],false);
			}

			//filtrar maxima area 
			if(isset($filtros["area_max"])) {
				$cond[] = "rebi_area <=" . $this->oBD->escapar($filtros["area_max"],false);
			}
			//filtrar minima area
			if(isset($filtros["area_min"])) {
				$cond[] = "rebi_area >=" . $this->oBD->escapar($filtros["area_min"],false);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM registro_bien";
			}			
			//$sql .= " ORDER BY fecha_creado ASC";		
			//revisando si hay inner join(dato de otra tabla)
			//echo $sql;
			if(!empty($inner_join)) {
				$sql .= " inner join ".$inner_join["tabla"] ." on " .$inner_join["tabla"].".".$inner_join["id_tabla"]."=registro_bien.".$inner_join["id_tabla"];
			}	
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			//echo $sql;die();
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}


	
	public function buscar($filtros=null,$inner_join=array("tabla"=>"catalogo_sbn","id_tabla"=>"casb_codigo","fk_origen"=>"casb_codigo")){
		try {
			$sql = "SELECT * FROM registro_bien";
			$cond = array();
			
			if(isset($filtros["rebi_id"])) {
					$cond[] = "rebi_id = " . $this->oBD->escapar($filtros["rebi_id"]);
			}
			if(isset($filtros["rebi_modalidad"])) {
					$cond[] = "rebi_modalidad = " . $this->oBD->escapar($filtros["rebi_modalidad"]);
			}
			if(isset($filtros["rebi_tipo_bien"])) {
					$cond[] = "rebi_tipo_bien = " . $this->oBD->escapar($filtros["rebi_tipo_bien"]);
			}
			if(isset($filtros["casb_codigo"])) {
					$cond[] = "catalogo_sbn.casb_codigo = " . $this->oBD->escapar($filtros["casb_codigo"]);
			}
			if(isset($filtros["rebi_fadquisicion"]) && $filtros["rebi_fadquisicion"]!="") {
					$hoy=date("Y-m-d");
					$condicion="";
					switch ($filtros["rebi_fadquisicion"]) {
						case 'week':
							$fecha_anterior=date("Y-m-d",strtotime($hoy."- 7 days")); 
							$condicion="rebi_fadquisicion between '" .$fecha_anterior."'' and '".$hoy."'";
							break;
						case 'month':
							$fecha_anterior=date("Y-m-d",strtotime($hoy."- 30 days")); 
							$condicion="rebi_fadquisicion between '" .$fecha_anterior."'' and '".$hoy."'";
							break;
						case 'fifteen':
							$fecha_anterior=date("Y-m-d",strtotime($hoy."- 15 days")); 
							$condicion="rebi_fadquisicion between '" .$fecha_anterior."'' and '".$hoy."'";
							break;
						default:
							$condicion="";
							break;
					}
					$cond[] = $condicion;
			}
			if(isset($filtros["rebi_vadquisicion"])) {
					$cond[] = "rebi_vadquisicion = " . $this->oBD->escapar($filtros["rebi_vadquisicion"]);
			}
			if(isset($filtros["rebi_ult_dep_acum"])) {
					$cond[] = "rebi_ult_dep_acum = " . $this->oBD->escapar($filtros["rebi_ult_dep_acum"]);
			}
			if(isset($filtros["rebi_ult_val_act"])) {
					$cond[] = "rebi_ult_val_act = " . $this->oBD->escapar($filtros["rebi_ult_val_act"]);
			}
			if(isset($filtros["tabl_estado"])) {
					$cond[] = "tabl_estado = " . $this->oBD->escapar($filtros["tabl_estado"]);
			}
			if(isset($filtros["rebi_detalles"])) {
					$cond[] = "rebi_detalles = " . $this->oBD->escapar($filtros["rebi_detalles"]);
			}
			if(isset($filtros["depe_id_ubica"])) {
					$cond[] = "depe_id_ubica = " . $this->oBD->escapar($filtros["depe_id_ubica"]);
			}
			if(isset($filtros["tabl_condicion"])) {
					$cond[] = "tabl_condicion = " . $this->oBD->escapar($filtros["tabl_condicion"]);
			}
			if(isset($filtros["rebi_reg_publicos"])) {
					$cond[] = "rebi_reg_publicos = " . $this->oBD->escapar($filtros["rebi_reg_publicos"]);
			}
			if(isset($filtros["rebi_area"])) {
					$cond[] = "rebi_area = " . $this->oBD->escapar($filtros["rebi_area"]);
			}
			if(isset($filtros["rebi_observaciones"])) {
					$cond[] = "rebi_observaciones = " . $this->oBD->escapar($filtros["rebi_observaciones"]);
			}
			if(isset($filtros["rebi_fregistro"])) {
					$cond[] = "rebi_fregistro = " . $this->oBD->escapar($filtros["rebi_fregistro"]);
			}
			if(isset($filtros["depe_id"])) {
					$cond[] = "depe_id = " . $this->oBD->escapar($filtros["depe_id"]);
			}
			if(isset($filtros["rebi_anno"])) {
					$cond[] = "rebi_anno = " . $this->oBD->escapar($filtros["rebi_anno"]);
			}
			if(isset($filtros["rebi_alquiler"])) {
					$cond[] = "rebi_alquiler = " . $this->oBD->escapar($filtros["rebi_alquiler"]);
			}
			if(isset($filtros["tabl_tipo_interior"]) && $filtros["tabl_tipo_interior"]!="") {
					$cond[] = "tabl_tipo_interior = " . $this->oBD->escapar($filtros["tabl_tipo_interior"]);
			}
			if(isset($filtros["rebi_numero_interior"])) {
					$cond[] = "rebi_numero_interior = " . $this->oBD->escapar($filtros["rebi_numero_interior"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			//filtrar maximo precio 
			if(isset($filtros["precio_max"])) {
				$cond[] = "rebi_ult_val_act <=" . $this->oBD->escapar($filtros["precio_max"],false);
			}
			//filtrar minimo precio
			if(isset($filtros["precio_min"])) {
				$cond[] = "rebi_ult_val_act >=" . $this->oBD->escapar($filtros["precio_min"],false);
			}
			//filtrar maxima area 
			if(isset($filtros["area_max"])) {
				$cond[] = "rebi_area <=" . $this->oBD->escapar($filtros["area_max"],false);
			}
			//filtrar minima area
			if(isset($filtros["area_min"])) {
				$cond[] = "rebi_area >=" . $this->oBD->escapar($filtros["area_min"],false);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM registro_bien";
			}
			if(!empty($inner_join)) {
				$sql .= " inner join ".$inner_join["tabla"] ." on " .$inner_join["tabla"].".".$inner_join["id_tabla"]."=registro_bien.".$inner_join["id_tabla"];
			}
			//agrego de manera manual un inner jion con la tabla "tabla","tipo_codigo" .. para tipos de interior
			$sql .= " left join tabla on tabla.tabl_id=registro_bien.tabl_tipo_interior ";			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			//aumento que no esté alquilado
				$sql .= " and rebi_alquiler=0";
			//echo $sql;die();		
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			/*foreach ($res as  $value) {
				echo $value["rebi_vadquisicion"];
				$value["rebi_vadquisicion"]=number_format($value["rebi_vadquisicion"],2,".",",");
			}*/
			/*for ($i=0; $i < count($res); $i++) { 
				$res[$i]["rebi_vadquisicion"]=number_format($res[$i]["rebi_vadquisicion"],2,".",",");
				$res[$i]["rebi_ult_val_act"]=number_format($res[$i]["rebi_ult_val_act"],2,".",",");
			}*/
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}
		
	public function insertar($rebi_modalidad,$rebi_tipo_bien,$casb_codigo,$rebi_fadquisicion,$rebi_vadquisicion,$rebi_ult_dep_acum,$rebi_ult_val_act,$tabl_estado,$rebi_detalles,$depe_id_ubica,$tabl_condicion,$rebi_reg_publicos,$rebi_area,$rebi_observaciones,$depe_id,$rebi_anno,$rebi_alquiler,$tabl_tipo_interior,$rebi_numero_interior,$rebi_foto_principal)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT rebi_id+1 FROM registro_bien ORDER BY rebi_id DESC limit 0,1 ");			
			$estados = array('rebi_modalidad'=>$rebi_modalidad
							,'rebi_tipo_bien'=>$rebi_tipo_bien
							,'casb_codigo'=>$casb_codigo
							,'rebi_fadquisicion'=>$rebi_fadquisicion
							,'rebi_vadquisicion'=>$rebi_vadquisicion
							,'rebi_ult_dep_acum'=>$rebi_ult_dep_acum
							,'rebi_ult_val_act'=>$rebi_ult_val_act
							,'tabl_estado'=>$tabl_estado
							,'rebi_detalles'=>$rebi_detalles
							,'depe_id_ubica'=>$depe_id_ubica
							,'tabl_condicion'=>$tabl_condicion
							,'rebi_reg_publicos'=>$rebi_reg_publicos
							,'rebi_area'=>$rebi_area
							,'rebi_observaciones'=>$rebi_observaciones
							,'depe_id'=>$depe_id
							,'rebi_anno'=>$rebi_anno
							,'rebi_alquiler'=>$rebi_alquiler
							,'tabl_tipo_interior'=>$tabl_tipo_interior
							,'rebi_numero_interior'=>$rebi_numero_interior	
							,'rebi_foto_principal'=>$rebi_foto_principal							
							);			
			return $this->oBD->insert('registro_bien', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_registro_bien_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $rebi_modalidad,$rebi_tipo_bien,$casb_codigo,$rebi_fadquisicion,$rebi_vadquisicion,$rebi_ult_dep_acum,$rebi_ult_val_act,$tabl_estado,$rebi_detalles,$depe_id_ubica,$tabl_condicion,$rebi_reg_publicos,$rebi_area,$rebi_observaciones,$depe_id,$rebi_anno,$rebi_alquiler,$tabl_tipo_interior,$rebi_numero_interior,$rebi_foto_principal)
	{
		try {			
			$estados = array('rebi_modalidad'=>$rebi_modalidad
							,'rebi_tipo_bien'=>$rebi_tipo_bien
							,'casb_codigo'=>$casb_codigo
							,'rebi_fadquisicion'=>$rebi_fadquisicion
							,'rebi_vadquisicion'=>$rebi_vadquisicion
							,'rebi_ult_dep_acum'=>$rebi_ult_dep_acum
							,'rebi_ult_val_act'=>$rebi_ult_val_act
							,'tabl_estado'=>$tabl_estado
							,'rebi_detalles'=>$rebi_detalles
							,'depe_id_ubica'=>$depe_id_ubica
							,'tabl_condicion'=>$tabl_condicion
							,'rebi_reg_publicos'=>$rebi_reg_publicos
							,'rebi_area'=>$rebi_area
							,'rebi_observaciones'=>$rebi_observaciones
							,'depe_id'=>$depe_id
							,'rebi_anno'=>$rebi_anno
							,'rebi_alquiler'=>$rebi_alquiler
							,'tabl_tipo_interior'=>$tabl_tipo_interior
							,'rebi_numero_interior'=>$rebi_numero_interior	
							,'rebi_foto_principal'=>$rebi_foto_principal								
							);
			$this->oBD->update('registro_bien ', $estados, array('rebi_id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('registro_bien', array('rebi_id' => $id));
			else 
				return $this->oBD->update('registro_bien', array('estado' => -1), array('rebi_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('registro_bien', array($propiedad => $valor), array('rebi_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Registro_bien").": " . $e->getMessage());
		}
	}  

		 
		
}
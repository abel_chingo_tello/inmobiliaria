<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-10-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatRol extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT idrol,idempresa,nombre,imagen,estado,usuario_registro,fecha_insert,fecha_update FROM rol";
			$cond = array();
			
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idempresa"])){
				if(is_array($filtros["idempresa"])){
					$cond[]=" idempresa IN ( ".implode(',',$filtros["idempresa"]).")";
				}else{
					$cond[] = " (idempresa = " . $filtros["idempresa"].") ";
				}
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["imagen"])) {
					$cond[] = "imagen = " . $this->oBD->escapar($filtros["imagen"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}else{ $cond[] = "estado >=0 ";}
			
			if(isset($filtros["usuario_registro"])) {
					$cond[] = "usuario_registro = " . $this->oBD->escapar($filtros["usuario_registro"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}
			if(isset($filtros["fecha_update"])) {
					$cond[] = "fecha_update = " . $this->oBD->escapar($filtros["fecha_update"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM rol";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";		
			//echo $sql;	
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
		
	public function insertar($idempresa,$nombre,$imagen,$estado,$usuario_registro)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT idrol+1 FROM rol ORDER BY idrol DESC limit 0,1 ");			
			$estados = array('idempresa'=>$idempresa
							,'nombre'=>$nombre
							,'imagen'=>$imagen
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro							
							);			
			return $this->oBD->insert('rol', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_rol_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$nombre,$imagen,$estado,$usuario_registro)
	{
		try {			
			$estados = array('idempresa'=>$idempresa
							,'nombre'=>$nombre
							,'imagen'=>$imagen
							,'estado'=>$estado
							,'usuario_registro'=>$usuario_registro								
							);
			$this->oBD->update('rol ', $estados, array('idrol' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('rol', array('idrol' => $id));
			else 
				return $this->oBD->update('rol', array('estado' => -1), array('idrol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('rol', array($propiedad => $valor), array('idrol' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol").": " . $e->getMessage());
		}
	}  

		 
		
}
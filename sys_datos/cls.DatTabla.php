<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-12-2020  
  * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */ 
class DatTabla extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT tabl_id,tabl_codigo,tabl_descripcion,tabl_fecharegistro,tabl_descripaux,tabl_codigoauxiliar FROM tabla";
			$cond = array();
			
			if(isset($filtros["tabl_id"])) {
					$cond[] = "tabl_id = " . $this->oBD->escapar($filtros["tabl_id"]);
			}
			if(isset($filtros["tabl_tipo"])) {
					$cond[] = "tabl_tipo = " . $this->oBD->escapar($filtros["tabl_tipo"]);
			}
			if(isset($filtros["tabl_codigo"])) {
					$cond[] = "tabl_codigo = " . $this->oBD->escapar($filtros["tabl_codigo"]);
			}
			if(isset($filtros["tabl_descripcion"])) {
					$cond[] = "tabl_descripcion = " . $this->oBD->escapar($filtros["tabl_descripcion"]);
			}
			if(isset($filtros["tabl_fecharegistro"])) {
					$cond[] = "tabl_fecharegistro = " . $this->oBD->escapar($filtros["tabl_fecharegistro"]);
			}
			if(isset($filtros["tabl_descripaux"])) {
					$cond[] = "tabl_descripaux = " . $this->oBD->escapar($filtros["tabl_descripaux"]);
			}
			if(isset($filtros["tabl_codigoauxiliar"])) {
					$cond[] = "tabl_codigoauxiliar = " . $this->oBD->escapar($filtros["tabl_codigoauxiliar"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM tabla";
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}
		
	public function insertar($tabl_codigo,$tabl_descripcion,$tabl_fecharegistro,$tabl_descripaux,$tabl_codigoauxiliar)
	{
		try {			
			//$id = $this->oBD->consultarEscalarSQL("SELECT tabl_id+1 FROM tabla ORDER BY tabl_id DESC limit 0,1 ");			
			$estados = array('tabl_codigo'=>$tabl_codigo
							,'tabl_descripcion'=>$tabl_descripcion
							,'tabl_fecharegistro'=>$tabl_fecharegistro
							,'tabl_descripaux'=>$tabl_descripaux
							,'tabl_codigoauxiliar'=>$tabl_codigoauxiliar							
							);			
			return $this->oBD->insert('tabla', $estados,true);
			//return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_tabla_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tabl_codigo,$tabl_descripcion,$tabl_fecharegistro,$tabl_descripaux,$tabl_codigoauxiliar)
	{
		try {			
			$estados = array('tabl_codigo'=>$tabl_codigo
							,'tabl_descripcion'=>$tabl_descripcion
							,'tabl_fecharegistro'=>$tabl_fecharegistro
							,'tabl_descripaux'=>$tabl_descripaux
							,'tabl_codigoauxiliar'=>$tabl_codigoauxiliar								
							);
			$this->oBD->update('tabla ', $estados, array('tabl_id' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('tabla', array('tabl_id' => $id));
			else 
				return $this->oBD->update('tabla', array('estado' => -1), array('tabl_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('tabla', array($propiedad => $valor), array('tabl_id' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tabla").": " . $e->getMessage());
		}
	}  

		 
		
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-02-2021  
  * @copyright	Copyright (C) 2021. Todos los derechos reservados.
 */ 
class DatVistapagos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Vistapagos").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM vistapagos";
			$cond = array();
			
			if(isset($filtros["fecha_vencimiento"])) {
					$cond[] = "fecha_vencimiento = " . $this->oBD->escapar($filtros["fecha_vencimiento"]);
			}
			if(isset($filtros["vivienda"])) {
					$cond[] = "vivienda = " . $this->oBD->escapar($filtros["vivienda"]);
			}
			if(isset($filtros["num_documento"])) {
					$cond[] = "num_documento = " . $this->oBD->escapar($filtros["num_documento"]);
			}
			if(isset($filtros["nombres"])) {
					$cond[] = "( nombres " . $this->oBD->like($filtros["nombres"])." or num_documento " . $this->oBD->like($filtros["nombres"]).")";
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			
			if(isset($filtros["monto_total"])) {
					$cond[] = "monto_total = " . $this->oBD->escapar($filtros["monto_total"]);
			}
			if(isset($filtros["monto_mensual"])) {
					$cond[] = "monto_mensual = " . $this->oBD->escapar($filtros["monto_mensual"]);
			}
			if(isset($filtros["fecha_insert"])) {
					$cond[] = "fecha_insert = " . $this->oBD->escapar($filtros["fecha_insert"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "nombre " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["fecha_inicio"]) && !isset($filtros["fecha_fin"])) {
				$cond[] = "DATE(fecha_insert) >= " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_fin"]) && !isset($filtros["fecha_inicio"])) {
				$cond[] = "DATE(fecha_insert) <=" . $this->oBD->escapar($filtros["fecha_fin"]);
			}
			if(isset($filtros["fecha_fin"]) && isset($filtros["fecha_inicio"])) {
				$cond[] = " ( DATE(fecha_insert) BETWEEN " . $this->oBD->escapar($filtros["fecha_inicio"]) ." and " . $this->oBD->escapar($filtros["fecha_fin"])." )";
			}			
			
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM vistapagos";
			}
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			//$sql .= " ORDER BY fecha_creado ASC";	
			//procedimiento que actualiza morosos y al dia
			$this->oBD->ejecutarSQL("CALL actualizarmorosos;");		
			
			$res=$this->oBD->consultarSQL($sql);
			if(isset($filtros["sqlget"]))  return empty($res) ? array() : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Vistapagos").": " . $e->getMessage());
		}
	}
		
		 
		
}
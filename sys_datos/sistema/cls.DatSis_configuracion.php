<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatSis_configuracion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null){
		try {
			$sql = "SELECT config,valor,autocargar FROM sys_configuracion";
			$cond = array();			
			if(isset($filtros["valor"])) {
					$cond[] = "valor = " . $this->oBD->escapar($filtros["valor"]);
			}
			if(isset($filtros["autocargar"])) {
					$cond[] = "autocargar = " . $this->oBD->escapar($filtros["autocargar"]);
			}			
			if(isset($filtros["texto"])) {
				$cond[] = "valor " . $this->oBD->like($filtros["texto"]);
			}			
			if(isset($filtros["sqlcontar"])){//si es contar
				$sql = "SELECT count(1) FROM sys_configuracion";
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//$sql .= " ORDER BY fecha_creado ASC";
			$res=$this->oBD->consultarSQL($sql);			
			if(isset($filtros["sqlget"]))  return empty($res) ? null : $res[0];
			return $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Sys_configuracion").": " . $e->getMessage());
		}
	}
	public function insertar($valor,$autocargar)
	{
		try {			
			$id = $this->oBD->consultarEscalarSQL("SELECT config+1 FROM sys_configuracion ORDER BY sys_configuracion DESC limit 0,1 ");			
			$estados = array('config' => $id
							
							,'valor'=>$valor
							,'autocargar'=>$autocargar							
							);			
			$this->oBD->insert('sys_configuracion', $estados);
			return $id;
		} catch(Exception $e) {
			//$this->cancelarTransaccion('dat_sys_configuracion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Sys_configuracion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $valor,$autocargar)
	{
		try {			
			$estados = array('valor'=>$valor
							,'autocargar'=>$autocargar								
							);
			$this->oBD->update('sys_configuracion ', $estados, array('config' => $id));		   
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sys_configuracion").": " . $e->getMessage());
		}
	}	
	public function eliminar($id,$deBD=false)
	{
		try {
			if($deBD)
				return $this->oBD->delete('sys_configuracion', array('config' => $id));
			else 
				return $this->oBD->update('sys_configuracion', array('estado' => -1), array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Sys_configuracion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{
		try {
			$this->oBD->update('sys_configuracion', array($propiedad => $valor), array('config' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Sys_configuracion").": " . $e->getMessage());
		}
	} 
}

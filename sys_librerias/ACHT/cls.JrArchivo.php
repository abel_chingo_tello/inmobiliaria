<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrArchivo
{
	public $extension;
	public $permitidos;
	
	public function __construct()
	{
		$this->permitidos = array('jpeg', 'png', 'gif', 'bmp');
	}
	
	public static function getExtensionBase64($data)
	{
		$ext = substr($data, 5, strpos($data, ';') - 5);
		$ext_ = explode('/', $ext);
		$ext = array('ext' => $ext_[1], 'mime_type' => $ext);
		if(in_array($ext_[0], array('image'))) {
			$ext['tipo_gen'] = 'imagen';
		}		
		if('jpeg' == $ext['ext']) {
			$ext['ext'] = 'jpg';
		}
		
		return $ext;
	}
	
	public static function mimeAExt($mime_type)
	{
		$ext = explode('/', $mime_type);
		$ext = array('ext' => $ext_[1], 'mime_type' => $ext);
		if('jpeg' == $ext['ext']){$ext['ext'] = 'jpg';}		
		if(in_array($ext_[0], array('image'))) {$ext['tipo_gen'] = 'imagen';}		
		return $ext;
	}
	
	public static function getExtension($nombre)
	{
		$nombre_ = explode('.', $nombre);
		$ext = strtolower($nombre_[count($nombre_)-1]);
		
		if('jpeg' == @$ext['ext']) {
			$ext['ext'] = 'jpg';
		}
		
		return $ext;
	}
	
	public static function cargar($nom_archivo, $ruta_destino, $re_nombre)
	{//id_file
		//[name] => clases.png, [type] => image/x-png, [tmp_name] => C:\WINDOWS\Temp\php7B.tmp
		//[error] => 0, [size] => 61115
		
		if(is_uploaded_file(@$_FILES[$nom_archivo]['tmp_name'])) {
			if(($_FILES[$nom_archivo]['size']/1024) > 2048) {
				throw new Exception('Solo puede subir fotos de hasta 2 MB');
			}
			
			$ext = JrArchivo::getExtension($_FILES[$nom_archivo]['name']);
			if(!in_array($ext, array('jpeg', 'png', 'gif', 'bmp', 'jpg'))){
				throw new Exception('Tipo de archivo no v�lido');
			}
			
			$nom_tmp = @explode('.', $_FILES[$nom_archivo]['name']);
			$re_nombre = empty($re_nombre) ? JrArchivo::limpiar_nombre($nom_tmp[0]) : $re_nombre;
			
			if(file_exists($ruta_destino . $re_nombre . '.' . $ext)) {
				$re_nombre .= '-1';
			}
			
			if(!move_uploaded_file($_FILES[$nom_archivo]['tmp_name'], $ruta_destino . $re_nombre . '.' . $ext)) {
				throw new Exception('El archivo ha sido movido');
			}
			
			@unlink($_FILES[$nom_archivo]['tmp_name']);
			
			$data = array('nombre_full' => $re_nombre . '.' . $ext, 'nombre' => $re_nombre, 'ext' => $ext, 'mime_type' => $_FILES[$nom_archivo]['type']);
						
			if(in_array($ext, array('jpeg', 'png', 'gif', 'bmp', 'jpg'))) {
				$data['tipo_gen'] = 'imagen';
			} elseif(in_array($ext, array('wmv'))) {
				$data['tipo_gen'] = 'video';
			}
			
			return $data;
		} else {
			throw new Exception(JrTexto::_('The file size should not exceed 2MB'));
		}
	}
	
	public static function cargar_base64($data64, $ruta_destino, $re_nombre)
	{
		try {
			if(empty($data64)) {
				throw new Exception(JrTexto::_('Select a file'));
			}			
			JrCargador::clase('ACHT::JrSession',RUTA_LIBS);			
			$ext_full = JrArchivo::getExtensionBase64($data64);
			$re_nombre = empty($re_nombre) ? date('YmdHis') : $re_nombre;
						
			$base64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data64));
			$linkdestino=$ruta_destino;
			$destino=RUTA_BASE.str_replace('/', SD,$ruta_destino);
			@mkdir($destino,0775,true);
			if(!is_dir($destino) || !is_writable($destino)) {					
				return array('code'=>'Error','msj'=>'Error de directorio - '.$destino);
			}

			if(file_exists($destino . $re_nombre . '.' . $ext_full['ext'])){$re_nombre .= date('YmdHis');}			
			file_put_contents($destino. $re_nombre.'.'.$ext_full['ext'], $base64);
			@chmod($destino. $re_nombre . '.' . $ext_full['ext'], 0775);
			$linkdestino=$linkdestino.$re_nombre.'.'.$ext_full['ext'];
			return array('code'=>200,'msj'=>JrTexto::_('File Upload success'),'link'=>$linkdestino,'nombre'=>$re_nombre.'.'.$ext_full['ext']);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public static function limpiar_nombre($cadena)
	{
		//$cadena = strtolower(htmlentities(self::utf8($cadena), ENT_QUOTES, "UTF-8"));
		$cadena = utf8_decode(strip_tags($cadena));
		$cadena = str_replace(' ', '-', $cadena);		
		$b = array("�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","'", "�", " ",",",".",";",":","�","!","�","?",'"','+','(',')');
		$c = array("a","e","i","o","u","a","e","i","o","u","a","e","i","o","u","n","","","","","","","","","","","",'','-','','');
		$cadena = str_replace($b, $c, $cadena);		
		return $cadena;
	}

	public static function subirarchivo($file,$destino,$nombre="",$tipo='image'){
		try {			
			$filename=$file["name"];
			$filetype=$file["type"];
			$fileruta=$file["tmp_name"];
			$filesize=$file["size"];
			$intipo='';
			if($tipo=='video'){
				$intipo=array('mp4','mov');
			}elseif($tipo=='image'){
				$intipo=array('jpg', 'jpeg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}elseif($tipo=='documento'){
				$intipo=array('pdf', 'ppt','doc','pptx','docx','xls','xlsx','zip','html');
			}
			$ext=strtolower(pathinfo($filename, PATHINFO_EXTENSION));
			$filename=str_replace(".".$ext, '', $filename);			
			if(!in_array($ext, $intipo)){
				return array('code'=>'Error','msj'=>JrTexto::_('File type incorrect'));
			}
			try{				
				$newname=(!empty($nombre)?$nombre:($filename.date('YmdHis'))).'.'.$ext;
				$linkdestino=$destino;

				$destino=RUTA_BASE.str_replace('/', SD,$destino);
				@mkdir($destino,0775,true);
				if(!is_dir($destino) || !is_writable($destino)) {					
					return array('code'=>'Error','msj'=>'Error de directorio - '.$destino);
				}
				if(move_uploaded_file($fileruta,$destino.$newname)) 
			  	{			   		
					$linkdestino=$linkdestino.$newname;
					@chmod($destino.$newname, 0775);
			   		return array('code'=>200,'msj'=>JrTexto::_('File Upload success'),'link'=>$linkdestino,'nombre'=>$newname);
			  	}else{
			  		return array('code'=>'Error','msj'=>$destino.$newname.' <br> Archivo no pudo ser subido');
			  	}
			}catch(Exception $e){				
				return array('code'=>'Error','msj'=>JrTexto::_('Error upload File'));
			}
		}catch(Exception $e) {
			return array('code'=>'Error','msj'=>JrTexto::_('Error upload File'));
		}

	}
}
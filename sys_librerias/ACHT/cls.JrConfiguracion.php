<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

class JrConfiguracion
{
	protected static $configs = array();
	protected static $instancia;
	
	public function __construct()
	{
		self::$configs['nombre_sitio'] = '';
		self::$configs['com_defecto'] = 'index';

	}
	
	public static function getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new self;
		}
		
		return self::$instancia;
	}
	
	public static function get($propiedad)
	{
		JrConfiguracion::getInstancia();
		
		if(isset(self::$configs[$propiedad])) {
			return self::$configs[$propiedad];
		} else {
			return null;
		}
	}
	
	public static function get_()
	{
		return self::$configs;
	}
}
<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
JrCargador::clase('PHPMailer::src::PHPMailer',RUTA_LIBS);
JrCargador::clase('PHPMailer::src::SMTP',RUTA_LIBS);
//JrCargador::clase('PHPMailer::src::Exception',RUTA_LIBS);
class JrCorreo
{
	private $remiteEmail = null;
	private $remiteNom = null;
	private $destinatarios = array();
	private $destinatarioscc = array();
	private $destinatariosbcc = array();
	private $adjuntos = array();
	private $asunto = 'Asunto por defecto';
	private $mensaje = 'sin mensaje';
	private $tipo = 'HTML';
	private $SMTPAuth=true;
	private $SMTPDebug=0; //0 no log; 1 mostrar log
	private $SMTPSecure   = "ssl";  
	public	$Host = 'smtp.gmail.com'; 
	public  $Port = 465; 
	public  $Username = 'abelchingodeveloper@gmail.com';
    public  $Password = 'developer2021';

	public function __construct()
	{
		$this->limpiarDestinatarios();
		$this->setRemitente($this->Username,'Servidor Web');
		//$this->setAsunto('Información de servidor');
	}
	
	
	public function limpiarDestinatarios()
	{
		$this->destinatarios = array();
		$this->destinatarioscc = array();
		$this->destinatariosbcc = array();
	}

	public function setRemitente($email, $nombre = null)
	{
		$email = urldecode($email);
		if(preg_match("/\r/", $email) || preg_match("/\n/", $email)) {
			return;
		}
		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}		
		$this->remiteEmail = $email;
		$this->remiteNom = $nombre;
	}

	public function setAsunto($asunto)
	{
		$this->asunto = (string) $asunto;
	}

	public function setMensaje($mensaje)
	{
		$this->mensaje = $mensaje;
	}

	public function sendPhpmailer(){	
		if(0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}

		//use PHPMailer\PHPMailer;
		$mail = new PHPMailer(true);

		$mail->SMTPDebug = $this->SMTPDebug;                    // Enable verbose debug output
	    $mail->isSMTP();                                        // Send using SMTP
	   	$mail->Host = $this->Host;                     			// Set the SMTP server to send through
	    $mail->SMTPAuth   = $this->SMTPAuth;                    // Enable SMTP authentication
	    $mail->Username   = $this->Username;                    // SMTP username
	    $mail->Password   = $this->Password;                    // SMTP password
	    $mail->SMTPSecure = $this->SMTPSecure;					// PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	    $mail->Port       = $this->Port;                        // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
	    $mail->From = !empty($this->remiteEmail)?$this->remiteEmail:$mail->Username;
		$mail->FromName = !empty($this->remiteNom)?$this->remiteNom:'SoporteInfo';		
		$mail->setFrom($mail->From, $mail->FromName);
		foreach($this->destinatarios as $destinatario) {
			$mail->addAddress($destinatario['email'], @$destinatario['nombre']);
		}
		if(!empty($this->destinatarioscc))
		foreach($this->destinatarioscc as $destinatario) {
			$mail->addCC($destinatario['email'], @$destinatario['nombre']);
		}
		if(!empty($this->destinatariosbcc))
		foreach($this->destinatariosbcc as $destinatario) {
			$mail->AddBCC($destinatario['email'], @$destinatario['nombre']);
		}
		//$mail->addReplyTo('info@example.com', 'Information');
		$mail->Subject = utf8_decode($this->asunto);
		if(!empty($this->adjuntos)){
			foreach ($this->adjuntos as $nombre=>$ruta) {
				$mail->addAttachment($ruta, $nombre);
				$arrRuta = explode('.', $ruta);
				$extension = end($arrRuta);
				if( $extension== 'png' || $extension=='jpg'){
					$mail->addEmbeddedImage($ruta, $nombre);
				}
			}
		}
		$mail->Body = $this->mensaje;
		$mail->IsHTML(true);       
		if(!$mail->Send()) {
			return false;
		}
		return true;		
	}


	public function addadjuntos($ruta, $nombre_mostrar)
	{
		if(!empty($nombre_mostrar)) {
			$this->adjuntos[$nombre_mostrar] = $ruta;
		} else {
			$this->adjuntos[] = $ruta;
		}
	}


	public function addDestinatario($direccion, $nombre = null,$adonde='Des')
	{//preg_match
		$direccion = urldecode($direccion);
		if(preg_match("/\r/", $direccion) || preg_match("/\n/", $direccion)) {
			return;
		}		
		$nombre = urldecode($nombre);
		if(preg_match("/\r/", $nombre) || preg_match("/\n/", $nombre)) {
			return;
		}
		if($adonde=='Des')
			$this->destinatarios[] = array('email' => $direccion, 'nombre' => @$nombre);
		elseif($adonde=='Cc')
			$this->destinatarioscc[] = array('email' => $direccion, 'nombre' => @$nombre);
		elseif($adonde=='Bcc')
			$this->destinatariosbcc[] = array('email' => $direccion, 'nombre' => @$nombre);
	}

	public function addDestinarioPhpmailer($direccion, $nombre = null)
	{ //preg_match
		$this->addDestinatario($direccion, $nombre);
	}

	public function sendMail($tipo='HTML'){
		if(0 == count($this->destinatarios)) {
			throw new Exception('Correo sin destinatario');
		}
		$strdestinatarios=[];
		foreach($this->destinatarios as $destinatario){
			if(!empty($destinatario['nombre'])) {
				$strdestinatarios[] = $destinatario['nombre'] . ' <'.$destinatario['email'].'>';
			} else {
				$strdestinatarios[] = $destinatario['email'];
			}				
		}
		$destinatarios = implode(", ", $strdestinatarios);
		$destinatarioscc='';
		if(!empty($this->destinatarioscc)){
			$strdestinatarioscc=[];
			foreach($this->destinatarioscc as $destinatario){
				if(!empty($destinatario['nombre'])) {
					$strdestinatarioscc[] = $destinatario['nombre'] . ' <'.$destinatario['email'].'>';
				} else {
					$strdestinatarioscc[] = $destinatario['email'];
				}
			}
			$destinatarioscc = 'Cc: '.(implode(", ", $strdestinatarioscc))."\r\n";
		}
		$destinatariosbcc='';
		if(!empty($this->destinatariosbcc)){
			$strdestinatariosbcc=[];
			foreach($this->destinatariosbcc as $destinatario){
				if(!empty($destinatario['nombre'])) {
					$strdestinatariosbcc[] = $destinatario['nombre'] . ' <'.$destinatario['email'].'>';
				} else {
					$strdestinatariosbcc[] = $destinatario['email'];
				}				
			}
			$destinatariosbcc = 'Bcc: '.(implode(", ", $strdestinatariosbcc))."\r\n";
		}
		$cabeceras = "From: ".$this->remiteNom." <".$this->remiteEmail.">\r\n";
		$cabeceras .= "Reply-To: ".$this->remiteEmail."\r\n"; 
		$cabeceras .= "MIME-Version: 1.0\r\n".$destinatarioscc.$destinatariosbcc."\r\n";
		$cabeceras .= "X-Priority: 1 (Highest)\n";
        if('HTML' == $tipo) {
			$cabeceras .= "Content-type: text/html; charset=iso-8859-1 \r\n";
			$mensaje = $this->mensaje;
		} else {
			$cabeceras .= "Content-Type: text/plain; charset=iso-8859-1 \r\n";
			$mensaje = wordwrap($this->mensaje, 80);
		}
		$enviado = @mail($destinatarios, $this->asunto, $mensaje, $cabeceras);
		if ($enviado) return true;
		else return false;
	}
}

<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

class JrExcepcion extends Exception
{
	public function __construct($msj)
	{
		echo '<h3>'.$msj.'</h3>';
		exit();
	}
}

<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

//funciona con class_upload
class JrImagen
{
	public static $ext;
	
	public function __construct()
	{
	}
	
	public static function crearImagen(&$imagen, $destino, $nombre, $ancho = 50, $alto = 50, $mant_relac = false)
	{
		$imagen->file_new_name_body = $nombre;
		$imagen->image_x = $ancho;
		$imagen->image_y = $alto;
		//$imagen->image_max_height = '80';
		
		//otros detalles
		$imagen->image_resize = true;
		$imagen->tinku = true;
		
		if(false === $mant_relac) {
			$imagen->image_ratio_crop = true;
			//$imagen->image_background_color = '#FF00FF';
		} else {
			//$imagen->image_ratio = true;
			$imagen->image_ratio_y = true;
		}
		
		return $imagen->Process($destino);
	}
	
	public static function mover($imagen, $destino, $nombre)
	{
		//[name] => clases.png, [type] => image/x-png, [tmp_name] => C:\WINDOWS\Temp\php7B.tmp
		//[error] => 0, [size] => 61115
		if(empty($_FILES[$imagen]) || !JrImagen::esImagen($_FILES[$imagen]['name'])) {return false;}
		
		$nom_tmp	= @explode('.', $_FILES[$imagen]['name']);
		$ext		= @$nom_tmp[count($nom_tmp) - 1];
		self::$ext = $ext;
		
		$nombre .= '.' . $ext;
		$destino .= $nombre;
		
		@move_uploaded_file($_FILES[$imagen]['tmp_name'], $destino);
		@unlink($_FILES[$imagen]['tmp_name']);
		
		return array('nombre' => $nombre, 'ext' => $ext);
	}
	
	public static function jr_mover($archivo, $destino, $nombre)
	{
		//[name] => clases.png, [type] => image/x-png, [tmp_name] => C:\WINDOWS\Temp\php7B.tmp
		//[error] => 0, [size] => 61115
		if(empty($archivo) && JrImagen::esImagen($archivo['name'])) {return false;}
		
		$nom_tmp	= @explode('.', $archivo['name']);
		$ext		= @$nom_tmp[count($nom_tmp) - 1];
		self::$ext = $ext;
		$nombre .= '.' . $ext;
		$destino .= $nombre;
		
		move_uploaded_file($archivo['tmp_name'], $destino);
		@unlink($archivo['tmp_name']);
		
		return array('nombre' => $nombre, 'ext' => $ext);
	}
	
	public static function copiar($origen, $destino)
	{
		if(is_file($origen)) {
			copy($origen, $destino);
			return JrImagen::eliminar($origen);
		}
	}
	
	public static function eliminar($archivo)
	{
		@unlink($archivo);
	}
	
	public static function esImagen($nombre)
	{
		$nombre_ = explode('.', $nombre);
		$ext = $nombre_[count($nombre_)-1];
		
		if(!in_array(strtolower($ext), array('png', 'gif', 'jpg', 'jpeg', 'jpg'))) {
			return false;
		}
		
		return true;
	}
	
	public function getToJpg($imagen, $destino)
	{
		//getimagesize = Array ( [0] => 3072 [1] => 2304 [2] => 2 [3] => width="3072" height="2304"
		//[bits] => 8 [channels] => 3 [mime] => image/jpeg )
		
		if(!is_file($imagen['tmp_name'])) {return;}		
		$img_det = getimagesize($imagen['tmp_name']);
		$ext = image_type_to_extension($img_det[2], false);
		$imagecreate = "imagecreatefrom" . $ext;		
		$img_orig = $imagecreate($imagen['tmp_name']);
		imagejpeg($img_orig, $destino . '.jpg');
		
		return $destino . '.jpg';
	}
}
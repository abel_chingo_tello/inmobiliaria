<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrObjeto
{
	private $errores = array();
	
	public function __construct(){}
	
	public function get($propiedad, $defecto = null)
	{
		if(isset($this->$propiedad)) {
			return $this->$propiedad;
		}
		return $defecto;
	}
	
	public function getPropiedades($public = true)
	{
		$vars  = get_object_vars($this);
		
        if($public) {
			foreach($vars as $key => $value) {
				if('_' == substr($key, 0, 1)) {
					unset($vars[$key]);
				}
			}
		}
		
        return $vars;
	}
	
	public static function set($propiedad, $value, $compat = null)
	{
		$previo = isset($this->$propiedad) ? $this->$propiedad : null;
		$this->$propiedad = $value;
		
		return $previo;
	}

	protected function setProperties($propiedades)
	{
		$propiedades = (array) $propiedades;

		if(is_array($propiedades)) {
			foreach ($propiedades as $k => $v) {
				$this->$k = $v;
			}
			
			return true;
		}
		
		return false;
	}
	
	public function setError($error)
	{
		array_push($this->_errors, $error);
	}
	
	public function toString()
	{
		return get_class($this);
	}
	
	public function __get($propiedad)
	{
		if(property_exists($this, $propiedad)) {
			return $this->$propiedad;
		}
	}
	
	public function __set($propiedad, $valor)
	{
		if(property_exists($this, $propiedad)) {
			$this->$propiedad = $valor;
		}
	}
}

<?php
/**
 * @autor		Abel chingo Tello
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

class JrSession extends JrObjeto
{
	protected static $instancia;
	
	public function __construct()
	{
		$this->iniciar();
	}
	
	private function iniciar()
	{
		$configs = JrConfiguracion::get_();
		if(!empty($configs['dom_sesion'])) {
			session_set_cookie_params(0, '/', $configs['dom_sesion']);
		}
		@ini_set('session.gc_maxlifetime', 7200);
		@session_start();
		//header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');		
		return true;
	}
	
	public static function &getInstancia()
	{
		if(!is_object(self::$instancia)) {
			self::$instancia = new JrSession();
		}
		
		return self::$instancia;
	}
	
	public static function set($nombre, $valor, $espacio = 'defecto')
	{
		
		$espacio = '__' . $espacio;	
       
		$viejo = isset($_SESSION[$espacio][$nombre]) ?  $_SESSION[$espacio][$nombre] : null;
		
		if(null === $valor) {
			unset($_SESSION[$espacio][$nombre]);
		} else {
			$_SESSION[$espacio][$nombre] = $valor;
		}		
		
		return $viejo;
	}
	
	public function get($nombre, $defecto = null, $espacio = 'defecto')
	{//msj, null, __MSJS
		$espacio = '__' . $espacio;		
		if(isset($_SESSION[$espacio][$nombre])) {
			return $_SESSION[$espacio][$nombre];
		}		
		return $defecto;
	}

	public function getAll($espacio='defecto'){
		$espacio = '__' . $espacio;
		if(isset($_SESSION[$espacio])) {
			return $_SESSION[$espacio];
		}
		return null;
	}
	
	public function existe($nombre, $espacio = 'defecto' )
	{
		$espacio = '__'.$espacio;
		
		if($this->estado !== 'activo' ) {
			return null;
		}
		
		return isset($_SESSION[$espacio][$nombre] );
	}
	
	public function limpiar($nombre, $espacio = 'defecto' )
	{
		$espacio = '__' . $espacio;
		
		$valor = null;
		if(isset($_SESSION[$espacio][$nombre] ) ) {
			$valor = $_SESSION[$espacio][$nombre];
			unset($_SESSION[$espacio][$nombre]);
		}
		
		return $valor;
	}
	
	public static function limpiarEspacio($espacio = 'defecto' )
	{
		$espacio = '__' . $espacio;
		@session_start();		
		if(isset($_SESSION[$espacio])) {
			unset($_SESSION[$espacio]);
		}
	}
	
	public function destruir_()
	{
		if(isset($_COOKIE[session_name()])) {
			setcookie(session_name(), '', time() - 42000, '/');
		}		
		session_unset();
		session_destroy();		
		return true;
	}
	
	public function reiniciar()
	{
		$this->destruir();
		$this->iniciar();
		
		return true;
	}
	
	public static function crearRandom($long = 32, $md5 = true)
	{
		$id = 0;		
		while (strlen($id) < $long) {$id .= mt_rand(0, mt_getrandmax());}		
		$id = uniqid($id, true);		
		if(true === $md5) {	$id = md5($id);}		
		return $id;
	}
	
	private function setCookie() {
		$cookie	= session_get_cookie_params();		
		if($this->forzarSSL){$cookie['secure'] = true;}		
		session_set_cookie_params($cookie['lifetime'], $cookie['path'], $cookie['domain'], $cookie['secure']);
	}
}
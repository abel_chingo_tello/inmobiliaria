<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		04/02/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class JrTools
{
	public static function pasaH24A12($hora_24, $no_seg = true)
	{
		$hora = explode(':', $hora_24);
		
		if($hora[0] >= 12) {
			$hora[0] = sprintf("%'.02d", $hora[0] - 12);
			$hora[1] .= 'PM';
		} else {
			$hora[1] .= 'AM';
		}
		
		if($no_seg) {
			unset($hora[2]);
		}
		
		return implode(':', $hora);
	}
	
	public static function getCadUrl($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		// trim
		$text = trim($text, '-');
		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);
		// lowercase
		$text = strtolower($text);
		
		if(empty($text)) {
			return 'n-a';
		}
		
		return $text;
	}
	
	public static function getMes($mes, $corto = false)
	{
		$mes = sprintf("%02s", $mes);
		$meses = array('01' => 'enero', '02' => 'febrero', '03' => 'marzo', '04' => 'abril', '05' => 'mayo', '06' => 'junio', '07' => 'julio', '08' => 'agosto', '09' => 'septiembre', '10' => 'octubre', '11' => 'noviembre', '12' => 'diciembre');
		
		$mes = @$meses[$mes];
		
		if(true === $corto) {
			$mes = substr($mes, 0, 3);
		}
		
		return $mes;
	}
	
	public static function getDiaSem($fecha, $corto = false)
	{//y-m-d
		$fecha = explode("-", $fecha);
		$dias = array('domingo', 'lunes', 'martes', 'mi�rcoles', 'jueves', 'viernes', 's�bado');
    	$dia_ = $dias[date("w", mktime(0, 0, 0, $fecha[1], $fecha[2], $fecha[0]))];
		
		if(true === $corto) {
			$dia_ = substr($dia_, 0, 3);
		}
		
		return $dia_;
	}
	
	public static function getFechaATexto($fecha, $corto = false)
	{//y-m-d
		//setlocale(LC_TIME, "Spanish");
		//return ucfirst(strftime('%A %d de %B del %Y', strtotime($fecha)));
		if($fecha == date('Y-m-d') && $corto) {
			return 'Hoy';
		}
		
		$fecha_ = @explode('-', $fecha);
		
		$tex = JrTools::getDiaSem($fecha, $corto).', '.$fecha_[2].' de '.JrTools::getMes($fecha_[1], $corto).' de '.$fecha_[0];
		
		return ucfirst($tex);
	}
	
	public static function fechaPara($arr, $t1 = false, $t2 = false)
	{
		$str = $arr;
		
		if(!is_array($arr)) {
			$t = $t1-($t2?$t2:time());
			
			$p = array(
				'{s}' => 1,
				'{i}' => 60,
				'{h}' => 60*60,
				'{d}' => 60*60*24,
				'{w}' => 60*60*24*7,
				'{m}' => 60*60*24*30,
				'{y}' => 60*60*24*365
			);
			
			preg_match_all("/\{[sihdwmy]\}/", $str, $ma);
			
			$found = Array();
			foreach ($ma[0] as &$m) {
				$found[$m] = $p[$m];
			}
			arsort($found);
			
			foreach ($found as $i => &$fo) {
				$str = str_replace($i, (int) ($t/$fo), $str);
				$t = $t % $fo;
			}
		
			return $str;
		} else {
			$p = array(
				's' => 1,
				'i' => 60,
				'h' => 60*60,
				'd' => 60*60*24,
				'w' => 60*60*24*7,
				'm' => 60*60*24*30,
				'y' => 60*60*24*365
			);
			
			if (!is_array($arr)) {
				$t2 = $t1;
				$t1 = $arr;
				$r = &$p;
			}
			else {
				foreach ($arr as $ar) {
					$r[$ar] = $p[$ar];
				}
			}
		
			arsort($r);
		
			$t = $t1-($t2 ? $t2 : time());
			foreach ($r as $i => $q) {
				$r[$i] = (int) ($t/$q);
				$t = $t % $q;
			}
		
			return $r;
		}
	}
	
	public static function chekFechaHora($fecha_hora, $solo_fecha = false)
    {
		if (true == $solo_fecha) {
			return (date('Y-m-d', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		} else {
			return (date('Y-m-d H:i:s', strtotime($fecha_hora)) == $fecha_hora) ? true : false;
		}
    }
}
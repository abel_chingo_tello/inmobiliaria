<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */
class JrModulos extends JrObjeto
{
	private $doc;
	private static $instancia;
	public function __construct(&$doc_)
	{
		$this->doc = $doc_;
	}
	
	public function hacer($posicion)
	{
		$contenidos = '';
		$modulos = $this->getModulos($posicion);
		foreach($modulos as $mod)  {
			$contenidos .= $this->doc->cargarProceso('modulo', $mod['nombre']);
		}
		return $contenidos;
	}
	
	public function getModulos($posicion)
	{
		return array();
	}
	
	public static function &getInstancia(&$doc_, $subsistema = null, $atributos = array())
	{
		if(!is_object(self::$instancia)) {
			$ruta = null;
			if(!empty($subsistema)) {
				$ruta .= $subsistema;
			}
			
			$clase = 'Mod';
			$ruta .= 'inc::' . $clase;
			
			try {				
				JrCargador::clase($ruta, RUTA_FISICA);
			} catch(Exception $e) {
				$clase = 'JrModulos';
			}			
			self::$instancia = new $clase($doc_);
		}		
		return self::$instancia;
	}
}
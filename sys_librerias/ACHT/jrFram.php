<?php
/**
 * @autor		Abel Chingo Tello
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

defined('RUTA_BASE') or die();

JrCargador::clase('ACHT::JrObjeto',RUTA_LIBS);
JrCargador::clase('ACHT::JrInstancia',RUTA_LIBS);
JrCargador::clase('ACHT::JrAplicacion',RUTA_LIBS);
JrCargador::clase('ACHT::JrPeticion',RUTA_LIBS);
JrCargador::clase('ACHT::JrWeb',RUTA_LIBS);
JrCargador::clase('ACHT::JrConfiguracion',RUTA_LIBS);
JrCargador::clase('ACHT::JrTexto',RUTA_LIBS);
JrCargador::clase('ACHT::JrTools',RUTA_LIBS);
<?php
/**
 * @autor		Abel chingo Tello, basado en Joomla
 * @fecha		08/08/2020
 * @copyright	Copyright (C) 2020. Todos los derechos reservados.
 */

defined('RUTA_RAIZ') or die();
if(!defined('SD')) define('SD', DIRECTORY_SEPARATOR);

class JrCargador
{
	/**
	 * Lee una clase desde el directorio especificado
	 */
	public static function clase($clase, $dir = null, $clave = 'libs::', $pref = 'cls.', $salirExcep = true)
	{
		try {

			static $rutas;
			//var_dump($rutas);
			
			if(!isset($rutas)) {
				$rutas = array();
			}
			$claveRuta = $clase;
			if(!isset($rutas[$claveRuta])) {
				if(!$dir) {
					$dir = dirname(__FILE__) . SD;
				}				
				$partes = explode('::', $clase);				
				$nomClase = array_pop($partes);
				$nomClase = ucfirst($nomClase);
				if(empty($partes)){	$partes[0]='';}
				if($partes[0]=='ACHT' && $dir==null) $dir=RUTA_LIBS;
				$archivo = $pref . $nomClase . '.php';
				$ruta_ = $dir . implode(SD, $partes) . SD . $archivo;
				$rs	= JrCargador::registrar($nomClase, $ruta_);				
				if(!is_file($ruta_)){					
					throw new Exception('Archivo no encontrado: '.$ruta_);
				}				
				require_once($ruta_);				
				$rutas[$claveRuta] = $rs;
			}			
			return $rutas[$claveRuta];
		} catch(Exception $e) {
			
			if(true == $salirExcep) {
				JrCargador::clase('ACHT::JrWebExcepcion',RUTA_LIBS);
				echo new JrWebExcepcion($e->getMessage());
				exit(0);
			} else {
				throw new Exception($e->getMessage());
			}
		}
	}
	
	/**
	 * Agrega una clase a la autocarga
	 */
	public static function &registrar($clase = null, $archivo = null)
	{
		static $clases;
		
		if(!isset($clases)) {
			$clases = array();
		}
		//print_r($clase);
		if($clase && is_file($archivo)) {
			$clase = strtolower($clase);
			$clases[$clase] = $archivo;
		}
		
		return $clases;
	}
	
	public static function cargar($clase)
	{
		$clase = strtolower($clase);		
		if(class_exists($clase)) {  return;	}		
		$clases = JrCargador::registrar();
		if(array_key_exists(strtolower($clase), $clases)) {
			require_once($clases[$clase]);
			return true;
		}
		return false;
	}
	
	public static function archivo($archivo, $dir = null, $clave = 'libs::')
	{
		try {
			static $rutas;			
			if(!isset($rutas)) {$rutas = array();}			
			$claveRuta = $clave ? $clave . $archivo : $archivo;			
			if(!isset($rutas[$claveRuta])) {
				if(!$dir) {	$dir = dirname(dirname(__FILE__)). SD;}				
				$partes = explode('::', $archivo);
				$nomArchivo_ = array_pop($partes);				
				//define la ruta
				$ruta_  = str_replace('::', SD, $archivo);
				$ruta_ = $dir . $ruta_ . '.php';				
				$rs	= JrCargador::registrar($nomArchivo_, $ruta_);				
				require_once($ruta_);				
				$rutas[$claveRuta] = $rs;
			}			
			return $rutas[$claveRuta];
		} catch(Exception $e) {
			JrCargador::clase('ACHT::JrWebExcepcion',RUTA_LIBS);
			echo new JrWebExcepcion($e->getMessage());
			exit(0);
		}
	}
}

function mi_autoload($clase)
{
	//echo $clase;
	if(JrCargador::cargar($clase)) {
		return true;
	}
	return false;
}

function jrExit($message = 0) {
    exit($message);
}

function jrClase($ruta){
	return JrCargador::clase($ruta);
}
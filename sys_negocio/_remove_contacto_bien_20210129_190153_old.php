<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		26-01-2021
 * @copyright	Copyright (C) 26-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatContacto_bien', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegContacto_bien 
{
	
	protected $id;
	protected $rebi_id;
	protected $email;
	protected $nombre;
	protected $telefono;
	protected $dni;
	protected $mensaje;
	
	protected $dataContacto_bien;
	protected $oDatContacto_bien;	

	public function __construct()
	{
		$this->oDatContacto_bien = new DatContacto_bien;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatContacto_bien->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatContacto_bien->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatContacto_bien->iniciarTransaccion('neg_i_Contacto_bien');
			$this->id = $this->oDatContacto_bien->insertar($this->rebi_id,$this->email,$this->nombre,$this->telefono,$this->dni,$this->mensaje);
			$this->oDatContacto_bien->terminarTransaccion('neg_i_Contacto_bien');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatContacto_bien->cancelarTransaccion('neg_i_Contacto_bien');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatContacto_bien->actualizar($this->id,$this->rebi_id,$this->email,$this->nombre,$this->telefono,$this->dni,$this->mensaje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Contacto_bien', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatContacto_bien->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataContacto_bien = $this->oDatContacto_bien->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataContacto_bien)) {
				throw new Exception(JrTexto::_("Contacto_bien").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataContacto_bien["id"];
			$this->rebi_id = $this->dataContacto_bien["rebi_id"];
			$this->email = $this->dataContacto_bien["email"];
			$this->nombre = $this->dataContacto_bien["nombre"];
			$this->telefono = $this->dataContacto_bien["telefono"];
			$this->dni = $this->dataContacto_bien["dni"];
			$this->mensaje = $this->dataContacto_bien["mensaje"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataContacto_bien = $this->oDatContacto_bien->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataContacto_bien)) {
				throw new Exception(JrTexto::_("Contacto_bien").' '.JrTexto::_("not registered"));
			}

			return $this->oDatContacto_bien->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
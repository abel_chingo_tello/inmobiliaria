<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

//JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBase
{
	private $oDat;
	
	protected $s_id_negocio;
	protected $s_id_tienda;
	protected $s_id_usuario;
	
	public function __construct(&$oDat = null)
	{
		if(!empty($oDat)) {
			$this->oDat = $oDat;
		}		
	}
	
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function __set($prop, $valor)
	{
		$this->set__($prop, $valor);
	}
	
	private function set__($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}
	
	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set__($prop_, $valor);
			}
		}
		
		$this->set__($prop, $valor);
	}
	
	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->oDat->setLimite($desde, $desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

}
<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		10-08-2020
 * @copyright	Copyright (C) 10-08-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBiblioteca', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);*/
class NegBiblioteca 
{
	protected $idbiblioteca;
	protected $idempresa;
	protected $idpersona;
	protected $tipo;
	protected $archivo;
	protected $nombre;
	protected $estado;
	protected $dataBiblioteca;
	protected $oDatBiblioteca;	

	public function __construct()
	{
		$this->oDatBiblioteca = new DatBiblioteca;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;			
			$this->oDatBiblioteca->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBiblioteca->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('biblioteca', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBiblioteca->iniciarTransaccion('neg_i_Biblioteca');
			$this->idbiblioteca = $this->oDatBiblioteca->insertar($this->idempresa,$this->idpersona,$this->tipo,$this->archivo,$this->nombre,$this->estado);
			$this->oDatBiblioteca->terminarTransaccion('neg_i_Biblioteca');	
			return $this->idbiblioteca;
		} catch(Exception $e) {	
		    $this->oDatBiblioteca->cancelarTransaccion('neg_i_Biblioteca');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('biblioteca', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBiblioteca->actualizar($this->idbiblioteca,$this->idempresa,$this->idpersona,$this->tipo,$this->archivo,$this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Biblioteca', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBiblioteca->eliminar($this->idbiblioteca,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdbiblioteca($pk){
		try {
			$this->dataBiblioteca = $this->oDatBiblioteca->buscar(array('sqlget'=>true,'idbiblioteca'=>$pk));
			if(empty($this->dataBiblioteca)) {
				throw new Exception(JrTexto::_("Biblioteca").' '.JrTexto::_("not registered"));
			}
			$this->idbiblioteca = $this->dataBiblioteca["idbiblioteca"];
			$this->idempresa = $this->dataBiblioteca["idempresa"];
			$this->idpersona = $this->dataBiblioteca["idpersona"];
			$this->tipo = $this->dataBiblioteca["tipo"];
			$this->archivo = $this->dataBiblioteca["archivo"];
			$this->nombre = $this->dataBiblioteca["nombre"];
			$this->estado = $this->dataBiblioteca["estado"];
			$this->fecha_insert = $this->dataBiblioteca["fecha_insert"];
			$this->fecha_update = $this->dataBiblioteca["fecha_update"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {			
			$this->dataBiblioteca = $this->oDatBiblioteca->buscar(array('sqlget'=>true,'idbiblioteca'=>$pk));
			if(empty($this->dataBiblioteca)) {
				throw new Exception(JrTexto::_("Biblioteca").' '.JrTexto::_("not registered"));
			}
			return $this->oDatBiblioteca->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
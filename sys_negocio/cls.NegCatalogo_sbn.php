<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		25-01-2021
 * @copyright	Copyright (C) 25-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCatalogo_sbn', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegCatalogo_sbn 
{
	
	protected $casb_codigo;
	protected $casb_descripcion;
	
	protected $dataCatalogo_sbn;
	protected $oDatCatalogo_sbn;	

	public function __construct()
	{
		$this->oDatCatalogo_sbn = new DatCatalogo_sbn;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatCatalogo_sbn->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatCatalogo_sbn->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('catalogo_sbn', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatCatalogo_sbn->iniciarTransaccion('neg_i_Catalogo_sbn');
			$this->casb_codigo = $this->oDatCatalogo_sbn->insertar($this->casb_descripcion);
			$this->oDatCatalogo_sbn->terminarTransaccion('neg_i_Catalogo_sbn');	
			return $this->casb_codigo;
		} catch(Exception $e) {	
		    $this->oDatCatalogo_sbn->cancelarTransaccion('neg_i_Catalogo_sbn');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('catalogo_sbn', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatCatalogo_sbn->actualizar($this->casb_codigo,$this->casb_descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Catalogo_sbn', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCatalogo_sbn->eliminar($this->casb_codigo,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCasb_codigo($pk){
		try {
			$this->dataCatalogo_sbn = $this->oDatCatalogo_sbn->buscar(array('sqlget'=>true,'casb_codigo'=>$pk));
			if(empty($this->dataCatalogo_sbn)) {
				throw new Exception(JrTexto::_("Catalogo_sbn").' '.JrTexto::_("not registered"));
			}
			$this->casb_codigo=$this->dataCatalogo_sbn["casb_codigo"];
			$this->casb_descripcion = $this->dataCatalogo_sbn["casb_descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('catalogo_sbn', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataCatalogo_sbn = $this->oDatCatalogo_sbn->buscar(array('sqlget'=>true,'casb_codigo'=>$pk));
			if(empty($this->dataCatalogo_sbn)) {
				throw new Exception(JrTexto::_("Catalogo_sbn").' '.JrTexto::_("not registered"));
			}

			return $this->oDatCatalogo_sbn->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
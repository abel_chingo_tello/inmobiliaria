<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-12-2020
 * @copyright	Copyright (C) 23-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatClientes', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegClientes 
{
	
	protected $clie_id;
	protected $tabl_tipocliente;
	protected $clie_apellidos;
	protected $clie_nombres;
	protected $clie_razsocial;
	protected $clie_dni;
	protected $clie_direccion;
	protected $clie_telefono;
	protected $clie_email;
	protected $clie_estado;
	protected $clie_zona;
	protected $clie_empleado;
	
	protected $dataClientes;
	protected $oDatClientes;	

	public function __construct()
	{
		$this->oDatClientes = new DatClientes;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatClientes->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatClientes->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('clientes', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatClientes->iniciarTransaccion('neg_i_Clientes');
			$this->clie_id = $this->oDatClientes->insertar($this->tabl_tipocliente,$this->clie_apellidos,$this->clie_nombres,$this->clie_razsocial,$this->clie_dni,$this->clie_direccion,$this->clie_telefono,$this->clie_email,$this->clie_estado,$this->clie_zona,$this->clie_empleado);
			$this->oDatClientes->terminarTransaccion('neg_i_Clientes');	
			return $this->clie_id;
		} catch(Exception $e) {	
		    $this->oDatClientes->cancelarTransaccion('neg_i_Clientes');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('clientes', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatClientes->actualizar($this->clie_id,$this->tabl_tipocliente,$this->clie_apellidos,$this->clie_nombres,$this->clie_razsocial,$this->clie_dni,$this->clie_direccion,$this->clie_telefono,$this->clie_email,$this->clie_estado,$this->clie_zona,$this->clie_empleado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatClientes->cambiarvalorcampo($this->clie_id,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Clientes', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatClientes->eliminar($this->clie_id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setClie_id($pk){
		try {
			$this->dataClientes = $this->oDatClientes->buscar(array('sqlget'=>true,'clie_id'=>$pk));
			if(empty($this->dataClientes)) {
				throw new Exception(JrTexto::_("Clientes").' '.JrTexto::_("not registered"));
			}
			$this->clie_id=$this->dataClientes["clie_id"];
			$this->tabl_tipocliente = $this->dataClientes["tabl_tipocliente"];
			$this->clie_apellidos = $this->dataClientes["clie_apellidos"];
			$this->clie_nombres = $this->dataClientes["clie_nombres"];
			$this->clie_razsocial = $this->dataClientes["clie_razsocial"];
			$this->clie_dni = $this->dataClientes["clie_dni"];
			$this->clie_direccion = $this->dataClientes["clie_direccion"];
			$this->clie_telefono = $this->dataClientes["clie_telefono"];
			$this->clie_email = $this->dataClientes["clie_email"];
			$this->clie_estado = $this->dataClientes["clie_estado"];
			$this->clie_zona = $this->dataClientes["clie_zona"];
			$this->clie_empleado = $this->dataClientes["clie_empleado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('clientes', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataClientes = $this->oDatClientes->buscar(array('sqlget'=>true,'clie_id'=>$pk));
			if(empty($this->dataClientes)) {
				throw new Exception(JrTexto::_("Clientes").' '.JrTexto::_("not registered"));
			}

			return $this->oDatClientes->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
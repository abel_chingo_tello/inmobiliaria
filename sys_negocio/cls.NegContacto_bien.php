<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		05-02-2021
 * @copyright	Copyright (C) 05-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatContacto_bien', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegContacto_bien 
{
	
	protected $id;
	protected $tipo_documento;
	protected $nro_documento;
	protected $apellidos;
	protected $direccion;
	protected $rebi_id;
	protected $email;
	protected $nombre;
	protected $telefono;
	protected $mensaje;
	protected $file_infocorp;
	protected $file_recomendacion;
	protected $file_policial_judicial;
	protected $file_extra_recomendacion;
	protected $punataje;
	
	protected $dataContacto_bien;
	protected $oDatContacto_bien;	

	public function __construct()
	{
		$this->oDatContacto_bien = new DatContacto_bien;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatContacto_bien->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatContacto_bien->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatContacto_bien->iniciarTransaccion('neg_i_Contacto_bien');
			$this->id = $this->oDatContacto_bien->insertar($this->tipo_documento,$this->nro_documento,$this->apellidos,$this->direccion,$this->rebi_id,$this->email,$this->nombre,$this->telefono,$this->mensaje,$this->file_infocorp,$this->file_recomendacion,$this->file_policial_judicial,$this->file_extra_recomendacion,$this->punataje);
			$this->oDatContacto_bien->terminarTransaccion('neg_i_Contacto_bien');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatContacto_bien->cancelarTransaccion('neg_i_Contacto_bien');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatContacto_bien->actualizar($this->id,$this->tipo_documento,$this->nro_documento,$this->apellidos,$this->direccion,$this->rebi_id,$this->email,$this->nombre,$this->telefono,$this->mensaje,$this->file_infocorp,$this->file_recomendacion,$this->file_policial_judicial,$this->file_extra_recomendacion,$this->punataje);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Contacto_bien', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatContacto_bien->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataContacto_bien = $this->oDatContacto_bien->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataContacto_bien)) {
				throw new Exception(JrTexto::_("Contacto_bien").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataContacto_bien["id"];
			$this->tipo_documento = $this->dataContacto_bien["tipo_documento"];
			$this->nro_documento = $this->dataContacto_bien["nro_documento"];
			$this->apellidos = $this->dataContacto_bien["apellidos"];
			$this->direccion = $this->dataContacto_bien["direccion"];
			$this->rebi_id = $this->dataContacto_bien["rebi_id"];
			$this->email = $this->dataContacto_bien["email"];
			$this->nombre = $this->dataContacto_bien["nombre"];
			$this->telefono = $this->dataContacto_bien["telefono"];
			$this->mensaje = $this->dataContacto_bien["mensaje"];
			$this->file_infocorp = $this->dataContacto_bien["file_infocorp"];
			$this->file_recomendacion = $this->dataContacto_bien["file_recomendacion"];
			$this->file_policial_judicial = $this->dataContacto_bien["file_policial_judicial"];
			$this->file_extra_recomendacion = $this->dataContacto_bien["file_extra_recomendacion"];
			$this->punataje = $this->dataContacto_bien["punataje"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('contacto_bien', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataContacto_bien = $this->oDatContacto_bien->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataContacto_bien)) {
				throw new Exception(JrTexto::_("Contacto_bien").' '.JrTexto::_("not registered"));
			}

			return $this->oDatContacto_bien->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
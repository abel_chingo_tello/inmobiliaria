<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		10-02-2021
 * @copyright	Copyright (C) 10-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCronogramapago_cab', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegCronogramapago_cab 
{
	
	protected $id;
	protected $persona_id;
	protected $rebi_id;
	protected $garantia;
	protected $meses_contrato;
	protected $monto_mensual;
	protected $dia_pago;
	protected $archivo_contrato;
	protected $observacion;
	protected $estado;
	protected $estado_pago;
	
	protected $dataCronogramapago_cab;
	protected $oDatCronogramapago_cab;	

	public function __construct()
	{
		$this->oDatCronogramapago_cab = new DatCronogramapago_cab;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatCronogramapago_cab->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatCronogramapago_cab->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_cab', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatCronogramapago_cab->iniciarTransaccion('neg_i_Cronogramapago_cab');
			$this->id = $this->oDatCronogramapago_cab->insertar($this->persona_id,$this->rebi_id,$this->garantia,$this->meses_contrato,$this->monto_mensual,$this->dia_pago,$this->archivo_contrato,$this->observacion,$this->estado,$this->estado_pago);
			$this->oDatCronogramapago_cab->terminarTransaccion('neg_i_Cronogramapago_cab');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatCronogramapago_cab->cancelarTransaccion('neg_i_Cronogramapago_cab');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_cab', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatCronogramapago_cab->actualizar($this->id,$this->persona_id,$this->rebi_id,$this->garantia,$this->meses_contrato,$this->monto_mensual,$this->dia_pago,$this->archivo_contrato,$this->observacion,$this->estado,$this->estado_pago);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Cronogramapago_cab', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCronogramapago_cab->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataCronogramapago_cab = $this->oDatCronogramapago_cab->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataCronogramapago_cab)) {
				throw new Exception(JrTexto::_("Cronogramapago_cab").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataCronogramapago_cab["id"];
			$this->persona_id = $this->dataCronogramapago_cab["persona_id"];
			$this->rebi_id = $this->dataCronogramapago_cab["rebi_id"];
			$this->garantia = $this->dataCronogramapago_cab["garantia"];
			$this->meses_contrato = $this->dataCronogramapago_cab["meses_contrato"];
			$this->monto_mensual = $this->dataCronogramapago_cab["monto_mensual"];
			$this->dia_pago = $this->dataCronogramapago_cab["dia_pago"];
			$this->archivo_contrato = $this->dataCronogramapago_cab["archivo_contrato"];
			$this->observacion = $this->dataCronogramapago_cab["observacion"];
			$this->estado = $this->dataCronogramapago_cab["estado"];
			$this->estado_pago = $this->dataCronogramapago_cab["estado_pago"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_cab', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataCronogramapago_cab = $this->oDatCronogramapago_cab->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataCronogramapago_cab)) {
				throw new Exception(JrTexto::_("Cronogramapago_cab").' '.JrTexto::_("not registered"));
			}

			return $this->oDatCronogramapago_cab->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
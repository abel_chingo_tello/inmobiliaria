<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		13-02-2021
 * @copyright	Copyright (C) 13-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatCronogramapago_det', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegCronogramapago_det 
{
	
	protected $id;
	protected $cab_id;
	protected $fecha_vencimiento;
	protected $concepto;
	protected $cargo;
	protected $pago;
	protected $saldo;
	protected $subtotal;
	protected $observacion;
	protected $fecha_pago;
	protected $estado;
	
	protected $dataCronogramapago_det;
	protected $oDatCronogramapago_det;	

	public function __construct()
	{
		$this->oDatCronogramapago_det = new DatCronogramapago_det;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatCronogramapago_det->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatCronogramapago_det->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_det', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatCronogramapago_det->iniciarTransaccion('neg_i_Cronogramapago_det');
			$this->id = $this->oDatCronogramapago_det->insertar($this->cab_id,$this->fecha_vencimiento,$this->concepto,$this->cargo,$this->pago,$this->saldo,$this->subtotal,$this->observacion,$this->fecha_pago,$this->estado);
			$this->oDatCronogramapago_det->terminarTransaccion('neg_i_Cronogramapago_det');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatCronogramapago_det->cancelarTransaccion('neg_i_Cronogramapago_det');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_det', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatCronogramapago_det->actualizar($this->id,$this->cab_id,$this->fecha_vencimiento,$this->concepto,$this->cargo,$this->pago,$this->saldo,$this->subtotal,$this->observacion,$this->fecha_pago,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Cronogramapago_det', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatCronogramapago_det->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataCronogramapago_det = $this->oDatCronogramapago_det->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataCronogramapago_det)) {
				throw new Exception(JrTexto::_("Cronogramapago_det").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataCronogramapago_det["id"];
			$this->cab_id = $this->dataCronogramapago_det["cab_id"];
			$this->fecha_vencimiento = $this->dataCronogramapago_det["fecha_vencimiento"];
			$this->concepto = $this->dataCronogramapago_det["concepto"];
			$this->cargo = $this->dataCronogramapago_det["cargo"];
			$this->pago = $this->dataCronogramapago_det["pago"];
			$this->saldo = $this->dataCronogramapago_det["saldo"];
			$this->subtotal = $this->dataCronogramapago_det["subtotal"];
			$this->observacion = $this->dataCronogramapago_det["observacion"];
			$this->fecha_pago = $this->dataCronogramapago_det["fecha_pago"];
			$this->estado = $this->dataCronogramapago_det["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('cronogramapago_det', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataCronogramapago_det = $this->oDatCronogramapago_det->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataCronogramapago_det)) {
				throw new Exception(JrTexto::_("Cronogramapago_det").' '.JrTexto::_("not registered"));
			}

			return $this->oDatCronogramapago_det->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
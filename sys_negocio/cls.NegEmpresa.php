<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-10-2020
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa', RUTA_BASE);
JrCargador::clase('sys_datos::DatEmpresa_config', RUTA_BASE);
JrCargador::clase('sys_datos::DatEmpresa_smtp', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa 
{
	
	protected $idempresa;
	protected $nombre;
	protected $direccion;
	protected $telefono;
	protected $correo;
	protected $logo;
	protected $ruc;
	protected $idioma;
	protected $subdominio;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataEmpresa;
	protected $oDatEmpresa;	

	public function __construct()
	{
		$this->oDatEmpresa = new DatEmpresa;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEmpresa->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function configuracion($filtros=array()){		
		$empresa= $this->oDatEmpresa->buscar($filtros);	
		if(!empty($empresa[0])){
			$sesion = JrSession::getInstancia();
			foreach ($empresa[0] as $key => $emp) {				
				$sesion->set($key, $emp, '_infoempresa_');
			}
			$oDatEmpresaconfig = new DatEmpresa_config;
			$configuracion=$oDatEmpresaconfig->buscar(array('idempresa'=>$empresa[0]['idempresa'],'estado'=>1));
			if(!empty($configuracion))
				foreach ($configuracion as $k => $v) {				
					$sesion->set($v["parametro"], $v["valor"], '_infoempresa_');
				}
			$oDatEmpresasmtp = new DatEmpresa_smtp;
			$smtp=$oDatEmpresasmtp->buscar(array('idempresa'=>$empresa[0]['idempresa'],'estado'=>1,'sqlget'=>true));
			if(!empty($smtp)){
				$sesion->set("smtp", $smtp, '_infoempresa_');
			}
		}		
		return $empresa;
	}

	public function getAlldatos($idempresa){
		$empresa= $this->oDatEmpresa->buscar(array('idempresa'=>$idempresa,'sqlget'=>true));
		if(!empty($empresa['idempresa'])){			
			$oDatEmpresaconfig = new DatEmpresa_config;
			$configuracion=$oDatEmpresaconfig->buscar(array('idempresa'=>$empresa['idempresa'],'estado'=>1));
			if(!empty($configuracion))
				foreach ($configuracion as $k => $v) {				
					$empresa[$v["parametro"]]=$v["valor"];
				}
			$oDatEmpresasmtp = new DatEmpresa_smtp;
			$smtp=$oDatEmpresasmtp->buscar(array('idempresa'=>$empresa['idempresa'],'estado'=>1,'sqlget'=>true));			
			if(!empty($smtp)){
				$empresa["smtp"]=$smtp;				
			}
		}		
		return $empresa;

	}

	public function setconfiguracion($key,$valor){
		if(!empty($key)&& !empty($valor)){
			$sesion = JrSession::getInstancia();
			$sesion->set($key, $valor, '_infoempresa_');
		}
	}

	public function getconfiguracion(){
		$sesion = JrSession::getInstancia();
		return $sesion->getAll('_infoempresa_');		
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa->iniciarTransaccion('neg_i_Empresa');
			$this->idempresa = $this->oDatEmpresa->insertar($this->nombre,$this->direccion,$this->telefono,$this->correo,$this->logo,$this->ruc,$this->idioma,$this->subdominio,$this->estado,$this->usuario_registro);
			$this->oDatEmpresa->terminarTransaccion('neg_i_Empresa');	
			return $this->idempresa;
		} catch(Exception $e) {	
		    $this->oDatEmpresa->cancelarTransaccion('neg_i_Empresa');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa->actualizar($this->idempresa,$this->nombre,$this->direccion,$this->telefono,$this->correo,$this->logo,$this->ruc,$this->idioma,$this->subdominio,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatEmpresa->cambiarvalorcampo($this->idempresa,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa->eliminar($this->idempresa,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdempresa($pk){
		try {
			$this->dataEmpresa = $this->oDatEmpresa->buscar(array('sqlget'=>true,'idempresa'=>$pk));
			if(empty($this->dataEmpresa)) {
				throw new Exception(JrTexto::_("Empresa").' '.JrTexto::_("not registered"));
			}
			$this->idempresa=$this->dataEmpresa["idempresa"];
			$this->nombre = $this->dataEmpresa["nombre"];
			$this->direccion = $this->dataEmpresa["direccion"];
			$this->telefono = $this->dataEmpresa["telefono"];
			$this->correo = $this->dataEmpresa["correo"];
			$this->logo = $this->dataEmpresa["logo"];
			$this->ruc = $this->dataEmpresa["ruc"];
			$this->idioma = $this->dataEmpresa["idioma"];
			$this->subdominio = $this->dataEmpresa["subdominio"];
			$this->estado = $this->dataEmpresa["estado"];
			$this->usuario_registro = $this->dataEmpresa["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa = $this->oDatEmpresa->buscar(array('sqlget'=>true,'idempresa'=>$pk));
			if(empty($this->dataEmpresa)) {
				throw new Exception(JrTexto::_("Empresa").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
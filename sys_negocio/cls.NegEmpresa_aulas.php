<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-10-2020
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa_aulas', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa_aulas 
{
	
	protected $idaula;
	protected $idlocal;
	protected $nombre;
	protected $idempresa;
	protected $estado;
	protected $tipo;
	protected $capacidad;
	protected $usuario_registro;
	
	protected $dataEmpresa_aulas;
	protected $oDatEmpresa_aulas;	

	public function __construct()
	{
		$this->oDatEmpresa_aulas = new DatEmpresa_aulas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa_aulas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEmpresa_aulas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_aulas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa_aulas->iniciarTransaccion('neg_i_Empresa_aulas');
			$this->idaula = $this->oDatEmpresa_aulas->insertar($this->idlocal,$this->nombre,$this->idempresa,$this->estado,$this->tipo,$this->capacidad,$this->usuario_registro);
			$this->oDatEmpresa_aulas->terminarTransaccion('neg_i_Empresa_aulas');	
			return $this->idaula;
		} catch(Exception $e) {	
		    $this->oDatEmpresa_aulas->cancelarTransaccion('neg_i_Empresa_aulas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_aulas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa_aulas->actualizar($this->idaula,$this->idlocal,$this->nombre,$this->idempresa,$this->estado,$this->tipo,$this->capacidad,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatEmpresa_aulas->cambiarvalorcampo($this->idaula,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa_aulas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa_aulas->eliminar($this->idaula,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdaula($pk){
		try {
			$this->dataEmpresa_aulas = $this->oDatEmpresa_aulas->buscar(array('sqlget'=>true,'idaula'=>$pk));
			if(empty($this->dataEmpresa_aulas)) {
				throw new Exception(JrTexto::_("Empresa_aulas").' '.JrTexto::_("not registered"));
			}
			$this->idaula=$this->dataEmpresa_aulas["idaula"];
			$this->idlocal = $this->dataEmpresa_aulas["idlocal"];
			$this->nombre = $this->dataEmpresa_aulas["nombre"];
			$this->idempresa = $this->dataEmpresa_aulas["idempresa"];
			$this->estado = $this->dataEmpresa_aulas["estado"];
			$this->tipo = $this->dataEmpresa_aulas["tipo"];
			$this->capacidad = $this->dataEmpresa_aulas["capacidad"];
			$this->usuario_registro = $this->dataEmpresa_aulas["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa_aulas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa_aulas = $this->oDatEmpresa_aulas->buscar(array('sqlget'=>true,'idaula'=>$pk));
			if(empty($this->dataEmpresa_aulas)) {
				throw new Exception(JrTexto::_("Empresa_aulas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa_aulas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
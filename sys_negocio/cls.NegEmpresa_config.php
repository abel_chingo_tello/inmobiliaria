<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-10-2020
 * @copyright	Copyright (C) 04-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa_config', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa_config 
{
	
	protected $idempresaconfig;
	protected $idempresa;
	protected $parametro;
	protected $valor;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataEmpresa_config;
	protected $oDatEmpresa_config;	

	public function __construct()
	{
		$this->oDatEmpresa_config = new DatEmpresa_config;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa_config->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEmpresa_config->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_config', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa_config->iniciarTransaccion('neg_i_Empresa_config');
			$this->idempresaconfig = $this->oDatEmpresa_config->insertar($this->idempresa,$this->parametro,$this->valor,$this->estado,$this->usuario_registro);
			$this->oDatEmpresa_config->terminarTransaccion('neg_i_Empresa_config');	
			return $this->idempresaconfig;
		} catch(Exception $e) {	
		    $this->oDatEmpresa_config->cancelarTransaccion('neg_i_Empresa_config');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_config', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa_config->actualizar($this->idempresaconfig,$this->idempresa,$this->parametro,$this->valor,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatEmpresa_config->cambiarvalorcampo($this->idempresaconfig,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa_config', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa_config->eliminar($this->idempresaconfig,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdempresaconfig($pk){
		try {
			$this->dataEmpresa_config = $this->oDatEmpresa_config->buscar(array('sqlget'=>true,'idempresaconfig'=>$pk));
			if(empty($this->dataEmpresa_config)) {
				throw new Exception(JrTexto::_("Empresa_config").' '.JrTexto::_("not registered"));
			}
			$this->idempresaconfig=$this->dataEmpresa_config["idempresaconfig"];
			$this->idempresa = $this->dataEmpresa_config["idempresa"];
			$this->parametro = $this->dataEmpresa_config["parametro"];
			$this->valor = $this->dataEmpresa_config["valor"];
			$this->estado = $this->dataEmpresa_config["estado"];
			$this->usuario_registro = $this->dataEmpresa_config["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa_config', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa_config = $this->oDatEmpresa_config->buscar(array('sqlget'=>true,'idempresaconfig'=>$pk));
			if(empty($this->dataEmpresa_config)) {
				throw new Exception(JrTexto::_("Empresa_config").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa_config->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-10-2020
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa_local', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa_local 
{
	
	protected $idlocal;
	protected $nombre;
	protected $idempresa;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataEmpresa_local;
	protected $oDatEmpresa_local;	

	public function __construct()
	{
		$this->oDatEmpresa_local = new DatEmpresa_local;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa_local->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEmpresa_local->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_local', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa_local->iniciarTransaccion('neg_i_Empresa_local');
			$this->idlocal = $this->oDatEmpresa_local->insertar($this->nombre,$this->idempresa,$this->estado,$this->usuario_registro);
			$this->oDatEmpresa_local->terminarTransaccion('neg_i_Empresa_local');	
			return $this->idlocal;
		} catch(Exception $e) {	
		    $this->oDatEmpresa_local->cancelarTransaccion('neg_i_Empresa_local');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_local', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa_local->actualizar($this->idlocal,$this->nombre,$this->idempresa,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatEmpresa_local->cambiarvalorcampo($this->idlocal,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa_local', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa_local->eliminar($this->idlocal,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdlocal($pk){
		try {
			$this->dataEmpresa_local = $this->oDatEmpresa_local->buscar(array('sqlget'=>true,'idlocal'=>$pk));
			if(empty($this->dataEmpresa_local)) {
				throw new Exception(JrTexto::_("Empresa_local").' '.JrTexto::_("not registered"));
			}
			$this->idlocal=$this->dataEmpresa_local["idlocal"];
			$this->nombre = $this->dataEmpresa_local["nombre"];
			$this->idempresa = $this->dataEmpresa_local["idempresa"];
			$this->estado = $this->dataEmpresa_local["estado"];
			$this->usuario_registro = $this->dataEmpresa_local["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa_local', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa_local = $this->oDatEmpresa_local->buscar(array('sqlget'=>true,'idlocal'=>$pk));
			if(empty($this->dataEmpresa_local)) {
				throw new Exception(JrTexto::_("Empresa_local").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa_local->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
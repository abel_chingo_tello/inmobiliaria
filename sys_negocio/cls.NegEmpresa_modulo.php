<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-10-2020
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa_modulo', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa_modulo 
{
	
	protected $idempresamodulo;
	protected $idmodulo;
	protected $idempresa;
	protected $estado;
	protected $idrol;
	protected $orden;
	protected $idmoduloapdre;
	protected $nomtemporal;
	protected $usuario_registro;
	
	protected $dataEmpresa_modulo;
	protected $oDatEmpresa_modulo;	

	public function __construct()
	{
		$this->oDatEmpresa_modulo = new DatEmpresa_modulo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa_modulo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			$datosall=$this->oDatEmpresa_modulo->buscar($filtros);
			$menus=array();
			if(!empty($datosall)){				
				$datos2=$datosall; // los principales	
				foreach($datosall as $kd => $vd){
					if(empty($vd["idmodulopadre"])){
						unset($datos2[$kd]);
						$tienehijos=$this->menuhijos($vd["idempresamodulo"],$datos2);
						if(!empty($tienehijos["hijos"])){
							$datos2=$tienehijos['datos'];
							$vd["hijos"]=$tienehijos['hijos'];
						}
						$orden=empty($menus[$vd["orden"]])?$vd["orden"]:(intval($vd["orden"])+1);
						$menus[$orden]=$vd;
					}
				}
			}
			return $menus;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function menuhijos($idmenupadre,$datos2){
		$datostmp=$datos2;
		$menus=array();
		if(!empty($datos2)){			
			foreach($datos2 as $k => $v){
				if($idmenupadre==$v["idmodulopadre"]){
					unset($datostmp[$k]);
					$tienehijos=$this->menuhijos($v["idempresamodulo"],$datostmp);
					if(!empty($tienehijos["hijos"])){
						$datostmp=$tienehijos['datos'];
						$v["hijos"]=$tienehijos['hijos'];						
					}
					$orden=empty($menus[$v["orden"]])?$v["orden"]:(intval($v["orden"])+1);
					$menus[$orden]=$v;
				}				
			}
		}
		return array("hijos"=>$menus,'datos'=>$datostmp);
	} 

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_modulo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa_modulo->iniciarTransaccion('neg_i_Empresa_modulo');
			$this->idempresamodulo = $this->oDatEmpresa_modulo->insertar($this->idmodulo,$this->idempresa,$this->estado,$this->idrol,$this->orden,$this->idmoduloapdre,$this->nomtemporal,$this->usuario_registro);
			$this->oDatEmpresa_modulo->terminarTransaccion('neg_i_Empresa_modulo');	
			return $this->idempresamodulo;
		} catch(Exception $e) {	
		    $this->oDatEmpresa_modulo->cancelarTransaccion('neg_i_Empresa_modulo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_modulo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa_modulo->actualizar($this->idempresamodulo,$this->idmodulo,$this->idempresa,$this->estado,$this->idrol,$this->orden,$this->idmoduloapdre,$this->nomtemporal,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatEmpresa_modulo->cambiarvalorcampo($this->idempresamodulo,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa_modulo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa_modulo->eliminar($this->idempresamodulo,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdempresamodulo($pk){
		try {
			$this->dataEmpresa_modulo = $this->oDatEmpresa_modulo->buscar(array('sqlget'=>true,'idempresamodulo'=>$pk));
			if(empty($this->dataEmpresa_modulo)) {
				throw new Exception(JrTexto::_("Empresa_modulo").' '.JrTexto::_("not registered"));
			}
			$this->idempresamodulo=$this->dataEmpresa_modulo["idempresamodulo"];
			$this->idmodulo = $this->dataEmpresa_modulo["idmodulo"];
			$this->idempresa = $this->dataEmpresa_modulo["idempresa"];
			$this->estado = $this->dataEmpresa_modulo["estado"];
			$this->idrol = $this->dataEmpresa_modulo["idrol"];
			$this->orden = $this->dataEmpresa_modulo["orden"];
			$this->idmoduloapdre = $this->dataEmpresa_modulo["idmodulopadre"];
			$this->nomtemporal = $this->dataEmpresa_modulo["nomtemporal"];
			$this->usuario_registro = $this->dataEmpresa_modulo["usuario_registro"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa_modulo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa_modulo = $this->oDatEmpresa_modulo->buscar(array('sqlget'=>true,'idempresamodulo'=>$pk));
			if(empty($this->dataEmpresa_modulo)) {
				throw new Exception(JrTexto::_("Empresa_modulo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa_modulo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function guardarorden($datos){
		try {
			return $this->oDatEmpresa_modulo->guardarorden($datos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-10-2020
 * @copyright	Copyright (C) 04-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatEmpresa_smtp', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegEmpresa_smtp 
{
	
	protected $idempresasmtp;
	protected $idempresa;
	protected $host;
	protected $email;
	protected $clave;
	protected $puerto;
	protected $cifrado;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataEmpresa_smtp;
	protected $oDatEmpresa_smtp;	

	public function __construct()
	{
		$this->oDatEmpresa_smtp = new DatEmpresa_smtp;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatEmpresa_smtp->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatEmpresa_smtp->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_smtp', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatEmpresa_smtp->iniciarTransaccion('neg_i_Empresa_smtp');
			$this->idempresasmtp = $this->oDatEmpresa_smtp->insertar($this->idempresa,$this->host,$this->email,$this->clave,$this->puerto,$this->cifrado,$this->estado,$this->usuario_registro);
			$this->oDatEmpresa_smtp->terminarTransaccion('neg_i_Empresa_smtp');	
			return $this->idempresasmtp;
		} catch(Exception $e) {	
		    $this->oDatEmpresa_smtp->cancelarTransaccion('neg_i_Empresa_smtp');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('empresa_smtp', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatEmpresa_smtp->actualizar($this->idempresasmtp,$this->idempresa,$this->host,$this->email,$this->clave,$this->puerto,$this->cifrado,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Empresa_smtp', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatEmpresa_smtp->eliminar($this->idempresasmtp,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdempresasmtp($pk){
		try {
			$this->dataEmpresa_smtp = $this->oDatEmpresa_smtp->buscar(array('sqlget'=>true,'idempresasmtp'=>$pk));
			if(empty($this->dataEmpresa_smtp)) {
				throw new Exception(JrTexto::_("Empresa_smtp").' '.JrTexto::_("not registered"));
			}
			$this->idempresasmtp=$this->dataEmpresa_smtp["idempresasmtp"];
			$this->idempresa = $this->dataEmpresa_smtp["idempresa"];
			$this->host = $this->dataEmpresa_smtp["host"];
			$this->email = $this->dataEmpresa_smtp["email"];
			$this->clave = $this->dataEmpresa_smtp["clave"];
			$this->puerto = $this->dataEmpresa_smtp["puerto"];
			$this->cifrado = $this->dataEmpresa_smtp["cifrado"];
			$this->estado = $this->dataEmpresa_smtp["estado"];
			$this->usuario_registro = $this->dataEmpresa_smtp["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('empresa_smtp', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataEmpresa_smtp = $this->oDatEmpresa_smtp->buscar(array('sqlget'=>true,'idempresasmtp'=>$pk));
			if(empty($this->dataEmpresa_smtp)) {
				throw new Exception(JrTexto::_("Empresa_smtp").' '.JrTexto::_("not registered"));
			}

			return $this->oDatEmpresa_smtp->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
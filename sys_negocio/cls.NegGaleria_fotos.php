<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		28-01-2021
 * @copyright	Copyright (C) 28-01-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatGaleria_fotos', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegGaleria_fotos 
{
	
	protected $id_galeria;
	protected $rebi_id;
	protected $titulo;
	protected $descripcion;
	protected $tipo;
	protected $imagen;
	
	protected $dataGaleria_fotos;
	protected $oDatGaleria_fotos;	

	public function __construct()
	{
		$this->oDatGaleria_fotos = new DatGaleria_fotos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatGaleria_fotos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array(),$innerjoin= array())
	{
		try {
					return $this->oDatGaleria_fotos->buscar($filtros,$innerjoin);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar($filtros = array())
	{
		try {
					return $this->oDatGaleria_fotos->listar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('galeria_fotos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatGaleria_fotos->iniciarTransaccion('neg_i_Galeria_fotos');
			$this->id_galeria = $this->oDatGaleria_fotos->insertar($this->rebi_id,$this->titulo,$this->descripcion,$this->tipo,$this->imagen);
			$this->oDatGaleria_fotos->terminarTransaccion('neg_i_Galeria_fotos');	
			return $this->id_galeria;
		} catch(Exception $e) {	
		    $this->oDatGaleria_fotos->cancelarTransaccion('neg_i_Galeria_fotos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('galeria_fotos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatGaleria_fotos->actualizar($this->id_galeria,$this->rebi_id,$this->titulo,$this->descripcion,$this->tipo,$this->imagen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Galeria_fotos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatGaleria_fotos->eliminar($this->id_galeria,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_galeria($pk){
		try {
			$this->dataGaleria_fotos = $this->oDatGaleria_fotos->buscar(array('sqlget'=>true,'id_galeria'=>$pk));
			if(empty($this->dataGaleria_fotos)) {
				throw new Exception(JrTexto::_("Galeria_fotos").' '.JrTexto::_("not registered"));
			}
			$this->id_galeria=$this->dataGaleria_fotos["id_galeria"];
			$this->rebi_id = $this->dataGaleria_fotos["rebi_id"];
			$this->titulo = $this->dataGaleria_fotos["titulo"];
			$this->descripcion = $this->dataGaleria_fotos["descripcion"];
			$this->tipo = $this->dataGaleria_fotos["tipo"];
			$this->imagen = $this->dataGaleria_fotos["imagen"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('galeria_fotos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataGaleria_fotos = $this->oDatGaleria_fotos->buscar(array('sqlget'=>true,'id_galeria'=>$pk));
			if(empty($this->dataGaleria_fotos)) {
				throw new Exception(JrTexto::_("Galeria_fotos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatGaleria_fotos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::sistema::DatGenerador', RUTA_BASE);
class NegGenerador
{
	protected $oDatGenerador;
	
	public function __construct()
	{
		$this->oDatGenerador= new DatGenerador;
	}	
	public function getTablas()
	{
		try {
			return $this->oDatGenerador->getTablas();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getCampos($tabla)
	{
		try {
			return $this->oDatGenerador->getCampos($tabla);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}
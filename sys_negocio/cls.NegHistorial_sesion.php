<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-10-2020
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHistorial_sesion', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegHistorial_sesion 
{
	
	protected $idhistorial;
	protected $idpersona;
	protected $idrol;
	protected $idempresa;
	protected $ip;
	protected $navegador;
	protected $idgrupoaula;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $lugar;
	
	protected $dataHistorial_sesion;
	protected $oDatHistorial_sesion;	

	public function __construct()
	{
		$this->oDatHistorial_sesion = new DatHistorial_sesion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHistorial_sesion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatHistorial_sesion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sesion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatHistorial_sesion->iniciarTransaccion('neg_i_Historial_sesion');
			$this->idhistorial = $this->oDatHistorial_sesion->insertar($this->idpersona,$this->idrol,$this->idempresa,$this->ip,$this->navegador,$this->idgrupoaula,$this->fecha_inicio,$this->fecha_final,$this->lugar);
			$this->oDatHistorial_sesion->terminarTransaccion('neg_i_Historial_sesion');	
			return $this->idhistorial;
		} catch(Exception $e) {	
		    $this->oDatHistorial_sesion->cancelarTransaccion('neg_i_Historial_sesion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sesion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatHistorial_sesion->actualizar($this->idhistorial,$this->idpersona,$this->idrol,$this->idempresa,$this->ip,$this->navegador,$this->idgrupoaula,$this->fecha_inicio,$this->fecha_final,$this->lugar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Historial_sesion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHistorial_sesion->eliminar($this->idhistorial,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdhistorial($pk){
		try {
			$this->dataHistorial_sesion = $this->oDatHistorial_sesion->buscar(array('sqlget'=>true,'idhistorial'=>$pk));
			if(empty($this->dataHistorial_sesion)) {
				throw new Exception(JrTexto::_("Historial_sesion").' '.JrTexto::_("not registered"));
			}
			$this->idhistorial=$this->dataHistorial_sesion["idhistorial"];
			$this->idpersona = $this->dataHistorial_sesion["idpersona"];
			$this->idrol = $this->dataHistorial_sesion["idrol"];
			$this->idempresa = $this->dataHistorial_sesion["idempresa"];
			$this->ip = $this->dataHistorial_sesion["ip"];
			$this->navegador = $this->dataHistorial_sesion["navegador"];
			$this->idgrupoaula = $this->dataHistorial_sesion["idgrupoaula"];
			$this->fecha_inicio = $this->dataHistorial_sesion["fecha_inicio"];
			$this->fecha_final = $this->dataHistorial_sesion["fecha_final"];
			$this->lugar = $this->dataHistorial_sesion["lugar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('historial_sesion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataHistorial_sesion = $this->oDatHistorial_sesion->buscar(array('sqlget'=>true,'idhistorial'=>$pk));
			if(empty($this->dataHistorial_sesion)) {
				throw new Exception(JrTexto::_("Historial_sesion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_sesion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
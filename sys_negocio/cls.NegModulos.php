<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-10-2020
 * @copyright	Copyright (C) 08-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatModulos', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegModulos 
{
	
	protected $idmodulo;
	protected $nombre;
	protected $descripcion;
	protected $estado;
	protected $url;
	protected $icono;
	protected $usuario_registro;
	
	protected $dataModulos;
	protected $oDatModulos;	

	public function __construct()
	{
		$this->oDatModulos = new DatModulos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatModulos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatModulos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatModulos->iniciarTransaccion('neg_i_Modulos');
			$this->idmodulo = $this->oDatModulos->insertar($this->nombre,$this->descripcion,$this->estado,$this->url,$this->icono,$this->usuario_registro);
			$this->oDatModulos->terminarTransaccion('neg_i_Modulos');	
			return $this->idmodulo;
		} catch(Exception $e) {	
		    $this->oDatModulos->cancelarTransaccion('neg_i_Modulos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatModulos->actualizar($this->idmodulo,$this->nombre,$this->descripcion,$this->estado,$this->url,$this->icono,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatModulos->cambiarvalorcampo($this->idmodulo,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Modulos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatModulos->eliminar($this->idmodulo,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmodulo($pk){
		try {
			$this->dataModulos = $this->oDatModulos->buscar(array('sqlget'=>true,'idmodulo'=>$pk));
			if(empty($this->dataModulos)) {
				throw new Exception(JrTexto::_("Modulos").' '.JrTexto::_("not registered"));
			}
			$this->idmodulo=$this->dataModulos["idmodulo"];
			$this->nombre = $this->dataModulos["nombre"];
			$this->descripcion = $this->dataModulos["descripcion"];
			$this->estado = $this->dataModulos["estado"];
			$this->url = $this->dataModulos["url"];
			$this->icono = $this->dataModulos["icono"];
			$this->usuario_registro = $this->dataModulos["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('modulos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataModulos = $this->oDatModulos->buscar(array('sqlget'=>true,'idmodulo'=>$pk));
			if(empty($this->dataModulos)) {
				throw new Exception(JrTexto::_("Modulos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatModulos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
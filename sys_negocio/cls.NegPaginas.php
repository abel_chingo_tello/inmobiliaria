<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		08-08-2020
 * @copyright	Copyright (C) 08-08-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPaginas', RUTA_BASE);
/*JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);*/
class NegPaginas 
{
	protected $idpagina;
	protected $idempresa;
	protected $titulo_enmenu;
	protected $titulo;
	protected $descripcion;
	protected $imagen;
	protected $estado;
	protected $enmenu;
	protected $usuario_registro;
	protected $fecha_insert;
	protected $fecha_update;
	
	protected $dataPaginas;
	protected $oDatPaginas;	

	public function __construct()
	{
		$this->oDatPaginas = new DatPaginas;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPaginas->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPaginas->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('paginas', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPaginas->iniciarTransaccion('neg_i_Paginas');
			$this->idpagina = $this->oDatPaginas->insertar($this->idempresa,$this->titulo_enmenu,$this->titulo,$this->descripcion,$this->imagen,$this->estado,$this->enmenu,$this->usuario_registro);
			$this->oDatPaginas->terminarTransaccion('neg_i_Paginas');	
			return $this->idpagina;
		} catch(Exception $e) {	
		    $this->oDatPaginas->cancelarTransaccion('neg_i_Paginas');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('paginas', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPaginas->actualizar($this->idpagina,$this->idempresa,$this->titulo_enmenu,$this->titulo,$this->descripcion,$this->imagen,$this->estado,$this->enmenu);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Paginas', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPaginas->eliminar($this->idpagina,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpagina($pk){
		try {
			$this->dataPaginas = $this->oDatPaginas->buscar(array('sqlget'=>true,'idpagina'=>$pk));
			if(empty($this->dataPaginas)) {
				throw new Exception(JrTexto::_("Paginas").' '.JrTexto::_("not registered"));
			}
			$this->idpagina = $this->dataPaginas["idpagina"];
			$this->idempresa = $this->dataPaginas["idempresa"];
			$this->titulo_enmenu = $this->dataPaginas["titulo_enmenu"];
			$this->titulo = $this->dataPaginas["titulo"];
			$this->descripcion = $this->dataPaginas["descripcion"];
			$this->imagen = $this->dataPaginas["imagen"];
			$this->estado = $this->dataPaginas["estado"];
			$this->enmenu = $this->dataPaginas["enmenu"];
			$this->usuario_registro = $this->dataPaginas["usuario_registro"];
			$this->fecha_insert = $this->dataPaginas["fecha_insert"];
			$this->fecha_update = $this->dataPaginas["fecha_update"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('paginas', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPaginas = $this->oDatPaginas->buscar(array('sqlget'=>true,'idpagina'=>$pk));
			if(empty($this->dataPaginas)) {
				throw new Exception(JrTexto::_("Paginas").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPaginas->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-02-2021
 * @copyright	Copyright (C) 17-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPagos', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPagos 
{
	
	protected $id;
	protected $persona_id;
	protected $preference_id;
	protected $cronogramapagocab_id;
	protected $cronogramapagodet_ids;
	protected $estado;
	
	protected $dataPagos;
	protected $oDatPagos;	

	public function __construct()
	{
		$this->oDatPagos = new DatPagos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPagos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatPagos->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('pagos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatPagos->iniciarTransaccion('neg_i_Pagos');
			$this->id = $this->oDatPagos->insertar($this->persona_id,$this->preference_id,$this->cronogramapagocab_id,$this->cronogramapagodet_ids,$this->estado);
			$this->oDatPagos->terminarTransaccion('neg_i_Pagos');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatPagos->cancelarTransaccion('neg_i_Pagos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('pagos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatPagos->actualizar($this->id,$this->persona_id,$this->preference_id,$this->cronogramapagocab_id,$this->cronogramapagodet_ids,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Pagos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPagos->eliminar($this->id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataPagos = $this->oDatPagos->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataPagos)) {
				throw new Exception(JrTexto::_("Pagos").' '.JrTexto::_("not registered"));
			}
			$this->id=$this->dataPagos["id"];
			$this->persona_id = $this->dataPagos["persona_id"];
			$this->preference_id = $this->dataPagos["preference_id"];
			$this->cronogramapagocab_id = $this->dataPagos["cronogramapagocab_id"];
			$this->cronogramapagodet_ids = $this->dataPagos["cronogramapagodet_ids"];
			$this->estado = $this->dataPagos["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('pagos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPagos = $this->oDatPagos->buscar(array('sqlget'=>true,'id'=>$pk));
			if(empty($this->dataPagos)) {
				throw new Exception(JrTexto::_("Pagos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPagos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
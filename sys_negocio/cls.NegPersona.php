<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-10-2020
 * @copyright	Copyright (C) 03-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPersona 
{
	
	protected $idpersona;
	protected $tipo_documento;
	protected $num_documento;
	protected $apellido_1;
	protected $apellido_2;
	protected $nombres;
	protected $usuario;
	protected $email;
	protected $clave;
	protected $verclave;
	protected $tocken;
	protected $foto;
	protected $genero;
	protected $fecha_nacimiento;
	protected $estado_civil;
	protected $tipo_usuario;
	protected $telefono;
	protected $ubigeo;
	protected $direccion;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataPersona;
	protected $oDatPersona;	

	public function __construct()
	{
		$this->oDatPersona = new DatPersona;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatPersona->iniciarTransaccion('neg_i_Persona');
			$this->idpersona = $this->oDatPersona->insertar($this->tipo_documento,$this->num_documento,$this->apellido_1,$this->apellido_2,$this->nombres,$this->usuario,$this->email,$this->clave,$this->verclave,$this->tocken,$this->foto,$this->genero,$this->fecha_nacimiento,$this->estado_civil,$this->tipo_usuario,$this->telefono,$this->ubigeo,$this->direccion,$this->estado,$this->usuario_registro);
			$this->oDatPersona->terminarTransaccion('neg_i_Persona');	
			return $this->idpersona;
		} catch(Exception $e) {	
		    $this->oDatPersona->cancelarTransaccion('neg_i_Persona');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatPersona->actualizar($this->idpersona,$this->tipo_documento,$this->num_documento,$this->apellido_1,$this->apellido_2,$this->nombres,$this->usuario,$this->email,$this->clave,$this->verclave,$this->tocken,$this->foto,$this->genero,$this->fecha_nacimiento,$this->estado_civil,$this->tipo_usuario,$this->telefono,$this->ubigeo,$this->direccion,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona->cambiarvalorcampo($this->idpersona,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona->eliminar($this->idpersona,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpersona($pk){
		try {
			$this->dataPersona = $this->oDatPersona->buscar(array('sqlget'=>true,'idpersona'=>$pk));
			if(empty($this->dataPersona)) {
				throw new Exception(JrTexto::_("Persona").' '.JrTexto::_("not registered"));
			}
			$this->idpersona=$this->dataPersona["idpersona"];
			$this->tipo_documento = $this->dataPersona["tipo_documento"];
			$this->num_documento = $this->dataPersona["num_documento"];
			$this->apellido_1 = $this->dataPersona["apellido_1"];
			$this->apellido_2 = $this->dataPersona["apellido_2"];
			$this->nombres = $this->dataPersona["nombres"];
			$this->usuario = $this->dataPersona["usuario"];
			$this->email = $this->dataPersona["email"];
			$this->clave = $this->dataPersona["clave"];
			$this->verclave = $this->dataPersona["verclave"];
			$this->tocken = $this->dataPersona["tocken"];
			$this->foto = $this->dataPersona["foto"];
			$this->genero = $this->dataPersona["genero"];
			$this->fecha_nacimiento = $this->dataPersona["fecha_nacimiento"];
			$this->estado_civil = $this->dataPersona["estado_civil"];
			$this->tipo_usuario = $this->dataPersona["tipo_usuario"];
			$this->telefono = $this->dataPersona["telefono"];
			$this->ubigeo = $this->dataPersona["ubigeo"];
			$this->direccion = $this->dataPersona["direccion"];
			$this->estado = $this->dataPersona["estado"];
			$this->usuario_registro = $this->dataPersona["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona = $this->oDatPersona->buscar(array('sqlget'=>true,'idpersona'=>$pk));
			if(empty($this->dataPersona)) {
				throw new Exception(JrTexto::_("Persona").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}
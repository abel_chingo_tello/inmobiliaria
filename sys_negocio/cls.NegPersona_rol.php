<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-10-2020
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegPersona_rol 
{
	protected $idpersonarol;
	protected $idempresa;
	protected $idpersona;
	protected $idrol;
	protected $usuario_registro;
	
	protected $dataPersona_rol;
	protected $oDatPersona_rol;	

	public function __construct()
	{
		$this->oDatPersona_rol = new DatPersona_rol;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_rol->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_rol->buscar($filtros);
					} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_rol', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatPersona_rol->iniciarTransaccion('neg_i_Persona_rol');
			$this->idpersonarol = $this->oDatPersona_rol->insertar($this->idempresa,$this->idpersona,$this->idrol,$this->usuario_registro);
			$this->oDatPersona_rol->terminarTransaccion('neg_i_Persona_rol');	
			return $this->idpersonarol;
		} catch(Exception $e) {	
		    $this->oDatPersona_rol->cancelarTransaccion('neg_i_Persona_rol');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_rol', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatPersona_rol->actualizar($this->idpersonarol,$this->idempresa,$this->idpersona,$this->idrol,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_rol', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_rol->eliminar($this->idpersonarol,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpersonarol($pk){
		try {
			$this->dataPersona_rol = $this->oDatPersona_rol->buscar(array('sqlget'=>true,'idpersonarol'=>$pk));
			if(empty($this->dataPersona_rol)) {
				throw new Exception(JrTexto::_("Persona_rol").' '.JrTexto::_("not registered"));
			}
			$this->idpersonarol=$this->dataPersona_rol["idpersonarol"];
			$this->idempresa = $this->dataPersona_rol["idempresa"];
			$this->idpersona = $this->dataPersona_rol["idpersona"];
			$this->idrol = $this->dataPersona_rol["idrol"];
			$this->usuario_registro = $this->dataPersona_rol["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_rol', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_rol = $this->oDatPersona_rol->buscar(array('sqlget'=>true,'idpersonarol'=>$pk));
			if(empty($this->dataPersona_rol)) {
				throw new Exception(JrTexto::_("Persona_rol").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_rol->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
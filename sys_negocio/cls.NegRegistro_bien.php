<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-12-2020
 * @copyright	Copyright (C) 24-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRegistro_bien', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRegistro_bien 
{
	protected $rebi_id;
	protected $rebi_modalidad;
	protected $rebi_tipo_bien;
	protected $casb_codigo;
	protected $rebi_fadquisicion;
	protected $rebi_vadquisicion;
	protected $rebi_ult_dep_acum;
	protected $rebi_ult_val_act;
	protected $tabl_estado;
	protected $rebi_detalles;
	protected $depe_id_ubica;
	protected $tabl_condicion;
	protected $rebi_reg_publicos;
	protected $rebi_area;
	protected $rebi_observaciones;
	protected $depe_id;
	protected $rebi_anno;
	protected $rebi_alquiler;
	protected $tabl_tipo_interior;
	protected $rebi_numero_interior;
	protected $rebi_foto_principal;
	
	protected $dataRegistro_bien;
	protected $oDatRegistro_bien;	

	public function __construct()
	{
		$this->oDatRegistro_bien = new DatRegistro_bien;
	}

	public function listarPrueba($filtros=array(),$inner_join=array())
	{
		return $this->oDatRegistro_bien->listarPrueba($filtros,$inner_join);
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRegistro_bien->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRegistro_bien->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('registro_bien', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatRegistro_bien->iniciarTransaccion('neg_i_Registro_bien');
			$this->rebi_id = $this->oDatRegistro_bien->insertar($this->rebi_modalidad,$this->rebi_tipo_bien,$this->casb_codigo,$this->rebi_fadquisicion,$this->rebi_vadquisicion,$this->rebi_ult_dep_acum,$this->rebi_ult_val_act,$this->tabl_estado,$this->rebi_detalles,$this->depe_id_ubica,$this->tabl_condicion,$this->rebi_reg_publicos,$this->rebi_area,$this->rebi_observaciones,$this->depe_id,$this->rebi_anno,$this->rebi_alquiler,$this->tabl_tipo_interior,$this->rebi_numero_interior,$this->rebi_foto_principal);
			$this->oDatRegistro_bien->terminarTransaccion('neg_i_Registro_bien');	
			return $this->rebi_id;
		} catch(Exception $e) {	
		    $this->oDatRegistro_bien->cancelarTransaccion('neg_i_Registro_bien');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('registro_bien', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatRegistro_bien->actualizar($this->rebi_id,$this->rebi_modalidad,$this->rebi_tipo_bien,$this->casb_codigo,$this->rebi_fadquisicion,$this->rebi_vadquisicion,$this->rebi_ult_dep_acum,$this->rebi_ult_val_act,$this->tabl_estado,$this->rebi_detalles,$this->depe_id_ubica,$this->tabl_condicion,$this->rebi_reg_publicos,$this->rebi_area,$this->rebi_observaciones,$this->depe_id,$this->rebi_anno,$this->rebi_alquiler,$this->tabl_tipo_interior,$this->rebi_numero_interior,$this->rebi_foto_principal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Registro_bien', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRegistro_bien->eliminar($this->rebi_id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setRebi_id($pk){
		try {
			$this->dataRegistro_bien = $this->oDatRegistro_bien->buscar(array('sqlget'=>true,'rebi_id'=>$pk));
			if(empty($this->dataRegistro_bien)) {
				throw new Exception(JrTexto::_("Registro_bien").' '.JrTexto::_("not registered"));
			}
			$this->rebi_id=$this->dataRegistro_bien["rebi_id"];
			$this->rebi_modalidad = $this->dataRegistro_bien["rebi_modalidad"];
			$this->rebi_tipo_bien = $this->dataRegistro_bien["rebi_tipo_bien"];
			$this->casb_codigo = $this->dataRegistro_bien["casb_codigo"];
			$this->rebi_fadquisicion = $this->dataRegistro_bien["rebi_fadquisicion"];
			$this->rebi_vadquisicion = $this->dataRegistro_bien["rebi_vadquisicion"];
			$this->rebi_ult_dep_acum = $this->dataRegistro_bien["rebi_ult_dep_acum"];
			$this->rebi_ult_val_act = $this->dataRegistro_bien["rebi_ult_val_act"];
			$this->tabl_estado = $this->dataRegistro_bien["tabl_estado"];
			$this->rebi_detalles = $this->dataRegistro_bien["rebi_detalles"];
			$this->depe_id_ubica = $this->dataRegistro_bien["depe_id_ubica"];
			$this->tabl_condicion = $this->dataRegistro_bien["tabl_condicion"];
			$this->rebi_reg_publicos = $this->dataRegistro_bien["rebi_reg_publicos"];
			$this->rebi_area = $this->dataRegistro_bien["rebi_area"];
			$this->rebi_observaciones = $this->dataRegistro_bien["rebi_observaciones"];
			$this->depe_id = $this->dataRegistro_bien["depe_id"];
			$this->rebi_anno = $this->dataRegistro_bien["rebi_anno"];
			$this->rebi_alquiler = $this->dataRegistro_bien["rebi_alquiler"];
			$this->tabl_tipo_interior = $this->dataRegistro_bien["tabl_tipo_interior"];
			$this->rebi_numero_interior = $this->dataRegistro_bien["rebi_numero_interior"];
			$this->rebi_foto_principal = $this->dataRegistro_bien["rebi_foto_principal"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('registro_bien', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRegistro_bien = $this->oDatRegistro_bien->buscar(array('sqlget'=>true,'rebi_id'=>$pk));
			if(empty($this->dataRegistro_bien)) {
				throw new Exception(JrTexto::_("Registro_bien").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRegistro_bien->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
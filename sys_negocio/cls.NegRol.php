<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		17-10-2020
 * @copyright	Copyright (C) 17-10-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRol', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegRol 
{
	
	protected $idrol;
	protected $idempresa;
	protected $nombre;
	protected $imagen;
	protected $estado;
	protected $usuario_registro;
	
	protected $dataRol;
	protected $oDatRol;	

	public function __construct()
	{
		$this->oDatRol = new DatRol;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRol->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatRol->buscar($filtros);
					} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rol', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatRol->iniciarTransaccion('neg_i_Rol');
			$this->idrol = $this->oDatRol->insertar($this->idempresa,$this->nombre,$this->imagen,$this->estado,$this->usuario_registro);
			$this->oDatRol->terminarTransaccion('neg_i_Rol');	
			return $this->idrol;
		} catch(Exception $e) {	
		    $this->oDatRol->cancelarTransaccion('neg_i_Rol');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('rol', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatRol->actualizar($this->idrol,$this->idempresa,$this->nombre,$this->imagen,$this->estado,$this->usuario_registro);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatRol->cambiarvalorcampo($this->idrol,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Rol', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatRol->eliminar($this->idrol,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdrol($pk){
		try {
			$this->dataRol = $this->oDatRol->buscar(array('sqlget'=>true,'idrol'=>$pk));
			if(empty($this->dataRol)) {
				throw new Exception(JrTexto::_("Rol").' '.JrTexto::_("not registered"));
			}
			$this->idrol=$this->dataRol["idrol"];
			$this->idempresa = $this->dataRol["idempresa"];
			$this->nombre = $this->dataRol["nombre"];
			$this->imagen = $this->dataRol["imagen"];
			$this->estado = $this->dataRol["estado"];
			$this->usuario_registro = $this->dataRol["usuario_registro"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('rol', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataRol = $this->oDatRol->buscar(array('sqlget'=>true,'idrol'=>$pk));
			if(empty($this->dataRol)) {
				throw new Exception(JrTexto::_("Rol").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRol->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
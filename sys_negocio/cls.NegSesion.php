<?php
/**
 * @autor		Abel Chingo Tello , ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona',RUTA_BASE);
JrCargador::clase('sys_datos::DatRol',RUTA_BASE);
JrCargador::clase('sys_datos::DatPersona_rol',RUTA_BASE);
JrCargador::clase('sys_datos::DatEmpresa',RUTA_BASE);
JrCargador::clase('ACHT::JrSession',RUTA_LIBS);
class NegSesion
{
	protected $oDatpersonarol;
	protected $oDatPersona;
	protected $oDatRoles;
	protected $oDatBolsa_empresas;
	static $keysesion = '_INGTAL_';

	public function __construct()
	{		
		$this->oDatpersonarol = new DatPersona_rol;
		$this->oDatRoles = new DatRol;
		$this->oDatBolsa_empresas = new DatEmpresa;
	}

	public static function existeSesion()
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (false === $idpersona) {
			return false;
		} else {
			return true;
		}
	}

	public static function EditarHistorial()
	{
		$sesion = JrSession::getInstancia();
	}

	public static function getUsuario()
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (empty($idpersona)) {
			return array('code' => 'Error', 'msj' => JrTexto::_('Please sign in.'));
		}
		return $sesion->getAll($keysesion);		
	}

	public  static function salir()
	{
		$keysesion = self::$keysesion;
		return JrSession::limpiarEspacio($keysesion);
	}

	public static function ingresar($datos){
		try{
			$usuario=!empty($datos["usuario"])?$datos["usuario"]:null;
			$clave=!empty($datos["clave"])?md5($datos["clave"]):null;
			$tocken=!empty($datos["tocken"])?$datos["tocken"]:null;
			$persona = array();
			$oDatPersona = new DatPersona;			
			if(!empty($usuario) && !empty($clave)){
				$persona = $oDatPersona->buscar(array('usuario' => $usuario, 'clave' => $clave, 'estado' => 1,'sqlget'=>true));
			}elseif(!empty($tocken)){
				$persona = $oDatPersona->buscar(array('tocken' => $tocken,'estado' => 1,'sqlget'=>true));
			}
			if (empty($persona)) return array('code' => 'Error', 'msj' => JrTexto::_('Usuario no logueado, Datos de usuario incorrectos'), 'islogin' => false);
			$oDatPersonarol = new DatPersona_rol;
			$idempresa=!empty($datos["idempresa"])?$datos["idempresa"]:null;
			$roles = $oDatPersonarol->buscar(array('idpersona' => $persona["idpersona"],'idempresa' => $idempresa));
			if (empty($roles)) {
				if ($persona["tipo_usuario"] == 's') {
					$roles[0] = array('rol' => 'Administrador', 'idrol' => 1, 'idempresa' => $idempresa);
					$roles[1] = array('rol' => 'Docente', 'idrol' => 2, 'idempresa' => $idempresa);
					$roles[2] = array('rol' => 'Alumno', 'idrol' => 3, 'idempresa' => $idempresa);
				} else return array('code' => 'Error', 'msj' => JrTexto::_('Usuario no tiene rol asignado'), 'islogin' => false);
			}
			$sesion = JrSession::getInstancia();
			$keysesion = (self::$keysesion);
			foreach ($persona as $k => $cp){
				$sesion->set($k, $cp, $keysesion);
			}
			$roles_ = $roles[0];
			$sesion->set('idrol', $roles_["idrol"] , $keysesion);
			$sesion->set('rol', $roles_["rol"] , $keysesion);
			$sesion->set('roles', $roles, $keysesion);
			return array('code' => 200, 'islogin' => true);
		}catch(Exception $ex){
			 return array('code' => 'Error', 'msj' => JrTexto::_('Error inesperado al iniciar sesion'), 'islogin' => false);
		}
	}

	public static function refresh($id){
		try{
			$oDatPersona = new DatPersona;
			$persona = $oDatPersona->buscar(array('idpersona' => $id,'sqlget'=>true));
			$sesion = JrSession::getInstancia();
			$keysesion = (self::$keysesion);
			foreach ($persona as $k => $cp){
				$sesion->set($k, $cp, $keysesion);
			}
		}catch(Exception $ex){
			 return array('code' => 'Error', 'msj' => JrTexto::_('Error inesperado al iniciar sesion'), 'islogin' => false);
		}
	}


	public function cambiar_rol($idrol, $rol, $dni, $idproyecto, $idempresa)
	{
		if (empty($rol) || empty($dni)) return false;
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$user = self::getUsuario();
		$roles = $this->oDatpersonarol->buscar(array('idpersonal' => $dni, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa));
		if ($user["tuser"] == 's') {
			//$hayadmin=false;
			//foreach ($roles as $k => $v) {if($v["idrol"]==1) $hayadmin=true;}
			//if($hayadmin==false){
			$sesion->set('rol', $rol, $keysesion);
			$sesion->set('idrol', $idrol, $keysesion);
			$sesion->set('idempresa', $idempresa, $keysesion);
			$sesion->set('idproyecto', $idproyecto, $keysesion);
			return true;
			//}
			//var_dump($roles);
		}
		if (!empty($roles)) {
			$sesion->set('roles', $roles, $keysesion);
			foreach ($roles as $rol_) {
				$rol = $rol_["rol"];
				$idrol_ = $rol_["idrol"];
				if ($idrol == $idrol_) {
					$sesion->set('rol', $rol, $keysesion);
					$sesion->set('idrol', $idrol_, $keysesion);
					$sesion->set('idempresa', $idempresa, $keysesion);
					$sesion->set('idproyecto', $idproyecto, $keysesion);
					return true;
				}
			}
			return false;
		} else return false;
		//$sesion->set('rol_original', $rol , '__admin_m3c');
	}

	public function verroles($idpersonal, $idproyecto, $idempresa)
	{
		return $this->oDatpersonarol->buscar(array('idpersonal' => $idpersonal, 'idproyecto' => $idproyecto, 'idempresa' => $idempresa));
	}

	public function misrolesallempresas($idpersonal, $idproyecto, $idempresa)
	{
		return $this->oDatpersonarol->buscar(array('idpersonal' => $idpersonal, 'noproyecto' => $idproyecto, 'noempresa' => $idempresa));
	}


	public static function get($prop, $espacio = '')
	{
		//var_dump($this->keysesion,'aaaaaaa');
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$espacio = (!empty($espacio) ? $espacio : $keysesion);
		return $sesion->get($prop, null, $espacio);
	}

	public static function set_($prop, $valor)
	{
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$idpersona = $sesion->get('idpersona', false, $keysesion);
		if (empty($idpersona)) {

			throw new Exception(JrTexto::_('Please sign in.'));
		}
		return $sesion->set($prop, $valor, $this->getKeysesion());
	}

	public static function set($prop, $valor, $espacio = '')
	{

		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		return $sesion->set($prop, $valor, (!empty($espacio) ? $espacio : $keysesion));
	}

	public static function tiene_acceso($menu, $accion)
	{
		if (!NegSesion::existeSesion()) {
			return false;
		}
		if ('superadmin' == NegSesion::get('rol')) {
			return true;
		}
		$oDatRoles = new DatRoles;
		$existe_privilegio = $oDatRoles->existe_permiso(NegSesion::get('rol'), $menu, $accion);
		if (!$existe_privilegio) {
			return false;
		}
		return true;
	}

	public static function tieneRol($nombreRol = '')
	{
		$existe = false;
		if (empty($nombreRol)) {
			return $existe;
		}
		$sesion = JrSession::getInstancia();
		$keysesion = self::$keysesion;
		$arrRoles = $sesion->get('roles', false, $keysesion);
		foreach ($arrRoles as  $r) {
			if (strtolower($r['rol']) === strtolower($nombreRol)) {
				$existe = true;
				break;
			}
		}
		return $existe;
	}

	public static function getEmpresa(){
		$sesion = JrSession::getInstancia();
		return $sesion->getAll('_infoempresa_');
	}

	public static function setEmpresa($prop,$valor){
		$sesion = JrSession::getInstancia();
		$sesion->set($prop, $valor,'_infoempresa_');
		return $sesion->getAll('_infoempresa_');
	}
}

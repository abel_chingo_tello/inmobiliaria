<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		22-12-2020
 * @copyright	Copyright (C) 22-12-2020. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTabla', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegTabla 
{
	
	protected $tabl_id;
	protected $tabl_codigo;
	protected $tabl_descripcion;
	protected $tabl_fecharegistro;
	protected $tabl_descripaux;
	protected $tabl_codigoauxiliar;
	
	protected $dataTabla;
	protected $oDatTabla;	

	public function __construct()
	{
		$this->oDatTabla = new DatTabla;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTabla->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatTabla->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tabla', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			$this->oDatTabla->iniciarTransaccion('neg_i_Tabla');
			$this->tabl_id = $this->oDatTabla->insertar($this->tabl_codigo,$this->tabl_descripcion,$this->tabl_fecharegistro,$this->tabl_descripaux,$this->tabl_codigoauxiliar);
			$this->oDatTabla->terminarTransaccion('neg_i_Tabla');	
			return $this->tabl_id;
		} catch(Exception $e) {	
		    $this->oDatTabla->cancelarTransaccion('neg_i_Tabla');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tabla', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/					
			return $this->oDatTabla->actualizar($this->tabl_id,$this->tabl_codigo,$this->tabl_descripcion,$this->tabl_fecharegistro,$this->tabl_descripaux,$this->tabl_codigoauxiliar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($deBD=false)
	{
		try {
			/*if(!NegSesion::tiene_acceso('Tabla', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTabla->eliminar($this->tabl_id,$deBD);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setTabl_id($pk){
		try {
			$this->dataTabla = $this->oDatTabla->buscar(array('sqlget'=>true,'tabl_id'=>$pk));
			if(empty($this->dataTabla)) {
				throw new Exception(JrTexto::_("Tabla").' '.JrTexto::_("not registered"));
			}
			$this->tabl_id=$this->dataTabla["tabl_id"];
			$this->tabl_codigo = $this->dataTabla["tabl_codigo"];
			$this->tabl_descripcion = $this->dataTabla["tabl_descripcion"];
			$this->tabl_fecharegistro = $this->dataTabla["tabl_fecharegistro"];
			$this->tabl_descripaux = $this->dataTabla["tabl_descripaux"];
			$this->tabl_codigoauxiliar = $this->dataTabla["tabl_codigoauxiliar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('tabla', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTabla = $this->oDatTabla->buscar(array('sqlget'=>true,'tabl_id'=>$pk));
			if(empty($this->dataTabla)) {
				throw new Exception(JrTexto::_("Tabla").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTabla->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	}
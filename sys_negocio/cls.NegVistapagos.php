<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-02-2021
 * @copyright	Copyright (C) 15-02-2021. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatVistapagos', RUTA_BASE);
// JrCargador::clase('sys_negocio::NegTools', RUTA_BASE);
class NegVistapagos 
{
	
	protected $fecha_vencimiento;
	protected $vivienda;
	protected $num_documento;
	protected $nombres;
	protected $estado;
	protected $monto_total;
	protected $monto_mensual;
	
	protected $dataVistapagos;
	protected $oDatVistapagos;	

	public function __construct()
	{
		$this->oDatVistapagos = new DatVistapagos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatVistapagos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscar($filtros = array())
	{
		try {
					return $this->oDatVistapagos->buscar($filtros);
				} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('vistapagos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataVistapagos = $this->oDatVistapagos->buscar(array('sqlget'=>true,''=>$pk));
			if(empty($this->dataVistapagos)) {
				throw new Exception(JrTexto::_("Vistapagos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatVistapagos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
}